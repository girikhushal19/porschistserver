const mongoose = require("mongoose");
const Schema = require("mongoose");

/*basic_plan_price: { type: String, default: null },
  showing_number_day: { type: Number, default: 0 },
  plan_purchase_date_time: { type: Date, default: null },
  top_search: { type: Number, default: 0 },
  top_search_payment_status: { type: Number, default: 0 },
  top_search_days: { type: Number, default: 0 },
  top_search_every_day: { type: Number, default: 0 },
  top_search_once_week: { type: Number, default: 0 },
  top_search_which_day: { type: Number, default: 0 },
  top_search_price: { type: String, default: null },
  top_urgent: { type: Number, default: 0 },
  top_urgent_price: { type: String, default: null },
  top_urgent_payment_status: { type: Number, default: 0 },
  top_urgent_date: { type: Date, default: null },*/

const UserPurchasedPlanSchema = new mongoose.Schema({
  order_id: { type: String , default: null },
  ad_id: [{ type: Schema.Types.ObjectId,ref:"advertisement" }],
  ad_type: { type: String , default: null },
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  maximum_upload: { type: Number , default: 0 },
  maximum_upload_top_search: { type: Number , default: 0 },
  basic_plan_id: [{ type: Schema.Types.ObjectId,ref:"userplans", default:null}],
  top_search_plan_id: [{ type: Schema.Types.ObjectId,ref:"normalusertopsearchplans" }],
  top_urgent_id: [{ type: Schema.Types.ObjectId,ref:"topurgents",default:null}],

  basic_access_plan_id: [{ type: Schema.Types.ObjectId,ref:"useraccessoriesplans", default:null}],
  top_access_urgent_id: [{ type: Schema.Types.ObjectId,ref:"topurgentsaccessories",default:null}],

  basic_plan_id_pro_user: { type: String , default:null},
  top_search_id_pro_user: { type: String , default:null},
  pro_user_plan_end_time: { type: Date, default: null },
  total_maximum_upload: { type: Number , default: 0 },
  
  additional_photo_check: { type: Boolean , default: false },
  paymentType: { type: String , default: null },
  transaction_id: { type: String , default: null },
  payment_intent: { type: String , default: null },
  payment_status:{ type: Number , default: 0 },
  price:{ type: Number , default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("userpurchasedplan", UserPurchasedPlanSchema);