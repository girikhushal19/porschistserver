const mongoose = require("mongoose");

const PageSchema = new mongoose.Schema({
  slug_name: { type: String, default: null },
  page_title: { type: String, default: null },
  description: { type: String, default: null },
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("page", PageSchema);