const mongoose = require("mongoose");

const VersionsSchema = new mongoose.Schema({
  model_id: { type: String, default: null },
  version: { type: String, default: null },
  title: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("versions", VersionsSchema);