const mongoose = require("mongoose");

const PaymentSchema = new mongoose.Schema({
  bank:{type:Array,default:[]},
    user_id:{type:String,default:""},
    amount:{type:Number,default:0},
    status:{type:Number,default:1}, // 1 = No action yet , 2 =Cancel , 3 = Accept
    created_at:{type:Date,default:Date.now},
    updated_at:{type:Date,default:Date.now}
});

module.exports = mongoose.model("paymentrequests",PaymentSchema);