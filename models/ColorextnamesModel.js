const mongoose = require("mongoose");

const colorextnamesSchema = new mongoose.Schema({
  color_ext: { type: String, default: null },
  color_name: { type: String, default: null },
  color_code: { type: String, default: null },
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("colorextnames", colorextnamesSchema);