const mongoose = require("mongoose");
const Schema = require("mongoose");

const UserfaqSchema = new mongoose.Schema({
  question: { type: String, default: null },
  answer: { type: String, default: null },
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("userfaq", UserfaqSchema);