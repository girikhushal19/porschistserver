const mongoose = require("mongoose");

const EmailNotificationSchema = new mongoose.Schema({
  title: { type: String, default: null },
  description: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("emailnotification", EmailNotificationSchema);