const mongoose = require("mongoose");
const Schema = require("mongoose");

const AdFavoriteSchema = new mongoose.Schema({
  ad_id: [{ type: Schema.Types.ObjectId,ref:"advertisement" }],
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("favorite", AdFavoriteSchema);