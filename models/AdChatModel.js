const mongoose = require("mongoose");
const Schema = require("mongoose");

const AdChatSchema = new mongoose.Schema({
  ad_id: [{ type: Schema.Types.ObjectId,ref:"advertisements" }],
  randomNumber:{type: String, default: null},
  sender_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  receiver_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  message_by: { type: Number, default: 1 },
  message: { type: String, default: null },
  mark_read_sender: { type: Number, default: 0 },
  mark_read_receiver: { type: Number, default: 0 },
  del_flag_sender: { type: Number, default: 0 },
  del_flag_reciever: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("adchat", AdChatSchema);