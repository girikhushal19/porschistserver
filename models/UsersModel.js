const mongoose = require("mongoose");

const bankSchema = new mongoose.Schema({
  bank_name:{type:String,default:null},
  account_holder_name:{type:String,default:null},
  iban:{type:String,default:null},
  country:{type:String,default:null},
  city:{type:String,default:null},
  bic:{type:String,default:null},
  address:{type:String,default:null},
  latitude:{type:String,default:null},
  longitude:{type:String,default:null},
  zip_code:{type:String,default:null}
});
const userSchema = new mongoose.Schema({
  user_type: { type: String, enum: ['normal_user', 'pro_user'], default: 'normal_user' },
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  email: { type: String, unique: true },
  paypal_email: { type: String, default: null },
  userName: { type: String, unique: true },
  password: { type: String },
  token: { type: String, default: null },
  deviceToken: { type: String, default: null },
  status: { type: Number, default: 1 },
  mobileNumber: { type: Number, default: null },
  whatsapp: { type: String, default: null },
  telephone: { type: Number, default: null },
  zip_code: { type: Number, default: null },
  gender: { type: String, default: null },
  city: { type: String, default: null },
  country: { type: String, default: null },
  
  enterprise: { type: String, default: null }, 
  additional_information: { type: String, default: null }, 
  address: { type: String, default: null },
  latitude: { type: String, default: null },
  longitude: { type: String, default: null },
  
  postalcode: { type: String, default: null },
  siret: { type: String, default: null },
  userDocument: { type: String, default: null },
  registration_number: { type: String, default: null },
  registration_certificate: { type: String, default: null },
  payment_option: { type: Number, default: 1 },
  otp: { type: Number, default: null },
  otp_used_or_not: { type: Number, default: 0 },
  email_verify: { type: Number, default: 0 },
  deleted_at: { type: Number, default: null },
  date_of_birth: { type: String, default: null },
  userImage: { type: String, default: null },
  companyImage: { type: String, default: null },
  activity_description: { type: String, default: null },
  website_url: { type: String, default: null },
  main_activity: { type: String, default: null },
  loggedin_time: { type: Date, default: null },
  bank:{type:[bankSchema]},
  seller_pending_amount: { type: Number, default: 0 },
  seller_paid_amount: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 

module.exports = mongoose.model("users", userSchema);