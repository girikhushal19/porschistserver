const mongoose = require("mongoose");
const Schema = require("mongoose");

const PaypalSchema = new mongoose.Schema({
  mode: { type: String, default: null },
  client_id: { type: String, default: null },
  client_secret: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("paypals", PaypalSchema);