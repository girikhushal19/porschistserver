const mongoose = require("mongoose");
const Schema = require("mongoose");

const DeliveryAddressSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  address: { type: String, default:null },
  location: {
   type: { type: String },
   coordinates: []
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("deliveryaddressusers", DeliveryAddressSchema);