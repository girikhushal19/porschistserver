const mongoose = require("mongoose");
const Schema = require("mongoose");

const SearchHistorySchema = new mongoose.Schema({
  ad_name: { type: String, default: null },
  user_id: { type: String, default: null },
  category:{type:String, default:null },
  address: { type: String, default:null },
  location: {
   type: { type: String },
   coordinates: []
  },
  item_count:{type:Number,default:0 },
  is_email:{type:Number,default:0 }, // 0 = Disable  1 = Enable
  searched_time:{ type:Date,default:Date.now },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model("searchhistories",SearchHistorySchema);