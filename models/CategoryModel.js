const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema({
  first_heading: { type: String, default: null },
  category: { type: String, default: null },
  images: { type: Array, default: null },
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("category", CategorySchema);