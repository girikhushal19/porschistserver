const mongoose = require("mongoose");

const OptionSchema = new mongoose.Schema({
  title: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("options", OptionSchema);