const mongoose = require("mongoose");
const Schema = require("mongoose");



/*
normal user aya

complition_status = 0 Incomplete 1 Complete
show_on_web = 0 No 1 Yes
90 din k liye free add kar do

iske baad wo plan lega 

ye common rahega dono type k user k liye
  basic_plan_price
  showing_number_day
  plan_purchase_date_time dateTime and null
  plan_end_date_time
ye common rahega dono type k user k liye


aab wo select kar skta h top par aane k liye plan
  top_search = 0 No 1 Yes
  top_search_price = null
  top_search_days = 0
  top_search_every_day = 0 No 1 Yes
  top_search_once_week = 0 No 1 Yes
  top_search_which_day = null or Day name
  top_search_plan_end_date_time

aab wo select kar skta h top urgent plan
isme field
  top_urgent = 0 No 1 Yes 
  top_urgent_price null
  top_urgent_plan_end_date_time

For pro user Main basic plan
New table needed
  plan maximum car can upload
  showing day number
  price
  search top add 0 No 1 Yes
  search top add price
  search top day



complition_status = 0 Incomplete 1 Complete
show_on_web = 0 No 1 Yes
90 din k liye free add kar do

iske baad wo plan lega 

ye common rahega dono type k user k liye
  ## basic_plan_price
  ## showing_number_day
  ## plan_purchase_date_time dateTime and null
ye common rahega dono type k user k liye


aab wo select kar skta h top par aane k liye plan
  ## top_search = 0 No 1 Yes
  ## top_search_price = null
  ## top_search_days = 0
  ## top_search_every_day = 0 No 1 Yes
  ## top_search_once_week = 0 No 1 Yes
  ## top_search_which_day = null or Day name

aab wo select kar skta h top urgent plan
isme field
  ## top_urgent = 0 No 1 Yes 
  ## top_urgent_price null


For pro user Main basic plan
New table needed
  ## plan maximum car can upload
  ## showing day number
  ## price
  ## search top add 0 No 1 Yes
  ## search top add price
  ## search top day



*/

const AdvertisementSchema = new mongoose.Schema({
  ad_name: { type: String, default: null  },
  category: { type: String , default: null },
  userId: { type: Schema.Types.ObjectId,ref:"users" },
  user_type:{ type: String, enum: ['normal_user', 'pro_user'], default: 'normal_user' },
  
  modelId: [{ type: Schema.Types.ObjectId,ref:"model", default: null }],
  model_variant: { type: String, default: null  },
  subModelId: [{ type: Schema.Types.ObjectId,ref:"model", default: null  }],
 
  type_of_chassis: [{ type: Schema.Types.ObjectId,ref:"carattribute", default: null  }],
  type_de_piece: [{ type: Schema.Types.ObjectId,ref:"carattribute", default: null  }],
  state: { type: String, default: null  },
  OEM: { type: String, default: null  },
  fuel: [{ type: Schema.Types.ObjectId,ref:"carattribute", default: null  }],
  vehicle_condition: [{ type: Schema.Types.ObjectId,ref:"carattribute", default: null  }],
  type_of_gearbox: [{ type: Schema.Types.ObjectId,ref:"carattribute", default: null  }],
  
  colorId: [{ type: Schema.Types.ObjectId,ref:"colors", default: null  }],
  color_exterieur: { type: String, default: null },
  colorIdInterior: [{ type: Schema.Types.ObjectId,ref:"colorinteriors", default: null  }],
  color_name: { type: String, default: null },
  
  registration_month: { type: String, default: null },
  registration_year: { type: Number, default: null },
  pors_warranty: { type: String, default: null },
  warranty: { type: String, default: null },
  warranty_month: { type: Number, default: 0 },
  pors_warranty_month: { type: Number, default: 0 },
  maintenance_booklet: { type: String, default: null },
  maintenance_invoice: { type: String, default: null },
  accidented: { type: String, default: null },
  original_paint: { type: String, default: null },
  matching_number: { type: String, default: null },
  matching_color_paint: { type: String, default: null },
  matching_color_interior: { type: String, default: null },
  price_for_pors: { type: String, default: null },
  price: { type: Number, default: null },
  pro_price: { type: Number, default: null },
  shipping_price: { type: Array, default: [] },


  shipping_price_1: { type: Number, default: 0 },
  shipping_price_2: { type: Number, default: 0 },
  shipping_price_4: { type: Number, default: 0 },
  shipping_price_5: { type: Number, default: 0 },
  shipping_type_1: { type: Number, default: 0 },
  shipping_type_2: { type: Number, default: 0 },
  shipping_type_3: { type: Number, default: 0 },
  shipping_type_4: { type: Number, default: 0 },
  shipping_type_5: { type: Number, default: 0 },
  shipping_type_1_text: { type: String, default: null },
  shipping_type_2_text: { type: String, default: null },
  shipping_type_3_text: { type: String, default: null },
  shipping_type_4_text: { type: String, default: null },
  shipping_type_5_text: { type: String, default: null },

  delivery_price: { type: Number, default: null },
  vat_tax: { type: Number, default: 0 },
  deductible_VAT: { type: String, default: null },
  number_of_owners: { type: String, default: null },
  cylinder_capacity: { type: String, default: null },
  mileage_kilometer: { type: Number, default: null },



  //paint_thickness: { type: String, default: null },
  paint_thickness_parts: { type: Object, default: null },
  body_repainted: { type: Object, default: null },
  engine_operation_hour: { type: String, default: null },
  report_piwi: { type: String, default: null },
  optionsArray:{ type: Array, default: null },
  piwi_report_date:{ type: Array, default: null },
  piwi_report_image:{ type: Array, default: null },

  mileage_maintanance_report_date:{ type: Array, default: null },
  mileage_maintanance_report_kilometer:{ type: Array, default: null },
  mileage_maintanance_report_image:{ type: Array, default: null },
  engine_operation_hour_image: { type: Array, default: null },
  piwi_checkbox:{ type: Array, default: null },

  description: { type: String, default: null },
  exterior_image: { type: Array, default: null },
  interior_image: { type: Array, default: null },
  trunk_engine_image: { type: Array, default: null },
  accessoires_image: { type: Array, default: null },
  accessoires_image_new: { type: Array, default: null },
  payment_type: { type: String, default: null },

  payment_status: { type: Number, default: 0 },
  delete_at:{type:Number,default:0},
  complition_status: { type: Number, default: 0 },
  show_on_web: { type: Number, default: 0 },
  activation_status: { type: Number, default: 1 },
  address: { type: String, default:null },
  location: {
   type: { type: String },
   coordinates: []
  },
  city: { type: String, default: null },
  zip_code: { type: Number, default: null },
  country: { type: String, default: null },
  
  zipcode: { type: String, default: null },
  contact_number: { type: Number, default: null },
  contact_show: { type: Boolean, default: false },
  additional_photo_check: { type: Boolean, default: false },
  basic_plan_price: { type: Number, default: null },
  basic_plan_status: { type: Number, default: 0 },
  showing_number_day: { type: Number, default: 0 },
  plan_purchase_date_time: { type: Date, default: null },
  plan_end_date_time: { type: Date, default: null },

  accessories_max_photo_status: { type: Number, default: 0 },
  accessories_max_photo: { type: Number, default: 0 },
  accessories_purchase_date_time: { type: Date, default: null },

  top_search: { type: Number, default: 0 },
  top_search_payment_status: { type: Number, default: 0 },
  top_search_days: { type: Number, default: 0 },
  top_search_every_day: { type: Number, default: 0 },
  top_search_once_week: { type: Number, default: 0 },
  top_search_which_day: { type: Number, default: 0 },
  top_search_price: { type: Number, default: null },
  top_search_plan_purchase_date_time: { type: Date, default: null },
  top_search_plan_end_date_time: { type: Date, default: null },

  top_urgent: { type: Number, default: 0 },
  top_urgent_day_number: { type: Number, default: 0 },
  top_urgent_price: { type: Number, default: null },
  top_urgent_payment_status: { type: Number, default: 0 },
  top_urgent_plan_purchase_date_time: { type: Date, default: null },
  top_urgent_plan_end_date_time: { type: Date, default: null },

  email_reminder: { type: Number, default: 0 },
  mobile_count: { type: Number, default: 0 },
  view_count: { type: Number, default: 0 },
  chat_count: { type: Number, default: 0 },

  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

AdvertisementSchema.index({location: '2dsphere'});
module.exports = mongoose.model("advertisement", AdvertisementSchema);

