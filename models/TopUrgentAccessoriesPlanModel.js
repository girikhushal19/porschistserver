const mongoose = require("mongoose");
const Schema = require("mongoose");

const TopUrgentAccessoriesPlanSchema = new mongoose.Schema({
  plan_type: { type: String, default: null }, // Car , Accessoires
  user_type: { type: String, default: null },
  day_number: { type: Number, default: null },
  price: { type: Number, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("topurgentsaccessories", TopUrgentAccessoriesPlanSchema);