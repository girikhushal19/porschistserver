const mongoose = require("mongoose");

const bannerSchema = new mongoose.Schema({
  first_heading: { type: String, default: null },
  second_heading: { type: String, default: null },
  images: { type: Array, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("banners", bannerSchema);