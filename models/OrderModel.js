const mongoose = require("mongoose");
const Schema = require("mongoose");

const OrderSchema = new mongoose.Schema({
  order_id: { type: String, default:null },
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  email: { type: String, default: null },
  mobileNumber: { type: String, default: null },

  ad_owner_user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  ad_id: [{ type: Schema.Types.ObjectId,ref:"advertisement" }],
  quantity:{type: Number, default: 1},
  tax: { type: Number, default: 0 },
  shipping_charge: { type: String, default: null },
  price:{type: Number, default: 1},
  final_price:{type: Number, default: 1},
  seller_profit:{type: Number, default: 0},
  
  pick_up_address: { type: String, default:null },
  pick_up_location: {
   type: { type: String },
   coordinates: []
  },
  address: { type: String, default:null },
  location: {
   type: { type: String },
   coordinates: []
  },

  ad_name:{type: String, default: null},
  category:{type: String, default: null},
  registration_year:{type: String, default: null},
  description:{type: String, default: null},
  state:{type: String, default: null},
  OEM:{type: String, default: null},
  type_de_piece:{type: String, default: null},
  contact_number:{type: String, default: null},
  transaction_id:{type: String, default: null},
  payment_intent:{type: String, default: null},

  merchant_status:{type: Number, default: 0},
  payment_type: { type: String, default: null },
  payment_status: { type: Number, default: 0 },
  shipping_status: { type: Number, default: 0 }, // 0 none  1 process  2 cancel 3 complete
  self_pickup: { type: Number, default: 0 },
  order_created_at: { type: Date, default: Date.now },
  payment_success_date: { type: Date, default: null},
  payment_time: { type: Date, default: null},
  is_paid: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("orders", OrderSchema);