const mongoose = require("mongoose");
const Schema = require("mongoose");

const UseraccessoriesplanSchema = new mongoose.Schema({
  user_type: { type: String, default: null },
  photo_number: { type: Number, default: null },
  price: { type: Number, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("useraccessoriesplans", UseraccessoriesplanSchema);