const mongoose = require("mongoose");


const ContactSchema = new mongoose.Schema({
  nom: { type: String, default: null },
  prenom: { type: String, default: null },
  enterprise_name: { type: String, default: null },
  email: { type: String, default: null },
  message: { type: String, default: null },
  subject: { type: String, default: null },
  mobile: { type: Number, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 

module.exports = mongoose.model("contacts", ContactSchema);