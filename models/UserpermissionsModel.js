const mongoose = require("mongoose");
const Schema = require("mongoose");

const userPermissionSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  module_name: { type: String, default: null },
  module_add: { type: Boolean, default: false },
  module_edit: { type: Boolean, default: false },
  module_view: { type: Boolean, default: false },
  module_delete: { type: Boolean, default: false },
  module_status: { type: Boolean, default: false },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("userpermissions", userPermissionSchema);