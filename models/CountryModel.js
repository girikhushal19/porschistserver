const mongoose = require("mongoose");

const CountrySchema = new mongoose.Schema({
  title: { type: String, default: null },
  tax: { type: Number, default: 0 },
  status: { type: Number, default: 1 }, //1 active 2 inactive 3 delete
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("country", CountrySchema);