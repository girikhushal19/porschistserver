const mongoose = require("mongoose");
const Schema = require("mongoose");

const CartSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  ad_owner_user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  ad_id: [{ type: Schema.Types.ObjectId,ref:"advertisement" }],
  quantity:{type: Number, default: 1},
  price:{type: Number, default: 1},
  address_id: [{ type: Schema.Types.ObjectId,ref:"deliveryaddressusers", default: null }],
  tax: { type: Number, default: 0 },
  self_pickup: { type: Number, default: 2 }, // 1 self , 2 delivery
  shipping_charge: { type: String, default: null },
  delivery_type: { type: Number, default: 1 }, 
  payment_type: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("carts", CartSchema);