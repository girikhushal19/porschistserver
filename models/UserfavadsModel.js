const mongoose = require("mongoose");
const Schema = require("mongoose");

const UserfavadsSchema = new mongoose.Schema({
  ad_id: { type: Schema.Types.ObjectId,ref:"advertisement" },
  ad_type: { type: String , default: null },
  user_id: { type: Schema.Types.ObjectId,ref:"users" },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("userfavads", UserfavadsSchema);