const mongoose = require("mongoose");
const Schema = require("mongoose");

const RatingSchema = new mongoose.Schema({
  ad_id:{type: String, default: null},
  order_id:{type: String, default: null},
  user_id:{type: String, default: null},
  seller_id:{type: String, default: null},
  rating:{type: Number, default: 0},
  review:{type: String, default: null},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("ratings", RatingSchema);