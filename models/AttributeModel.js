const mongoose = require("mongoose");

const AttributeSchema = new mongoose.Schema({
  attribute_type: { type: String, default: null },
  attribute_name: { type: String, default: null },
  parent_id: { type: String, default: null },
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("carattribute", AttributeSchema);