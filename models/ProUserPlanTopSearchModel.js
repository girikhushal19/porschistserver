const mongoose = require("mongoose");
const Schema = require("mongoose");

const ProUserPlanTopSearchSchema = new mongoose.Schema({
  user_type: { type: String, default: null },
  plan_type: { type: String, default: null },
  day_number: { type: Number, default: 0 },
  maximum_upload: { type: Number, default: 0 },
  price: { type: Number, default: 0 },
  top_price: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("prousertopsearchplans", ProUserPlanTopSearchSchema);