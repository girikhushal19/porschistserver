const mongoose = require("mongoose");

const SubModelsSchema = new mongoose.Schema({
  model_name: { type: String, default: null },
  parent_id: { type: String, default: null},
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("submodel", SubModelsSchema);