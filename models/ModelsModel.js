const mongoose = require("mongoose");

const ModelsSchema = new mongoose.Schema({
  attribute_type: { type: String, default: null },
  model_name: { type: String, default: null },
  fileImage: { type: String, default: null },
  black_white_image: { type: String, default: null },
  parent_id: { type: String, default: null},
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("model", ModelsSchema);