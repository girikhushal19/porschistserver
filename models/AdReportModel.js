const mongoose = require("mongoose");
const Schema = require("mongoose");

const AdReportSchema = new mongoose.Schema({
  reason: { type: String, default: null },
  message: { type: String, default: null },
  ad_id: { type: String, default: null }, 
  user_id: { type: String, default: null }, 
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("adreports", AdReportSchema);