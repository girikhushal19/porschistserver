const mongoose = require("mongoose");
const Schema = require("mongoose");

const SellerEarningsSchema = new mongoose.Schema({
  order_id: { type: Array, default: [] },
  seller_id: { type: String, default: null },
  amount: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("sellerearnings", SellerEarningsSchema);