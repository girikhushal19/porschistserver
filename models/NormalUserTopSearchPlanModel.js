const mongoose = require("mongoose");
const Schema = require("mongoose");

const NormalUserTopSearchPlanSchema = new mongoose.Schema({
  plan_day_type:{type:Number,default:1}, //1 every day , 2 once in week
  plan_type: { type: String, default: null },
  user_type: { type: String, default: null },
  day_number: { type: Number, default: null },
  price: { type: Number, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("normalusertopsearchplans", NormalUserTopSearchPlanSchema);