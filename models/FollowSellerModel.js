const mongoose = require("mongoose");
const Schema = require("mongoose");

const FollowSllerSchema = new mongoose.Schema({
  user_id:{type:Schema.Types.ObjectId,ref:"users"},
  seller_id:{type:Schema.Types.ObjectId,ref:"users"},
  created_at:{type:Date,default:Date.now},
  updated_at:{type:Date,default:Date.now}
});
module.exports = mongoose.model("followsellers",FollowSllerSchema);