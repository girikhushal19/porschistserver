const mongoose = require("mongoose");
const Schema = require("mongoose");

const YesnoSchema = new mongoose.Schema({
  color_text: { type: String, default: null },
  color_value: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("yesno", YesnoSchema);