
const validator = require('../helpers/validate');

const getAdvertisementvalidation = (req, res, next) => {
    const validationRule = {
        "ad_id": "required",
        "contact_number": "required"
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status)
        {
            var allError = err;
            //var jsonObj = [];
            var item = {};
            var x=0;
            for(let er_key in allError)
            {
                if(er_key == "errors")
                {
                    for(let i in allError.errors)
                    {
                        for(let key in allError.errors[i])
                        {
                            //console.log(key+"---->"+allError.errors[i][key]);
                            item [x] = allError.errors[i][key];
                            x++;
                        }             
                    }
                    
                }
            }
            //console.log(jsonObj);
            //console.log(item);
            var count = Object.keys(item).length;
            //console.log(count);
            var errorMessage = "";
            if(count > 0)
            {
                errorMessage = item[0];
            }
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: errorMessage
                });
        }else{
            next();
        }
    });
}

module.exports = { 
  getAdvertisementvalidation
}