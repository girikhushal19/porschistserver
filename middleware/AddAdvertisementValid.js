
const validator = require('../helpers/validate');

const getAdvertisementvalidation = (req, res, next) => {
    const validationRule = {
        "category": "required",
        "ad_name": "required",
        "user_id": "required",
        "user_type": "required",
        "model_id": "required",
        "model_variant": "required",
        //"sub_model_id": "required",
        "fuel": "required",
        "vehicle_condition": "required",
        "type_of_gearbox": "required",
        "color": "required",
        "attribute_air_rim": "required",
        "registration_year": "required",
        "warranty": "required",
        "maintenance_booklet": "required",
        "maintenance_invoice": "required",
        "accidented": "required",
        "original_paint": "required",
        "matching_number": "required",
        "matching_color_paint": "required",
        "matching_color_interior": "required",
        "price": "required",
        //"pro_price": "required",
        //"vat_tax": "required",
        "deductible_VAT": "required",
        "number_of_owners": "required",
        "cylinder_capacity": "required",
        "mileage_kilometer": "required",
        //"paint_thickness": "required",
        "description": "required",
        //"images": "required",
        "payment_type": "required",
        //"payment_status": "required",
        "address": "required",
        "latitude": "required",
        "longitude": "required",
        "city": "required",
        "zipcode": "required",
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status)
        {
            var allError = err;
            //var jsonObj = [];
            var item = {};
            var x=0;
            for(let er_key in allError)
            {
                if(er_key == "errors")
                {
                    for(let i in allError.errors)
                    {
                        for(let key in allError.errors[i])
                        {
                            //console.log(key+"---->"+allError.errors[i][key]);
                            item [x] = allError.errors[i][key];
                            x++;
                        }             
                    }
                    
                }
            }
            //console.log(jsonObj);
            //console.log(item);
            var count = Object.keys(item).length;
            //console.log(count);
            var errorMessage = "";
            if(count > 0)
            {
                errorMessage = item[0];
            }
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: errorMessage
                });
        }else{
            next();
        }
    });
}

module.exports = { 
  getAdvertisementvalidation
}