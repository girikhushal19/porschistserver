
const validator = require('../helpers/validate');

const getAdvertisementvalidation = (req, res, next) => {
    const validationRule = {
        "ad_id": "required",
        "model_id": "required",
        "model_variant": "required",
        "registration_year": "required",
        "type_of_chassis": "required",
        "fuel": "required",
        "type_of_gearbox": "required",
        "vehicle_condition": "required",
        "color": "required",
        "warranty": "required",
        //warranty_month if yes 
        "maintenance_booklet": "required",
        "maintenance_invoice": "required",
        "accidented": "required",
        "original_paint": "required",
        "matching_number": "required",
        "matching_color_paint": "required",
        "matching_color_interior": "required",
        "price_for_pors": "required",
        "deductible_VAT": "required",
        "number_of_owners": "required",
        "cylinder_capacity": "required",
        "mileage_kilometer": "required",
        //paint_thickness_parts
        //body_repainted
        "engine_operation_hour": "required",
        //piwi_report
        //mileage_maintanance_report
        "piwi_checkbox": "required",
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status)
        {
            var allError = err;
            //var jsonObj = [];
            var item = {};
            var x=0;
            for(let er_key in allError)
            {
                if(er_key == "errors")
                {
                    for(let i in allError.errors)
                    {
                        for(let key in allError.errors[i])
                        {
                            //console.log(key+"---->"+allError.errors[i][key]);
                            item [x] = allError.errors[i][key];
                            x++;
                        }             
                    }
                    
                }
            }
            //console.log(jsonObj);
            //console.log(item);
            var count = Object.keys(item).length;
            //console.log(count);
            var errorMessage = "";
            if(count > 0)
            {
                errorMessage = item[0];
            }
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: errorMessage
                });
        }else{
            next();
        }
    });
}

module.exports = { 
  getAdvertisementvalidation
}