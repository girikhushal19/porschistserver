import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdCarPostProfessionalComponent } from './ad-car-post-professional/ad-car-post-professional.component';

const routes: Routes = [

    {
      path : "adCarPostProfessional",
      component : AdCarPostProfessionalComponent
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
