import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdCarPostProfessionalComponent } from './ad-car-post-professional.component';

describe('AdCarPostProfessionalComponent', () => {
  let component: AdCarPostProfessionalComponent;
  let fixture: ComponentFixture<AdCarPostProfessionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdCarPostProfessionalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdCarPostProfessionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
