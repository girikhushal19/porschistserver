import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import { AdCarPostProfessionalComponent } from './ad-car-post-professional/ad-car-post-professional.component';


@NgModule({
  declarations: [
    AdCarPostProfessionalComponent
  ],
  imports: [
    CommonModule,
    SiteRoutingModule
  ]
})
export class SiteModule { }
