import { TestBed } from '@angular/core/testing';

import { AllreadyloggedinService } from './allreadyloggedin.service';

describe('AllreadyloggedinService', () => {
  let service: AllreadyloggedinService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllreadyloggedinService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
