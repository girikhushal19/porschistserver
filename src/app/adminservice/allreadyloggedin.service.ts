import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AllreadyloggedinService {
  token:any;email:any;user_type:any;loggedInDetail:any;
  constructor() { }

  checkAllreadyLoggedIn()
  {
    this.token = localStorage.getItem("token");
    this.user_type = localStorage.getItem("user_type");
    console.log("Token "+this.token);
    console.log("User type "+this.user_type);
  }
}
