import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-page',
  templateUrl: './all-page.component.html',
  styleUrls: ['./all-page.component.css']
})
export class AllPageComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allPage:any;queryParam:any;numbers:any;allPageCount:any;apiStringify:any;deletePage:any;base_url_node_plain:any;myJson:any;queryParam_new:any;updatePageStatusApi:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allPageCount = this.base_url_node+"allPageCount";
    this.allPage = this.base_url_node+"allPage";
    this.deletePage = this.base_url_node+"deletePage";
    this.updatePageStatusApi = this.base_url_node+"updatePageStatusApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allPageCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allPageCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allPage,this.queryParam).subscribe((response:any)=>{
      //console.log("allPage"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  deleteRecord(id=null)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id};
    this._http.post(this.deletePage,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  updatePageStatus(id=null,status:number)
  {
    /*console.log(id);
    console.log(status);*/
    this.queryParam_new = {"id":id,"status":status};
    this._http.post(this.updatePageStatusApi,this.queryParam_new).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
}
