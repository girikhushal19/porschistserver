import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesActiveProUserAdComponent } from './accessories-active-pro-user-ad.component';

describe('AccessoriesActiveProUserAdComponent', () => {
  let component: AccessoriesActiveProUserAdComponent;
  let fixture: ComponentFixture<AccessoriesActiveProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesActiveProUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesActiveProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
