import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarInactiveProUserAdComponent } from './car-inactive-pro-user-ad.component';

describe('CarInactiveProUserAdComponent', () => {
  let component: CarInactiveProUserAdComponent;
  let fixture: ComponentFixture<CarInactiveProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarInactiveProUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarInactiveProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
