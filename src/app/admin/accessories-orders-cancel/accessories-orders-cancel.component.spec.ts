import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesOrdersCancelComponent } from './accessories-orders-cancel.component';

describe('AccessoriesOrdersCancelComponent', () => {
  let component: AccessoriesOrdersCancelComponent;
  let fixture: ComponentFixture<AccessoriesOrdersCancelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesOrdersCancelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesOrdersCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
