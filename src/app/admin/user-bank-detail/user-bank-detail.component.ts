import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-user-bank-detail',
  templateUrl: './user-bank-detail.component.html',
  styleUrls: ['./user-bank-detail.component.css']
})
export class UserBankDetailComponent implements OnInit {
  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allUsers:any;queryParam:any;numbers:any;allUsersBankDetail:any;apiStringify:any;updateUserStatusApi:any;base_url_node_plain:any;myJson:any;
  user_id:any;singleRecord:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allUsersBankDetail = this.base_url_node+"allUsersBankDetail";
    
    this.user_id = this.actRoute.snapshot.params['id'];
    this.queryParam = {"id":this.user_id};

    this._http.post(this.allUsersBankDetail,this.queryParam).subscribe((response:any)=>{
      // console.log("allUsersBankDetail");
       console.log(response);
      this.apiResponse = response;
      this.singleRecord = this.apiResponse.record;

      console.log(this.singleRecord);
      //console.log("this.numbers "+this.numbers);
    });

  }

  ngOnInit(): void {
  }

}
