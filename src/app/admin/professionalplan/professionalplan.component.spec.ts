import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalplanComponent } from './professionalplan.component';

describe('ProfessionalplanComponent', () => {
  let component: ProfessionalplanComponent;
  let fixture: ComponentFixture<ProfessionalplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfessionalplanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
