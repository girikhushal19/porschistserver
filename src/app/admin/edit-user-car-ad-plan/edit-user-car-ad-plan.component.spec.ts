import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserCarAdPlanComponent } from './edit-user-car-ad-plan.component';

describe('EditUserCarAdPlanComponent', () => {
  let component: EditUserCarAdPlanComponent;
  let fixture: ComponentFixture<EditUserCarAdPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUserCarAdPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUserCarAdPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
