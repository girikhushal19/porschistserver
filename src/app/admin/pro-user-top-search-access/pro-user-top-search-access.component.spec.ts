import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchAccessComponent } from './pro-user-top-search-access.component';

describe('ProUserTopSearchAccessComponent', () => {
  let component: ProUserTopSearchAccessComponent;
  let fixture: ComponentFixture<ProUserTopSearchAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchAccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
