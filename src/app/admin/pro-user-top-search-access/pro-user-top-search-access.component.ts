import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-pro-user-top-search-access',
  templateUrl: './pro-user-top-search-access.component.html',
  styleUrls: ['./pro-user-top-search-access.component.css']
})
export class ProUserTopSearchAccessComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addPlanProUserTopSearch:any;allPlanProUser:any;queryParam:any;record:any;allPlanProUserRecord:any;
  old_plan_type="accessories_plan";
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addPlanProUserTopSearch = this.base_url_node+"addPlanProUserTopSearch";
    this.allPlanProUser = this.base_url_node+"allPlanProUserAccess"; 
    //console.log("here");
     
  }

  ngOnInit(): void {
    this._http.post(this.allPlanProUser,this.queryParam).subscribe((response:any)=>{
      //console.log("allPlanProUser"+response);
      //this.apiResponse = response;
      this.allPlanProUserRecord = response.record;   
    });

  }

  form = new UntypedFormGroup({
    //day_number: new UntypedFormControl('', [Validators.required]), 
    plan_type: new UntypedFormControl('', []), 
    maximum_upload: new UntypedFormControl('', [Validators.required]), 
    //price: new UntypedFormControl('', [Validators.required]), 
    top_price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addPlanProUserTopSearch,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
              window.location.reload();
            }, 500);
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
