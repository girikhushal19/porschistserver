import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAccessoriesParticularComponent } from './all-accessories-particular.component';

describe('AllAccessoriesParticularComponent', () => {
  let component: AllAccessoriesParticularComponent;
  let fixture: ComponentFixture<AllAccessoriesParticularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllAccessoriesParticularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllAccessoriesParticularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
