import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserFaqComponent } from './add-user-faq.component';

describe('AddUserFaqComponent', () => {
  let component: AddUserFaqComponent;
  let fixture: ComponentFixture<AddUserFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUserFaqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddUserFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
