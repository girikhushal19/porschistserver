import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-invoice',
  templateUrl: './all-invoice.component.html',
  styleUrls: ['./all-invoice.component.css']
})
export class AllInvoiceComponent implements OnInit {

  invoice_id:any[] = [];
  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allCompletedShipment:any;queryParam:any;numbers:any;allCompletedShipmentCount:any;apiStringify:any;updateDriverStatusApi:any;base_url_node_plain:any;myJson:any;exportCsvDriverApi:any;allMerchant:any;
  selectedIndex: number;allMerchantForInvoice:any;generateInvoiceApi:any;
  //dtOptions: DataTables.Settings = {}; 

  seller_id:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    //console.log(this.base_url_node);


    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
       window.location.href = this.base_url;
    }

    this.seller_id = this.actRoute.snapshot.params['id'];


    this.exportCsvDriverApi = this.base_url_node+"exportCsvDriverApi";
    //this.allCompletedShipmentCount = this.base_url_node+"allPaidInvoice";
    this.allCompletedShipment = this.base_url_node+"allPaidInvoice";
     

 
    this.selectedIndex = 0;
    this.getallBanner(0);
  }

  ngOnInit(): void {
 
    
  }
  getallBanner(numofpage=0)
  {
    console.log("heeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
    this.queryParam = {"numofpage":numofpage,"seller_id":this.seller_id};
    this._http.post(this.allCompletedShipment,this.queryParam).subscribe((response:any)=>{
      //console.log("allCompletedShipment"+response);
      
      
      //this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.allMerchant = response.record;
      console.log("this record "+this.allMerchant);
      
    });
  }
  
}
