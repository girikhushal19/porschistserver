import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllVersionComponent } from './all-version.component';

describe('AllVersionComponent', () => {
  let component: AllVersionComponent;
  let fixture: ComponentFixture<AllVersionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllVersionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
