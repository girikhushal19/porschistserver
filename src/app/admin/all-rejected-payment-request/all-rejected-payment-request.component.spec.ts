import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRejectedPaymentRequestComponent } from './all-rejected-payment-request.component';

describe('AllRejectedPaymentRequestComponent', () => {
  let component: AllRejectedPaymentRequestComponent;
  let fixture: ComponentFixture<AllRejectedPaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllRejectedPaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllRejectedPaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
