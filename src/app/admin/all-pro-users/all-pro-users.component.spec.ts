import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProUsersComponent } from './all-pro-users.component';

describe('AllProUsersComponent', () => {
  let component: AllProUsersComponent;
  let fixture: ComponentFixture<AllProUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllProUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
