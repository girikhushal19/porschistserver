import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-version',
  templateUrl: './edit-version.component.html',
  styleUrls: ['./edit-version.component.css']
})
export class EditVersionComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addPlanNormalUser:any;allPlanNormalUser:any;queryParam:any;record:any;edit_id:any;old_title:any;
  addModelsSubmit:any;getModel:any;allModelList:any; old_model_id:string="";
  allWebSubModel:any;old_version:any;
  allVersions:any;allSubModelList:any;model_id_on_change:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addPlanNormalUser = this.base_url_node+"editVersions";
    this.allWebSubModel = this.base_url_node+"allWebSubModelByName";
    //this.allPlanNormalUser = this.base_url_node+"allPlanNormalUser"; 
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id']; 
    this._http.get(this.base_url_node+"getVersions/"+this.edit_id).subscribe((response:any)=>{
      console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.record;

      this.old_model_id = this.record.model_id; 
      this.old_title = this.record.title; 
      this.old_version = this.record.version; 
        //console.log("old_attribute_type"+this.old_attribute_type)

    });

    this.getModel = this.base_url_node+"getModelAdmin";
    this._http.post(this.getModel,{category:"Porsche Voitures"}).subscribe((response:any)=>{
      console.log("response get model"+response);
      this.allModelList = response.result;
    });


    this.queryParam = { "parent_id":this.model_id_on_change  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      //console.log(this.model_id_on_change);
      
      console.log("response of api"+response);

      this.apiResponse = response;
      this.allVersions = this.apiResponse.all_version;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
      }
    });

    
  }

  ngOnInit(): void {
     

  }

  form = new UntypedFormGroup({
    id: new UntypedFormControl('', [ ]), 
    title: new UntypedFormControl('', [Validators.required]), 
    model_id: new UntypedFormControl('', [Validators.required]), 
    version: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addPlanNormalUser,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            //allVersions
            window.history.back();
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

  onChangeModel(event:any){
    this.model_id_on_change=event;
    /*console.log("hello");
    console.log(this.selectedDeviceR);
    console.log("category");
    console.log(this.form.value.category);*/
    //allSubCatListList
    this.queryParam = { "parent_id":this.model_id_on_change  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      //console.log(this.model_id_on_change);
      
      console.log("response of api"+response);

      this.apiResponse = response;
      this.allVersions = this.apiResponse.all_version;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
      }
    });
  }

}
