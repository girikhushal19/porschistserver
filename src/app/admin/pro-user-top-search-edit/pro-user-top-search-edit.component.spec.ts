import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchEditComponent } from './pro-user-top-search-edit.component';

describe('ProUserTopSearchEditComponent', () => {
  let component: ProUserTopSearchEditComponent;
  let fixture: ComponentFixture<ProUserTopSearchEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
