import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchComponent } from './pro-user-top-search.component';

describe('ProUserTopSearchComponent', () => {
  let component: ProUserTopSearchComponent;
  let fixture: ComponentFixture<ProUserTopSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
