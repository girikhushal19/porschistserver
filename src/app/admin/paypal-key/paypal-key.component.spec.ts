import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaypalKeyComponent } from './paypal-key.component';

describe('PaypalKeyComponent', () => {
  let component: PaypalKeyComponent;
  let fixture: ComponentFixture<PaypalKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaypalKeyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaypalKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
