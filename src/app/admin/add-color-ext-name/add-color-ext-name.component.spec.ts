import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddColorExtNameComponent } from './add-color-ext-name.component';

describe('AddColorExtNameComponent', () => {
  let component: AddColorExtNameComponent;
  let fixture: ComponentFixture<AddColorExtNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddColorExtNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddColorExtNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
