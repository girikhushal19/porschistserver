import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserEditAccessPlanComponent } from './pro-user-edit-access-plan.component';

describe('ProUserEditAccessPlanComponent', () => {
  let component: ProUserEditAccessPlanComponent;
  let fixture: ComponentFixture<ProUserEditAccessPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserEditAccessPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserEditAccessPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
