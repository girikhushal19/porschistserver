import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-pro-user-edit-access-plan',
  templateUrl: './pro-user-edit-access-plan.component.html',
  styleUrls: ['./pro-user-edit-access-plan.component.css']
})
export class ProUserEditAccessPlanComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;editPlanProUser:any;getSinglePlanProUser:any;queryParam:any;record:any;edit_id:any;
  old_day_number:any;old_price:any;old_maximum_upload:any;old_top_price:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.edit_id = this.actRoute.snapshot.params['id'];

    this.editPlanProUser = this.base_url_node+"editPlanProUser";
    this.getSinglePlanProUser = this.base_url_node+"getSinglePlanProUser"; 
    //console.log("here");
    this.queryParam = {"edit_id":this.edit_id};
    this._http.post(this.getSinglePlanProUser,this.queryParam).subscribe((response:any)=>{
      //console.log("getSinglePlanProUser"+response);
      //this.apiResponse = response;
      this.record = response.record;
      //console.log(this.record);
      this.old_day_number = this.record[0].day_number;
      this.old_price = this.record[0].price;
      this.old_maximum_upload = this.record[0].maximum_upload;
      this.old_top_price = this.record[0].top_price;
    });
  }

  ngOnInit(): void {

  }

  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    day_number: new UntypedFormControl('', [Validators.required]), 
    maximum_upload: new UntypedFormControl('', [Validators.required]), 
    price: new UntypedFormControl('', [Validators.required]), 
    //top_price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.editPlanProUser,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                //window.location.href = this.base_url+"allNormalUsers";
                window.history.back();
            }, 2000); 
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
