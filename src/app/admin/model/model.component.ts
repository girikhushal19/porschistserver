import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";

  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;user_type:any;apiResponse:any;formValue:any;addModelsSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  images2:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addModelsSubmit = this.base_url_node+"addModelsSubmit";
    this.getModel = this.base_url_node+"getModel";
    this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    
  }


  ngOnInit(): void {
    this._http.post(this.getCarCategory,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarCategory"+JSON.stringify(response));
      this.allCarCategory = response.result;
    });

    this._http.post(this.getModel,this.formValue).subscribe((response:any)=>{
      //console.log("response get model"+response);
      this.allModelList = response.result;
    });
  }

  form = new UntypedFormGroup({
    parent_id: new UntypedFormControl('', []),
    attribute_type: new UntypedFormControl('', [Validators.required]),
    model_name: new UntypedFormControl('', [Validators.required]),
    file: new UntypedFormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any) {
      if (event.target.files.length > 0)
      {
        const file = event.target.files[0];
        this.images = file;
        //console.log(file);
         
      }
  }

  onFileChange2(event:any) {
    if (event.target.files.length > 0)
    {
      const file = event.target.files[0];
      this.images2 = file;
      //console.log(file);
       
    }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
      this.formData.append('file', this.images);
      this.formData.append('file2', this.images2);
      this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      console.log(this.form.value);
      console.log(this.form.value.model_name);
      this._http.post(this.addModelsSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            this.form.reset();
            this._http.post(this.getModel,this.formValue).subscribe((response:any)=>{
              console.log("response get model"+response);
              this.allModelList = response.result;
            });
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
