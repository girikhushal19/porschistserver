import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllNormalUsersComponent } from './all-normal-users.component';

describe('AllNormalUsersComponent', () => {
  let component: AllNormalUsersComponent;
  let fixture: ComponentFixture<AllNormalUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllNormalUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllNormalUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
