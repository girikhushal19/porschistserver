import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchAllAccessComponent } from './pro-user-top-search-all-access.component';

describe('ProUserTopSearchAllAccessComponent', () => {
  let component: ProUserTopSearchAllAccessComponent;
  let fixture: ComponentFixture<ProUserTopSearchAllAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchAllAccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchAllAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
