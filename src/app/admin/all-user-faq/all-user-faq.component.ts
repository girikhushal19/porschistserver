import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-user-faq',
  templateUrl: './all-user-faq.component.html',
  styleUrls: ['./all-user-faq.component.css']
})
export class AllUserFaqComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allDriverFaq:any;queryParam:any;numbers:any;allDriverFaqCount:any;apiStringify:any;deleteBanner:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;updateCatStatusApi:any;
  dtOptions: DataTables.Settings = {}; 
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allDriverFaqCount = this.base_url_node+"allUserFaqCount";
    //console.log(this.allDriverFaqCount);
    this.allDriverFaq = this.base_url_node+"allUserFaq";
    this.updateCatStatusApi = this.base_url_node+"updateUserFaqStatusApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allDriverFaqCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allDriverFaqCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
    this.dtOptions = {
      "searching": false,
      "responsive": true,
      "autoWidth": false,
      "language": {
	    "search": "Recherche",
	    //"info": "Recherche",
	    //"entries": "Recherche",
	    }
    };
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allDriverFaq,this.queryParam).subscribe((response:any)=>{
      //console.log("allDriverFaq"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }

  updateCatStatus(id=null,status:number)
  {
    /*console.log(id);
    console.log(status);*/
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateCatStatusApi,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  
}
