import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserFaqComponent } from './all-user-faq.component';

describe('AllUserFaqComponent', () => {
  let component: AllUserFaqComponent;
  let fixture: ComponentFixture<AllUserFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUserFaqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllUserFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
