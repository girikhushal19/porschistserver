import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPaidPaymentRequestComponent } from './all-paid-payment-request.component';

describe('AllPaidPaymentRequestComponent', () => {
  let component: AllPaidPaymentRequestComponent;
  let fixture: ComponentFixture<AllPaidPaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPaidPaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllPaidPaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
