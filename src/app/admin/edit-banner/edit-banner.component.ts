import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-edit-banner',
  templateUrl: './edit-banner.component.html',
  styleUrls: ['./edit-banner.component.css']
})
export class EditBannerComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only="";
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editBannerImageSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  edit_id:any;getSingleBanner:any;old_first_heading:any;old_second_heading:any;old_iamge:any;
  
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editBannerImageSubmit = this.base_url_node+"editBannerImageSubmit";
    this.getSingleBanner = this.base_url_node+"getSingleBanner";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    let queryParam = {edit_id:this.edit_id};
    this._http.post(this.getSingleBanner,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse);
        this.old_first_heading = this.apiResponse.record.first_heading;
        this.old_second_heading = this.apiResponse.record.second_heading;
        if(this.apiResponse.record.images.length > 0)
        {
          this.old_iamge = this.apiResponse.record.images;
        }
        
        this.apiResponse = {"error":false,"msg":""};
      }
    });
  }


  ngOnInit(): void {
      this.myFiles = [];this.video = null;


  }

  form = new UntypedFormGroup({
    images: new UntypedFormControl('',  []),
    first_heading: new UntypedFormControl('',  [Validators.required]),
    second_heading: new UntypedFormControl('',  [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }


  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        }
        this.formData.append('edit_id', this.edit_id); 
        this.formData.append('first_heading', this.form.value.first_heading); 
        this.formData.append('second_heading', this.form.value.second_heading); 
        
      //
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      console.log(this.formData);
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.editBannerImageSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            window.location.href= this.base_url+"allBanner";
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
