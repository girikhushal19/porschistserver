import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import {AllreadyloggedinService} from '../../adminservice/allreadyloggedin.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private adminLogin:string = "";
  private adminLoginCheck:string = "";
  private base_url:string = "";
  
  fileName = '';
      test:any;formData:any;formValue:any;userImageName:any;
      apiResponse:any;token:any;user_type:any;
      apiStringify:any;user_id:any;loggedInDetail:any;base_url_node_admin:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private allreadyLoginAuthObj:AllreadyloggedinService)
  {
    this.base_url_node_admin = loginAuthObj.base_url_node_admin;
    console.log(this.base_url_node_admin);
    this.adminLogin = this.base_url_node_admin+"adminLogin";    

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    console.log("token "+this.token);
    console.log("user_type "+this.user_type);
    if(this.token !== "")
    {
      if(this.user_type === "admin" || this.user_type === "sub_admin")
      {
        //console.log("here");
        window.location.href = this.base_url+"dashboard";
      }
    }

  }

  ngOnInit(): void {
    
    //console.log("on init");
  }

  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('',[Validators.required, Validators.minLength(6)])
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) {         //{1}
  Object.keys(formGroup.controls).forEach(field => {  //{2}
    const control = formGroup.get(field);             //{3}
    if (control instanceof UntypedFormControl) {             //{4}
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof UntypedFormGroup) {        //{5}
      this.validateAllFormFields(control);            //{6}
    }
  });
}

  submit(){
    console.log("here");
      if (this.form.valid)
      {
        this.formValue = this.form.value;
        //this.formValue.file = this.userImageName;
          //console.log('form submitted'); 
          //console.log(this.formValue); 
          //this.test = this.fileToUpload;
          this._http.post(this.adminLogin,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          //console.log("response of api");
          //console.log(JSON.stringify(response));
          this.apiResponse = response;
           /*this.apiStringify = JSON.stringify(response);
           console.log(this.apiStringify);
           return false;*/
          //console.log(this.apiResponse.token);
          //console.log(this.apiResponse.data[0]._id);
          //console.log(this.apiResponse.data[0].email);
          // console.log("response"+  this.apiResponse);
          // console.log("response"+  this.apiResponse.userRecord);
          // console.log("response"+  this.apiResponse.userRecord._id);
          /*console.log(this.apiResponse);
          console.log(this.apiResponse.userRecord.user_type);
          return false;*/
          if(this.apiResponse.error == false)
          {
            localStorage.setItem("token",this.apiResponse.userRecord.token);
            localStorage.setItem("_id",this.apiResponse.userRecord._id);
            localStorage.setItem("email",this.apiResponse.userRecord.email);
            localStorage.setItem("user_type",this.apiResponse.userRecord.user_type);
          
            window.location.href = this.base_url+"dashboard";
          }
        });
        //var formData = new FormData();
        //formData.append('userfile', this.form.get('fileSource').value);
        //console.log(this.form);
      }else{
        //console.log('erro form submitted');
        this.validateAllFormFields(this.form); 
        // validate all form fields
      }
  }
}
