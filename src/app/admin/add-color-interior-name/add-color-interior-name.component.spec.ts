import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddColorInteriorNameComponent } from './add-color-interior-name.component';

describe('AddColorInteriorNameComponent', () => {
  let component: AddColorInteriorNameComponent;
  let fixture: ComponentFixture<AddColorInteriorNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddColorInteriorNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddColorInteriorNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
