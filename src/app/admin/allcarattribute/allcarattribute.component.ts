import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-allcarattribute',
  templateUrl: './allcarattribute.component.html',
  styleUrls: ['./allcarattribute.component.css']
})
export class AllcarattributeComponent implements OnInit {


  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allSubAttribute:any;queryParam:any;numbers:any;allSubAttributeCount:any;apiStringify:any;
  selectedIndex: number;updateAttributeStatusApi: any;queryParamNew: any;numofpage_0: any;
  getCarCategory:any; allCarCategory:any;

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.updateAttributeStatusApi = this.base_url_node+"updateAttributeStatusApi";
    this.allSubAttributeCount = this.base_url_node+"allSubAttributeCount";
    this.allSubAttribute = this.base_url_node+"allSubAttribute";
    this.getCarCategory = this.base_url_node+"getCarCategory";
    this.getallSubAttribute(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allSubAttributeCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allSubAttributeCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });

    this._http.post(this.getCarCategory,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarCategory"+response);
      this.allCarCategory = response.result;
    });
  }

  getallSubAttribute(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allSubAttribute,this.queryParam).subscribe((response:any)=>{
      //console.log("allSubAttribute"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateAttributeStatus(id=null,status:number)
  {
    this.queryParamNew = {"id":id,"status":status};
    this._http.post(this.updateAttributeStatusApi,this.queryParamNew).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.numofpage_0 = 0;
        this.queryParam = {"numofpage":this.numofpage_0};
        //console.log(this.queryParam);
        this._http.post(this.allSubAttribute,this.queryParam).subscribe((response:any)=>{
          //console.log("allModel"+JSON.stringify(response));
          this.record = response.record;
          
          //console.log("totalPageNumber"+this.totalPageNumber);
          //console.log(this.numbers);
        });
      }
    });
  }


  form = new UntypedFormGroup({
    attribute_type: new UntypedFormControl('', []),
    attribute_name: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this._http.post(this.allSubAttributeCount,this.form.value).subscribe((response:any)=>{
      //console.log("allSubAttributeCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers "+this.numbers);
    });
    //console.log(this.form.value);
    this._http.post(this.allSubAttribute,this.form.value).subscribe((response:any)=>{
      //console.log("allUsers"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }


}
