import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllcarattributeComponent } from './allcarattribute.component';

describe('AllcarattributeComponent', () => {
  let component: AllcarattributeComponent;
  let fixture: ComponentFixture<AllcarattributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllcarattributeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllcarattributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
