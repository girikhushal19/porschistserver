import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditColorInteriorNameComponent } from './edit-color-interior-name.component';

describe('EditColorInteriorNameComponent', () => {
  let component: EditColorInteriorNameComponent;
  let fixture: ComponentFixture<EditColorInteriorNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditColorInteriorNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditColorInteriorNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
