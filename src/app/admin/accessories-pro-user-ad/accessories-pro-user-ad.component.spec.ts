import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesProUserAdComponent } from './accessories-pro-user-ad.component';

describe('AccessoriesProUserAdComponent', () => {
  let component: AccessoriesProUserAdComponent;
  let fixture: ComponentFixture<AccessoriesProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesProUserAdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessoriesProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
