import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-add-top-urgent',
  templateUrl: './add-top-urgent.component.html',
  styleUrls: ['./add-top-urgent.component.css']
})
export class AddTopUrgentComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addTopListPlanNormalUser:any;allPlanNormalUser:any;queryParam:any;record:any;
  old_car:string="Car";
  old_access:string="Accessoires";
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addTopListPlanNormalUser = this.base_url_node+"addTopUrgentPlanNormalUser";
    //this.allPlanNormalUser = this.base_url_node+"allPlanNormalUser"; 
    //console.log("here");
    
  }

  ngOnInit(): void {
     

  }

  form = new UntypedFormGroup({
    plan_type: new UntypedFormControl('', [Validators.required]), 
    day_number: new UntypedFormControl('', [Validators.required]), 
    price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addTopListPlanNormalUser,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            this.form.reset();
            

          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
