import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTopUrgentComponent } from './add-top-urgent.component';

describe('AddTopUrgentComponent', () => {
  let component: AddTopUrgentComponent;
  let fixture: ComponentFixture<AddTopUrgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTopUrgentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTopUrgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
