import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesInActiveProUserAdComponent } from './accessories-in-active-pro-user-ad.component';

describe('AccessoriesInActiveProUserAdComponent', () => {
  let component: AccessoriesInActiveProUserAdComponent;
  let fixture: ComponentFixture<AccessoriesInActiveProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesInActiveProUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesInActiveProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
