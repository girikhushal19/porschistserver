import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-accessories-in-active-pro-user-ad',
  templateUrl: './accessories-in-active-pro-user-ad.component.html',
  styleUrls: ['./accessories-in-active-pro-user-ad.component.css']
})
export class AccessoriesInActiveProUserAdComponent implements OnInit {

    dtOptions: DataTables.Settings = {};

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allAccessoriesActiveProUser:any;queryParam:any;numbers:any;allAccessoriesActiveProUserCount:any;apiStringify:any;updateUserStatusApi:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;

  getModelAdmin:any; getAttrTypeDePiece:any; allAttrTypeDePiece:any;  registration_yearAdmin:any; allRegYear:any; 
 

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allAccessoriesActiveProUserCount = this.base_url_node+"allAccessoriesInActiveProUserCount";
    this.allAccessoriesActiveProUser = this.base_url_node+"allAccessoriesInActiveProUser";
    this.updateUserStatusApi = this.base_url_node+"updateAdStatusApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allAccessoriesActiveProUserCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveProUserCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });

    this._http.post(this.getModelAdmin,this.queryParam).subscribe((response:any)=>{
      this.allModelList = response.result;
      //console.log("getModelAdmin"+this.allModelList);
    });
    this._http.post(this.getAttrTypeDePiece,this.queryParam).subscribe((response:any)=>{
      this.allAttrTypeDePiece = response.record;
      //console.log("allAttrTypeDePiece"+this.allAttrTypeDePiece);
    });
    this._http.post(this.registration_yearAdmin,this.queryParam).subscribe((response:any)=>{
      this.allRegYear = response.record;
      //console.log("allRegYear"+this.allRegYear);
    });
    
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allAccessoriesActiveProUser,this.queryParam).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveProUser"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateUserStatus(id=null,status:number)
  {
     
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateUserStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
  form = new UntypedFormGroup({
    modelId: new UntypedFormControl('', []),
    title: new UntypedFormControl('', []), 
    attribute_type: new UntypedFormControl('', []),
    state: new UntypedFormControl('', []),
    OEM: new UntypedFormControl('', []),
    minPrice: new UntypedFormControl('', []),
    maxPrice: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {

    this._http.post(this.allAccessoriesActiveProUserCount,this.form.value).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveProUserCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
    //console.log(this.form.value);
    this._http.post(this.allAccessoriesActiveProUser,this.form.value).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveProUser"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
}
