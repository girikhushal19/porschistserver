import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserCarAdPlanComponent } from './all-user-car-ad-plan.component';

describe('AllUserCarAdPlanComponent', () => {
  let component: AllUserCarAdPlanComponent;
  let fixture: ComponentFixture<AllUserCarAdPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUserCarAdPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllUserCarAdPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
