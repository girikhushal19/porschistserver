import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesOrdersProcessComponent } from './accessories-orders-process.component';

describe('AccessoriesOrdersProcessComponent', () => {
  let component: AccessoriesOrdersProcessComponent;
  let fixture: ComponentFixture<AccessoriesOrdersProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesOrdersProcessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesOrdersProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
