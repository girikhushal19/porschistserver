import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProuserCarAdPlanComponent } from './edit-prouser-car-ad-plan.component';

describe('EditProuserCarAdPlanComponent', () => {
  let component: EditProuserCarAdPlanComponent;
  let fixture: ComponentFixture<EditProuserCarAdPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProuserCarAdPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditProuserCarAdPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
