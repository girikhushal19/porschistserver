import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCarAttributeComponent } from './edit-car-attribute.component';

describe('EditCarAttributeComponent', () => {
  let component: EditCarAttributeComponent;
  let fixture: ComponentFixture<EditCarAttributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCarAttributeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditCarAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
