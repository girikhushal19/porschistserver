import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-demo-add-car',
  templateUrl: './demo-add-car.component.html',
  styleUrls: ['./demo-add-car.component.css']
})
export class DemoAddCarComponent implements OnInit {

 

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allModel:any;queryParam:any;numbers:any;allModelCount:any;base_url_node_only:any;
 updateModelStatusApi:any;queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;

 personalDetails!: UntypedFormGroup;
    addressDetails!: UntypedFormGroup;
    educationalDetails!: UntypedFormGroup;
    personal_step = false;
    address_step = false;
    education_step = false;
    step = 1;

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService,private formBuilder: UntypedFormBuilder)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
     
    //this.getModel = this.base_url_node+"getModel";
    //console.log("here");
    

    this.updateModelStatus = this.base_url_node+"updateModelStatusApi";


  }

  ngOnInit(): void {
    /*this.personalDetails = this.formBuilder.group({
      name: new UntypedFormControl('', []),
      email: new UntypedFormControl('', [Validators.required]),
      phone: new UntypedFormControl('', [Validators.required])
    });*/
    this.addressDetails = this.formBuilder.group({
        city: new UntypedFormControl('', [Validators.required]),
        address: new UntypedFormControl('', [Validators.required]),
        pincode: new UntypedFormControl('', [Validators.required])
    });
    this.educationalDetails = this.formBuilder.group({
        highest_qualification: new UntypedFormControl('', [Validators.required]),
        university: new UntypedFormControl('', [Validators.required]),
        total_marks: new UntypedFormControl('', [Validators.required])
    });
  }


  abc = new UntypedFormGroup({
    firstname: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required]),
    phone: new UntypedFormControl('', [Validators.required])
  });
  
  get abcc(){
    return this.abc.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }


  get personal() { return this.personalDetails.controls; }
    
    get address() { return this.addressDetails.controls; }
  
    get education() { return this.educationalDetails.controls; }

    next(){
 
      if(this.step==1)
      {
        //console.log("hereeee");
        //this.personal_step = true;
        if (this.abc.invalid) { this.validateAllFormFields(this.abc); return  }
        this.step++
      }
  
      else if(this.step==2){
          //this.address_step = true;
          //if (this.addressDetails.invalid) {   return }
              this.step++;
      }

      
  
    }
  
    previous(){
      this.step--
     
      /*if(this.step==1){
        this.address_step = false;
      }
      if(this.step==2){
        this.education_step = false;
      }*/
     
    }
  
    submit(){
      console.log("hereeeeeeeee");
      if(this.step==3)
      {
        console.log("iffffff");
        this.education_step = true;
        //if (this.educationalDetails.invalid) { return }
        alert("Well done!!")
      }else{
        console.log("elseeeeeee");
      }

    }

    


}
