import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoAddCarComponent } from './demo-add-car.component';

describe('DemoAddCarComponent', () => {
  let component: DemoAddCarComponent;
  let fixture: ComponentFixture<DemoAddCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoAddCarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemoAddCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
