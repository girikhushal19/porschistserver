import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllColorExtNameComponent } from './all-color-ext-name.component';

describe('AllColorExtNameComponent', () => {
  let component: AllColorExtNameComponent;
  let fixture: ComponentFixture<AllColorExtNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllColorExtNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllColorExtNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
