import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-send-push-notification',
  templateUrl: './send-push-notification.component.html',
  styleUrls: ['./send-push-notification.component.css']
})
export class SendPushNotificationComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;sendPushNotificationSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.sendPushNotificationSubmit = this.base_url_node+"sendPushNotificationSubmit";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    
  }


  ngOnInit(): void {
       
  }

  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: FormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
         
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.sendPushNotificationSubmit,this.formValue).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            this.form.reset();
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
