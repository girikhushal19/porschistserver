import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.css']
})
export class EditCountryComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addPlanNormalUser:any;allPlanNormalUser:any;queryParam:any;record:any;edit_id:any;old_title:any; old_tax:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addPlanNormalUser = this.base_url_node+"editCountry";
    //this.allPlanNormalUser = this.base_url_node+"allPlanNormalUser"; 
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id']; 
    this._http.get(this.base_url_node+"getCountry/"+this.edit_id).subscribe((response:any)=>{
      console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.record;

      
      this.old_title = this.record.title; 
      this.old_tax = this.record.tax; 
        //console.log("old_attribute_type"+this.old_attribute_type)

    });
  }

  ngOnInit(): void {
     

  }

  form = new UntypedFormGroup({
    id: new UntypedFormControl(this.actRoute.snapshot.params['id'], [ ]), 
    title: new UntypedFormControl('', [Validators.required]), 
    tax: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addPlanNormalUser,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            //allVersions
            window.history.back();
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
