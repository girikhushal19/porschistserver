import { Component, OnInit } from '@angular/core';
import { Editor } from 'ngx-editor';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.css']
})
export class AddPageComponent implements OnInit {

  editor: Editor;
  html: any;
  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addPageSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addPageSubmit = this.base_url_node+"addPageSubmit";
     
    //console.log("here");
     this.editor = new Editor();
     this.html = "";  
  }


  ngOnInit(): void {
    this.editor = new Editor();
  }
   ngOnDestroy(): void {
    this.editor.destroy();
  }


  form = new UntypedFormGroup({
    slug_name: new UntypedFormControl('', [Validators.required]), 
    page_title: new UntypedFormControl('', [Validators.required]), 
    html: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.addPageSubmit,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            this.form.reset();
            
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
