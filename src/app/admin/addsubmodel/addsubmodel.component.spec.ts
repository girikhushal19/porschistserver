import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsubmodelComponent } from './addsubmodel.component';

describe('AddsubmodelComponent', () => {
  let component: AddsubmodelComponent;
  let fixture: ComponentFixture<AddsubmodelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddsubmodelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsubmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
