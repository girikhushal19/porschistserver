import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarActiveNormalUserAdComponent } from './car-active-normal-user-ad.component';

describe('CarActiveNormalUserAdComponent', () => {
  let component: CarActiveNormalUserAdComponent;
  let fixture: ComponentFixture<CarActiveNormalUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarActiveNormalUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarActiveNormalUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
