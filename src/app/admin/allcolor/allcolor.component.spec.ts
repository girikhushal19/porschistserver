import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllcolorComponent } from './allcolor.component';

describe('AllcolorComponent', () => {
  let component: AllcolorComponent;
  let fixture: ComponentFixture<AllcolorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllcolorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllcolorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
