import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubModelComponent } from './edit-sub-model.component';

describe('EditSubModelComponent', () => {
  let component: EditSubModelComponent;
  let fixture: ComponentFixture<EditSubModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSubModelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditSubModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
