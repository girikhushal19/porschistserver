import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-sub-model',
  templateUrl: './edit-sub-model.component.html',
  styleUrls: ['./edit-sub-model.component.css']
})
export class EditSubModelComponent implements OnInit {

  base_url = "";base_url_node = "";images:any;
  token:any;user_type:any;apiResponse:any;formValue:any;editModelsSubmit:any;getModel:any;allModelList:any;getqueryParam:any;edit_id:any;getSingleSubModel:any;record:any;old_attribute_type:any;old_parent_id:any;old_model_name:any;base_url_node_only:any;formData:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editModelsSubmit = this.base_url_node+"editModelsSubmit";
    this.getModel = this.base_url_node+"getModelAdmin";
    //console.log("here");

    this.getSingleSubModel = this.base_url_node+"getSingleSubModel";

    this.edit_id = this.actRoute.snapshot.params['id'];

    this.getqueryParam = {"id":this.edit_id};
    this._http.post(this.getSingleSubModel,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleSubModel"+JSON.stringify(response));
      this.record = response.record;
      //this.old_attribute_type = this.record[0].attribute_type;
      this.old_model_name = this.record[0].model_name;
      this.old_parent_id = this.record[0].parent_id;


      console.log("this.formValue ", this.old_parent_id);
    this._http.post(this.getModel,{"parent_id":this.old_parent_id}).subscribe((response:any)=>{
      //console.log("response get model"+response);
      this.allModelList = response.result;
    });

    
      //console.log(this.old_model_name);
      //console.log("old_attribute_type"+this.old_attribute_type)

    });


    
  }


  ngOnInit(): void {
    
  }



  form = new UntypedFormGroup({
    parent_id: new UntypedFormControl('', []),
    model_name: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
      this.formData.append('file', this.images);
      this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);
      this.formData.append('edit_id', this.edit_id);
      this._http.post(this.editModelsSubmit,this.formData).subscribe((response:any)=>{
         // console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                //window.location.href = this.base_url+"allNormalUsers";
                window.history.back();
              }, 2000); 
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
