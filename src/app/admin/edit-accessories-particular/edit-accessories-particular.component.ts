import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-accessories-particular',
  templateUrl: './edit-accessories-particular.component.html',
  styleUrls: ['./edit-accessories-particular.component.css']
})
export class EditAccessoriesParticularComponent implements OnInit {



  base_url = "";base_url_node = "";edit_id:any;getSingleAccessoriesPlanNormalUser:any
  token:any;user_type:any;apiResponse:any;formValue:any;editAccessoriesPlanNormalUser:any;allPlanNormalUser:any;queryParam:any;record:any;old_photo_number:any;old_price:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editAccessoriesPlanNormalUser = this.base_url_node+"editAccessoriesPlanNormalUser";
    this.getSingleAccessoriesPlanNormalUser = this.base_url_node+"getSingleAccessoriesPlanNormalUser";

    //this.allPlanNormalUser = this.base_url_node+"allPlanNormalUser"; 
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.queryParam = {"edit_id":this.edit_id};
    this._http.post(this.getSingleAccessoriesPlanNormalUser,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse);
        this.old_photo_number = this.apiResponse.record.photo_number;
        this.old_price = this.apiResponse.record.price;
        
        
        this.apiResponse = {"error":false,"msg":""};
      }
    });

  }

  ngOnInit(): void {
     

  }

  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []),
    photo_number: new UntypedFormControl('', [Validators.required]), 
    price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.editAccessoriesPlanNormalUser,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            //this.form.reset();
            setTimeout(() => {
              window.location.href = this.base_url+"allAccessoriesParticular";
            }, 2000); 

          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
