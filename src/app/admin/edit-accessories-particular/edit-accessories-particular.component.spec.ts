import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccessoriesParticularComponent } from './edit-accessories-particular.component';

describe('EditAccessoriesParticularComponent', () => {
  let component: EditAccessoriesParticularComponent;
  let fixture: ComponentFixture<EditAccessoriesParticularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAccessoriesParticularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditAccessoriesParticularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
