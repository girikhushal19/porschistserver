import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopUrgentParticularAccessoriesComponent } from './top-urgent-particular-accessories.component';

describe('TopUrgentParticularAccessoriesComponent', () => {
  let component: TopUrgentParticularAccessoriesComponent;
  let fixture: ComponentFixture<TopUrgentParticularAccessoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopUrgentParticularAccessoriesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TopUrgentParticularAccessoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
