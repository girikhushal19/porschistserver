import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-model',
  templateUrl: './edit-model.component.html',
  styleUrls: ['./edit-model.component.css']
})
export class EditModelComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";
  edit_id: string;
  base_url = "";base_url_node = ""; getqueryParam:any;
  token:any;getSingleModel:any;formData:any;images:any;user_type:any;apiResponse:any;formValue:any;editModelsSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;old_attribute_type:any;record:any;old_fileImage:any;old_model_name:any;base_url_node_only:any;old_fileImage2:any;
  images2:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editModelsSubmit = this.base_url_node+"editModelsSubmit";
    //this.getModel = this.base_url_node+"getModel";
    this.getCarCategory = this.base_url_node+"getCarCategory";
    this.getSingleModel = this.base_url_node+"getSingleModel";
    //console.log("here");
    this.edit_id = this.actRoute.snapshot.params['id'];

    this.getqueryParam = {"id":this.edit_id};
    this._http.post(this.getSingleModel,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.record;
      this.old_attribute_type = this.record[0].attribute_type;
      this.old_model_name = this.record[0].model_name;
      this.old_fileImage = this.record[0].fileImage;
      this.old_fileImage2 = this.record[0].black_white_image;
      
      //console.log("old_attribute_type"+this.old_attribute_type)

    });
  }


  ngOnInit(): void {
    this._http.post(this.getCarCategory,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarCategory"+JSON.stringify(response));
      this.allCarCategory = response.result;
    });

    /*this._http.post(this.getModel,this.formValue).subscribe((response:any)=>{
      //console.log("response get model"+response);
      this.allModelList = response.result;
    });*/
  }

  form = new UntypedFormGroup({
    parent_id: new UntypedFormControl('', []),
    attribute_type: new UntypedFormControl('', [Validators.required]),
    model_name: new UntypedFormControl('', [Validators.required]),
    file: new UntypedFormControl('', []),
    black_white_file: new UntypedFormControl('', []),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any) {
      if (event.target.files.length > 0)
      {
        const file = event.target.files[0];
        this.images = file;
        //console.log(file);
         
      }
  }
  onFileChange2(event:any) {
      if (event.target.files.length > 0)
      {
        const file = event.target.files[0];
        this.images2 = file;
        //console.log(file);
         
      }
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
      this.formData.append('file', this.images);
      this.formData.append('file2', this.images2);
      this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);
      this.formData.append('edit_id', this.edit_id);
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      console.log(this.form.value);
      console.log(this.form.value.model_name);
      this._http.post(this.editModelsSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
              setTimeout(() => {
                //window.location.href = this.base_url+"allNormalUsers";
                window.history.back();
              }, 2000); 
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
