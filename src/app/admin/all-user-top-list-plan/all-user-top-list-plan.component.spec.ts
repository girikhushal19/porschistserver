import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserTopListPlanComponent } from './all-user-top-list-plan.component';

describe('AllUserTopListPlanComponent', () => {
  let component: AllUserTopListPlanComponent;
  let fixture: ComponentFixture<AllUserTopListPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUserTopListPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllUserTopListPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
