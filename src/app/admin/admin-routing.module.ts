import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminlogoutComponent } from './adminlogout/adminlogout.component';
import { RegistrationComponent } from './registration/registration.component';
import { ModelComponent } from './model/model.component';
import { AllmodelComponent } from './allmodel/allmodel.component';
import { SubmodelComponent } from './submodel/submodel.component';
import { AddsubmodelComponent } from './addsubmodel/addsubmodel.component';
import { CarattributeComponent } from './carattribute/carattribute.component';
import { AllcarattributeComponent } from './allcarattribute/allcarattribute.component';
import { AddcolorComponent } from './addcolor/addcolor.component';
import { AllcolorComponent } from './allcolor/allcolor.component';
import { IndividualplanComponent } from './individualplan/individualplan.component';
import { ProfessionalplanComponent } from './professionalplan/professionalplan.component';
import {TopurgentComponent } from './topurgent/topurgent.component';
import { AddBannerComponent } from './add-banner/add-banner.component';
import { AllBannerComponent } from './all-banner/all-banner.component';
import { AllCategoryComponent } from './all-category/all-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { AllNormalUsersComponent } from './all-normal-users/all-normal-users.component';
import { AllProUsersComponent } from './all-pro-users/all-pro-users.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AccessoriesNormalUserAdComponent } from './accessories-normal-user-ad/accessories-normal-user-ad.component';
import { AccessoriesProUserAdComponent } from './accessories-pro-user-ad/accessories-pro-user-ad.component';
import { AccessoriesActiveNormalUserAdComponent } from './accessories-active-normal-user-ad/accessories-active-normal-user-ad.component';
import { AccessoriesActiveProUserAdComponent } from './accessories-active-pro-user-ad/accessories-active-pro-user-ad.component';
import { CarNormalUserAdComponent } from './car-normal-user-ad/car-normal-user-ad.component';
import { CarProUserAdComponent } from './car-pro-user-ad/car-pro-user-ad.component';
import { CarActiveProUserAdComponent } from './car-active-pro-user-ad/car-active-pro-user-ad.component';
import { CarActiveNormalUserAdComponent } from './car-active-normal-user-ad/car-active-normal-user-ad.component';
import { SendPushNotificationComponent } from './send-push-notification/send-push-notification.component';
import { AllPushNotificationComponent } from './all-push-notification/all-push-notification.component';
import { SendEmailNotificationComponent } from './send-email-notification/send-email-notification.component';
import { AllEmailNotificationComponent } from './all-email-notification/all-email-notification.component';
import { AddPageComponent } from './add-page/add-page.component';
import { AllPageComponent } from './all-page/all-page.component';
import { EditModelComponent } from './edit-model/edit-model.component';
import { EditSubModelComponent } from './edit-sub-model/edit-sub-model.component';
import { EditCarAttributeComponent } from './edit-car-attribute/edit-car-attribute.component';
import { EditColorComponent } from './edit-color/edit-color.component';
import { EditUserCarAdPlanComponent } from './edit-user-car-ad-plan/edit-user-car-ad-plan.component';
import { EditProuserCarAdPlanComponent } from './edit-prouser-car-ad-plan/edit-prouser-car-ad-plan.component';
import { AllUserCarAdPlanComponent } from './all-user-car-ad-plan/all-user-car-ad-plan.component';
import { AllProuserCarAdPlanComponent } from './all-prouser-car-ad-plan/all-prouser-car-ad-plan.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { AccessoriesOrdersComponent } from './accessories-orders/accessories-orders.component';

import { AddSubAdminComponent } from './add-sub-admin/add-sub-admin.component';
import { AllSubAdminComponent } from './all-sub-admin/all-sub-admin.component';
import { EditSubAdminComponent } from './edit-sub-admin/edit-sub-admin.component';
import { PermissionSubAdminComponent } from './permission-sub-admin/permission-sub-admin.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';

import { UploadFileComponent } from './upload-file/upload-file.component';
import { DemoAddCarComponent } from './demo-add-car/demo-add-car.component';
import { EditBannerComponent } from './edit-banner/edit-banner.component';

import { UserTopListPlanComponent } from './user-top-list-plan/user-top-list-plan.component';
import { AllUserTopListPlanComponent } from './all-user-top-list-plan/all-user-top-list-plan.component';
import { EditUserTopListPlanComponent } from './edit-user-top-list-plan/edit-user-top-list-plan.component';

import { AddAccessoriesParticularComponent } from './add-accessories-particular/add-accessories-particular.component';
import { AllAccessoriesParticularComponent } from './all-accessories-particular/all-accessories-particular.component';
import { EditAccessoriesParticularComponent } from './edit-accessories-particular/edit-accessories-particular.component';
import { TopUrgentParticularAccessoriesComponent } from './top-urgent-particular-accessories/top-urgent-particular-accessories.component';

import { ProUserTopSearchComponent } from './pro-user-top-search/pro-user-top-search.component';
import { ProUserTopSearchAllComponent } from './pro-user-top-search-all/pro-user-top-search-all.component';
import { ProUserTopSearchEditComponent } from './pro-user-top-search-edit/pro-user-top-search-edit.component';


import { ProUserTopSearchAccessComponent } from './pro-user-top-search-access/pro-user-top-search-access.component';
import { ProUserTopSearchAllAccessComponent } from './pro-user-top-search-all-access/pro-user-top-search-all-access.component';
import { ProUserTopSearchEditAccessComponent } from './pro-user-top-search-edit-access/pro-user-top-search-edit-access.component';
import { ProUserAddAccessPlanComponent } from './pro-user-add-access-plan/pro-user-add-access-plan.component';
import { ProUserAllAccessPlanComponent } from './pro-user-all-access-plan/pro-user-all-access-plan.component';
import { ProUserEditAccessPlanComponent } from './pro-user-edit-access-plan/pro-user-edit-access-plan.component';
import { SettingComponent } from './setting/setting.component';

import { AddUserFaqComponent } from './add-user-faq/add-user-faq.component';
import { AllUserFaqComponent } from './all-user-faq/all-user-faq.component';
import { EditUserFaqComponent } from './edit-user-faq/edit-user-faq.component';
import { StripeKeyComponent } from './stripe-key/stripe-key.component';
import { PaypalKeyComponent } from './paypal-key/paypal-key.component';
import { AccessoriesOrdersProcessComponent } from './accessories-orders-process/accessories-orders-process.component';
import { AccessoriesOrdersCancelComponent } from './accessories-orders-cancel/accessories-orders-cancel.component';
import { AccessoriesOrdersCompleteComponent } from './accessories-orders-complete/accessories-orders-complete.component';
import { AccessoriesInActiveProUserAdComponent } from './accessories-in-active-pro-user-ad/accessories-in-active-pro-user-ad.component';
import { CarInActiveNormalUserAdComponent } from './car-in-active-normal-user-ad/car-in-active-normal-user-ad.component';
import { CarInactiveProUserAdComponent } from './car-inactive-pro-user-ad/car-inactive-pro-user-ad.component';
import { UserBankDetailComponent } from './user-bank-detail/user-bank-detail.component';
import { UserInvoicesComponent } from './user-invoices/user-invoices.component';

import { AddOptionsComponent } from './add-options/add-options.component';
import { AllOptionsComponent } from './all-options/all-options.component';
import { EditOptionsComponent } from './edit-options/edit-options.component';
import { ManageTitleComponent } from './manage-title/manage-title.component';


import { AddVersionComponent } from './add-version/add-version.component';
import { AllVersionComponent } from './all-version/all-version.component';
import { EditVersionComponent } from './edit-version/edit-version.component';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { AllInvoiceComponent } from './all-invoice/all-invoice.component';

import { AddCountryComponent } from './add-country/add-country.component';
import { AllCountryComponent } from './all-country/all-country.component';
import { EditCountryComponent } from './edit-country/edit-country.component';

import { AddColorExtNameComponent } from './add-color-ext-name/add-color-ext-name.component';
import { AllColorExtNameComponent } from './all-color-ext-name/all-color-ext-name.component';
import { EditColorExtNameComponent } from './edit-color-ext-name/edit-color-ext-name.component';

import { AddColorInteriorComponent } from './add-color-interior/add-color-interior.component';
import { AllColorInteriorComponent } from './all-color-interior/all-color-interior.component';
import { EditColorInteriorComponent } from './edit-color-interior/edit-color-interior.component';
import { AddColorInteriorNameComponent } from './add-color-interior-name/add-color-interior-name.component';
import { AllColorInteriorNameComponent } from './all-color-interior-name/all-color-interior-name.component';
import { EditColorInteriorNameComponent } from './edit-color-interior-name/edit-color-interior-name.component';

import { AddTopUrgentComponent } from './add-top-urgent/add-top-urgent.component';
import { AllTopUrgentComponent } from './all-top-urgent/all-top-urgent.component';
import { EditTopUrgentComponent } from './edit-top-urgent/edit-top-urgent.component';

import { AllPaymentRequestComponent } from './all-payment-request/all-payment-request.component';
import { AllPendingPaymentRequestComponent } from './all-pending-payment-request/all-pending-payment-request.component';
import { AllPaidPaymentRequestComponent } from './all-paid-payment-request/all-paid-payment-request.component';
import { AllRejectedPaymentRequestComponent } from './all-rejected-payment-request/all-rejected-payment-request.component';



const routes: Routes = [
      {
        path : "",
        component : HomeComponent
      },
      {
        path : "dashboard",
        component : DashboardComponent
      },
      {
        path : "logout",
        component : AdminlogoutComponent
      },
      {
        path : "adminRegistration",
        component : RegistrationComponent
      },
      {
        path : "addModel",
        component : ModelComponent
      },
      {
        path : "allModel",
        component : AllmodelComponent
      },
      {
        path : "allSubModel",
        component : SubmodelComponent
      },
      {
        path : "addSubModel",
        component : AddsubmodelComponent
      },
      {
        path : "addCarAttribute",
        component : CarattributeComponent
      },
      {
        path : "allCarAttribute",
        component : AllcarattributeComponent
      },
      {
        path : "addColor",
        component : AddcolorComponent
      },
      {
        path : "allColor",
        component : AllcolorComponent
      },
      {
        path : "userCarAdPlan",
        component : IndividualplanComponent
      },
      {
        path : "ProuserCarAdPlan",
        component : ProfessionalplanComponent
      },
      {
        path : "topUrgent",
        component : TopurgentComponent
      },
      {
        path : "addBanner",
        component : AddBannerComponent
      },
      {
        path : "editBanner/:id",
        component : EditBannerComponent
      },
      {
        path : "allBanner",
        component : AllBannerComponent
      },
      {
        path : "allCategory",
        component : AllCategoryComponent
      },
      {
        path : "editCategory/:id",
        component : EditCategoryComponent
      },
      {
        path : "allNormalUsers",
        component : AllNormalUsersComponent
      },
      {
        path : "allProUsers",
        component : AllProUsersComponent
      },
      {
        path : "editUser/:id",
        component : EditUserComponent
      },
      {
        path : "accessoriesNormalUserAd",
        component : AccessoriesNormalUserAdComponent
      },
      {
        path : "accessoriesProUserAd",
        component : AccessoriesProUserAdComponent
      },
      {
        path : "accessoriesActiveNormalUserAd",
        component : AccessoriesActiveNormalUserAdComponent
      },
      {
        path : "accessoriesActiveProUserAd",
        component : AccessoriesActiveProUserAdComponent
      },
      {
        path : "carNormalUserAd",
        component : CarNormalUserAdComponent
      },
      {
        path : "carProUserAd",
        component : CarProUserAdComponent
      },
      {
        path : "carActiveProUserAd",
        component : CarActiveProUserAdComponent
      },
      {
        path : "carActiveNormalUserAd",
        component : CarActiveNormalUserAdComponent
      },

      {
        path : "carInActiveProUserAd",
        component : CarInactiveProUserAdComponent
      },
      {
        path : "carInActiveNormalUserAd",
        component : CarInActiveNormalUserAdComponent
      },

      {
        path : "sendPushNotification",
        component : SendPushNotificationComponent
      },
      {
        path : "allPushNotification",
        component : AllPushNotificationComponent
      },
      {
        path : "sendEmailNotification",
        component : SendEmailNotificationComponent
      },
      {
        path : "allEmailNotification",
        component : AllEmailNotificationComponent
      },
      {
        path : "addPage",
        component : AddPageComponent
      },
      {
        path : "allPage",
        component : AllPageComponent
      },
      {
        path : "editModel/:id",
        component : EditModelComponent
      },
      {
        path : "editSubModel/:id",
        component : EditSubModelComponent
      },
      {
        path : "editCarAttribute/:id",
        component : EditCarAttributeComponent
      },
      {
        path : "editColor/:id",
        component : EditColorComponent
      },
      {
        path : "addColorExtName",
        component : AddColorExtNameComponent
      },
      {
        path : "allColorExtName",
        component : AllColorExtNameComponent
      },
      {
        path : "editColorExtName/:id",
        component : EditColorExtNameComponent
      },
      {
        path : "addColorInterior",
        component : AddColorInteriorComponent
      },
      {
        path : "allColorInterior",
        component : AllColorInteriorComponent
      },
      {
        path : "editColorInterior/:id",
        component : EditColorInteriorComponent
      },
      {
        path : "addColorInteriorName",
        component : AddColorInteriorNameComponent
      },
      {
        path : "allColorInteriorName",
        component : AllColorInteriorNameComponent
      },
      {
        path : "editColorInteriorName/:id",
        component : EditColorInteriorNameComponent
      },
      {
        path : "editUserCarAdPlan/:id",
        component : EditUserCarAdPlanComponent
      },
      {
        path : "editProuserCarAdPlan/:id",
        component : EditProuserCarAdPlanComponent
      },
      {
        path : "allUserCarAdPlan",
        component : AllUserCarAdPlanComponent
      },
      {
        path : "allProuserCarAdPlan",
        component : AllProuserCarAdPlanComponent
      },
      {
        path : "editPage/:id",
        component : EditPageComponent
      },
      {
        path : "accessoriesOrders",
        component : AccessoriesOrdersComponent
      },
      {
        path : "addSubAdmin",
        component : AddSubAdminComponent
      },
      {
        path : "allSubAdmin",
        component : AllSubAdminComponent
      },
      {
        path : "editSubAdmin/:id",
        component : EditSubAdminComponent
      },
      {
        path : "permissionSubAdmin/:id",
        component : PermissionSubAdminComponent
      },
      {
        path : "editAdminProfile/:id",
        component : EditAdminProfileComponent
      },
      {
        path : "adminChangePassword/:id",
        component : AdminChangePasswordComponent
      },
      {
        path : "uploadFile",
        component : UploadFileComponent
      },
      {
        path : "demoAddCar",
        component : DemoAddCarComponent
      },
      {
        path : "addUserTopListPlan",
        component : UserTopListPlanComponent
      },
      {
        path : "allUserTopListPlan",
        component : AllUserTopListPlanComponent
      },
      {
        path : "editUserTopListPlan/:id",
        component : EditUserTopListPlanComponent
      },
      {
        path : "addAccessoriesParticular",
        component : AddAccessoriesParticularComponent
      },
      {
        path : "allAccessoriesParticular",
        component : AllAccessoriesParticularComponent
      },
      {
        path : "editAccessoriesParticular/:id",
        component : EditAccessoriesParticularComponent
      },
      {
        path : "topUrgentParticularAccessories",
        component : TopUrgentParticularAccessoriesComponent
      },
      {
        path : "addProUserTopSearch",
        component : ProUserTopSearchComponent
      },
      {
        path : "allProUserTopSearch",
        component : ProUserTopSearchAllComponent
      },
      {
        path : "editProUserTopSearch/:id",
        component : ProUserTopSearchEditComponent
      },
      {
        path : "proUserAccessAdPlan",
        component : ProUserAddAccessPlanComponent
      },
      {
        path : "allProUserAccessAdPlan",
        component : ProUserAllAccessPlanComponent
      },
      {
        path : "editProUserAccessAdPlan/:id",
        component : ProUserEditAccessPlanComponent
      },


      {
        path : "addProUserTopSearchAccess",
        component : ProUserTopSearchAccessComponent
      },
      {
        path : "allProUserAccessAdPlanAccess",
        component : ProUserTopSearchAllAccessComponent
      },
      {
        path : "editProUserAccessAdPlanAccess/:id",
        component : ProUserTopSearchEditAccessComponent
      },
      {
        path : "setting",
        component : SettingComponent
      },
      {
        path : "addUserFaq",
        component : AddUserFaqComponent
      },
      {
        path : "allUserFaq",
        component : AllUserFaqComponent
      },
      {
        path : "editUserFaq/:id",
        component : EditUserFaqComponent
      },

      {
        path : "stripeKey",
        component : StripeKeyComponent
      },
      {
        path : "paypalKey",
        component : PaypalKeyComponent
      },
      {
        path : "accessoriesOrdersProcess",
        component : AccessoriesOrdersProcessComponent
      },
      {
        path : "accessoriesOrdersCancel",
        component : AccessoriesOrdersCancelComponent
      },
      {
        path : "accessoriesOrdersComplete",
        component : AccessoriesOrdersCompleteComponent
      },
      {
        path : "accessoriesInActiveProUserAd",
        component : AccessoriesInActiveProUserAdComponent
      },
      {
        path : "userBankDetail/:id",
        component : UserBankDetailComponent
      },
      {
        path : "userInvoices/:id",
        component : UserInvoicesComponent
      },
      {
        path : "addOptions",
        component : AddOptionsComponent
      },
      {
        path : "allOptions",
        component : AllOptionsComponent
      },
      {
        path : "editOptions/:id",
        component : EditOptionsComponent
      },
      {
        path : "manageTitle",
        component : ManageTitleComponent
      },
      {
        path : "addVersions",
        component : AddVersionComponent
      },
      {
        path : "allVersions",
        component : AllVersionComponent
      },
      {
        path : "editVersions/:id",
        component : EditVersionComponent
      },
      {
        path : "addInvoice/:id",
        component : AddInvoiceComponent
      },
      {
        path : "allInvoice/:id",
        component : AllInvoiceComponent
      },
      {
        path : "addCountry",
        component : AddCountryComponent
      },
      {
        path : "allCountry",
        component : AllCountryComponent
      },
      {
        path : "editCountry/:id",
        component : EditCountryComponent
      },
      {
        path : "addTopUrgent",
        component : AddTopUrgentComponent
      },
      {
        path : "allTopUrgent",
        component : AllTopUrgentComponent
      },
      {
        path : "editTopUrgent/:id",
        component : EditTopUrgentComponent
      },
      {
        path : "allPaymentRequest",
        component : AllPaymentRequestComponent
      },
      {
        path : "allPendingPaymentRequest",
        component : AllPendingPaymentRequestComponent
      },
      {
        path : "allPaidPaymentRequest",
        component : AllPaidPaymentRequestComponent
      },
      {
        path : "allRejectedPaymentRequest",
        component : AllRejectedPaymentRequestComponent
      }
      
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
