import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarActiveProUserAdComponent } from './car-active-pro-user-ad.component';

describe('CarActiveProUserAdComponent', () => {
  let component: CarActiveProUserAdComponent;
  let fixture: ComponentFixture<CarActiveProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarActiveProUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarActiveProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
