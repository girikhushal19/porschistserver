import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllTopUrgentComponent } from './all-top-urgent.component';

describe('AllTopUrgentComponent', () => {
  let component: AllTopUrgentComponent;
  let fixture: ComponentFixture<AllTopUrgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllTopUrgentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllTopUrgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
