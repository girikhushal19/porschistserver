import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTopUrgentComponent } from './edit-top-urgent.component';

describe('EditTopUrgentComponent', () => {
  let component: EditTopUrgentComponent;
  let fixture: ComponentFixture<EditTopUrgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTopUrgentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditTopUrgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
