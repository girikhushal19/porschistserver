import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-pro-user-top-search-all',
  templateUrl: './pro-user-top-search-all.component.html',
  styleUrls: ['./pro-user-top-search-all.component.css']
})
export class ProUserTopSearchAllComponent implements OnInit {


  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allPlanProUserTopSearch:any;queryParam:any;numbers:any;allPlanProUserTopSearchCount:any;apiStringify:any;deletePushNotification:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allPlanProUserTopSearchCount = this.base_url_node+"allPlanProUserTopSearchCount";
    this.allPlanProUserTopSearch = this.base_url_node+"allPlanProUserTopSearch";
    //this.deletePushNotification = this.base_url_node+"deletePushNotification";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this.queryParam = {plan_type: 'car_plan'};
    this._http.post(this.allPlanProUserTopSearchCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allPlanProUserTopSearchCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage,plan_type: 'car_plan'};
    this._http.post(this.allPlanProUserTopSearch,this.queryParam).subscribe((response:any)=>{
      //console.log("allPlanProUserTopSearch"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }

}
