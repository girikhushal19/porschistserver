import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchAllComponent } from './pro-user-top-search-all.component';

describe('ProUserTopSearchAllComponent', () => {
  let component: ProUserTopSearchAllComponent;
  let fixture: ComponentFixture<ProUserTopSearchAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchAllComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
