import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccessoriesParticularComponent } from './add-accessories-particular.component';

describe('AddAccessoriesParticularComponent', () => {
  let component: AddAccessoriesParticularComponent;
  let fixture: ComponentFixture<AddAccessoriesParticularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAccessoriesParticularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddAccessoriesParticularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
