import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditColorExtNameComponent } from './edit-color-ext-name.component';

describe('EditColorExtNameComponent', () => {
  let component: EditColorExtNameComponent;
  let fixture: ComponentFixture<EditColorExtNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditColorExtNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditColorExtNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
