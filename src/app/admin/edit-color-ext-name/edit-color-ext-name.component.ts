import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-edit-color-ext-name',
  templateUrl: './edit-color-ext-name.component.html',
  styleUrls: ['./edit-color-ext-name.component.css']
})
export class EditColorExtNameComponent implements OnInit {



  base_url = "";base_url_node = ""; record:any;
  token:any;user_type:any;apiResponse:any;formValue:any;addColorsSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;
  allColorExt:any; edit_id:string="";old_color_name:string="";old_color_ext:string="";
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addColorsSubmit = this.base_url_node+"editColorsExtSubmit";
     
    //console.log("here");
    this._http.get(this.base_url_node+"getColor").subscribe((response:any)=>{
      console.log(response);
      if(response.error == false)
      {
        this.allColorExt = response.record;
      }
    })
    
    this.edit_id = this.actRoute.snapshot.params['id'];
    let getqueryParam = {"id":this.edit_id};
    this._http.post(this.base_url_node+"getSingleColorExt",getqueryParam).subscribe((response:any)=>{
      //console.log(JSON.stringify(response));
      this.record = response.record;
      ///console.log(this.record);
      this.old_color_name = this.record[0].color_name;
      this.old_color_ext = this.record[0].color_ext;
    });

  }


  ngOnInit(): void {
  }



  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    color_ext: new UntypedFormControl('', [Validators.required]), 
    color_name: new UntypedFormControl('', [Validators.required]), 
    color_code: new UntypedFormControl('', []), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addColorsSubmit,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
              //window.location.href = this.base_url+"allNormalUsers";
              window.history.back();
            }, 2000); 
            
          }
      });
      
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
