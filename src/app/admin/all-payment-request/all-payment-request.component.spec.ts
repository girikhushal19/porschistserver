import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPaymentRequestComponent } from './all-payment-request.component';

describe('AllPaymentRequestComponent', () => {
  let component: AllPaymentRequestComponent;
  let fixture: ComponentFixture<AllPaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllPaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
