import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserAddAccessPlanComponent } from './pro-user-add-access-plan.component';

describe('ProUserAddAccessPlanComponent', () => {
  let component: ProUserAddAccessPlanComponent;
  let fixture: ComponentFixture<ProUserAddAccessPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserAddAccessPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserAddAccessPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
