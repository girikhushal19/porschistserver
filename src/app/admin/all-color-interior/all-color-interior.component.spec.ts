import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllColorInteriorComponent } from './all-color-interior.component';

describe('AllColorInteriorComponent', () => {
  let component: AllColorInteriorComponent;
  let fixture: ComponentFixture<AllColorInteriorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllColorInteriorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllColorInteriorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
