import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesOrdersComponent } from './accessories-orders.component';

describe('AccessoriesOrdersComponent', () => {
  let component: AccessoriesOrdersComponent;
  let fixture: ComponentFixture<AccessoriesOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesOrdersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
