import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pro-user-top-search-edit-access',
  templateUrl: './pro-user-top-search-edit-access.component.html',
  styleUrls: ['./pro-user-top-search-edit-access.component.css']
})
export class ProUserTopSearchEditAccessComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addPlanProUserTopSearch:any;edit_id:any;webProUserSingleTopSearch:any;queryParam:any;record:any;webProUserSingleTopSearchRecord:any;
  old_top_price:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.addPlanProUserTopSearch = this.base_url_node+"editPlanProUserTopSearch";
    this.webProUserSingleTopSearch = this.base_url_node+"webProUserSingleTopSearch"; 
    //console.log("here");
     
  }

  ngOnInit(): void {
    this.queryParam = {id:this.edit_id};
    this._http.post(this.webProUserSingleTopSearch,this.queryParam).subscribe((response:any)=>{
      //console.log("webProUserSingleTopSearch"+response);
      //this.apiResponse = response;
      if(response.error == false)
      {
        this.old_top_price = response.record.top_price;
      }  
    });

  }

  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []),
    //day_number: new UntypedFormControl('', [Validators.required]), 
    //maximum_upload: new UntypedFormControl('', [Validators.required]), 
    //price: new UntypedFormControl('', [Validators.required]), 
    top_price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addPlanProUserTopSearch,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            //this.form.reset();
            setTimeout(() => {
              //window.location.href = this.base_url+"allNormalUsers";
              window.history.back();
            }, 2000);

          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
