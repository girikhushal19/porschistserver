import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserTopSearchEditAccessComponent } from './pro-user-top-search-edit-access.component';

describe('ProUserTopSearchEditAccessComponent', () => {
  let component: ProUserTopSearchEditAccessComponent;
  let fixture: ComponentFixture<ProUserTopSearchEditAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserTopSearchEditAccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserTopSearchEditAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
