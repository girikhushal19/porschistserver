import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPendingPaymentRequestComponent } from './all-pending-payment-request.component';

describe('AllPendingPaymentRequestComponent', () => {
  let component: AllPendingPaymentRequestComponent;
  let fixture: ComponentFixture<AllPendingPaymentRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPendingPaymentRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllPendingPaymentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
