import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarattributeComponent } from './carattribute.component';

describe('CarattributeComponent', () => {
  let component: CarattributeComponent;
  let fixture: ComponentFixture<CarattributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarattributeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarattributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
