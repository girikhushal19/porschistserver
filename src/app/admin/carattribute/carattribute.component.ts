import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-carattribute',
  templateUrl: './carattribute.component.html',
  styleUrls: ['./carattribute.component.css']
})
export class CarattributeComponent implements OnInit {


  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addCarAttributeSubmit:any;getCarAttribute:any;getCarCategory:any;getWhereCarAttribute:any;allCarAttribute:any;allCarCategory:any;queryParam:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addCarAttributeSubmit = this.base_url_node+"addCarAttributeSubmit";
    this.getCarAttribute = this.base_url_node+"getCarAttribute";
    this.getCarCategory = this.base_url_node+"getCarCategory";
    this.getWhereCarAttribute = this.base_url_node+"getWhereCarAttribute";
    //console.log("here");
    
  }


  ngOnInit(): void {
    this._http.post(this.getCarAttribute,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarAttribute"+response);
      this.allCarAttribute = response.result;
    });
    this._http.post(this.getCarCategory,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarCategory"+response);
      this.allCarCategory = response.result;
    });
  }

  onOptionsSelected(selectedValue:any)
  {
    console.log(selectedValue);
    this.queryParam = {"selectedValue":selectedValue};
    this._http.post(this.getWhereCarAttribute,this.queryParam).subscribe((response:any)=>{
      console.log("response get CarCategory"+response);
      this.allCarAttribute = response.result;
    });
  }


  form = new UntypedFormGroup({
    parent_id: new UntypedFormControl('', []),
    attribute_type: new UntypedFormControl('', [Validators.required]), 
    attribute_name: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.addCarAttributeSubmit,this.formValue).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            this.form.reset();
            this._http.post(this.getCarAttribute,this.formValue).subscribe((response:any)=>{
              console.log("response get model"+response);
              this.allCarAttribute = response.result;
            });
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
