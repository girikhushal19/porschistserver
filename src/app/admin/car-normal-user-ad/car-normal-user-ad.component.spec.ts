import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarNormalUserAdComponent } from './car-normal-user-ad.component';

describe('CarNormalUserAdComponent', () => {
  let component: CarNormalUserAdComponent;
  let fixture: ComponentFixture<CarNormalUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarNormalUserAdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarNormalUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
