import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesOrdersCompleteComponent } from './accessories-orders-complete.component';

describe('AccessoriesOrdersCompleteComponent', () => {
  let component: AccessoriesOrdersCompleteComponent;
  let fixture: ComponentFixture<AccessoriesOrdersCompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesOrdersCompleteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesOrdersCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
