import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarProUserAdComponent } from './car-pro-user-ad.component';

describe('CarProUserAdComponent', () => {
  let component: CarProUserAdComponent;
  let fixture: ComponentFixture<CarProUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarProUserAdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarProUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
