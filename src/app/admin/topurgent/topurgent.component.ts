import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-topurgent',
  templateUrl: './topurgent.component.html',
  styleUrls: ['./topurgent.component.css']
})
export class TopurgentComponent implements OnInit {



  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addTopUrgentPlan:any;allTopUrgentPlan:any;queryParam:any;record:any;old_record:any;old_price:any;old_id:any;old_day_number:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addTopUrgentPlan = this.base_url_node+"addTopUrgentPlan";
    this.allTopUrgentPlan = this.base_url_node+"allTopUrgentPlan"; 
    //console.log("here");
    
  }

  ngOnInit(): void {
    this._http.post(this.allTopUrgentPlan,this.queryParam).subscribe((response:any)=>{
      //console.log("allTopUrgentPlan"+response);
      //this.apiResponse = response;
      this.record = response.record;
      this.old_price = this.record[0].price;
      this.old_day_number = this.record[0].day_number;
      this.old_id = this.record[0]._id;
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
    });
  }

  form = new UntypedFormGroup({
    old_id: new UntypedFormControl('', []), 
    day_number: new UntypedFormControl('', [Validators.required]), 
    price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.addTopUrgentPlan,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
             
            this._http.post(this.allTopUrgentPlan,this.queryParam).subscribe((response:any)=>{
              //console.log("allTopUrgentPlan"+response);
              //this.apiResponse = response;
              this.record = response.record;   
              this.old_price = this.record[0].price;
              this.old_id = this.record[0]._id;
            });

          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
