import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopurgentComponent } from './topurgent.component';

describe('TopurgentComponent', () => {
  let component: TopurgentComponent;
  let fixture: ComponentFixture<TopurgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopurgentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopurgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
