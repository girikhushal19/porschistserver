import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEmailNotificationComponent } from './all-email-notification.component';

describe('AllEmailNotificationComponent', () => {
  let component: AllEmailNotificationComponent;
  let fixture: ComponentFixture<AllEmailNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllEmailNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllEmailNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
