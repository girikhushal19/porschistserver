import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-all-email-notification',
  templateUrl: './all-email-notification.component.html',
  styleUrls: ['./all-email-notification.component.css']
})
export class AllEmailNotificationComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allEmailNotification:any;queryParam:any;numbers:any;allEmailNotificationCount:any;apiStringify:any;deleteEmailNotification:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allEmailNotificationCount = this.base_url_node+"allEmailNotificationCount";
    this.allEmailNotification = this.base_url_node+"allEmailNotification";
    this.deleteEmailNotification = this.base_url_node+"deleteEmailNotification";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allEmailNotificationCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allEmailNotificationCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allEmailNotification,this.queryParam).subscribe((response:any)=>{
      //console.log("allEmailNotification"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  deleteRecord(id=null)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id};
    this._http.post(this.deleteEmailNotification,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
}
