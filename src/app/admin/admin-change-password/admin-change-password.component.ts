import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-admin-change-password',
  templateUrl: './admin-change-password.component.html',
  styleUrls: ['./admin-change-password.component.css']
})
export class AdminChangePasswordComponent implements OnInit {
   
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;adminChangePasswordSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getAdminProfile:any;getqueryParam:any;removeQueryParam:any;old_category:any;old_price:any;old_images:any;record:any;removeCategoryImage:any;old_email:any;old_oldPassword:any;old_newPassword:any;old_userImage:any;old_id:any;logged_in_user_id:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.adminChangePasswordSubmit = this.base_url_node+"adminChangePasswordSubmitApi";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    
    this.logged_in_user_id = localStorage.getItem("_id");

   }
   get f(){
    return this.form.controls;
  }
  ngOnInit(): void {
      this.myFiles = [];
  }

  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('', []),
    old_password: new UntypedFormControl('', [Validators.required]),
    new_password: new UntypedFormControl('', [Validators.required]),
  });
  
  

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   

   

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      
      //console.log(this.formData);
      this._http.post(this.adminChangePasswordSubmit,this.form.value).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.href = this.base_url+"logout";
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
