import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddColorInteriorComponent } from './add-color-interior.component';

describe('AddColorInteriorComponent', () => {
  let component: AddColorInteriorComponent;
  let fixture: ComponentFixture<AddColorInteriorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddColorInteriorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddColorInteriorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
