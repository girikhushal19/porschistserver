import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-permission-sub-admin',
  templateUrl: './permission-sub-admin.component.html',
  styleUrls: ['./permission-sub-admin.component.css']
})
export class PermissionSubAdminComponent implements OnInit {
  imageSrc: string = '';
  fileInputLabel: string = "";

  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;user_type:any;apiResponse:any;formValue:any;permissionSubAdminSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;user_module:any;checkbox_check:any;user_id:any;
  banner_module:any;
  category_module:any;
  model_module:any;
  sub_model_module:any;
  attribute_module:any;
  color_module:any;
  usercaradplan_module:any;
  prousercaradplan_module:any;
  cartopurgent_module:any;
  accessoriesUserAd_module:any;
  carUserAd_module:any;
  accessoriesOrders_module:any;
  pushNotification_module:any;
  emailNotification_module:any;
  page_module:any;
  getSubAdminSingle:any;
  getSubAdminAllPermission:any;

  queryParam:any;
  old_firstName:any;
  old_lastName:any;
  old_email:any;
  user_module_module_add:any;user_module_module_edit:any;user_module_module_view:any;user_module_module_delete:any;user_module_module_status:any;
  banner_module_module_add:any;banner_module_module_edit:any;banner_module_module_view:any;banner_module_module_delete:any;banner_module_module_status:any;
  category_module_module_view:any;category_module_module_edit:any;category_module_module_status:any;
  model_module_module_add:any;model_module_module_edit:any;model_module_module_status:any;
  model_module_module_view:any;
  sub_model_module_module_add:any;sub_model_module_module_edit:any;sub_model_module_module_status:any;
  sub_model_module_module_view:any;
  attribute_module_module_add:any;attribute_module_module_edit:any;attribute_module_module_status:any;
  attribute_module_module_view:any;
  color_module_module_add:any;color_module_module_edit:any;color_module_module_status:any;
  color_module_module_view:any;
  usercaradplan_module_module_add:any;usercaradplan_module_module_edit:any;usercaradplan_module_module_status:any;
  usercaradplan_module_module_view:any;

  prousercaradplan_module_module_add:any;prousercaradplan_module_module_edit:any;prousercaradplan_module_module_status:any;prousercaradplan_module_module_view:any;

  accessoriesUserAd_module_module_add:any;accessoriesUserAd_module_module_edit:any;accessoriesUserAd_module_module_status:any;accessoriesUserAd_module_module_view:any;

  carUserAd_module_module_add:any;carUserAd_module_module_edit:any;carUserAd_module_module_status:any;carUserAd_module_module_view:any;

  cartopurgent_module_module_edit:any;
  cartopurgent_module_module_view:any;

  accessoriesOrders_module_module_view:any;

  pushNotification_module_module_add:any;
  pushNotification_module_module_view:any;
  pushNotification_module_module_delete:any;

  emailNotification_module_module_add:any;
  emailNotification_module_module_view:any;
  emailNotification_module_module_delete:any;
  emailNotification_module_module_status:any;
  emailNotification_module_module_edit:any;

  page_module_module_add:any;
  page_module_module_view:any;
  page_module_module_edit:any;
  page_module_module_status:any;
  page_module_module_delete:any;

  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.permissionSubAdminSubmit = this.base_url_node+"permissionSubAdminSubmit";
    this.getSubAdminSingle = this.base_url_node+"getSubAdminSingle";
    this.getSubAdminAllPermission = this.base_url_node+"getSubAdminAllPermission";
    
    this.user_id = this.actRoute.snapshot.params['id']; 

    //console.log(this.user_id);
    this.user_module = "user_module";
    this.banner_module = "banner_module";
    this.category_module = "category_module";
    this.model_module = "model_module";
    this.sub_model_module = "sub_model_module";
    this.attribute_module = "attribute_module";
    this.color_module = "color_module";
    this.usercaradplan_module = "usercaradplan_module";
    this.prousercaradplan_module = "prousercaradplan_module";
    this.cartopurgent_module = "cartopurgent_module";
    this.accessoriesUserAd_module = "accessoriesUserAd_module";
    this.carUserAd_module = "carUserAd_module";
    this.accessoriesOrders_module = "accessoriesOrders_module";
    this.pushNotification_module = "pushNotification_module";
    this.emailNotification_module = "emailNotification_module";
    this.page_module = "page_module";

    this.checkbox_check = true;
  }


  ngOnInit(): void {
     this.queryParam = {"id":this.user_id};
     this._http.post(this.getSubAdminSingle,this.queryParam).subscribe((response:any)=>{
      /*console.log(response);
      console.log(response.record);
      console.log(response.record.firstName);
      console.log(response.record.lastName);
      console.log(response.record.email);
      old_firstName:any;
      old_lastName:any;
      old_email:any;*/
      if(response.error == false)
      {
        this.old_firstName = response.record.firstName;
        this.old_lastName = response.record.lastName;
        this.old_email = response.record.email;
      }
     });
     this.queryParam = {"id":this.user_id};
     this._http.post(this.getSubAdminAllPermission,this.queryParam).subscribe((response:any)=>{
        
        if(response.error == false)
        {
          console.log(response);
          console.log(response.record);
          for(let i=0; i<response.record.length; i++)
          {
            if(response.record[i].module_name == "user_module")
            {
              this.user_module_module_add = response.record[i].module_add;
              this.user_module_module_view = response.record[i].module_view;
              this.user_module_module_edit = response.record[i].module_edit;
              this.user_module_module_status = response.record[i].module_status;
              this.user_module_module_delete = response.record[i].module_delete;
            }
            if(response.record[i].module_name == "banner_module")
            {
              this.banner_module_module_add = response.record[i].module_add;
              this.banner_module_module_view = response.record[i].module_view;
              this.banner_module_module_edit = response.record[i].module_edit;
              this.banner_module_module_status = response.record[i].module_status;
              this.banner_module_module_delete = response.record[i].module_delete;
            }
            if(response.record[i].module_name == "category_module")
            {
              this.category_module_module_view = response.record[i].module_view;
              this.category_module_module_edit = response.record[i].module_edit;
              this.category_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "model_module")
            {
              this.model_module_module_add = response.record[i].module_add;
              this.model_module_module_view = response.record[i].module_view;
              this.model_module_module_edit = response.record[i].module_edit;
              this.model_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "sub_model_module")
            {
              this.sub_model_module_module_add = response.record[i].module_add;
              this.sub_model_module_module_view = response.record[i].module_view;
              this.sub_model_module_module_edit = response.record[i].module_edit;
              this.sub_model_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "attribute_module")
            {
              this.attribute_module_module_add = response.record[i].module_add;
              this.attribute_module_module_view = response.record[i].module_view;
              this.attribute_module_module_edit = response.record[i].module_edit;
              this.attribute_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "color_module")
            {
              this.color_module_module_add = response.record[i].module_add;
              this.color_module_module_view = response.record[i].module_view;
              this.color_module_module_edit = response.record[i].module_edit;
              this.color_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "usercaradplan_module")
            {
              this.usercaradplan_module_module_add = response.record[i].module_add;
              this.usercaradplan_module_module_view = response.record[i].module_view;
              this.usercaradplan_module_module_edit = response.record[i].module_edit;
              this.usercaradplan_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "prousercaradplan_module")
            {
              this.prousercaradplan_module_module_add = response.record[i].module_add;
              this.prousercaradplan_module_module_view = response.record[i].module_view;
              this.prousercaradplan_module_module_edit = response.record[i].module_edit;
              this.prousercaradplan_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "accessoriesUserAd_module")
            {
              this.accessoriesUserAd_module_module_add = response.record[i].module_add;
              this.accessoriesUserAd_module_module_view = response.record[i].module_view;
              this.accessoriesUserAd_module_module_edit = response.record[i].module_edit;
              this.accessoriesUserAd_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "carUserAd_module")
            {
              this.carUserAd_module_module_add = response.record[i].module_add;
              this.carUserAd_module_module_view = response.record[i].module_view;
              this.carUserAd_module_module_edit = response.record[i].module_edit;
              this.carUserAd_module_module_status = response.record[i].module_status;
            }
            if(response.record[i].module_name == "cartopurgent_module")
            {
              this.cartopurgent_module_module_view = response.record[i].module_view;
              this.cartopurgent_module_module_edit = response.record[i].module_edit;
            }
            if(response.record[i].module_name == "accessoriesOrders_module")
            {
              this.accessoriesOrders_module_module_view = response.record[i].module_view;
            }
            if(response.record[i].module_name == "pushNotification_module")
            {
              this.pushNotification_module_module_add = response.record[i].module_add;
              this.pushNotification_module_module_view = response.record[i].module_view;
              this.pushNotification_module_module_delete = response.record[i].module_delete;
            }
            if(response.record[i].module_name == "emailNotification_module")
            {
              this.emailNotification_module_module_add = response.record[i].module_add;
              this.emailNotification_module_module_view = response.record[i].module_view;
              this.emailNotification_module_module_delete = response.record[i].module_delete;
            }
            if(response.record[i].module_name == "page_module")
            {
              this.page_module_module_add = response.record[i].module_add;
              this.page_module_module_view = response.record[i].module_view;
              this.page_module_module_delete = response.record[i].module_delete;
              this.page_module_module_edit = response.record[i].module_edit;
              this.page_module_module_status = response.record[i].module_status;
            }
          }
          

          

        }
     });
  }

  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('', []),
    firstName: new UntypedFormControl('', []),
    lastName: new UntypedFormControl('', []),
    email: new UntypedFormControl('', []),
    user_module: new UntypedFormControl('', []),
    user_add: new UntypedFormControl('', []),
    user_edit: new UntypedFormControl('', []),
    user_view: new UntypedFormControl('', []),
    user_delete: new UntypedFormControl('', []),
    user_status: new UntypedFormControl('', []),
    
    banner_module: new UntypedFormControl('', []),
    banner_add: new UntypedFormControl('', []),
    banner_edit: new UntypedFormControl('', []),
    banner_view: new UntypedFormControl('', []),
    banner_delete: new UntypedFormControl('', []),
    banner_status: new UntypedFormControl('', []),

    category_module: new UntypedFormControl('', []),
    category_view: new UntypedFormControl('', []),
    category_edit: new UntypedFormControl('', []),
    category_status: new UntypedFormControl('', []),

    model_module: new UntypedFormControl('', []),
    model_add: new UntypedFormControl('', []),
    model_view: new UntypedFormControl('', []),
    model_edit: new UntypedFormControl('', []),
    model_status: new UntypedFormControl('', []),

    sub_model_module: new UntypedFormControl('', []),
    sub_model_add: new UntypedFormControl('', []),
    sub_model_view: new UntypedFormControl('', []),
    sub_model_edit: new UntypedFormControl('', []),
    sub_model_status: new UntypedFormControl('', []),

    attribute_module: new UntypedFormControl('', []),
    attribute_add: new UntypedFormControl('', []),
    attribute_view: new UntypedFormControl('', []),
    attribute_edit: new UntypedFormControl('', []),
    attribute_status: new UntypedFormControl('', []),

    color_module: new UntypedFormControl('', []),
    color_add: new UntypedFormControl('', []),
    color_view: new UntypedFormControl('', []),
    color_edit: new UntypedFormControl('', []),
    color_status: new UntypedFormControl('', []),

    usercaradplan_module: new UntypedFormControl('', []),
    usercaradplan_add: new UntypedFormControl('', []),
    usercaradplan_view: new UntypedFormControl('', []),
    usercaradplan_edit: new UntypedFormControl('', []),
    usercaradplan_status: new UntypedFormControl('', []),

    prousercaradplan_module: new UntypedFormControl('', []),
    prousercaradplan_add: new UntypedFormControl('', []),
    prousercaradplan_view: new UntypedFormControl('', []),
    prousercaradplan_edit: new UntypedFormControl('', []),
    prousercaradplan_status: new UntypedFormControl('', []),

    cartopurgent_module: new UntypedFormControl('', []),
    cartopurgent_add: new UntypedFormControl('', []),
    cartopurgent_view: new UntypedFormControl('', []),
    cartopurgent_edit: new UntypedFormControl('', []),
    cartopurgent_status: new UntypedFormControl('', []),

    accessoriesUserAd_module: new UntypedFormControl('', []),
    accessoriesUserAd_add: new UntypedFormControl('', []),
    accessoriesUserAd_view: new UntypedFormControl('', []),
    accessoriesUserAd_edit: new UntypedFormControl('', []),
    accessoriesUserAd_status: new UntypedFormControl('', []),

    carUserAd_module: new UntypedFormControl('', []),
    carUserAd_add: new UntypedFormControl('', []),
    carUserAd_view: new UntypedFormControl('', []),
    carUserAd_edit: new UntypedFormControl('', []),
    carUserAd_status: new UntypedFormControl('', []),

    accessoriesOrders_module: new UntypedFormControl('', []),
    accessoriesOrders_add: new UntypedFormControl('', []),
    accessoriesOrders_view: new UntypedFormControl('', []),
    accessoriesOrders_edit: new UntypedFormControl('', []),
    accessoriesOrders_status: new UntypedFormControl('', []),

    pushNotification_module: new UntypedFormControl('', []),
    pushNotification_add: new UntypedFormControl('', []),
    pushNotification_view: new UntypedFormControl('', []),
    pushNotification_delete: new UntypedFormControl('', []),
    pushNotification_status: new UntypedFormControl('', []),

    emailNotification_module: new UntypedFormControl('', []),
    emailNotification_add: new UntypedFormControl('', []),
    emailNotification_view: new UntypedFormControl('', []),
    emailNotification_delete: new UntypedFormControl('', []),
    emailNotification_status: new UntypedFormControl('', []),


    page_module: new UntypedFormControl('', []),
    page_add: new UntypedFormControl('', []),
    page_edit: new UntypedFormControl('', []),
    page_view: new UntypedFormControl('', []),
    page_delete: new UntypedFormControl('', []),
    page_status: new UntypedFormControl('', []),

  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      console.log(this.form.value);
      this._http.post(this.permissionSubAdminSubmit,this.form.value).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            //this.form.reset();
              window.location.reload();
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
