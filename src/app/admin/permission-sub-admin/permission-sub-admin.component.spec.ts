import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionSubAdminComponent } from './permission-sub-admin.component';

describe('PermissionSubAdminComponent', () => {
  let component: PermissionSubAdminComponent;
  let fixture: ComponentFixture<PermissionSubAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermissionSubAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PermissionSubAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
