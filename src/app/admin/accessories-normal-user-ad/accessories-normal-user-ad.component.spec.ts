import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesNormalUserAdComponent } from './accessories-normal-user-ad.component';

describe('AccessoriesNormalUserAdComponent', () => {
  let component: AccessoriesNormalUserAdComponent;
  let fixture: ComponentFixture<AccessoriesNormalUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesNormalUserAdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessoriesNormalUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
