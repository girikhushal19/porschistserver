import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserFaqComponent } from './edit-user-faq.component';

describe('EditUserFaqComponent', () => {
  let component: EditUserFaqComponent;
  let fixture: ComponentFixture<EditUserFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUserFaqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUserFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
