import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-user-faq',
  templateUrl: './edit-user-faq.component.html',
  styleUrls: ['./edit-user-faq.component.css']
})
export class EditUserFaqComponent implements OnInit {
  edit_id: string;
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addFaqDriverSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  getFaqUserSingle:any;getqueryParam:any;record:any;old_question:any;old_answer:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.edit_id = this.actRoute.snapshot.params['id'];
    console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};
    this.getFaqUserSingle = this.base_url_node+"getFaqUserSingle"; 

    this._http.post(this.getFaqUserSingle,this.getqueryParam).subscribe((response:any)=>{

      this.record = response.record;
      if(response.record)
      {
        this.old_question = this.record.question;
        this.old_answer = this.record.answer;
      }
      
    });

    this.addFaqDriverSubmit = this.base_url_node+"editFaqUserSubmit";
    
  }
  ngOnInit(): void {
       
  }

  form = new UntypedFormGroup({
    question: new UntypedFormControl('', [Validators.required]),
    answer: new UntypedFormControl('', [Validators.required]),
    id: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   
  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      //console.log(this.formData);
      this._http.post(this.addFaqDriverSubmit,this.form.value).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            window.history.back();
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
