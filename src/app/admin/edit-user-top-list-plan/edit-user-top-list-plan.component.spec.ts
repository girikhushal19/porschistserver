import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserTopListPlanComponent } from './edit-user-top-list-plan.component';

describe('EditUserTopListPlanComponent', () => {
  let component: EditUserTopListPlanComponent;
  let fixture: ComponentFixture<EditUserTopListPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUserTopListPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUserTopListPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
