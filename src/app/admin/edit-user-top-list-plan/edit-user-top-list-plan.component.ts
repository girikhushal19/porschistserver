import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-user-top-list-plan',
  templateUrl: './edit-user-top-list-plan.component.html',
  styleUrls: ['./edit-user-top-list-plan.component.css']
})
export class EditUserTopListPlanComponent implements OnInit {
  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;editPlanNormalUserTopList:any;getSinglePlanNormalUserTopList:any;queryParam:any;record:any;edit_id:any;
  old_day_number:any;old_price:any;
  old_car:string="Car";
  old_access:string="Accessoires";
  old_plan_type:any;
  //old_plan_day_type:number=1;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.edit_id = this.actRoute.snapshot.params['id'];


    this.editPlanNormalUserTopList = this.base_url_node+"editPlanNormalUserTopList";
    this.getSinglePlanNormalUserTopList = this.base_url_node+"getSinglePlanNormalUserTopList"; 
    //console.log("here");
    this.queryParam = {"edit_id":this.edit_id}
    this._http.post(this.getSinglePlanNormalUserTopList,this.queryParam).subscribe((response:any)=>{
      //console.log("getSinglePlanNormalUserTopList"+response);
      //this.apiResponse = response;
      this.record = response.record; 
      this.old_day_number = this.record[0].day_number;
      this.old_price = this.record[0].price;
      this.old_plan_type = this.record[0].plan_type;
      //this.old_plan_day_type = this.record[0].plan_day_type;
    });

  }

  ngOnInit(): void {
    

  }

  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    //plan_day_type: new UntypedFormControl('', [Validators.required]), 
    plan_type: new UntypedFormControl('', [Validators.required]), 
    day_number: new UntypedFormControl('', [Validators.required]), 
    price: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      this._http.post(this.editPlanNormalUserTopList,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                //window.location.href = this.base_url+"allNormalUsers";
                window.history.back();
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
