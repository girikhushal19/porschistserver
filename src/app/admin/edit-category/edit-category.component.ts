import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {
  edit_id: string;
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;base_url_node_plain:any;video:any;user_type:any;apiResponse:any;formValue:any;editCategorySubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSingleCategory:any;getqueryParam:any;removeQueryParam:any;old_category:any;old_price:any;old_images:any;record:any;removeCategoryImage:any;old_first_heading:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    
    this.base_url_node_plain = this.loginAuthObj.base_url_node;
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editCategorySubmit = this.base_url_node+"editCategorySubmit";
    this.removeCategoryImage = this.base_url_node+"removeCategoryImageApi";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");




    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};
    this.getSingleCategory = this.base_url_node+"getSingleCategory"; 
    this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getSingleCategory"+response);
    //this.apiResponse = response;
      this.record = response.record;

      /*console.log(this.record); 
      console.log(this.record[0]);*/
      
      this.old_first_heading = this.record[0].first_heading;
      this.old_category = this.record[0].category;
      //this.old_price = this.record[0].price;
      this.old_images = this.record[0].images;
      /*console.log(this.record[0]._id); 
      console.log(this.record[0].price); */
      //console.log(this.old_category); 
    });

   }

  ngOnInit(): void {
      this.myFiles = [];this.video = null;
  }

  form = new UntypedFormGroup({
    //category: new UntypedFormControl('', [Validators.required]),
    images: new UntypedFormControl('', []),
    first_heading: new UntypedFormControl('',  [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  onVideoChange(event:any) {
    
      if (event.target.files.length > 0)
      {
        const video_file = event.target.files[0];
        this.video = video_file;
        //console.log(file);
         
      }
  }

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
      //this.formData.append('video', this.video);
        //this.formData.append('category', this.form.value.category);
        this.formData.append('first_heading', this.form.value.first_heading); 
        this.formData.append('edit_id', this.record[0]._id);
      //this.formData.append('file', this.images);
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        }
        //console.log(this.video);
        
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      
      //console.log("form valuess");
      //console.log(this.formData);
      
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      

      this._http.post(this.editCategorySubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.href = this.base_url+"allCategory";
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  removeCategory(id:string,filename:string,path:string)
  {
    console.log(id);
    console.log(filename);
    console.log(path);
    this.removeQueryParam = { "id":id,"filename":filename,"path":path };
    this._http.post(this.removeCategoryImage,this.removeQueryParam).subscribe((response:any)=>{
      console.log(response);
      this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleCategory"+response);
      //this.apiResponse = response;
        this.record = response.record;

        /*console.log(this.record); 
        console.log(this.record[0]); */
   
        this.old_category = this.record[0].category;
        //this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });
    });
  }

}
