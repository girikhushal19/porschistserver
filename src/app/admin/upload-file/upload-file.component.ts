import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {


  //base_url = "http://localhost:4200/";
  token:any;user_type:any;base_url:any;status:number;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    /*console.log("here"+this.token);
    console.log(this.user_type);*/
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url+"admin";
    }
    this.status = 0;
  }

  ngOnInit(): void {
  }


  files: File[] = [];
  
     event:any;
  
    onSelect(event:any) {
        console.log("onselect");
        console.log(this.files)
        this.files.push(...event.addedFiles);

        const formData = new FormData();
    
        for (var i = 0; i < this.files.length; i++) { 
          formData.append("file[]", this.files[i]);
        }
   
        /*this._http.post('http://localhost:8001/upload.php', formData)
        .subscribe(res => {
           console.log(res);
           alert('Uploaded Successfully.');
        })*/
    }
    

    onFilesAdded(event:any) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }

  onFilesRejected(event:any) {
    console.log(event);
  }

  
    // onRemove(statusss:any) {
    //     console.log("hereeeeee");
    //     console.log(statusss);
    //     statusss = 2;
    //     console.log(this.files);
    //     console.log(statusss);
    //     this.files.splice(this.files.indexOf(statusss), 1);
    // }
    onRemove(event:any) {
      console.log(event);
      this.files.splice(this.files.indexOf(event), 1);
    }
}
