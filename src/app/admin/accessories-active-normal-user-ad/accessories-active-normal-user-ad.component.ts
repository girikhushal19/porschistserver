import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-accessories-active-normal-user-ad',
  templateUrl: './accessories-active-normal-user-ad.component.html',
  styleUrls: ['./accessories-active-normal-user-ad.component.css']
})
export class AccessoriesActiveNormalUserAdComponent implements OnInit {

    dtOptions: DataTables.Settings = {};

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allAccessoriesActiveNormalUser:any;queryParam:any;numbers:any;allAccessoriesActiveNormalUserCount:any;apiStringify:any;updateUserStatusApi:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allAccessoriesActiveNormalUserCount = this.base_url_node+"allAccessoriesActiveNormalUserCount";
    this.allAccessoriesActiveNormalUser = this.base_url_node+"allAccessoriesActiveNormalUser";
    this.updateUserStatusApi = this.base_url_node+"updateAdStatusApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allAccessoriesActiveNormalUserCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveNormalUserCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allAccessoriesActiveNormalUser,this.queryParam).subscribe((response:any)=>{
      //console.log("allAccessoriesActiveNormalUser"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateUserStatus(id=null,status:number)
  {
     
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateUserStatusApi,this.queryParam).subscribe((response:any)=>{ 
      console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
}
