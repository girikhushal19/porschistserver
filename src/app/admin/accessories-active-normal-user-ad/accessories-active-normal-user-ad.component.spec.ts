import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesActiveNormalUserAdComponent } from './accessories-active-normal-user-ad.component';

describe('AccessoriesActiveNormalUserAdComponent', () => {
  let component: AccessoriesActiveNormalUserAdComponent;
  let fixture: ComponentFixture<AccessoriesActiveNormalUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesActiveNormalUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesActiveNormalUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
