import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProUserAllAccessPlanComponent } from './pro-user-all-access-plan.component';

describe('ProUserAllAccessPlanComponent', () => {
  let component: ProUserAllAccessPlanComponent;
  let fixture: ComponentFixture<ProUserAllAccessPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProUserAllAccessPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProUserAllAccessPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
