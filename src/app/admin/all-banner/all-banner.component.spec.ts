import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllBannerComponent } from './all-banner.component';

describe('AllBannerComponent', () => {
  let component: AllBannerComponent;
  let fixture: ComponentFixture<AllBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
