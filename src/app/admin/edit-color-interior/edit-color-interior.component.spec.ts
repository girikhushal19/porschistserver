import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditColorInteriorComponent } from './edit-color-interior.component';

describe('EditColorInteriorComponent', () => {
  let component: EditColorInteriorComponent;
  let fixture: ComponentFixture<EditColorInteriorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditColorInteriorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditColorInteriorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
