import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
@Component({
  selector: 'app-adminlogout',
  templateUrl: './adminlogout.component.html',
  styleUrls: ['./adminlogout.component.css']
})
export class AdminlogoutComponent implements OnInit {
  base_url:any;
  constructor(private loginAuthObj:LoginauthenticationService) { 
    this.base_url = this.loginAuthObj.base_url;
  }

  ngOnInit(): void {
    console.log("call logout function");
    localStorage.clear();
    window.location.href = this.base_url;
  }

}
