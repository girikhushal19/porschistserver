import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTopListPlanComponent } from './user-top-list-plan.component';

describe('UserTopListPlanComponent', () => {
  let component: UserTopListPlanComponent;
  let fixture: ComponentFixture<UserTopListPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTopListPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserTopListPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
