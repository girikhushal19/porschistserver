import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-submodel',
  templateUrl: './submodel.component.html',
  styleUrls: ['./submodel.component.css']
})
export class SubmodelComponent implements OnInit {


  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allSubModel:any;queryParam:any;numbers:any;allSubModelCount:any;apiStringify:any;updateSubModelStatus:any;queryParamNew:any;numofpage_0:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.updateSubModelStatus = this.base_url_node+"updateSubModelStatus";
    this.allSubModelCount = this.base_url_node+"subModelTotalCount";
    this.allSubModel = this.base_url_node+"allSubModel";
    this.getallSubModel(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allSubModelCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allSubModelCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers "+this.numbers);
    });
  }

  getallSubModel(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allSubModel,this.queryParam).subscribe((response:any)=>{
      //console.log("allSubModel"+response);
      //this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  updateSubModalStatus(id=null,status:number)
  {
    /*console.log("hii");
    console.log(id);
    console.log(status);*/
    this.queryParamNew = {"id":id,"status":status};
    this._http.post(this.updateSubModelStatus,this.queryParamNew).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.numofpage_0 = 0;
        this.queryParam = {"numofpage":this.numofpage_0};
        //console.log(this.queryParam);
        this._http.post(this.allSubModel,this.queryParam).subscribe((response:any)=>{
          //console.log("allModel"+JSON.stringify(response));
          this.record = response.record;
          
          //console.log("totalPageNumber"+this.totalPageNumber);
          //console.log(this.numbers);
        });
      }
    });
  }


  form = new UntypedFormGroup({
    model_name: new UntypedFormControl('', []),
    sub_model_name: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    this._http.post(this.allSubModelCount,this.form.value).subscribe((response:any)=>{
      //console.log("allSubModelCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers "+this.numbers);
    });
    //console.log(this.form.value);
    this._http.post(this.allSubModel,this.form.value).subscribe((response:any)=>{
      //console.log("allUsers"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }


}
