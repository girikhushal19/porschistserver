import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  imageSrc: string = '';edit_id: string;
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editUserSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSingleUser:any;getqueryParam:any;old_city:any;old_description:any;old_images:any;record:any;old_email:any;old_firstName:any;old_lastName:any;old_mobileNumber:any;old_id:any;old_userImage:any;base_url_node_only:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editUserSubmit = this.base_url_node+"editUserSubmit";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCategory = this.base_url_node+"getAdminCategory";
    //console.log("here");

    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};
    this.getSingleUser = this.base_url_node+"getSingleUserApi"; 
    this._http.post(this.getSingleUser,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getSingleUser"+response);
    //this.apiResponse = response;
      this.record = response.record;

      //console.log(this.record); 
      //console.log(this.record);
      this.old_email = this.record.email;
      this.old_firstName = this.record.firstName;
      this.old_lastName = this.record.lastName; 
      this.old_mobileNumber = this.record.mobileNumber; 
      this.old_id = this.record._id; 
      this.old_userImage = this.record.userImage; 

      /*console.log(this.record._id); 
      console.log(this.record.price); */
      //console.log(this.old_category); 
    });
  }
  ngOnInit(): void {
      this.myFiles = [];
  }

  form = new UntypedFormGroup({
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    images: new UntypedFormControl('', []),
    email: new UntypedFormControl('', [Validators.required]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    //fileSource: new FormControl('', [Validators.required])
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
 

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData(); 
        this.formData.append('firstName', this.form.value.firstName);
        this.formData.append('lastName', this.form.value.lastName);
        this.formData.append('email', this.form.value.email);
        this.formData.append('mobileNumber', this.form.value.mobileNumber);
        this.formData.append('id', this.edit_id);
      //this.formData.append('file', this.images);

        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        } 
        
      console.log(this.formData);
      this._http.post(this.editUserSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
              //window.location.href = this.base_url+"allNormalUsers";
              window.history.back();
          }, 2000); 
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
