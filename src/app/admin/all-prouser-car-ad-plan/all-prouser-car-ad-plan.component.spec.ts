import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProuserCarAdPlanComponent } from './all-prouser-car-ad-plan.component';

describe('AllProuserCarAdPlanComponent', () => {
  let component: AllProuserCarAdPlanComponent;
  let fixture: ComponentFixture<AllProuserCarAdPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllProuserCarAdPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllProuserCarAdPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
