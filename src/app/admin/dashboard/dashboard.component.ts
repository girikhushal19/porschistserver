import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  //base_url = "http://localhost:4200/";
   token:any;user_type:any;base_url:any;allDonationCount:any;allForumCount:any;
  allUsersCount:any;base_url_node:any;base_url_node_plain:any;
  queryParam:any;allUserCount:any;allActiveUserCount:any;allInActiveUserCount:any;allDonation:any;
  allInactiveForum:any;allForum:any;allActiveForum:any;allLikeDislikeCount:any;allRecentDonation:any;allRecentForum:any;allForumRecord:any;allDonationRecord:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //console.log("here"+this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type === "" )
    {
      window.location.href = this.base_url;
    }
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.allUsersCount = this.base_url_node+"allUsersCountDashboard";
  }

  ngOnInit(): void {
    this._http.post(this.allUsersCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allUsersCount"+response);
      if(response.error == false)
      {
        this.allUserCount = response.record.allUserCount;
        this.allActiveUserCount = response.record.allActiveUserCount;
        this.allInActiveUserCount = response.record.allInActiveUserCount;
      }
      /*console.log(response.record);
      console.log(response.record.allUserCount);
      console.log(this.allUserCount); */
    });

  }


  files: File[] = [];
  
     event:any;
  
    onSelect(event:any) {
        console.log(this.event);
        this.files.push(...event.addedFiles);
  
        const formData = new FormData();
    
        for (var i = 0; i < this.files.length; i++) { 
          formData.append("file[]", this.files[i]);
        }
   
        /*this._http.post('http://localhost:8001/upload.php', formData)
        .subscribe(res => {
           console.log(res);
           alert('Uploaded Successfully.');
        })*/
    }
    

    onFilesAdded(event:any) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }

  onFilesRejected(event:any) {
    console.log(event);
  }

  
    onRemove(event:any) {
        console.log(this.event);
        this.files.splice(this.files.indexOf(this.event), 1);
    }



}
