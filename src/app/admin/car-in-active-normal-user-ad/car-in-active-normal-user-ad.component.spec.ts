import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarInActiveNormalUserAdComponent } from './car-in-active-normal-user-ad.component';

describe('CarInActiveNormalUserAdComponent', () => {
  let component: CarInActiveNormalUserAdComponent;
  let fixture: ComponentFixture<CarInActiveNormalUserAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarInActiveNormalUserAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarInActiveNormalUserAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
