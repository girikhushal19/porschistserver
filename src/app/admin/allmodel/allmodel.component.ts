import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-allmodel',
  templateUrl: './allmodel.component.html',
  styleUrls: ['./allmodel.component.css']
})
export class AllmodelComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allModel:any;queryParam:any;numbers:any;allModelCount:any;base_url_node_only:any;
  selectedIndex: number;updateModelStatusApi:any;queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
     
    //this.getModel = this.base_url_node+"getModel";
    //console.log("here");
    

    this.updateModelStatus = this.base_url_node+"updateModelStatusApi";
    this.allModelCount = this.base_url_node+"modelTotalCount";
    this.allModel = this.base_url_node+"allModelList";
    this.getCarCategory = this.base_url_node+"getCarCategory";
    this.getallModel(0);
    this.selectedIndex = 0;


  }

  ngOnInit(): void {

    this._http.post(this.allModelCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allModelCount"+JSON.stringify(response));
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });

    this._http.post(this.getCarCategory,this.formValue).subscribe((response:any)=>{
      //console.log("response get CarCategory"+JSON.stringify(response));
      this.allCarCategory = response.result;
    });

  }
  getallModel(numofpage=0)
  {
    this.selectedIndex = numofpage;


    //console.log("numofpage"+numofpage);
    this.queryParam = {"numofpage":numofpage};
    //console.log(this.queryParam);
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("allModel"+JSON.stringify(response));
      this.record = response.record;
      
      //console.log("totalPageNumber"+this.totalPageNumber);
      //console.log(this.numbers);
    });
  }
  updateModalStatus(id=null,status:number)
  {
    /*console.log("hii");
    console.log(id);
    console.log(status);*/
    this.queryParamNew = {"id":id,"status":status};
    this._http.post(this.updateModelStatus,this.queryParamNew).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.numofpage_0 = 0;
        this.queryParam = {"numofpage":this.numofpage_0};
        //console.log(this.queryParam);
        this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
          //console.log("allModel"+JSON.stringify(response));
          this.record = response.record;
          
          //console.log("totalPageNumber"+this.totalPageNumber);
          //console.log(this.numbers);
        });
      }
    });
  }



  form = new UntypedFormGroup({
    model_name: new UntypedFormControl('', []),
    attribute_type: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  submit()
  {
    //console.log(this.form.value);
    this._http.post(this.allModel,this.form.value).subscribe((response:any)=>{
      //console.log("allUsers"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }


}
