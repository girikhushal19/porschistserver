import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllColorInteriorNameComponent } from './all-color-interior-name.component';

describe('AllColorInteriorNameComponent', () => {
  let component: AllColorInteriorNameComponent;
  let fixture: ComponentFixture<AllColorInteriorNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllColorInteriorNameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllColorInteriorNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
