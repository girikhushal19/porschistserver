const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {


	allCarNormalUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	    String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarNormalUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	   // String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    //String_qr['show_on_web'] = 1;
	    //String_qr['activation_status'] = 1;

	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

			//console.log("String_qr "+JSON.stringify(String_qr));
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1,'show_on_web':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allCarProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
	    var String_qr = {};

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    //String_qr['activation_status'] = 1;

	    if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }
	    if(model_id && model_id != "")
	    {
	    	String_qr['modelId'] = model_id;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    if(colorId && colorId != "")
	    {
	    	String_qr['colorId'] = colorId;
	    }

	    if(attribute_air_rim && attribute_air_rim != "")
	    {
	    	String_qr['attribute_air_rim'] = attribute_air_rim;
	    }
	    if(warranty && warranty != "")
	    {
	    	String_qr['warranty'] = warranty;
	    }
	    if(maintenance_booklet && maintenance_booklet != "")
	    {
	    	String_qr['maintenance_booklet'] = maintenance_booklet;
	    }
	    if(maintenance_invoice && maintenance_invoice != "")
	    {
	    	String_qr['maintenance_invoice'] = maintenance_invoice;
	    }
	    if(accidented && accidented != "")
	    {
	    	String_qr['accidented'] = accidented;
	    }
	    if(original_paint && original_paint != "")
	    {
	    	String_qr['original_paint'] = original_paint;
	    }
	    if(matching_number && matching_number != "")
	    {
	    	String_qr['matching_number'] = matching_number;
	    }
	    if(matching_color_paint && matching_color_paint != "")
	    {
	    	String_qr['matching_color_paint'] = matching_color_paint;
	    }
	    if(matching_color_interior && matching_color_interior != "")
	    {
	    	String_qr['matching_color_interior'] = matching_color_interior;
	    }
	    if(priceMin && priceMin != ""  && priceMax != ""  && priceMax != "" )
	    {
	    	String_qr['price'] = {  $gte:req.query.priceMin, $lte: req.query.priceMax } 
	    }

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1,"show_on_web":1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},



	allCarActiveNormalUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};
	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;
			if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarActiveNormalUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;

	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allCarInActiveNormalUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;

	    var String_qr = {};
	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	    //String_qr['activation_status'] = 1;
	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarInActiveNormalUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	    //String_qr['activation_status'] = 1;

	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	         // console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},


	allCarActiveProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;

	    var String_qr = {};
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;
	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarActiveProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;

	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1,"show_on_web":1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allCarInActiveProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;

	    var String_qr = {};
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	    //String_qr['activation_status'] = 1;
	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allCarInActiveProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_of_chassis',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.query.category);
			var { category,modelId,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,minPrice ,maxPrice,registration_year,attribute_type,title } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Porsche Voitures";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	    //String_qr['activation_status'] = 1;

	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_of_chassis'] = attribute_type;
	    }
	    if(registration_year && registration_year != "")
	    {
	    	//console.log("hereeee registration_year" + registration_year);
	    	String_qr['registration_year'] = registration_year;
	    }
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'warranty':1,'warranty_month':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1,'model_variant':1,'maintenance_booklet':1,'maintenance_invoice':1,'accidented':1,'original_paint':1,'matching_number':1,'matching_color_paint':1,'matching_color_interior':1,'price_for_pors':1,'number_of_owners':1,'cylinder_capacity':1,'mileage_kilometer':1,'engine_operation_hour':1,"show_on_web":1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	getCarModelAdmin: async function(req, res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    try{
      allModels = await ModelsModel.find({ parent_id: null,attribute_type:"Porsche Voitures" });
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            result: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },
};