const express = require('express');
const UsersModel = require("../../models/UsersModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');
 

/*/*/
module.exports = {
	allUsersNameOnly:async(req,res)=>{
		try{
			var record = await UsersModel.find({},{firstName:1,lastName:1});
			var return_response = {"error":false,errorMessage:"Success",record:record};
	    return res.status(200).send(return_response);
		}catch(error)
		{
			var return_response = {"error":true,errorMessage:error.message};
	            res.status(200).send(return_response);
		}
	},
	allNormalUsers:async function(req,res,next)
	{
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	var { firstName,lastName,mobileNumber,email,city,status } = req.body;
		  	var String_qr = {};
		    if(firstName && firstName != "")
		    {
		    	String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
		    }
		    if(lastName && lastName != "")
		    {
		    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
		    }
		    if(email && email != "")
		    {
		    	String_qr['email'] = { $regex: '.*' + email + '.*' };
		    }
		    if(mobileNumber && mobileNumber != "")
		    {
		    	String_qr['mobileNumber'] = mobileNumber;
		    }
		    if(status && status != "")
		    {
		    	String_qr['status'] = status;
		    }
		    String_qr['user_type'] = "normal_user";

	      UsersModel.find(String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	allNormalUsersCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       UsersModel.countDocuments({ user_type: 'normal_user' }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allProUsers:async function(req,res,next)
	{
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	var { firstName,lastName,mobileNumber,email,city,status } = req.body;
		  	var String_qr = {};
		    if(firstName && firstName != "")
		    {
		    	String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
		    }
		    if(lastName && lastName != "")
		    {
		    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
		    }
		    if(email && email != "")
		    {
		    	String_qr['email'] = { $regex: '.*' + email + '.*' };
		    }
		    if(mobileNumber && mobileNumber != "")
		    {
		    	String_qr['mobileNumber'] = mobileNumber;
		    }
		    if(status && status != "")
		    {
		    	String_qr['status'] = status;
		    }
		    String_qr['user_type'] = "pro_user";

	      UsersModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	allProUsersCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       UsersModel.countDocuments({ user_type:"pro_user" }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	getSingleUserApi:async function(req,res,next)
	{
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       UsersModel.findOne({ _id:req.body.id }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
	            res.status(200).send(return_response);
	          }
	        });
	    }catch(err){
	      console.log(err);
	    }
	},
	updateUserStatusApi: async function(req, res,next)
	{
		//console.log(req.body.status);
	    UsersModel.updateOne({ "_id":req.body.id }, 
	    {status:req.body.status}, function (uerr, docs) {
	    if (uerr){
	      //console.log(uerr);
	      res.status(200)
	        .send({
	            error: true,
	            success: false,
	            errorMessage: uerr
	        });
	    }
	    else{
	      res.status(200)
	        .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès complet de la mise à jour du statut"
	        });
	      }
	    });
	},

	editUserSubmit: async function(req,res,next)
	{
		//console.log("hereeee");
	    //console.log(req.body);
	    //console.log(req.body);
      	//console.log(req.files);
      	//console.log(req.file.filename);

	    var firstName = req.body.firstName;
	    var lastName = req.body.lastName;
	    var email = req.body.email;
	    var mobileNumber = req.body.mobileNumber;
	    var city = req.body.city;
	    var description = req.body.description;
	    var id = req.body.id;
	    UsersModel.count({ email:email , '_id': {$ne: id} },(errs,emailCount)=>
        {
          if(errs)
          {
            console.log(errs);
          }else{
          	//console.log(emailCount);
          	if(emailCount == 1)
          	{
          		res.status(200)
			        .send({
			            error: true,
			            success: false,
			            errorMessage: "Cet email est prêt à être utilisé par un autre utilisateur"
			        });
          	}else{
      		UsersModel.count({ mobileNumber:mobileNumber , '_id': {$ne: id} },(errs,emailCount)=>
		        {
		          if(errs)
		          {
		            console.log(errs);
		          }else{
		          	//console.log(emailCount);
		          	if(emailCount == 1)
		          	{
		          		res.status(200)
					        .send({
					            error: true,
					            success: false,
					            errorMessage: "Ce numéro de mobile est prêt à être utilisé par un autre utilisateur"
					        });
		          	}else{
		          		//console.log("hereeeee");	
		          		UsersModel.findOne({ "_id":id }).exec((err, userRecord)=>{
		          			if(err)
					        {
					          //console.log("herrrrrerr");
					          console.log(err);
					        }else{
					        	//console.log(userRecord);
					        	if(userRecord)
				          		{
				          		if(req.files.length > 0)
							        {
							          /*console.log("iffff");
							          console.log(req.files);return false;*/
							          var userImage = req.files[0].filename;
							          //console.log(req.files[0].filename);
							          var oldFileName = userRecord.userImage;
							          var uploadDir = 'public/uploads/userProfile/';
							          let fileNameWithPath = uploadDir + oldFileName;
							          //console.log(fileNameWithPath);

							          if (fs.existsSync(fileNameWithPath))
							          {
							            fs.unlink(fileNameWithPath, (err) => 
							            {
							              console.log("unlink file error "+err);
							            });
							          }
							        }else{
							          //console.log("elseee");
							          if(userRecord.userImage)
							          {
							            var userImage = userRecord.userImage;
							          }else{
							            var userImage = null;
							          }
							        }
							        /*console.log("userImage");
							        console.log(userImage);
							        return false;*/
				          			UsersModel.updateOne({ "_id":id }, 
								    {
								    	firstName:firstName,
								    	lastName:lastName,
								    	email:email,
								    	mobileNumber:mobileNumber,
								    	city:city,
								    	description:description,
								    	userImage:userImage,
								    }, function (uerr, docs) {
								    if (uerr){
								      //console.log(uerr);
								      res.status(200)
								        .send({
								            error: true,
								            success: false,
								            errorMessage: uerr
								        });
								    }
								    else{
								      res.status(200)
								        .send({
								            error: false,
								            success: true,
								            errorMessage: "Succès complet de la mise à jour du statut"
								        });
								      }
								   });
				          		}else{
				          			res.status(200)
								        .send({
								            error: true,
								            success: false,
								            errorMessage: "Identifiant d'utilisateur invalide"
								     });
				          		}
					        }
		          		});
		          	}
		          }
		        });
          	}
          }
        });
	},
	


	exportCsvNormalUserApi:async function(req,res,next)
	{
		var String_qr = {};
		String_qr['user_type'] = "normal_user";

		UsersModel.find(String_qr, { _id:1,firstName:1,lastName:1,email:1,mobileNumber:1,gender:1,address:1,status:1 }).lean(true).exec((err, data)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(data.length > 0)
        {
			    data.forEach(object=>
          {
            if(object.status==1)
            {
		        	object.status="Actif";
		        }else
		        {
		         	object.status="Inactif";
		        }

            if(object.gender=="M")
            {
		        	object.gender="Homme";
		        }else if(object.gender=="F")
		        {
		         	object.gender="Femme";
		        }
          })
          //console.log(JSON.stringify(data));

			     var fields = ['id', 'firstName','lastName', 'email','mobileNumber','gender','address','status'];

			     	//const fields = ['field1', 'field2', 'field3'];
						const opts = { fields };
			     	const parser = new Parser(opts);
  					const csv = parser.parse(data);
  					//var path=  Date.now()+'.csv'; 
  					var path=  'normalUserData.csv'; 
             fs.writeFile(path, csv, function(err,data) {
              if (err) {throw err;}
              else{ 
                res.download(path); // This is what you need
              }
          });
        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
	},
	exportCsvProUserApi:async function(req,res,next)
	{
		var String_qr = {};
		String_qr['user_type'] = "pro_user";

		UsersModel.find(String_qr, { _id:1,firstName:1,lastName:1,email:1,mobileNumber:1,gender:1,address:1,status:1,country:1 ,enterprise:1,postalcode:1,siret:1,email_verify:1 }).lean(true).exec((err, data)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(data.length > 0)
        {
			    data.forEach(object=>
          {
            if(object.status==1)
            {
		        	object.status="Actif";
		        }else
		        {
		         	object.status="Inactif";
		        }
            if(object.email_verify==1)
            {
		        	object.email_verify="Oui";
		        }else
		        {
		         	object.email_verify="Non";
		        }

            if(object.gender=="M")
            {
		        	object.gender="Homme";
		        }else if(object.gender=="F")
		        {
		         	object.gender="Femme";
		        }
          })
          //console.log(JSON.stringify(data));

			     var fields = ['id', 'firstName','lastName', 'email','mobileNumber','gender','enterprise','address','country','postalcode','siret','email_verify','status'];

			     	//const fields = ['field1', 'field2', 'field3'];
						const opts = { fields };
			     	const parser = new Parser(opts);
  					const csv = parser.parse(data);
  					//var path=  Date.now()+'.csv'; 
  					var path=  'proUserData.csv'; 
             fs.writeFile(path, csv, function(err,data) {
              if (err) {throw err;}
              else{ 
                res.download(path); // This is what you need
              }
          });
        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
	},
	
	allUsersCountDashboard:async function(req,res,next)
	{
	  try{
    	UsersModel.countDocuments({  }).exec((err, allUserCount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
		    	UsersModel.countDocuments({ status:1 }).exec((err1, allActiveUserCount)=>
		      {
		        //console.log("countResult "+totalcount);
		        if(err1)
		        {
		          console.log(err1);
		        }else{
				    	UsersModel.countDocuments({ status:2 }).exec((err2, allInActiveUserCount)=>
				      {
				        //console.log("countResult "+totalcount);
				        if(err2)
				        {
				          console.log(err2);
				        }else{
				        	var record = { "allUserCount":allUserCount,"allActiveUserCount":allActiveUserCount,"allInActiveUserCount":allInActiveUserCount };
				        	var return_response = {"error":false,errorMessage:"Succès","record":record};
            			res.status(200).send(return_response);
				        }
				      });
		        }
		      });
        }
      });
	  }catch(err){
	    console.log(err);
	  }
	},
	allUsersBankDetail:async(req,res)=>{
		try{
			//console.log(req.body);
			var {id} = req.body;
			var result = await UsersModel.find({_id:id});
			var return_response = {"error":false,errorMessage:"Succès",record:result};
      return res.status(200).send(return_response);
		}catch(error)
		{
			var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
		}
	}
};