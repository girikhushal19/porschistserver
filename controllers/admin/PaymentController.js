const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const UsersModel = require("../../models/UsersModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const SettingModel = require("../../models/SettingModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
const PaymentRequest = require('../../models/PaymentRequest');
//const customConstant = require('../../helpers/customConstant');

const paypal = require('paypal-rest-sdk');
// const paypal = require('paypal-rest-sdk');

module.exports = {
  adminGetAllPaymentRequestCount:async(req,res)=>{
    try{
      
      await PaymentRequest.aggregate([
        {
          $group:{
            _id:null,
            count:{$sum:1}
          }
        }
      ]).then((result)=>{
        if(result.length > 0)
        {
          let totalPageNumber = result[0].count / 10;
          totalPageNumber = Math.ceil(totalPageNumber);
          var return_response = {"error":false,success: true,errorMessage:"Success",totalPageNumber:totalPageNumber};
          return  res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:0};
          return  res.status(200).send(return_response);
        } 
        
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllPaymentRequest:async(req,res)=>{
    try{
      let {page_no} = req.body;
      page_no = page_no ? page_no : 0;
      let limit = 10;
      let skip = limit * page_no;
      await PaymentRequest.aggregate([
        {
          $lookup:{
            from:"users",
            let:{"userObjId":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$userObjId"]
                  }
                }
              },
              {
                $project:{
                  "firstName": 1,
                  "lastName": 1,
                  "email": 1,
                  "mobileNumber": 1,
                  "user_type": 1,
                  "enterprise": 1,
                  "telephone": 1,
                  "companyImage": 1,
                  "whatsapp": 1
                }
              }
            ],
            as:"user_rec"
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        },
        {
          $sort:{"created_at":-1}
        }
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        return  res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllPendingPaymentRequestCount:async(req,res)=>{
    try{
      
      await PaymentRequest.aggregate([
        {
          $match:{
            status:1
          }
        },
        {
          $group:{
            _id:null,
            count:{$sum:1}
          }
        }
      ]).then((result)=>{
        if(result.length > 0)
        {
          let totalPageNumber = result[0].count / 10;
          totalPageNumber = Math.ceil(totalPageNumber);
          var return_response = {"error":false,success: true,errorMessage:"Success",totalPageNumber:totalPageNumber};
          return  res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:0};
          return  res.status(200).send(return_response);
        } 
        
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllPendingPaymentRequest:async(req,res)=>{
    try{
      let {page_no} = req.body;
      page_no = page_no ? page_no : 0;
      let limit = 10;
      let skip = limit * page_no;
      await PaymentRequest.aggregate([
        {
          $match:{
            status:1
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"userObjId":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$userObjId"]
                  }
                }
              },
              {
                $project:{
                  "firstName": 1,
                  "lastName": 1,
                  "email": 1,
                  "mobileNumber": 1,
                  "user_type": 1,
                  "enterprise": 1,
                  "telephone": 1,
                  "companyImage": 1,
                  "whatsapp": 1
                }
              }
            ],
            as:"user_rec"
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        },
        {
          $sort:{"created_at":-1}
        }
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        return  res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllPaidPaymentRequestCount:async(req,res)=>{
    try{
      
      await PaymentRequest.aggregate([
        {
          $match:{
            status:3
          }
        },
        {
          $group:{
            _id:null,
            count:{$sum:1}
          }
        }
      ]).then((result)=>{
        if(result.length > 0)
        {
          let totalPageNumber = result[0].count / 10;
          totalPageNumber = Math.ceil(totalPageNumber);
          var return_response = {"error":false,success: true,errorMessage:"Success",totalPageNumber:totalPageNumber};
          return  res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:0};
          return  res.status(200).send(return_response);
        } 
        
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllPaidPaymentRequest:async(req,res)=>{
    try{
      let {page_no} = req.body;
      page_no = page_no ? page_no : 0;
      let limit = 10;
      let skip = limit * page_no;
      await PaymentRequest.aggregate([
        {
          $match:{
            status:3
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"userObjId":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$userObjId"]
                  }
                }
              },
              {
                $project:{
                  "firstName": 1,
                  "lastName": 1,
                  "email": 1,
                  "mobileNumber": 1,
                  "user_type": 1,
                  "enterprise": 1,
                  "telephone": 1,
                  "companyImage": 1,
                  "whatsapp": 1
                }
              }
            ],
            as:"user_rec"
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        },
        {
          $sort:{"created_at":-1}
        }
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        return  res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllRejectedPaymentRequestCount:async(req,res)=>{
    try{
      
      await PaymentRequest.aggregate([
        {
          $match:{
            status:2
          }
        },
        {
          $group:{
            _id:null,
            count:{$sum:1}
          }
        }
      ]).then((result)=>{
        if(result.length > 0)
        {
          let totalPageNumber = result[0].count / 10;
          totalPageNumber = Math.ceil(totalPageNumber);
          var return_response = {"error":false,success: true,errorMessage:"Success",totalPageNumber:totalPageNumber};
          return  res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:0};
          return  res.status(200).send(return_response);
        } 
        
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  adminGetAllRejectedPaymentRequest:async(req,res)=>{
    try{
      let {page_no} = req.body;
      page_no = page_no ? page_no : 0;
      let limit = 10;
      let skip = limit * page_no;
      await PaymentRequest.aggregate([
        {
          $match:{
            status:2
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"userObjId":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$userObjId"]
                  }
                }
              },
              {
                $project:{
                  "firstName": 1,
                  "lastName": 1,
                  "email": 1,
                  "mobileNumber": 1,
                  "user_type": 1,
                  "enterprise": 1,
                  "telephone": 1,
                  "companyImage": 1,
                  "whatsapp": 1
                }
              }
            ],
            as:"user_rec"
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        },
        {
          $sort:{"created_at":-1}
        }
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        return  res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  myTestApi:async(req,res)=>{
    try{
      let today_date = new Date();
      //userId
      var record = await AddAdvertisementModel.aggregate([
        {
          $match:{
            $and:[
              {
                category: 'Porsche Voitures',
              },
              {
                email_reminder:0,
              },
              {
                show_on_web: 1,
              },
              {
                complition_status: 1,
              },
              {
                delete_at:0,
              },
              {
                plan_end_date_time:{ $gte:today_date }
              }
            ]
          }
        },
        {
          $addFields:{
            "user_id":{$arrayElemAt:["$userId",0]}
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"user_id":"$user_id"},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$user_id"]
                  }
                }
              }
            ],
            as:"usr"
          }
        },
        {
          $skip:0
        },
        {
          $limit:3
        },
        {
          $project:{
            "ad_name":1,
            "userId":1,
            "user_id":1,
            "plan_end_date_time":1,
            "usr.email":1,
            "usr.firstName":1,
            "usr.lastName":1
          }
        }
        
      ]);
      console.log("record ", record);
      var return_response = {"error":false,success: true,errorMessage:"Success",record:record};
      return  res.status(200).send(return_response);
    }catch(e)
    {
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  }
};