const express = require('express');
const UserfaqModel = require("../../models/UserfaqModel");
const AdminModel = require('../../models/AdminModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');

/*/*/
module.exports = {
	addFaqDriverSubmit: async function(req, res,next)
  {
  	try{
      var {question,answer} = req.body;
      DriverfaqModel.count({question:question,answer:answer},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount == 0)
          {
              DriverfaqModel.create({
                answer:answer,
                question:question,
              }, function (uerr, docs) {
                if(uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement ajouté avec succès"
                    });
                  }
            });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Enregistrer tous les prêts disponibles"
              });
          }
        }
      });
  	}catch(err){
  		console.log(err);
  	}
  },

  allDriverFaqCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       DriverfaqModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allDriverFaq: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{

      DriverfaqModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  

  getFaqDriverSingle: async function(req, res,next)
  {
    try{
      var id = req.body.id;
      DriverfaqModel.findOne({ _id:id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  editFaqDriverSubmit: async function(req, res,next)
  {
  	try{
      var {question,answer,id} = req.body;
      DriverfaqModel.count({_id:id},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount > 0)
          {
            DriverfaqModel.updateOne({_id:id},{
                answer:answer,
                question:question,
              }, function (uerr, docs) {
                if(uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement complet des succès mis à jour"
                    });
                  }
            });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Quelque chose a mal tourné"
              });
          }
        }
      });
  	}catch(err){
  		console.log(err);
  	}
  },
  updateDriverFaqStatusApi: async function(req, res,next)
  {
    DriverfaqModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },

  addFaqUserSubmit: async function(req, res,next)
  {
  	try{
      var {question,answer} = req.body;
      UserfaqModel.count({question:question,answer:answer},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount == 0)
          {
            UserfaqModel.create({
                answer:answer,
                question:question,
              }, function (uerr, docs) {
                if(uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement ajouté avec succès"
                    });
                  }
            });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Enregistrer tous les prêts disponibles"
              });
          }
        }
      });
  	}catch(err){
  		console.log(err);
  	}
  },
  

  editFaqUserSubmit: async function(req, res,next)
  {
  	try{
      var {question,answer,id} = req.body;
      UserfaqModel.count({_id:id},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount > 0)
          {
            UserfaqModel.updateOne({_id:id},{
                answer:answer,
                question:question,
              }, function (uerr, docs) {
                if(uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement complet des succès mis à jour"
                    });
                  }
            });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Quelque chose a mal tourné"
              });
          }
        }
      });
  	}catch(err){
  		console.log(err);
  	}
  },
  allUserFaqCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      UserfaqModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allUserFaq: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{

      UserfaqModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  getFaqUserSingle: async function(req, res,next)
  {

    try{
      var id = req.body.id;
      UserfaqModel.findOne({ _id:id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  updateUserFaqStatusApi: async function(req, res,next)
  {
    UserfaqModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
};