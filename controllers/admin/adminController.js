const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const BannerModel = require('../../models/BannerModel');
const UserpermissionsModel = require('../../models/UserpermissionsModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');

/*/*/
module.exports = {
  adminLogin: async function(req, res,next)
  {
    // Get user input
    const { email, password } = req.body;
    // Validate user input
    // Validate if user exist in our database
    var user = "";
    var c_date = new Date();
    //console.log(c_date);
    user = await AdminModel.findOne({ email });
    //console.log("user record"+user);return false;
    //mongoose.set("debug",true);
    if (user && (await bcrypt.compare(password, user.password)))
    {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        "Porschists",
        /*process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }*/
      );
      // save user token
      user.token = token;
      AdminModel.updateOne({ "email":email }, 
      { token:token,
        loggedin_time : c_date
      }, function (uerr, docs) {
        if (uerr){
            console.log(uerr);
            res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr,
                      //userRecord:user
                  });
        }else if(user.status == 0)
        {
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Compte utilisateur inactif",
          });
        }else{
          //console.log("User controller login method : ", docs);
          res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Utilisateur connecté avec succès",
                userRecord:user
          });
        }
      });
      // user
      //res.status(200).json(user);
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Connexion invalide"
                });
      }
    },
  adminRegistration: async function(req,res,next)
  {
    //console.log("controller registration method");
    //console.log(req.body);
    //res.send("herfsdfe");
    var encryptedPassword = "";
       try {
        const { userName, email, password , firstName, lastName } = req.body;
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
      //console.log("encryptedPassword khushal "+encryptedPassword);

        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );

        AdminModel.count({ email },(errs,emailCount)=>
        {
          if(errs)
          {
            console.log(errs);
          }else{
            //console.log(emailCount);
            if(emailCount === 0)
            {
              AdminModel.count({ userName },(userNameerrs,userNameCount)=>
              {
                if(userNameerrs)
                {
                  console.log(errs);
                }else{
                  if(userNameCount === 0)
                  {
                    
                    //Encrypt user password
                    //encryptedPassword = bcrypt.hash(password, 10);
                    //console.log(encryptedPassword);
                    //return false;
                    // Create token
                    // save user token
                    //user.token = token;
                    //encryptedPassword = "$2a$10$2XpyurefqmxN40GU0RVXQ.dVhX7DSxxiXw4PvH/w4IM6apJ6zlv4.";
                    //console.log("encryptedPassword"+encryptedPassword);
                    // Create user in our database
                    //console.log("herrrrrrrrrrrrrr");
                    const user =  AdminModel.create({
                      lastName,
                      firstName,
                      userName,
                      email: email.toLowerCase(), // sanitize: convert email to lowercase
                      password: encryptedPassword,
                      token: token,
                    });
                    // return new user
                    res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Enregistrement réussi"
                          });
                  }else{
                    //email exist
                    res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Nom d'utilisateur déjà existant"
                          });
                  }
                }
                
              });
            }else{
              //email exist
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Email déjà existant"
                    });
            }
          }
        }); 
    }catch (err) {
        console.log(err);
    }
  },
  addBannerImageSubmit: async function(req,res,next)
  {
    try{
      /*console.log(req.files);
      console.log(req.files[0]);
      console.log(req.files[1]);*/
      //console.log(req.files.file);
      var images = [];
      if(req.files)
      {
        if(req.files)
        {
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        }

        BannerModel.create({
          first_heading:req.body.first_heading,
          second_heading:req.body.second_heading,
          images:images
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Pas d'image"};
              res.status(200)
              .send(return_response);
      }
    }catch (err) {
        console.log(err);
    }
  },

  editBannerImageSubmit: async function(req, res,next)
  {
    try{
      //console.log(req.files);return false;

      var {category} = req.body;
      var images = [];
      //var video = [];
      if(req.files)
      {
        if(req.files.length > 0)
        {
          /*console.log("ifff"); 
          */
          //console.log(req.files.file[0].filename);
          //var p_r_i = req.files;
          //console.log(p_r_i.length);
          for(let m=0; m<1; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
          }
          //console.log(exterior_image);
        }
      }

      /*console.log(req.files);
      console.log(images);
      //console.log(images);

      return false;*/
      BannerModel.find({ _id:req.body.edit_id }, {images:1}).exec((err, result)=>
      {
        if(err)
        {
          //console.log("herrrrrerr");
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              if(images.length>0)
              {
                //console.log("images is here");
                if(result[0].images.length > 0)
                {
                  //console.log(result[0].images[0]);
                  if(result[0].images[0].path)
                  {
                    if (fs.existsSync(result[0].images[0].path))
                    {
                      fs.unlink(result[0].images[0].path, (err) => 
                      {
                        console.log("unlink file error "+err);
                      });
                    }
                  }
                }
              }else{
                //console.log("images is not here");
                images = result[0].images;
              }
              //console.log(req.body);
              /*console.log(images);
              return false;*/
             

              //console.log("elseee");
              BannerModel.updateOne({ "_id":req.body.edit_id }, 
              {
                first_heading:req.body.first_heading,
                second_heading:req.body.second_heading,
                images:images
              }, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
              }
              else{
                res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Enregistrement complet des succès mis à jour"
                  });
                }
              });
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  allBannerCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       BannerModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  getSingleBanner: async function(req, res,next)
  {
    try{
      BannerModel.findOne({ _id:req.body.edit_id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  allBanner: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      BannerModel.find({ }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  deleteBanner: async function(req, res,next){
    var categoryRecord = await BannerModel.find({ _id:req.body.id}); 
    if(categoryRecord)
    {
      //console.log(categoryRecord);
      //console.log(categoryRecord[0].images[0].filename);
      //var uploadDir = './public/uploads/userProfile/';

      let fileNameWithPath = categoryRecord[0].images[0].path;
      //console.log(fileNameWithPath);
      if (fs.existsSync(fileNameWithPath))
      {
        fs.unlink(fileNameWithPath, (err) => 
        {
          console.log("unlink file error "+err);
        });
      }
       
      BannerModel.deleteOne({
        _id:req.body.id
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }
  },
  addSubAdminSubmit:async function(req,res,next)
  {
    //console.log(req.body);
    //console.log(req.body.password.length);
    //console.log(req.files);
    //console.log(req.files[0]);
    //console.log(req.files[0].filename);
    var adminImage = null;
    if(req.files)
    {
      if(req.files.length > 0)
      {
        adminImage = req.files[0].filename;
      }
    }
    var encryptedPassword = "";
       try {
        const { userName, email, password , firstName, lastName } = req.body;
        if(password.length < 8)
        {
          res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "La longueur minimale du mot de passe est de 8 chiffres"
              });
        }else{
          // check if user already exist
          // Validate if user exist in our database
          encryptedPassword = await bcrypt.hash(password, 10);
          //console.log("xx"+xx);
          //console.log("encryptedPassword khushal "+encryptedPassword);

          var token =  jwt.sign(
            { user_id:  Math.random().toString(36).slice(2), email },
            "Porschists",
          );

          AdminModel.count({ email },(errs,emailCount)=>
          {
            if(errs)
            {
              console.log(errs);
            }else{
              //console.log(emailCount);
              if(emailCount === 0)
              {
                AdminModel.count({ userName:email },(userNameerrs,userNameCount)=>
                {
                  if(userNameerrs)
                  {
                    console.log(errs);
                  }else{
                    if(userNameCount === 0)
                    {
                      const user =  AdminModel.create({
                        user_type:"sub_admin",
                        lastName,
                        firstName,
                        userName:email,
                        email: email.toLowerCase(), // sanitize: convert email to lowercase
                        password: encryptedPassword,
                        token: token,
                        adminImage: adminImage,
                        status: 1,
                      });
                      // return new user
                      res.status(200)
                            .send({
                                error: false,
                                success: true,
                                errorMessage: "Succès de l'enregistrement complet"
                            });
                    }else{
                      //email exist
                      res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: "Nom d'utilisateur tout prêt existe"
                            });
                    }
                  }
                  
                });
              }else{
                //email exist
                res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: "Email tout prêt existant"
                      });
              }
            }
          }); 
        }
        
    }catch (err) {
        console.log(err);
    }
  },
  allSubAdminCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      var { firstName,lastName,mobileNumber,email,city,status } = req.body;
    
      var String_qr = {}; 
      //String_qr['firstName'] = firstName;
      if(firstName && firstName != "")
      {
        String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
      }
      if(lastName && lastName != "")
      {
        String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
      }
      if(email && email != "")
      {
        String_qr['email'] = { $regex: '.*' + email + '.*' };
      }
      if(status && status != "")
      {
        String_qr['status'] = status;
      }
      String_qr['user_type'] = "sub_admin";

       AdminModel.countDocuments(String_qr).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },

  allSubAdmin:async function(req,res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      var { firstName,lastName,mobileNumber,email,city,status } = req.body;
    
      var String_qr = {}; 
      //String_qr['firstName'] = firstName;
      if(firstName && firstName != "")
      {
        String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
      }
      if(lastName && lastName != "")
      {
        String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
      }
      if(email && email != "")
      {
        String_qr['email'] = { $regex: '.*' + email + '.*' };
      }
      if(status && status != "")
      {
        String_qr['status'] = status;
      }
      String_qr['user_type'] = "sub_admin";

      AdminModel.find(String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  exportCsvSubAdminApi:async function(req,res,next)
  {
    try{
      var { firstName,lastName,mobileNumber,email,city,status } = req.body;
      var String_qr = {}; 
      //String_qr['firstName'] = firstName;
      /*if(firstName && firstName != "")
      {
        String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
      }
      if(lastName && lastName != "")
      {
        String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
      }
      if(email && email != "")
      {
        String_qr['email'] = { $regex: '.*' + email + '.*' };
      }
      if(status && status != "")
      {
        String_qr['status'] = status;
      }*/
      String_qr['user_type'] = "sub_admin";
      
      AdminModel.find(String_qr, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {

            result.forEach(object=>
            {
              if(object.status==1)
              {
                object.status="Actif";
              }
              else
              {
                object.status="Inactif";
              }
            })


            var fields = ['id', "firstName",'lastName','email','status'];
            //const fields = ['field1', 'field2', 'field3'];
            const opts = { fields };
            const parser = new Parser(opts);
            const csv = parser.parse(result);
            //var path=  Date.now()+'.csv'; 
            var path=  'allSubAdminData.csv'; 
             fs.writeFile(path, csv, function(err,result) {
              if (err) {throw err;}
              else{ 
                res.download(path); // This is what you need
              }
            });
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },

  updateSubAdminApi: async function(req, res,next)
  {
    //console.log(req.body.status);
      AdminModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
  },
  

  getSingleSubAdminApi:async function(req,res,next)
  {
      //console.log("here");
      //console.log(req.body);
      try{
         AdminModel.findOne({ _id:req.body.id }).exec((err, totalcount)=>
          {
            //console.log("countResult "+totalcount);
            if(err)
            {
              console.log(err);
            }else{
              var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
              res.status(200).send(return_response);
            }
          });
      }catch(err){
        console.log(err);
      }
  },
  editSubAdminSubmit:async function(req,res,next)
  {
    //console.log(req.body);
    //console.log(req.files);
    try{
      const { userName, email , firstName, lastName } = req.body;
      var user_id = req.body.id;
      var adminImage = null;
      var userRecord = await AdminModel.findOne({ "_id":user_id }); 
      if(userRecord)
      {
        if(req.files)
        {
          if(req.files.length > 0)
          {

            adminImage = req.files[0].filename;
            var oldFileName = userRecord.adminImage;
            var uploadDir = './public/uploads/admin/';
            let fileNameWithPath = uploadDir + oldFileName;
            //console.log(fileNameWithPath);
            //console.log("here");
            if (fs.existsSync(fileNameWithPath))
            {
              fs.unlink(uploadDir + oldFileName, (err) => 
              {
                console.log("unlink file error "+err);
              });
            }
            //return false;
          }else{
            if(userRecord.adminImage)
            {
              adminImage = userRecord.adminImage;
            }else{
              adminImage = null;
            }
          }
        }else{
            if(userRecord.adminImage)
            {
              adminImage = userRecord.adminImage;
            }else{
              adminImage = null;
            }
          }

          //console.log(adminImage);return false;
          var userRecordEmailCheck = await AdminModel.findOne({ "_id":{$ne:user_id},"email":email }); 
          //console.log("userRecordEmailCheck"+userRecordEmailCheck);
          if(userRecordEmailCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Cet email existe déjà pour un autre utilisateur"
            });
          }else{
              AdminModel.findByIdAndUpdate({ _id: user_id },{ email: email.toLowerCase(),firstName:firstName,lastName:lastName,adminImage:adminImage }, function (err, user) {
                if (err) {
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Un problème est survenu",
                        errorOrignalMessage: err
                    });
                } else {
                 // console.log("Record updated success-fully");
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement mis à jour avec succès"
                    });
                }
               });
          }
      }else{
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "ID indisponible"
          });
      }
      
    }catch(err){
      console.log(err);
    }
  },


  getAdminProfile: async function(req, res,next)
  {
    //console.log(req.body);
    try{
      AdminModel.findOne({ _id:req.body.id }).exec((err, userRecord)=>{
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log(userRecord);
            var return_response = {"error":false,errorMessage:"success","record":userRecord};
            res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  editAdminProfileSubmitApi: async function(req,res,next)
  {
      //console.log("hereeee");
      //console.log(req.body);
      /*console.log(req.body);
      console.log(req.files);
      return false;*/
      var firstName = req.body.firstName;
      var lastName = req.body.lastName;
      var email = req.body.email;
      
      var id = req.body.id;
      
      AdminModel.findOne({ "_id":id }).exec((err, userRecord)=>{
        if(err)
      {
        //console.log("herrrrrerr");
        console.log(err);
      }else{
        //console.log(userRecord);
        if(userRecord)
          {
            if(req.files)
            {
              //console.log("iffff");
              var userImage = req.files[0].filename;
              //console.log(req.files[0].filename);
              var oldFileName = userRecord.userImage;
              var uploadDir = 'public/uploads/adminProfile/';
              let fileNameWithPath = uploadDir + oldFileName;
              //console.log(fileNameWithPath);

              if (fs.existsSync(fileNameWithPath))
              {
                fs.unlink(fileNameWithPath, (err) => 
                {
                  console.log("unlink file error "+err);
                });
              }
            }else{
              //console.log("elseee");
              if(userRecord.userImage)
              {
                var userImage = userRecord.userImage;
              }else{
                var userImage = null;
              }
            }
            /*console.log("userImage");
            console.log(userImage);
            return false;*/
            AdminModel.updateOne({ "_id":id }, 
            {
              firstName:firstName,
              lastName:lastName,
              email:email,
              adminImage:userImage,
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Succès complet de la mise à jour du statut"
                });
              }
           });
          }else{
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Identifiant d'utilisateur invalide"
         });
        }
      }
    });    
  },
  adminChangePasswordSubmitApi: async function(req,res,next)
  {
    //console.log(req.body);
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var encryptedPassword = await bcrypt.hash(new_password, 10);
    user = await AdminModel.findOne({ _id:req.body.user_id });
    //console.log(user);return false;  
    if(user && (await bcrypt.compare(old_password, user.password)))
    {
      if(new_password.length < 8)
      {
        var return_response = {"error":true,success: false,errorMessage:"La longueur du nouveau mot de passe ne doit pas être inférieure à huit chiffres."};
            res.status(200)
            .send(return_response);
      }else{
        //console.log("hereee");
        AdminModel.updateOne({ _id:req.body.user_id }, 
        {
          password:encryptedPassword,
        }, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }
        else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès total du changement de mot de passe"
            });
          }
       });
      }
    }else{
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'ancien mot de passe est incorrect"
      });
    }
  },

};