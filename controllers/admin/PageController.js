const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const EmailNotificationModel = require('../../models/EmailNotificationModel');
const PageModel = require('../../models/PageModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
   addPageSubmit:async function(req,res,next)
   {
       //console.log(req.body);
       try
       {
         var { slug_name , page_title,html } = req.body;
             PageModel.count({slug_name:slug_name},(errs, allReadyLike)=>
             {
               //console.log("allReadyLike"+allReadyLike);
               if(allReadyLike == 0)
               {
                 PageModel.create({
                     slug_name:slug_name,
                     page_title:page_title,
                     description:html
                  },function(err,result){
                   if(err)
                   {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                  }else{
                      var return_response = {"error":false,success: true,errorMessage:"Succès"};
                        res.status(200)
                        .send(return_response);
                  }
                });
               }else{
                 PageModel.updateOne({ slug_name:slug_name }, 
                 {
                     page_title:page_title,
                     description:html
                 }, function (uerr, docs) {
                 if (uerr){
                   //console.log(uerr);
                   res.status(200)
                     .send({
                         error: true,
                         success: false,
                         errorMessage: uerr
                     });
                 }
                 else{
                   res.status(200)
                     .send({
                         error: false,
                         success: true,
                         errorMessage: "Enregistrement mis à jour avec succès"
                     });
                   }
                 });
               }
             });
      }catch(error)
      {
         //console.log(error)
         res.status(404).json({
           status: 'fail',
           message: error,
         });
      }
   },
   editPageSubmit:async function(req,res,next)
   {
      //console.log(req.body);
      try
      {
        var { slug_name , page_title,html,edit_id } = req.body;
        PageModel.updateOne({ _id:edit_id }, 
        {
             slug_name:slug_name,
             page_title:page_title,
             description:html
        }, function (uerr, docs) {
         if (uerr){
           //console.log(uerr);
           res.status(200)
             .send({
                 error: true,
                 success: false,
                 errorMessage: uerr
             });
        }
         else{
           res.status(200)
             .send({
                 error: false,
                 success: true,
                 errorMessage: "Enregistrement mis à jour avec succès"
             });
           }
        });
      }catch(error)
      {
         //console.log(error)
         res.status(404).json({
           status: 'fail',
           message: error,
         });
      }
   },
   allPage:async function(req,res,next)
   {
      var xx = 0;
      var total = 0;
      var perPageRecord = 10;
      var fromindex = 0;
      if(req.body.numofpage == 0)
      {
        fromindex = 0;
      }else{
        fromindex = perPageRecord * req.body.numofpage
      }
      try{


        PageModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
        {
          if(err)
          {
            console.log(err);
          }else{
            //console.log("result ");
            //console.log(typeof result);
            //console.log("result"+result);
            if(result.length > 0)
            {
               var return_response = {"error":false,errorMessage:"Succès","record":result};
                    res.status(200).send(return_response);
            }else{
              var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
              res.status(200).send(return_response);
            }

          }
        });

      }catch(err){
        console.log(err);
      }
   },
  allPageCount:async function(req,res,next)
  {
      let total = 0;
      var totalPageNumber = 1;
      var perPageRecord = 10;
      //console.log("here");
      //console.log(req.body);
      try{
         PageModel.countDocuments({  }).exec((err, totalcount)=>
          {
            //console.log("countResult "+totalcount);
            if(err)
            {
              console.log(err);
            }else{
              total = totalcount;
              totalPageNumber = total / perPageRecord;
              //console.log("totalPageNumber"+totalPageNumber);
              totalPageNumber = Math.ceil(totalPageNumber);
              //console.log("totalPageNumber"+totalPageNumber);
              //console.log("totalcount new fun"+totalcount);
              //console.log("all record count "+totalcount);
              var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
              res.status(200).send(return_response);
            }
          });
         
      }catch(err){
        console.log(err);
      }
  },
  deletePage: async function(req, res,next)
  {
      PageModel.deleteOne({
          _id:req.body.id
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
  },
  updatePageStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
      PageModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
  },
  getSinglePage: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.id;
      PageModel.find({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result.length > 0)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement",
                  record:{}
              });
          }
        }
      });

    }catch(err){
      console.log(err);
    }
  },
};