const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
const ColorModel = require("../../models/ColorModel");
const ColorinteriorsModel = require("../../models/ColorinteriorsModel");
const ColorinternamesModel = require("../../models/ColorinternamesModel");
const ColorextnamesModel = require("../../models/ColorextnamesModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');

/*/*/
module.exports = {
	addCarAttributeSubmit:async function(req, res,next)
	{
		try{
  			var { attribute_type,attribute_name,parent_id  } = req.body;
	      //console.log("attribute_name"+attribute_name);
	       
	      if(parent_id == "" || parent_id == "null")
	      {
	        
	        parent_id = null;
	      }
	      AttributeModel.count({attribute_name,attribute_type},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	          if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
	              AttributeModel.create({
	                parent_id,
	                attribute_type,
	                attribute_name
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Attribut ajouté avec succès"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "Attribut déjà existant"
	              });
	          }
	        }
	      });
	  	}catch(err){
	  		console.log(err);
	  	}
	},
	editCarAttributeSubmit:async function(req, res,next)
	{
			/*console.log("hereeeeeeee");
			console.log(req.body);*/
		try{
			var { attribute_type,attribute_name,parent_id,edit_id  } = req.body;
	      //console.log("attribute_name"+attribute_name);
	      if(parent_id == "" || parent_id == "null")
	      {
	        
	        parent_id = null;
	      }
	      AttributeModel.findByIdAndUpdate({ _id: edit_id },
        { parent_id,
	                attribute_type,
	                attribute_name },
         function (err, user) {
		      if (err) {
		        res.status(200)
		          .send({
		              error: true,
		              success: false,
		              errorMessage: "Un problème est survenu",
		              errorOrignalMessage: err
		          });
		      } else {
		        //console.log("Record updated success-fully");
		          res.status(200)
		          .send({
		              error: false,
		              success: true,
		              errorMessage: "Enregistrement mis à jour avec succès"
		          });
		      }
		     });
	  	}catch(err){
	  		console.log(err);
	  	}
	},

	addColorsSubmit:async function(req, res,next)
	{
		try{
  			var { color_name,color_code  } = req.body;
	      //console.log("color_name "+color_name);
	      //console.log("color_code "+color_code);
	       
	      
	      /*ColorModel.count({color_name,color_code},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	          if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
	              ColorModel.create({
	                color_name,
	                color_code
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Color added success-fully"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "Color combination all-ready exist"
	              });
	          }
	        }
	      });*/
	      ColorModel.count({color_name,color_code},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	        	//console.log("CategoryCount "+CategoryCount);

	        	if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
	              ColorModel.create({
	                color_name,
	                color_code,
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Couleur ajoutée avec succès"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "La combinaison de couleurs existe déjà"
	              });
	          }




	        }
	      });

	  	}catch(err){
	  		console.log(err);
	  	}
	},
	editColorsSubmit:async function(req, res,next)
	{
		try{
			var edit_id = req.body.edit_id;
			var { color_name,color_code  } = req.body;
			ColorModel.findByIdAndUpdate({ _id: edit_id },
	    { color_name,color_code },
	     function (err, user) {
	      if (err) {
	        res.status(200)
	          .send({
	              error: true,
	              success: false,
	              errorMessage: "Un problème est survenu",
	              errorOrignalMessage: err
	          });
	      } else {
	        //console.log("Record updated success-fully");
	          res.status(200)
	          .send({
	              error: false,
	              success: true,
	              errorMessage: "L'enregistrement a été mis à jour avec succès"
	          });
	      }
	     });
		}catch(err){
			console.log(err);
		}
	},

	editColorsInteriorSubmit:async function(req, res,next)
	{
		try{
			var edit_id = req.body.edit_id;
			var { color_name,color_code  } = req.body;
			ColorinteriorsModel.findByIdAndUpdate({ _id: edit_id },
	    { color_name,color_code },
	     function (err, user) {
	      if (err) {
	        res.status(200)
	          .send({
	              error: true,
	              success: false,
	              errorMessage: "Un problème est survenu",
	              errorOrignalMessage: err
	          });
	      } else {
	        //console.log("Record updated success-fully");
	          res.status(200)
	          .send({
	              error: false,
	              success: true,
	              errorMessage: "L'enregistrement a été mis à jour avec succès"
	          });
	      }
	     });
		}catch(err){
			console.log(err);
		}
	},

	addColorsInteriorSubmit:async function(req, res,next)
	{
		try{
  			var { color_name,color_code  } = req.body;
	       
	      ColorinteriorsModel.count({color_name,color_code},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	        	//console.log("CategoryCount "+CategoryCount);
	        	if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
              ColorinteriorsModel.create({
	                color_name,
	                color_code,
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Couleur ajoutée avec succès"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "La combinaison de couleurs existe déjà"
	              });
	          }
	        }
	      });

	  	}catch(err){
	  		console.log(err);
	  	}
	},



	addColorsExtSubmit:async function(req, res,next)
	{
		try{
  			var { color_name,color_code,color_ext  } = req.body;
	      
	      ColorextnamesModel.count({color_name,color_code,color_ext},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	        	//console.log("CategoryCount "+CategoryCount);

	        	if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
              ColorextnamesModel.create({
	                color_name,
                  color_ext,
	                color_code,
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Couleur ajoutée avec succès"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "La combinaison de couleurs existe déjà"
	              });
	          }
	        }
	      });

	  	}catch(err){
	  		console.log(err);
	  	}
	},

	addColorsInteriorNameSubmit:async function(req, res,next)
	{
		try{
  			var { color_name,color_code,color_ext  } = req.body;
	      
	      ColorinternamesModel.count({color_name,color_code,color_ext},(error,CategoryCount)=>{
	        if(error)
	        {
	          console.log(error);
	        }else{
	        	//console.log("CategoryCount "+CategoryCount);

	        	if(CategoryCount == 0)
	          {
	          	//console.log("ifffff "+CategoryCount);
              ColorinternamesModel.create({
	                color_name,
                  color_ext,
	                color_code,
	              });
	            res.status(200)
	                .send({
	                    error: false,
	                    success: true,
	                    errorMessage: "Couleur ajoutée avec succès"
	                });
	          }else{
	          	//console.log("elseeeee  "+CategoryCount);
	            res.status(200)
	              .send({
	                  error: true,
	                  success: false,
	                  errorMessage: "La combinaison de couleurs existe déjà"
	              });
	          }
	        }
	      });

	  	}catch(err){
	  		console.log(err);
	  	}
	},
	editColorsExtSubmit:async function(req, res,next)
	{
		try{
			var edit_id = req.body.edit_id;
			var { color_name,color_code,color_ext  } = req.body;
			ColorextnamesModel.findByIdAndUpdate({ _id: edit_id },
	    { color_name,color_code,color_ext },
	     function (err, user) {
	      if (err) {
	        res.status(200)
	          .send({
	              error: true,
	              success: false,
	              errorMessage: "Un problème est survenu",
	              errorOrignalMessage: err
	          });
	      } else {
	        //console.log("Record updated success-fully");
	          res.status(200)
	          .send({
	              error: false,
	              success: true,
	              errorMessage: "L'enregistrement a été mis à jour avec succès"
	          });
	      }
	     });
		}catch(err){
			console.log(err);
		}
	},
	editColorsInteriorNameSubmit:async function(req, res,next)
	{
		try{
			var edit_id = req.body.edit_id;
			var { color_name,color_code,color_ext  } = req.body;
			ColorinternamesModel.findByIdAndUpdate({ _id: edit_id },
	    { color_name,color_code,color_ext },
	     function (err, user) {
	      if (err) {
	        res.status(200)
	          .send({
	              error: true,
	              success: false,
	              errorMessage: "Un problème est survenu",
	              errorOrignalMessage: err
	          });
	      } else {
	        //console.log("Record updated success-fully");
	          res.status(200)
	          .send({
	              error: false,
	              success: true,
	              errorMessage: "L'enregistrement a été mis à jour avec succès"
	          });
	      }
	     });
		}catch(err){
			console.log(err);
		}
	},
	getCarAttribute: async function(req, res,next)
	{
	    //console.log("all data");
	    //console.log(req.body);
	    try{
	      allModels = await AttributeModel.find({ parent_id: null });
	      if(allModels){
	        res.status(200)
	            .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès",
	            result: allModels
	        });
	      }else{
	        //res.status(400).send("Invalid Credentials");
	        res.status(200)
	            .send({
	            error: true,
	            success: false,
	            errorMessage: "Pas d'enregistrement"
	        });
	      }
	    }catch(err){
	      console.log(err);
	    }
	},
	getSingleCarAttribute: async function(req, res,next)
	{
	    //console.log("all data");
	    //console.log(req.body);
	    var id = req.body.id;
	    try{
	    	var selectedValue = req.body.selectedValue;
	      allModels = await AttributeModel.find({_id:id });
	      //console.log(allModels);
	      if(allModels){
	        res.status(200)
	            .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès",
	            record: allModels
	        });
	      }else{
	        //res.status(400).send("Invalid Credentials");
	        res.status(200)
	            .send({
	            error: true,
	            success: false,
	            errorMessage: "Pas d'enregistrement"
	        });
	      }
	    }catch(err){
	      console.log(err);
	    }
	},
	getWhereCarAttribute: async function(req, res,next)
	{
	    //console.log("all data");
	    //console.log(req.body);
	    try{
	    	var selectedValue = req.body.selectedValue;
	      allModels = await AttributeModel.find({ attribute_type:selectedValue,parent_id: null });
	      //console.log(allModels);
	      if(allModels){
	        res.status(200)
	            .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès",
	            result: allModels
	        });
	      }else{
	        //res.status(400).send("Invalid Credentials");
	        res.status(200)
	            .send({
	            error: true,
	            success: false,
	            errorMessage: "Pas d'enregistrement"
	        });
	      }
	    }catch(err){
	      console.log(err);
	    }
	},
  allSubAttributeCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
	    	var {attribute_type,attribute_name} = req.body;
	    	var String_qr = {};

	    	if(attribute_type && attribute_type != "")
	      {
	        String_qr['attribute_type'] = attribute_type;
	      }
	      if(attribute_name && attribute_name != "")
	      {
	        String_qr['attribute_name'] = { $regex: '.*' + attribute_name + '.*' };
	      }
	      String_qr['parent_id'] = { $ne: null };

       AttributeModel.countDocuments(String_qr).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allSubAttribute: async function(req, res,next)
  {
  	//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
    	var {attribute_type,attribute_name} = req.body;
    	var String_qr = {};

    	if(attribute_type && attribute_type != "")
      {
        String_qr['attribute_type'] = attribute_type;
      }
      if(attribute_name && attribute_name != "")
      {
        String_qr['attribute_name'] = { $regex: '.*' + attribute_name + '.*' };
      }
      String_qr['parent_id'] = { $ne: null };
      
      //console.log(String_qr);

      AttributeModel.find(String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+JSON.stringify(result));
          if(result.length > 0)
          {
            var xx = result.length - 1;
            for(let i=0; i<result.length; i++)
            {
              /*result[i].newKey = "value here";
              console.log("in side loop "+result[i]);
              console.log("in side loop "+result[i].model_name);*/
              //console.log("in side loop "+result[i].parent_id);
              var parent_id = result[i].parent_id;
              AttributeModel.findOne({ _id:parent_id }).lean(true).exec((errr, resulttt)=>
              { 
                if(resulttt)
                {
                  //console.log("parent_record "+resulttt._id);
                  //console.log("parent_record "+resulttt.model_name);
                  result[i].rootAttribute = resulttt.attribute_name;
                }
                if(xx == i)
                {
                  //console.log("inside if");
                  var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
                }
              });
            }
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  exportallCarAttributeApi: async function(req, res,next)
  {
  	//mongoose.set('debug', true);
    
    try{
    	var {attribute_type,attribute_name} = req.body;
    	var String_qr = {};

    	/*if(attribute_type && attribute_type != "")
      {
        String_qr['attribute_type'] = attribute_type;
      }
      if(attribute_name && attribute_name != "")
      {
        String_qr['attribute_name'] = { $regex: '.*' + attribute_name + '.*' };
      }*/
      String_qr['parent_id'] = { $ne: null };
      
      //console.log(String_qr);

      AttributeModel.find(String_qr, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+JSON.stringify(result));
          if(result.length > 0)
          {
            var xx = result.length - 1;
            for(let i=0; i<result.length; i++)
            {
              /*result[i].newKey = "value here";
              console.log("in side loop "+result[i]);
              console.log("in side loop "+result[i].model_name);*/
              //console.log("in side loop "+result[i].parent_id);
              var parent_id = result[i].parent_id;
              AttributeModel.findOne({ _id:parent_id }).lean(true).exec((errr, resulttt)=>
              { 
                if(resulttt)
                {
                  //console.log("parent_record "+resulttt._id);
                  //console.log("parent_record "+resulttt.model_name);
                  result[i].rootAttribute = resulttt.attribute_name;
                }
                if(xx == i)
                {
                	
                  var fields = ['id', "rootAttribute",'attribute_type','attribute_name'];
                  //const fields = ['field1', 'field2', 'field3'];
                  const opts = { fields };
                  const parser = new Parser(opts);
                  const csv = parser.parse(result);
                  //var path=  Date.now()+'.csv'; 
                  var path=  'allCarAttributeData.csv'; 
                   fs.writeFile(path, csv, function(err,result){
                    if (err) {throw err;}
                    else{ 
                      res.download(path); // This is what you need
                    }
                  }); 

                }
              });
            }
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allColorCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       ColorModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allColor: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ColorModel.find({ parent_id: { $ne: null } }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allColorExtCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      ColorextnamesModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allColorExt: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ColorextnamesModel.find({}, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },


  allColorInteriorCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      ColorinteriorsModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allColorInterior: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ColorinteriorsModel.find({ parent_id: { $ne: null } }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },

  allColorInteriorNameCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      ColorinternamesModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allColorInteriorName: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ColorinternamesModel.find({ parent_id: { $ne: null } }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },


  getSingleColor: async function(req, res,next)
  {
    var id = req.body.id;
    try{
      allModels = await ColorModel.find({_id:id });
      //console.log(allModels);
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },
  
  getSingleColorExt: async function(req, res,next)
  {
    var id = req.body.id;
    try{
      allModels = await ColorextnamesModel.find({_id:id });
      //console.log(allModels);
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },

  getSingleColorInterior: async function(req, res,next)
  {
    var id = req.body.id;
    try{
      allModels = await ColorinteriorsModel.find({_id:id });
      //console.log(allModels);
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },


  getSingleColorInteriorName: async function(req, res,next)
  {
    var id = req.body.id;
    try{
      allModels = await ColorinternamesModel.find({_id:id });
      //console.log(allModels);
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },


  updateAttributeStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
    AttributeModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  updateColorStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
    ColorModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  updateColorExtStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
    ColorextnamesModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  updateColorInteriorStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
    ColorinteriorsModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  updateColorInteriorNameStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
    ColorinternamesModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
};