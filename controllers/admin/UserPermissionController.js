const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const BannerModel = require('../../models/BannerModel');
const UserpermissionsModel = require('../../models/UserpermissionsModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	userPermissionRecord:async function(req,res,next)
	{
		UserpermissionsModel.find({ user_id:req.body.logged_in_user_id }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(result.length > 0)
        {
           var return_response = {"error":false,errorMessage:"Succès","record":result};
                res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
	},
  permissionSubAdminSubmit:async function(req,res,next)
  {
    console.log(req.body);
    try{
    	var user_add = user_edit = user_delete = user_view = user_status = false;
    	var banner_add = banner_edit = banner_delete = banner_view = banner_status = false;
    	var category_add = category_edit = category_delete = category_view = category_status = false;
    	var model_add = model_edit = model_delete = model_view = model_status = false;
    	var sub_model_add = sub_model_edit = sub_model_delete = sub_model_view = sub_model_status = false;
    	var attribute_add = attribute_edit = attribute_delete = attribute_view = attribute_status = false;
    	var color_add = color_edit = color_delete = color_view = color_status = false;
    	var usercaradplan_add = usercaradplan_edit = usercaradplan_delete = usercaradplan_view = usercaradplan_status = false;
    	var prousercaradplan_add = prousercaradplan_edit = prousercaradplan_delete = prousercaradplan_view = prousercaradplan_status = false;
    	var cartopurgent_add = cartopurgent_edit = cartopurgent_delete = cartopurgent_view = cartopurgent_status = false;
    	var accessoriesUserAd_add = accessoriesUserAd_edit = accessoriesUserAd_delete = accessoriesUserAd_view = accessoriesUserAd_status = false;
    	var carUserAd_add = carUserAd_edit = carUserAd_delete = carUserAd_view = carUserAd_status = false;
    	var accessoriesOrders_add = accessoriesOrders_edit = accessoriesOrders_delete = accessoriesOrders_view = accessoriesOrders_status = false;
    	var pushNotification_add = pushNotification_edit = pushNotification_delete = pushNotification_view = pushNotification_status = false;
    	var emailNotification_add = emailNotification_edit = emailNotification_delete = emailNotification_view = emailNotification_status = false;
    	var page_add = page_edit = page_delete = page_view = page_status = false;
    	
    	var user_id = req.body.user_id;
      var user_module = req.body.user_module;
      var banner_module = req.body.banner_module;
      var category_module = req.body.category_module;
      var model_module = req.body.model_module;
      var sub_model_module = req.body.sub_model_module;
      var attribute_module = req.body.attribute_module;
      var color_module = req.body.color_module;
      var usercaradplan_module = req.body.usercaradplan_module;
      var prousercaradplan_module = req.body.prousercaradplan_module;
      var cartopurgent_module = req.body.cartopurgent_module;
      var accessoriesUserAd_module = req.body.accessoriesUserAd_module;
      var carUserAd_module = req.body.carUserAd_module;
      var accessoriesOrders_module = req.body.accessoriesOrders_module;
      var pushNotification_module = req.body.pushNotification_module;
      var emailNotification_module = req.body.emailNotification_module;
      var page_module = req.body.page_module;

    	if(req.body.user_add != '' && req.body.user_add != undefined)
      {
      	user_add = req.body.user_add;
      }
    	if(req.body.user_edit != '' && req.body.user_edit != undefined)
      {
      	user_edit = req.body.user_edit;
      }
    	if(req.body.user_delete != '' && req.body.user_delete != undefined)
      {
      	user_delete = req.body.user_delete;
      }
    	if(req.body.user_view != '' && req.body.user_view != undefined)
      {
      	user_view = req.body.user_view;
      }
    	if(req.body.user_status != '' && req.body.user_status != undefined)
      {
      	user_status = req.body.user_status;
      }


    	if(req.body.banner_add != '' && req.body.banner_add != undefined)
      {
      	banner_add = req.body.banner_add;
      }
    	if(req.body.banner_edit != '' && req.body.banner_edit != undefined)
      {
      	banner_edit = req.body.banner_edit;
      }
    	if(req.body.banner_delete != '' && req.body.banner_delete != undefined)
      {
      	banner_delete = req.body.banner_delete;
      }
    	if(req.body.banner_view != '' && req.body.banner_view != undefined)
      {
      	banner_view = req.body.banner_view;
      }
    	if(req.body.banner_status != '' && req.body.banner_status != undefined)
      {
      	banner_status = req.body.banner_status;
      }


    	if(req.body.category_add != '' && req.body.category_add != undefined)
      {
      	category_add = req.body.category_add;
      }
    	if(req.body.category_edit != '' && req.body.category_edit != undefined)
      {
      	category_edit = req.body.category_edit;
      }
    	if(req.body.category_delete != '' && req.body.category_delete != undefined)
      {
      	category_delete = req.body.category_delete;
      }
    	if(req.body.category_view != '' && req.body.category_view != undefined)
      {
      	category_view = req.body.category_view;
      }
    	if(req.body.category_status != '' && req.body.category_status != undefined)
      {
      	category_status = req.body.category_status;
      }


    	if(req.body.model_add != '' && req.body.model_add != undefined)
      {
      	model_add = req.body.model_add;
      }
    	if(req.body.model_edit != '' && req.body.model_edit != undefined)
      {
      	model_edit = req.body.model_edit;
      }
    	if(req.body.model_delete != '' && req.body.model_delete != undefined)
      {
      	model_delete = req.body.model_delete;
      }
    	if(req.body.model_view != '' && req.body.model_view != undefined)
      {
      	model_view = req.body.model_view;
      }
    	if(req.body.model_status != '' && req.body.model_status != undefined)
      {
      	model_status = req.body.model_status;
      }


    	if(req.body.sub_model_add != '' && req.body.sub_model_add != undefined)
      {
      	sub_model_add = req.body.sub_model_add;
      }
    	if(req.body.sub_model_edit != '' && req.body.sub_model_edit != undefined)
      {
      	sub_model_edit = req.body.sub_model_edit;
      }
    	if(req.body.sub_model_delete != '' && req.body.sub_model_delete != undefined)
      {
      	sub_model_delete = req.body.sub_model_delete;
      }
    	if(req.body.sub_model_view != '' && req.body.sub_model_view != undefined)
      {
      	sub_model_view = req.body.sub_model_view;
      }
    	if(req.body.sub_model_status != '' && req.body.sub_model_status != undefined)
      {
      	sub_model_status = req.body.sub_model_status;
      }


    	if(req.body.attribute_add != '' && req.body.attribute_add != undefined)
      {
      	attribute_add = req.body.attribute_add;
      }
    	if(req.body.attribute_edit != '' && req.body.attribute_edit != undefined)
      {
      	attribute_edit = req.body.attribute_edit;
      }
    	if(req.body.attribute_delete != '' && req.body.attribute_delete != undefined)
      {
      	attribute_delete = req.body.attribute_delete;
      }
    	if(req.body.attribute_view != '' && req.body.attribute_view != undefined)
      {
      	attribute_view = req.body.attribute_view;
      }
    	if(req.body.attribute_status != '' && req.body.attribute_status != undefined)
      {
      	attribute_status = req.body.attribute_status;
      }


    	if(req.body.color_add != '' && req.body.color_add != undefined)
      {
      	color_add = req.body.color_add;
      }
    	if(req.body.color_edit != '' && req.body.color_edit != undefined)
      {
      	color_edit = req.body.color_edit;
      }
    	if(req.body.color_delete != '' && req.body.color_delete != undefined)
      {
      	color_delete = req.body.color_delete;
      }
    	if(req.body.color_view != '' && req.body.color_view != undefined)
      {
      	color_view = req.body.color_view;
      }
    	if(req.body.color_status != '' && req.body.color_status != undefined)
      {
      	color_status = req.body.color_status;
      }


    	if(req.body.usercaradplan_add != '' && req.body.usercaradplan_add != undefined)
      {
      	usercaradplan_add = req.body.usercaradplan_add;
      }
    	if(req.body.usercaradplan_edit != '' && req.body.usercaradplan_edit != undefined)
      {
      	usercaradplan_edit = req.body.usercaradplan_edit;
      }
    	if(req.body.usercaradplan_delete != '' && req.body.usercaradplan_delete != undefined)
      {
      	usercaradplan_delete = req.body.usercaradplan_delete;
      }
    	if(req.body.usercaradplan_view != '' && req.body.usercaradplan_view != undefined)
      {
      	usercaradplan_view = req.body.usercaradplan_view;
      }
    	if(req.body.usercaradplan_status != '' && req.body.usercaradplan_status != undefined)
      {
      	usercaradplan_status = req.body.usercaradplan_status;
      }

    	if(req.body.prousercaradplan_add != '' && req.body.prousercaradplan_add != undefined)
      {
      	prousercaradplan_add = req.body.prousercaradplan_add;
      }
    	if(req.body.prousercaradplan_edit != '' && req.body.prousercaradplan_edit != undefined)
      {
      	prousercaradplan_edit = req.body.prousercaradplan_edit;
      }
    	if(req.body.prousercaradplan_delete != '' && req.body.prousercaradplan_delete != undefined)
      {
      	prousercaradplan_delete = req.body.prousercaradplan_delete;
      }
    	if(req.body.prousercaradplan_view != '' && req.body.prousercaradplan_view != undefined)
      {
      	prousercaradplan_view = req.body.prousercaradplan_view;
      }
    	if(req.body.prousercaradplan_status != '' && req.body.prousercaradplan_status != undefined)
      {
      	prousercaradplan_status = req.body.prousercaradplan_status;
      }


    	if(req.body.cartopurgent_add != '' && req.body.cartopurgent_add != undefined)
      {
      	cartopurgent_add = req.body.cartopurgent_add;
      }
    	if(req.body.cartopurgent_edit != '' && req.body.cartopurgent_edit != undefined)
      {
      	cartopurgent_edit = req.body.cartopurgent_edit;
      }
    	if(req.body.cartopurgent_delete != '' && req.body.cartopurgent_delete != undefined)
      {
      	cartopurgent_delete = req.body.cartopurgent_delete;
      }
    	if(req.body.cartopurgent_view != '' && req.body.cartopurgent_view != undefined)
      {
      	cartopurgent_view = req.body.cartopurgent_view;
      }
    	if(req.body.cartopurgent_status != '' && req.body.cartopurgent_status != undefined)
      {
      	cartopurgent_status = req.body.cartopurgent_status;
      }

    	if(req.body.accessoriesUserAd_add != '' && req.body.accessoriesUserAd_add != undefined)
      {
      	accessoriesUserAd_add = req.body.accessoriesUserAd_add;
      }
    	if(req.body.accessoriesUserAd_edit != '' && req.body.accessoriesUserAd_edit != undefined)
      {
      	accessoriesUserAd_edit = req.body.accessoriesUserAd_edit;
      }
    	if(req.body.accessoriesUserAd_delete != '' && req.body.accessoriesUserAd_delete != undefined)
      {
      	accessoriesUserAd_delete = req.body.accessoriesUserAd_delete;
      }
    	if(req.body.accessoriesUserAd_view != '' && req.body.accessoriesUserAd_view != undefined)
      {
      	accessoriesUserAd_view = req.body.accessoriesUserAd_view;
      }
    	if(req.body.accessoriesUserAd_status != '' && req.body.accessoriesUserAd_status != undefined)
      {
      	accessoriesUserAd_status = req.body.accessoriesUserAd_status;
      }


    	if(req.body.carUserAd_add != '' && req.body.carUserAd_add != undefined)
      {
      	carUserAd_add = req.body.carUserAd_add;
      }
    	if(req.body.carUserAd_edit != '' && req.body.carUserAd_edit != undefined)
      {
      	carUserAd_edit = req.body.carUserAd_edit;
      }
    	if(req.body.carUserAd_delete != '' && req.body.carUserAd_delete != undefined)
      {
      	carUserAd_delete = req.body.carUserAd_delete;
      }
    	if(req.body.carUserAd_view != '' && req.body.carUserAd_view != undefined)
      {
      	carUserAd_view = req.body.carUserAd_view;
      }
    	if(req.body.carUserAd_status != '' && req.body.carUserAd_status != undefined)
      {
      	carUserAd_status = req.body.carUserAd_status;
      }


    	if(req.body.accessoriesOrders_add != '' && req.body.accessoriesOrders_add != undefined)
      {
      	accessoriesOrders_add = req.body.accessoriesOrders_add;
      }
    	if(req.body.accessoriesOrders_edit != '' && req.body.accessoriesOrders_edit != undefined)
      {
      	accessoriesOrders_edit = req.body.accessoriesOrders_edit;
      }
    	if(req.body.accessoriesOrders_delete != '' && req.body.accessoriesOrders_delete != undefined)
      {
      	accessoriesOrders_delete = req.body.accessoriesOrders_delete;
      }
    	if(req.body.accessoriesOrders_view != '' && req.body.accessoriesOrders_view != undefined)
      {
      	accessoriesOrders_view = req.body.accessoriesOrders_view;
      }
    	if(req.body.accessoriesOrders_status != '' && req.body.accessoriesOrders_status != undefined)
      {
      	accessoriesOrders_status = req.body.accessoriesOrders_status;
      }




    	if(req.body.pushNotification_add != '' && req.body.pushNotification_add != undefined)
      {
      	pushNotification_add = req.body.pushNotification_add;
      }
    	if(req.body.pushNotification_edit != '' && req.body.pushNotification_edit != undefined)
      {
      	pushNotification_edit = req.body.pushNotification_edit;
      }
    	if(req.body.pushNotification_delete != '' && req.body.pushNotification_delete != undefined)
      {
      	pushNotification_delete = req.body.pushNotification_delete;
      }
    	if(req.body.pushNotification_view != '' && req.body.pushNotification_view != undefined)
      {
      	pushNotification_view = req.body.pushNotification_view;
      }
    	if(req.body.pushNotification_status != '' && req.body.pushNotification_status != undefined)
      {
      	pushNotification_status = req.body.pushNotification_status;
      }



    	if(req.body.emailNotification_add != '' && req.body.emailNotification_add != undefined)
      {
      	emailNotification_add = req.body.emailNotification_add;
      }
    	if(req.body.emailNotification_edit != '' && req.body.emailNotification_edit != undefined)
      {
      	emailNotification_edit = req.body.emailNotification_edit;
      }
    	if(req.body.emailNotification_delete != '' && req.body.emailNotification_delete != undefined)
      {
      	emailNotification_delete = req.body.emailNotification_delete;
      }
    	if(req.body.emailNotification_view != '' && req.body.emailNotification_view != undefined)
      {
      	emailNotification_view = req.body.emailNotification_view;
      }
    	if(req.body.emailNotification_status != '' && req.body.emailNotification_status != undefined)
      {
      	emailNotification_status = req.body.emailNotification_status;
      }


    	if(req.body.page_add != '' && req.body.page_add != undefined)
      {
      	page_add = req.body.page_add;
      }
    	if(req.body.page_edit != '' && req.body.page_edit != undefined)
      {
      	page_edit = req.body.page_edit;
      }
    	if(req.body.page_delete != '' && req.body.page_delete != undefined)
      {
      	page_delete = req.body.page_delete;
      }
    	if(req.body.page_view != '' && req.body.page_view != undefined)
      {
      	page_view = req.body.page_view;
      }
    	if(req.body.page_status != '' && req.body.page_status != undefined)
      {
      	page_status = req.body.page_status;
      }


      /*console.log(model_delete); 
      console.log(model_status);return false;*/
      UserpermissionsModel.findOne({ user_id:user_id,module_name:user_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:user_add,
              module_edit:user_edit,
              module_view:user_view,
              module_delete:user_delete,
              module_status:user_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                //var return_response = {"error":false,success: true,errorMessage:"Success"};
                //  res.status(200)
                //  .send(return_response);
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:user_module,
              module_add:user_add,
              module_edit:user_edit,
              module_view:user_view,
              module_delete:user_delete,
              module_status:user_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                //var return_response = {"error":false,success: true,errorMessage:"Success"};
               //   res.status(200)
                //  .send(return_response);
              }
            });
          }
        }
      });

      UserpermissionsModel.findOne({ user_id:user_id,module_name:banner_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:banner_add,
              module_edit:banner_edit,
              module_view:banner_view,
              module_delete:banner_delete,
              module_status:banner_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:banner_module,
              module_add:banner_add,
              module_edit:banner_edit,
              module_view:banner_view,
              module_delete:banner_delete,
              module_status:banner_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });

      UserpermissionsModel.findOne({ user_id:user_id,module_name:category_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:category_add,
              module_edit:category_edit,
              module_view:category_view,
              module_delete:category_delete,
              module_status:category_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:category_module,
              module_add:category_add,
              module_edit:category_edit,
              module_view:category_view,
              module_delete:category_delete,
              module_status:category_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });

      UserpermissionsModel.findOne({ user_id:user_id,module_name:model_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:model_add,
              module_edit:model_edit,
              module_view:model_view,
              module_delete:model_delete,
              module_status:model_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:model_module,
              module_add:model_add,
              module_edit:model_edit,
              module_view:model_view,
              module_delete:model_delete,
              module_status:model_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });

      UserpermissionsModel.findOne({ user_id:user_id,module_name:sub_model_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:sub_model_add,
              module_edit:sub_model_edit,
              module_view:sub_model_view,
              module_delete:sub_model_delete,
              module_status:sub_model_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:sub_model_module,
              module_add:sub_model_add,
              module_edit:sub_model_edit,
              module_view:sub_model_view,
              module_delete:sub_model_delete,
              module_status:sub_model_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      UserpermissionsModel.findOne({ user_id:user_id,module_name:attribute_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:attribute_add,
              module_edit:attribute_edit,
              module_view:attribute_view,
              module_delete:attribute_delete,
              module_status:attribute_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:attribute_module,
              module_add:attribute_add,
              module_edit:attribute_edit,
              module_view:attribute_view,
              module_delete:attribute_delete,
              module_status:attribute_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:color_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:color_add,
              module_edit:color_edit,
              module_view:color_view,
              module_delete:color_delete,
              module_status:color_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:color_module,
              module_add:color_add,
              module_edit:color_edit,
              module_view:color_view,
              module_delete:color_delete,
              module_status:color_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:usercaradplan_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:usercaradplan_add,
              module_edit:usercaradplan_edit,
              module_view:usercaradplan_view,
              module_delete:usercaradplan_delete,
              module_status:usercaradplan_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:usercaradplan_module,
              module_add:usercaradplan_add,
              module_edit:usercaradplan_edit,
              module_view:usercaradplan_view,
              module_delete:usercaradplan_delete,
              module_status:usercaradplan_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:prousercaradplan_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:prousercaradplan_add,
              module_edit:prousercaradplan_edit,
              module_view:prousercaradplan_view,
              module_delete:prousercaradplan_delete,
              module_status:prousercaradplan_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:prousercaradplan_module,
              module_add:prousercaradplan_add,
              module_edit:prousercaradplan_edit,
              module_view:prousercaradplan_view,
              module_delete:prousercaradplan_delete,
              module_status:prousercaradplan_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:cartopurgent_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:cartopurgent_add,
              module_edit:cartopurgent_edit,
              module_view:cartopurgent_view,
              module_delete:cartopurgent_delete,
              module_status:cartopurgent_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:cartopurgent_module,
              module_add:cartopurgent_add,
              module_edit:cartopurgent_edit,
              module_view:cartopurgent_view,
              module_delete:cartopurgent_delete,
              module_status:cartopurgent_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:accessoriesUserAd_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:accessoriesUserAd_add,
              module_edit:accessoriesUserAd_edit,
              module_view:accessoriesUserAd_view,
              module_delete:accessoriesUserAd_delete,
              module_status:accessoriesUserAd_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:accessoriesUserAd_module,
              module_add:accessoriesUserAd_add,
              module_edit:accessoriesUserAd_edit,
              module_view:accessoriesUserAd_view,
              module_delete:accessoriesUserAd_delete,
              module_status:accessoriesUserAd_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:carUserAd_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:carUserAd_add,
              module_edit:carUserAd_edit,
              module_view:carUserAd_view,
              module_delete:carUserAd_delete,
              module_status:carUserAd_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:carUserAd_module,
              module_add:carUserAd_add,
              module_edit:carUserAd_edit,
              module_view:carUserAd_view,
              module_delete:carUserAd_delete,
              module_status:carUserAd_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:accessoriesOrders_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:accessoriesOrders_add,
              module_edit:accessoriesOrders_edit,
              module_view:accessoriesOrders_view,
              module_delete:accessoriesOrders_delete,
              module_status:accessoriesOrders_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:accessoriesOrders_module,
              module_add:accessoriesOrders_add,
              module_edit:accessoriesOrders_edit,
              module_view:accessoriesOrders_view,
              module_delete:accessoriesOrders_delete,
              module_status:accessoriesOrders_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      
      UserpermissionsModel.findOne({ user_id:user_id,module_name:pushNotification_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:pushNotification_add,
              module_edit:pushNotification_edit,
              module_view:pushNotification_view,
              module_delete:pushNotification_delete,
              module_status:pushNotification_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:pushNotification_module,
              module_add:pushNotification_add,
              module_edit:pushNotification_edit,
              module_view:pushNotification_view,
              module_delete:pushNotification_delete,
              module_status:pushNotification_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      UserpermissionsModel.findOne({ user_id:user_id,module_name:emailNotification_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:emailNotification_add,
              module_edit:emailNotification_edit,
              module_view:emailNotification_view,
              module_delete:emailNotification_delete,
              module_status:emailNotification_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                 res.status(200)
                 .send(return_response);*/
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:emailNotification_module,
              module_add:emailNotification_add,
              module_edit:emailNotification_edit,
              module_view:emailNotification_view,
              module_delete:emailNotification_delete,
              module_status:emailNotification_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }
        }
      });
      UserpermissionsModel.findOne({ user_id:user_id,module_name:page_module },{ _id:1 }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log(totalcount);return false;
          if(totalcount)
          {
            //console.log(totalcount);
            //console.log(totalcount._id);
            //console.log("here");
            UserpermissionsModel.updateOne({ "_id":totalcount._id },{
              module_add:page_add,
              module_edit:page_edit,
              module_view:page_view,
              module_delete:page_delete,
              module_status:page_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Succès"};
                 res.status(200)
                 .send(return_response);
              }
            });
          }else{
            //console.log("here create");
            UserpermissionsModel.create({
              user_id:user_id,
              module_name:page_module,
              module_add:page_add,
              module_edit:page_edit,
              module_view:page_view,
              module_delete:page_delete,
              module_status:page_status
            },function(err,result){
              if(err)
              {
                //console.log(err);return false;
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Succès"};
                  res.status(200)
                  .send(return_response);
              }
            });
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  getSubAdminSingle:async function(req,res,next)
  {
    //console.log("here");
    //console.log(req.body);
    try{
     AdminModel.findOne({ _id:req.body.id }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log("countResult "+totalcount);
          var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  
  },
  getSubAdminAllPermission:async function(req,res,next)
  {
    //console.log("here");
    //console.log(req.body);
    try{
     UserpermissionsModel.find({ user_id:req.body.id }).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          //console.log("countResult "+totalcount);
          if(totalcount.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":totalcount};
            res.status(200).send(return_response);
          }
          
        }
      });
    }catch(err){
      console.log(err);
    }
  
  },
};