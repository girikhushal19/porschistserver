const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const AttributeModel = require('../../models/AttributeModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	allAccessoriesNormalUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Pièces et accessoires";
			
	    String_qr['complition_status'] = 1;
	    String_qr['delete_at'] = 0;

	    //String_qr['activation_status'] = 1;

	     
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_de_piece'] = attribute_type;
	    }
	    if(state && state != "")
	    {
	    	String_qr['state'] = state;
	    }
	    if(OEM && OEM != "")
	    {
	    	String_qr['OEM'] = OEM;
	    }
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesNormalUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_de_piece',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.body.category);
	    var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Pièces et accessoires";
			
	    String_qr['complition_status'] = 1;
	    String_qr['delete_at'] = 0;

	    //String_qr['activation_status'] = 1;

	     
	    if(modelId && modelId != "")
	    {
	    	String_qr['modelId'] = modelId;
	    }
	    if(attribute_type && attribute_type != "")
	    {
	    	String_qr['type_de_piece'] = attribute_type;
	    }
	    if(state && state != "")
	    {
	    	String_qr['state'] = state;
	    }
	    if(OEM && OEM != "")
	    {
	    	String_qr['OEM'] = OEM;
	    }
	    if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
			//mongoose.set("debug",true)
		console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'accessoires_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'state':1,'OEM':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allAccessoriesActiveNormalUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Pièces et accessoires";
	    String_qr['delete_at'] = 0;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesActiveNormalUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_de_piece',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.body.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.body;
	    var String_qr = {};

	    String_qr['user_type'] = "normal_user";
	    String_qr['category'] = "Pièces et accessoires";
	    String_qr['delete_at'] = 0;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;

	    if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }
	    if(model_id && model_id != "")
	    {
	    	String_qr['modelId'] = model_id;
	    }
	    if(fuel && fuel != "")
	    {
	    	String_qr['fuel'] = fuel;
	    }
	    if(vehicle_condition && vehicle_condition != "")
	    {
	    	String_qr['vehicle_condition'] = vehicle_condition;
	    }
	    if(type_of_gearbox && type_of_gearbox != "")
	    {
	    	String_qr['type_of_gearbox'] = type_of_gearbox;
	    }
	    if(colorId && colorId != "")
	    {
	    	String_qr['colorId'] = colorId;
	    }

	    if(attribute_air_rim && attribute_air_rim != "")
	    {
	    	String_qr['attribute_air_rim'] = attribute_air_rim;
	    }
	    if(warranty && warranty != "")
	    {
	    	String_qr['warranty'] = warranty;
	    }
	    if(maintenance_booklet && maintenance_booklet != "")
	    {
	    	String_qr['maintenance_booklet'] = maintenance_booklet;
	    }
	    if(maintenance_invoice && maintenance_invoice != "")
	    {
	    	String_qr['maintenance_invoice'] = maintenance_invoice;
	    }
	    if(accidented && accidented != "")
	    {
	    	String_qr['accidented'] = accidented;
	    }
	    if(original_paint && original_paint != "")
	    {
	    	String_qr['original_paint'] = original_paint;
	    }
	    if(matching_number && matching_number != "")
	    {
	    	String_qr['matching_number'] = matching_number;
	    }
	    if(matching_color_paint && matching_color_paint != "")
	    {
	    	String_qr['matching_color_paint'] = matching_color_paint;
	    }
	    if(matching_color_interior && matching_color_interior != "")
	    {
	    	String_qr['matching_color_interior'] = matching_color_interior;
	    }
	    if(priceMin && priceMin != ""  && priceMax != ""  && priceMax != "" )
	    {
	    	String_qr['price'] = {  $gte:req.body.priceMin, $lte: req.body.priceMax } 
	    }

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'accessoires_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'state':1,'OEM':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).sort({"natural":-1}).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allAccessoriesProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	    //String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    //String_qr['show_on_web'] = 1;
			var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;

			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_de_piece',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.body.category);
	    //var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.body;
	    var String_qr = {};
			
			var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;

			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	    //String_qr['payment_status'] = 0;
	    String_qr['complition_status'] = 1;
	    //String_qr['show_on_web'] = 1;
	    //String_qr['activation_status'] = 1;

	    
				//mongoose.set("debug",true);
				//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'accessoires_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'state':1,'OEM':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allAccessoriesActiveProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
			var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;


			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	     
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;

	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesActiveProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_de_piece',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.body.category);
	    var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;

	    var String_qr = {};

			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 1;
	    String_qr['activation_status'] = 1;

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'accessoires_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'state':1,'OEM':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allAccessoriesInActiveProUserCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
			var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;


			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}
	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	     
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	   // String_qr['activation_status'] = 1;

	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       AddAdvertisementModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesInActiveProUser:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [{
	        path:'userId',
	        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'type_de_piece',
		        select:['attribute_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }];

	    //console.log(req.body);
	    //console.log(req.body.category);
	    var { title,modelId,attribute_type,state,OEM,maxPrice,minPrice } = req.body;

	    var String_qr = {};

			if(modelId && modelId != "")
			{
				String_qr['modelId'] = modelId;
			}
			if(attribute_type && attribute_type != "")
			{
				String_qr['type_de_piece'] = attribute_type;
			}
			if(state && state != "")
			{
				String_qr['state'] = state;
			}
			if(OEM && OEM != "")
			{
				String_qr['OEM'] = OEM;
			}
			if(minPrice && minPrice != "")
			{
				String_qr['price'] = {  $gte:minPrice } 
			}
			if(maxPrice  && maxPrice != "" )
			{
				String_qr['price'] = {  $lte: maxPrice } 
			}
			if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
			{
				String_qr['price'] = { $lte:maxPrice , $gte:minPrice }
			}
			if(title && title != "")
			{
				String_qr['ad_name'] = {'$regex' : '^'+title, '$options' : 'i'}
			}

	    String_qr['user_type'] = "pro_user";
	    String_qr['category'] = "Pièces et accessoires";
	    //String_qr['payment_status'] = 1;
	    String_qr['complition_status'] = 1;
	    String_qr['show_on_web'] = 0;
	    //String_qr['activation_status'] = 1;

		//console.log(String_qr);
      	AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'accessoires_image':1,'price':1, 'pro_price':1, 'userId':1, 'created_at':1, 'contact_number':1, 'address':1,'registration_year':1,'state':1,'OEM':1,'delivery_price':1,'vat_tax':1,'deductible_VAT':1,'activation_status':1,'payment_status':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	getAttrTypeDePiece:async function(req,res,next)
	{
    try{
      const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      AttributeModel.find({parent_id:"62e4f5a35ed7e4f294719634"}, {}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Dossier",
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Dossier",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
	},
	getAttrTypeDeChassis:async function(req,res,next)
	{
    try{
      const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      AttributeModel.find({parent_id:"62e3d257ec6f493144a2533a"}, {}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
	},
  registration_yearAdmin:async function(req,res,next)
  {
      //console.log("get profile");
    try{
    	arr = {};
    	var current_year = new Date().getFullYear();
    	var after_thirty_year = current_year - 75;
    	//console.log(current_year);
    	//console.log("after_thirty_year "+after_thirty_year);
    	var x=0;
			for(let i=current_year; i>=after_thirty_year; i--)
    	{
    		arr[x] = i;
    		x++;
    	}
    	//console.log(arr);

    	res.status(200)
	        .send({
	            error: false,
	            success: true,
	            errorMessage: "Enregistrement de l'année",
	            record:arr,
	        });
    }catch(err){
        console.log(err);
    }
  },
	updateAdStatusApi:async(req,res)=>{
		try{
			var {id,status} = req.body;
			await AddAdvertisementModel.updateOne({_id:id},{activation_status:status}).then((result)=>{
				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "Enregistrement complet des succès actualisés",
				});
			}).catch((error)=>{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: error.message,
				});
			});
		}catch(error)
		{
			return res.status(200)
			.send({
					error: true,
					success: false,
					errorMessage: error.message,
			});
		}
	}
};