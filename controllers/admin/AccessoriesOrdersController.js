const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const OrderModel = require("../../models/OrderModel");
const SellerEarningsModel = require("../../models/SellerEarningsModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {


	allAccessoriesOrdersCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['payment_status'] = 1;
			if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       OrderModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesOrdersApi:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [
    		{
		        path:'user_id',
		        select:['firstName','lastName']
			},
			{
		        path:'ad_owner_user_id',
		        select:['firstName','lastName']
			}
		    ];
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
	    var String_qr = {};

	    String_qr['payment_status'] = 1;
	    //String_qr['activation_status'] = 1;

	    if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    

		//console.log(String_qr);
      	OrderModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},

	allAccessoriesOrdersProcessCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 1;
			if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       OrderModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesOrdersProcessApi:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [
    		{
		        path:'user_id',
		        select:['firstName','lastName']
			},
			{
		        path:'ad_owner_user_id',
		        select:['firstName','lastName']
			}
		    ];
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
	    var String_qr = {};

	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 1;
	    //String_qr['activation_status'] = 1;

	    if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    

		//console.log(String_qr);
      	OrderModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},

	allAccessoriesOrdersCancelCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 2;
			if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       OrderModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesOrdersCancelApi:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [
    		{
		        path:'user_id',
		        select:['firstName','lastName']
			},
			{
		        path:'ad_owner_user_id',
		        select:['firstName','lastName']
			}
		    ];
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
	    var String_qr = {};

	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 2;
	    //String_qr['activation_status'] = 1;

	    if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    

		//console.log(String_qr);
      	OrderModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allAccessoriesOrdersCompleteCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    var String_qr = {};
	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 3;
			if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    //String_qr['activation_status'] = 1;
	    /*if(category && category != "")
	    {
	    	String_qr['category'] = category;
	    }*/
	    try{
	       OrderModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	allAccessoriesOrdersCompleteApi:async function(req,res,next)
	{
		//mongoose.set('debug', true);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
    	const queryJoin = [
    		{
		        path:'user_id',
		        select:['firstName','lastName']
			},
			{
		        path:'ad_owner_user_id',
		        select:['firstName','lastName']
			}
		    ];
			var user_id = req.body.user_id;
			var owner_id = req.body.owner_id;
	    //console.log(req.body);
	    //console.log(req.query.category);
	    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
	    var String_qr = {};

	    String_qr['payment_status'] = 1;
	    String_qr['shipping_status'] = 3;
	    //String_qr['activation_status'] = 1;

	    if(user_id && user_id != "")
	    {
	    	String_qr['user_id'] = user_id;
	    }
	    if(owner_id && owner_id != "")
	    {
	    	String_qr['ad_owner_user_id'] = owner_id;
	    }
	    

		//console.log(String_qr);
      	OrderModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
  		{
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+JSON.stringify(result));
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"Succès","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	    });
	    }catch(err){
	      console.log(err);
	    }
  
	},
	allOrderIncompleteInvoice:async function(req,res,next)
	{
		try{
			let {seller_id} = req.body;
			if(!seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requis",
        });
      }

			// shipping_status: 3 , is_paid
			//mongoose.set("debug",true);
			let result = await OrderModel.aggregate([
				{
					$match:{ ad_owner_user_id: mongoose.Types.ObjectId(seller_id)}
				},
				{
					$match:{shipping_status:3}
				},
				{
					$match:
					{
						is_paid:0
					}
				},
				{ 
					$group : 
					{ 
						_id : "$order_id",
						"firstName":{"$first":"$firstName"},
						"lastName":{"$first":"$lastName"},
						"email":{"$first":"$email"},
						"mobileNumber":{"$first":"$mobileNumber"},
						"final_price":{"$first":"$final_price"},
						//"ad_name":{"$first":"$ad_name"},
						"order_created_at":{"$first":"$order_created_at"}
					}
				}
				
			]);
			if(result.length > 0)
			{
				var return_response = {"error":false,errorMessage:"Succès","record":result};
				res.status(200).send(return_response);
			}else{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
				res.status(200).send(return_response);
			}
		}catch(e){
			//console.log(e);
			var return_response = {"error":true,errorMessage:e.message};
			return 	res.status(200).send(return_response);
		}
	},
	generateInvoice:async function(req,res,next)
	{
		try{
			//console.log(req.body);return false;
			let {seller_id,order_id} = req.body;
			//mongoose.set("debug",true);
			if(!seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requis",
        });
      }
			if(!order_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de la commande est requis",
        });
      }
			let result = await OrderModel.aggregate([
				{ 
					$match : 
					{ 
						order_id: {$in : order_id } 
					}
				},
				{
					$match:{shipping_status:3}
				},
				{
					$match:
					{
						is_paid:0
					}
				},
				{
					 $group : 
					 { 
							_id : "$order_id",
							"final_price":{"$first":"$final_price"}
						} 
				}
			]);
			if(result.length > 0)
			{
				let final_amount = 0;
				result.forEach((val)=>{
					final_amount = final_amount + val.final_price;
				});

				let valOfCreate = await SellerEarningsModel.create({
					seller_id:seller_id,
					order_id:order_id,
					amount:final_amount
				});
				if(valOfCreate)
				{
					await OrderModel.updateMany({order_id:{$in:order_id}},{is_paid:1}).then((result)=>{ 
						return res.status(200).send({"error":false,errorMessage:"Succès du fonds transféré dans son intégralité"});
					}).catch((err)=>{
						return res.status(200).send({"error":true,errorMessage:"Quelque chose n'a pas fonctionné"});
					})
				}else{
					return res.status(200).send({"error":true,errorMessage:"Quelque chose n'a pas fonctionné"});
				}
			}else{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
				res.status(200).send(return_response);
			}
		}catch(e){
			//console.log(e);
			var return_response = {"error":true,errorMessage:e.message};
			return	res.status(200).send(return_response);
		}
	},
	getSellerEarning:async function(req,res,next)
	{
		try{
			//console.log(req.body);
			let {seller_id} = req.body;
			//mongoose.set("debug",true);
			if(!seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requis",
        });
      }
			let result = await SellerEarningsModel.aggregate([
				{ 
					$match : 
					{ 
						seller_id: seller_id
					}
				},
				{
					$sort:{
						"created_at":-1
					}
				}
			]);
			if(result.length > 0)
			{
				return res.status(200).send({"error":false,errorMessage:"Succès",record:result});
			}else{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
				res.status(200).send(return_response);
			}
		}catch(e){
			//console.log(e);
			var return_response = {"error":true,errorMessage:e.message};
			return	res.status(200).send(return_response);
		}
	},
	getSellerEarningMultipleOrder:async function(req,res,next)
	{
		try{
			//console.log(req.body);
			let {id} = req.body;
			//mongoose.set("debug",true);
			if(!id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant est requis",
        });
      }
			//mongoose.set("debug",true);
			let first_re = await SellerEarningsModel.findOne({_id:id},{order_id:1});
			if(!first_re)
			{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":[]};
				return res.status(200).send(return_response);
			}
			let order_id = first_re.order_id;
			let result = await SellerEarningsModel.aggregate([
				{ 
					$match : 
					{ 
						_id: mongoose.Types.ObjectId(id)
					}
				},
				{
					$lookup:{
						from:"orders",
						pipeline:[
							{
								$match:{
									"order_id":{$in:order_id}
								}
							}
						],
						as:"data"
					}
				}
			]);
			if(result.length > 0)
			{
				return res.status(200).send({"error":false,errorMessage:"Succès",record:result});
			}else{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
				res.status(200).send(return_response);
			}
		}catch(e){
			//console.log(e);
			var return_response = {"error":true,errorMessage:e.message};
			return	res.status(200).send(return_response);
		}
	},
	allPaidInvoice:async function(req,res,next)
	{
		try{
			let {seller_id} = req.body;
			if(!seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requis",
        });
      }

			// shipping_status: 3 , is_paid
			//mongoose.set("debug",true);
			let result = await OrderModel.aggregate([
				{
					$match:{ ad_owner_user_id: mongoose.Types.ObjectId(seller_id)}
				},
				{
					$match:{shipping_status:3}
				},
				{
					$match:
					{
						is_paid:1
					}
				}
				
			]);
			if(result.length > 0)
			{
				var return_response = {"error":false,errorMessage:"Succès","record":result};
				res.status(200).send(return_response);
			}else{
				var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
				res.status(200).send(return_response);
			}
		}catch(e){
			//console.log(e);
			var return_response = {"error":true,errorMessage:e.message};
			return	res.status(200).send(return_response);
		}
	}
};