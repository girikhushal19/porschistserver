const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const EmailNotificationModel = require('../../models/EmailNotificationModel');
const SettingModel = require('../../models/SettingModel');
const PageModel = require('../../models/PageModel');
const UserfaqModel = require("../../models/UserfaqModel");
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
const OptionsModel = require('../../models/OptionsModel');
const HomeTitleModel = require('../../models/HomeTitleModel');
const VersionsModel = require('../../models/VersionsModel');
const CountryModel = require('../../models/CountryModel');

const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
 
const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.porschists.com',
    port: 465,
    auth: {
       user: 'contact@porschists.com',
       pass: 'OD-oeu&mMbU%'
    }
});

/*/*/
module.exports = {

  sendPushNotificationSubmit:async function(req,res,next)
  {
      try{
        /*console.log(req.files);
        console.log(req.files[0]);
        console.log(req.files[1]);*/
        //console.log(req.body);
      	var title = req.body.title; 	
      	var description = req.body.description; 	
      	/*console.log(title);
      	console.log(description);
      	return false;*/

        var String_qr = {status:1,deviceToken: { $ne: null }};
  	    UsersModel.find( String_qr , {deviceToken:1}).populate().lean(true).exec((err, result)=>
  	    {
  	        if(err)
  	        {
  	          console.log(err);
  	        }else{
              if(result.length > 0)
              {
                for (var i = 0; i < result.length; i++)
                {
                  //console.log(result[i].deviceToken);
                   
                }
                PushNotificationModel.create({
                    title:title,
                    description:description,
                  },function(err,result){
                   if(err)
                   {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                  }else{
                      var return_response = {"error":false,success: true,errorMessage:"Succès"};
                        res.status(200)
                        .send(return_response);
                  }
                });
              }else{
                  PushNotificationModel.create({
                    title:title,
                    description:description,
                  },function(err,result){
                   if(err)
                   {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                  }else{
                      var return_response = {"error":false,success: true,errorMessage:"Succès"};
                        res.status(200)
                        .send(return_response);
                  }
                });
              }
  	        }
  	    });
      }catch (err) {
          console.log(err);
      }
  },

  allPushNotification:async function(req,res,next)
  {
      var xx = 0;
      var total = 0;
      var perPageRecord = 10;
      var fromindex = 0;
      if(req.body.numofpage == 0)
      {
        fromindex = 0;
      }else{
        fromindex = perPageRecord * req.body.numofpage
      }
      try{


        PushNotificationModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
        {
          if(err)
          {
            console.log(err);
          }else{
            //console.log("result ");
            //console.log(typeof result);
            //console.log("result"+result);
            if(result.length > 0)
            {
               var return_response = {"error":false,errorMessage:"Succès","record":result};
                    res.status(200).send(return_response);
            }else{
              var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
              res.status(200).send(return_response);
            }

          }
        });

      }catch(err){
        console.log(err);
      }
    },
  allPushNotificationCount:async function(req,res,next)
  {
      let total = 0;
      var totalPageNumber = 1;
      var perPageRecord = 10;
      //console.log("here");
      //console.log(req.body);
      try{
         PushNotificationModel.countDocuments({  }).exec((err, totalcount)=>
          {
            //console.log("countResult "+totalcount);
            if(err)
            {
              console.log(err);
            }else{
              total = totalcount;
              totalPageNumber = total / perPageRecord;
              //console.log("totalPageNumber"+totalPageNumber);
              totalPageNumber = Math.ceil(totalPageNumber);
              //console.log("totalPageNumber"+totalPageNumber);
              //console.log("totalcount new fun"+totalcount);
              //console.log("all record count "+totalcount);
              var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
              res.status(200).send(return_response);
            }
          });
         
      }catch(err){
        console.log(err);
      }
  },
  deletePushNotification: async function(req, res,next)
  {
      PushNotificationModel.deleteOne({
          _id:req.body.id
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
  },

  sendEmailNotificationSubmit:async function(req,res,next)
  {
    try{
      /*console.log(req.files);
      console.log(req.files[0]);
      console.log(req.files[1]);*/
      //console.log(req.body);
      var title = req.body.title;   
      var description = req.body.description;   
      /*console.log(title);
      console.log(description);
      return false;*/
      var error_have = 0;
      var String_qr = {status:1};
      UsersModel.find( String_qr , {email:1}).populate().lean(true).exec((err, result)=>
      {
          if(err)
          {
            console.log(err);
          }else{
            if(result.length > 0)
            {
              for (var i = 0; i < result.length; i++)
              {
                //console.log(result[i].email);
                const message = {
                    from: 'contact@porschists.com', // Sender address
                    to: result[i].email,         // recipients
                    subject: title, // Subject line
                    text: description // Plain text body
                };
                transport.sendMail(message, function(err, info) {
                    if (err) {
                      console.log(err);
                      error_have = 1;
                    } else {
                      //console.log('mail has sent.');
                      //console.log(info);
                    }
                });
                 
              }
              if(error_have == 1)
              {
                var return_response = {"error":true,success: false,errorMessage:"Une erreur dans le processus d'envoi de l'email"};
                      res.status(200)
                      .send(return_response);
              }
              EmailNotificationModel.create({
                  title:title,
                  description:description,
                },function(err,result){
                 if(err)
                 {
                    var return_response = {"error":true,success: false,errorMessage:err};
                      res.status(200)
                      .send(return_response);
                }else{
                    var return_response = {"error":false,success: true,errorMessage:"Succès"};
                      res.status(200)
                      .send(return_response);
                }
              });
            }else{
                EmailNotificationModel.create({
                  title:title,
                  description:description,
                },function(err,result){
                 if(err)
                 {
                    var return_response = {"error":true,success: false,errorMessage:err};
                      res.status(200)
                      .send(return_response);
                }else{
                    var return_response = {"error":false,success: true,errorMessage:"Succès"};
                      res.status(200)
                      .send(return_response);
                }
              });
            }
          }
      });
    }catch (err) {
        console.log(err);
    }
  },
  allEmailNotification:async function(req,res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      EmailNotificationModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  allEmailNotificationCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       EmailNotificationModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
      });
    }catch(err){
      console.log(err);
    }
  },
  deleteEmailNotification: async function(req, res,next)
  {
    EmailNotificationModel.deleteOne({
        _id:req.body.id
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
    });
  },

  settingSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {tax_percent,access_pur_tax_percent } = req.body;
      var taxResult = await SettingModel.findOne({attribute_key:"tax_percent"},{_id:1});
      await SettingModel.findOneAndUpdate({ attribute_key:"access_pur_tax_percent"  },{
        attribute_value:access_pur_tax_percent
      });
      if(taxResult)
      {
        await SettingModel.findOneAndUpdate({ _id:taxResult._id  },{
          attribute_key:"tax_percent",
          attribute_value:tax_percent
        }).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }else{
        await SettingModel.create({
          attribute_key:"tax_percent",
          attribute_value:tax_percent
        }).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  getSetting:async function(req,res,next)
  {
    SettingModel.find({  }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  getWebTax:async function(req,res,next)
  {
    try{
      SettingModel.findOne({ attribute_key:"tax_percent" }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"success","result":result};
                res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: true,errorMessage:error.message};
                res.status(200)
                .send(return_response);
    }
    
  },
  
  testEmailSend:async function(req,res,next)
  {
    console.log('sending email..');
        const message = {
        from: 'contact@porschists.com', // Sender address
        to: 'khushal789@mailinator.com',         // recipients
        subject: 'test mail from Nodejs', // Subject line
        text: 'Successfully! received mail using nodejs' // Plain text body
    };
    transport.sendMail(message, function(err, info) {
        if (err) {
          console.log(err)
        } else {
          console.log('mail has sent.');
          console.log(info);
        }
    });
  },
  getAboutUs:async(req,res)=>{
    try{
      await PageModel.findOne({slug_name:"about_us"}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",data:result};
        res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200).send(return_response);
    }
  },
  getTermsCondition:async(req,res)=>{
    try{
      await PageModel.findOne({slug_name:"terms_condition"}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",data:result};
        res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200).send(return_response);
    }
  },
  getPrivacyPolicy:async(req,res)=>{
    try{
      await PageModel.findOne({slug_name:"about_us"}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",data:result};
        res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200).send(return_response);
    }
  },
  getcontactUs:async(req,res)=>{
    try{
      await PageModel.findOne({slug_name:"contact_us"}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",data:result};
        res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200).send(return_response);
    }
  },

  getUserFaq:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      await UserfaqModel.find({ status:1 }, {question:1,answer:1}).then((result)=>
      {
        if(result.length > 0)
        {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                res.status(200).send(return_response);
        }else{
          var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }
      }).catch((error)=>{
        var return_response = {"error":true,errorMessage:error.message };
          res.status(200).send(return_response);
      });
    }catch(err){
      console.log(err);
    }
  },

  getSingleStripe:async function(req,res,next)
  {
    StripeModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  editStripeSubmit:async function(req,res,next)
  {
    try{
      StripeModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          stripe_key:req.body.stripe_key,
          stripe_secret:req.body.stripe_secret,
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getSinglePaypal:async function(req,res,next)
  {
    PaypalModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  editPaypalSubmit:async function(req,res,next)
  {
    try{
      PaypalModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          client_id:req.body.client_id,
          client_secret:req.body.client_secret,
          mode:req.body.mode,
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  addOptions:async(req,res)=>{
    try{
      let {title} = req.body;
      await OptionsModel.create({title}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Option ajoutée succès complet"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  allOptions:async(req,res)=>{
    try{ 
      await OptionsModel.find({}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  getOptions:async(req,res)=>{
    try{ 
      let id = req.params.id;
      await OptionsModel.findOne({_id:id}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  editOptions:async(req,res)=>{
    try{
      let {title,id} = req.body;
      await OptionsModel.updateOne({_id:id},{title}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Option ajoutée succès complet"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },


  addVersions:async(req,res)=>{
    try{
      let {title,model_id,version} = req.body;
      await VersionsModel.create({title,model_id,version}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"L'enregistrement de la version a été ajouté avec succès"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },

  allVersions:async(req,res)=>{
    try{ 
      await VersionsModel.find({}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  getVersions:async(req,res)=>{
    try{ 
      let id = req.params.id;
      await VersionsModel.findOne({_id:id}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  editVersions:async(req,res)=>{
    try{
      let {title,id,model_id,version} = req.body;
      await VersionsModel.updateOne({_id:id},{title,model_id,version}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Option ajoutée succès complet"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },


  addCountry:async(req,res)=>{
    try{
      let {title,tax} = req.body;
      await CountryModel.create({title:title,tax:tax}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Pays ajouté succès pleinement"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  allCountry:async(req,res)=>{
    try{ 
      await CountryModel.find({}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  getCountry:async(req,res)=>{
    try{ 
      let id = req.params.id;
      await CountryModel.findOne({_id:id}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  editCountry:async(req,res)=>{
    try{
      // console.log(req.body);
      // return false;
      let {title,tax,id} = req.body;
      await CountryModel.updateOne({_id:id},{title:title,tax:tax}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Enregistrement complet des succès actualisés"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  getHomeTitle:async(req,res)=>{
    try{ 
      //let id = req.params.id;
      await HomeTitleModel.findOne({ }).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  edittHomeTitle:async(req,res)=>{
    try{
      let {title,id} = req.body;
      if(!id)
      {
        await HomeTitleModel.create({title}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Titre de la maison mis à jour succès entièrement"};
          return res.status(200).send(return_response);
        }).catch((e)=>{
          var return_response = {"error":true,success: false,errorMessage:e.message};
          return res.status(200).send(return_response);
        });
      }else{
        await HomeTitleModel.updateOne({_id:id},{title}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Titre de la maison mis à jour succès entièrement"};
          return res.status(200).send(return_response);
        }).catch((e)=>{
          var return_response = {"error":true,success: false,errorMessage:e.message};
          return res.status(200).send(return_response);
        });
      }
      
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
};