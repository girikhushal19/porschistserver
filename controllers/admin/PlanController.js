const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const ProUserPlanTopSearchModel = require("../../models/ProUserPlanTopSearchModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  addPlanNormalUser:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      var { day_number , price } = req.body;
          UserPlanModel.count({day_number:day_number,price:price},(errs, allReadyLike)=>
          {
            //console.log("allReadyLike"+allReadyLike);
            if(allReadyLike == 0)
            {
              UserPlanModel.create({
              user_type:"normal_user",
              day_number,
              price
            });
            var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
            res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
                res.status(200).send(return_response);
            }
          });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  editPlanNormalUser:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      var { day_number , price ,edit_id} = req.body;
      UserPlanModel.updateOne({ "_id":req.body.edit_id }, 
        {day_number:day_number,price:price}, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }
        else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement mis à jour avec succès"
            });
          }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  getSinglePlanNormalUser: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.edit_id;
      UserPlanModel.find({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result.length > 0)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement",
                  record:{}
              });
          }
        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allPlanNormalUserCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       UserPlanModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allPlanNormalUser:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      UserPlanModel.find({  }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  addPlanProUser:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      var { top_price,day_number,maximum_upload , price,plan_type } = req.body;
          ProUserPlanModel.count({top_price:top_price,day_number:day_number,maximum_upload:maximum_upload,price:price},(errs, allReadyLike)=>
          {
            //console.log("allReadyLike"+allReadyLike);
            if(allReadyLike == 0)
            {
              ProUserPlanModel.create({
                plan_type:plan_type,
              user_type:"pro_user",
              maximum_upload,
              day_number,
              price,
              top_price
            });
            var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
            res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
                res.status(200).send(return_response);
            }
          });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  editPlanProUser:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      var { top_price,day_number,maximum_upload , price,edit_id } = req.body;
      ProUserPlanModel.findByIdAndUpdate({ _id: edit_id },
        { 
          top_price:top_price,
          day_number:day_number,
          maximum_upload:maximum_upload,
          price:price
        },
         function (err, user) {
      if (err) {
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Quelque chose s'est mal passé",
              errorOrignalMessage: err
          });
      } else {
        //console.log("Record updated success-fully");
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Enregistrement mis à jour avec succès"
          });
      }
     });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allPlanProUserCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    var plan_type = req.body.plan_type;
    try{
       ProUserPlanModel.countDocuments({ plan_type:plan_type }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allPlanProUser:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      var plan_type = req.body.plan_type;
      let result = await ProUserPlanModel.find({ plan_type:plan_type }, {});
      if(result.length >0)
      {
        var return_response = {"error":false,errorMessage:"Succès","record":result};
      }else{
        var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
      }
      return res.status(200).send(return_response);
    }catch(err){
      console.log(err);
    }
  },
  getSinglePlanProUser: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.edit_id;
      ProUserPlanModel.find({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result.length > 0)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement",
                  record:{}
              });
          }
        }
      });

    }catch(err){
      console.log(err);
    }
  },
  addTopUrgentPlan:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { price,day_number } = req.body;
      if(req.body.old_id)
      {
        TopUrgentPlan.findOneAndUpdate({ _id:req.body.old_id },{
          user_type:"normal_user",
          day_number:day_number,
          price
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        TopUrgentPlan.create({
          user_type:"normal_user",
          day_number:day_number,
          price
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            //console.log(result);
            //console.log(result._id);
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      } 
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allTopUrgentPlan:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          TopUrgentPlan.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"record",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200)
            .send(return_response);
    }
  },
  allTopUrgentPlanAccessories:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      TopUrgentAccessoriesPlanModel.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"record",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200)
            .send(return_response);
    }
  },
  addAccessoriesPlanNormalUser:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var { photo_number , price } = req.body;
      UseraccessoriesplanModel.count({photo_number:photo_number,price:price},(errs, allReadyLike)=>
      {
        //console.log("allReadyLike"+allReadyLike);
        if(allReadyLike == 0)
        {
          UseraccessoriesplanModel.create({
            user_type:"normal_user",
            photo_number,
            price
          },function(err,result){
            if(err){
              //console.log(err);
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
                res.status(200)
                .send(return_response);
            }
          });
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
            res.status(200).send(return_response);
        }
      });
    }catch(error){
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allAccessoriesPlanNormalUser:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      UseraccessoriesplanModel.find({  }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  getSingleAccessoriesPlanNormalUser: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.edit_id;
      UseraccessoriesplanModel.findOne({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement"
            });
          }
        }
      });

    }catch(err){
      console.log(err);
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: err
      });
    }
  },
  editAccessoriesPlanNormalUser:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var { photo_number , price,edit_id } = req.body;
      UseraccessoriesplanModel.updateOne({_id:edit_id},{
        user_type:"normal_user",
        photo_number,
        price
      },function(err,result){
        if(err){
          //console.log(err);
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch(error){
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  addTopUrgentPlanAccessories:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { price,day_number } = req.body;
      if(req.body.old_id)
      {
        TopUrgentAccessoriesPlanModel.findOneAndUpdate({ _id:req.body.old_id },{
          user_type:"normal_user",
          day_number:day_number,
          price
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        TopUrgentAccessoriesPlanModel.create({
          user_type:"normal_user",
          day_number:day_number,
          price
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            //console.log(result);
            //console.log(result._id);
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      } 
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  addPlanProUserTopSearch:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { top_price,day_number,maximum_upload , price,plan_type } = req.body;

      ProUserPlanModel.findOne({_id:maximum_upload},{day_number:1,maximum_upload:1}).exec((err, record)=>{
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200).send(return_response);
        }else{
          console.log(record);
          if(record)
          {
            ProUserPlanTopSearchModel.count({top_price:top_price,day_number:record.day_number,maximum_upload:record.maximum_upload,price:price},(errs, allReadyLike)=>
            {
              //console.log("allReadyLike"+allReadyLike);
              if(allReadyLike == 0)
              {
                ProUserPlanTopSearchModel.create({
                  plan_type:plan_type,
                  user_type:"pro_user",
                  maximum_upload:record.maximum_upload,
                  day_number:record.day_number,
                  price,
                  top_price
                  },function(errCreate,resultCreate){
                    if(errCreate)
                    {
                      var return_response = {"error":true,success: false,errorMessage:errCreate};
                      res.status(200).send(return_response);
                    }else{
                      var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
                      res.status(200)
                      .send(return_response);
                    }
                  });
              }else{
                var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
                  res.status(200).send(return_response);
              }
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Quelque chose a mal tourné"};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allPlanProUserTopSearchCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      var plan_type = req.body.plan_type;
      ProUserPlanTopSearchModel.countDocuments({ plan_type:plan_type }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allPlanProUserTopSearch:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    var plan_type = req.body.plan_type;
    try{
      ProUserPlanTopSearchModel.find({ plan_type:plan_type }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  webProUserSingleTopSearch:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
   
    try{
      var id = req.body.id;
      ProUserPlanTopSearchModel.findOne({ _id:id  }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  editPlanProUserTopSearch:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { top_price ,edit_id} = req.body;

      ProUserPlanTopSearchModel.findOne({_id:edit_id},{day_number:1,maximum_upload:1}).exec((err, record)=>{
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200).send(return_response);
        }else{
          console.log(record);
          if(record)
          {
            ProUserPlanTopSearchModel.updateOne({_id:edit_id},{
              top_price:top_price
              },function(errCreate,resultCreate){
                if(errCreate)
                {
                  var return_response = {"error":true,success: false,errorMessage:errCreate};
                  res.status(200).send(return_response);
                }else{
                  var return_response = {"error":false,success: true,errorMessage:"Enregistrement complet des succès mis à jour"};
                  res.status(200)
                  .send(return_response);
                }
              });
              
            
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Quelque chose a mal tourné"};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allPlanProUserAccess:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var plan_type = req.body.plan_type;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ProUserPlanModel.find({ plan_type:plan_type }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
};