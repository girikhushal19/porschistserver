const express = require('express');
//const mongoose = require("mongoose");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express(); 
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');
const VersionsModel = require("../../models/VersionsModel");

/*/*/
module.exports = {
	addModelsSubmit: async function(req, res,next)
  {
  	//console.log("all data");
  	//console.log(req.body);
    //console.log(req.body.all_text_data);
  	try{
  		var { parent_id,model_name,attribute_type  } = req.body;
      //var fileImage = req.file.modelFile;
      //console.log(req.file.filename);
      var fileImage = null;
      var black_white_image = null;
      if(req.files)
      {
        if(req.files.file)
        {
          if(req.files.file.length > 0)
          {
            var fileImage = req.files.file[0].filename;
            
          }else{
            var fileImage = null;
          }
        }else{
            var fileImage = null;
        }
      }else{
        var fileImage = null;
      }
      
      if(req.files)
      {
        if(req.files.file2)
        {
          console.log("file2");
          console.log(req.files.file2);
          if(req.files.file2.length > 0)
          {
            console.log(req.files.file2[0].filename);
            var black_white_image = req.files.file2[0].filename;
            
          }else{
            var black_white_image = null;
          }
        }else{
            var black_white_image = null;
        }
      }else{
        var black_white_image = null;
      }
      //console.log("model_name"+model_name);
      //console.log("parent_id"+parent_id);
      if(parent_id == "" || parent_id == "null")
      {
        //console.log("here if condition");
        parent_id = null;
      }
      ModelsModel.count({model_name,attribute_type,parent_id},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount == 0)
          {
              ModelsModel.create({
                parent_id,
                attribute_type,
                fileImage,
                model_name,
                black_white_image
              });
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Modèles ajoutés avec succès"
                });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Les modèles sont prêts à être utilisés"
              });
          }
        }
      })
  	}catch(err){
  		console.log(err);
  	}
  },
  editModelsSubmit: async function(req, res,next)
  {
    //console.log(req.file);
    console.log(req.files);
    //return false;
    /*console.log("all data");
    console.log(req.body);
    console.log(req.file);*/
    var black_white_image = null;
    var { model_name,parent_id,attribute_type,edit_id } = req.body;
    try{
      if(!parent_id)
      {
        parent_id = null;
      }
      allModels = await ModelsModel.find({ _id: edit_id });
      //console.log(allModels);
      //console.log(allModels[0].fileImage);
      if(req.files.file)
      {
        if(req.files.file.length > 0)
        {
          var fileImage = req.files.file[0].filename;
          var oldFileName = allModels[0].fileImage;
          var uploadDir = './public/uploads/models/';
          let fileNameWithPath = uploadDir + oldFileName;
          //console.log(fileNameWithPath);

          if (fs.existsSync(fileNameWithPath))
          {
            fs.unlink(uploadDir + oldFileName, (err) => 
            {
              console.log("unlink file error "+err);
            });
          }
        }
      }else{
        //console.log(allModels);return false;
        if(allModels[0].fileImage)
        {
          var fileImage = allModels[0].fileImage;
        }else{
          var fileImage = null;
        }
      }

      if(req.files.file2)
      {
        console.log("file2");
        console.log(req.files.file2);
        if(req.files.file2.length > 0)
        {
          console.log(req.files.file2[0].filename);
          var black_white_image = req.files.file2[0].filename;
          var oldFileName2 = allModels[0].black_white_image;
          var uploadDir = './public/uploads/models/';
          let fileNameWithPath = uploadDir + oldFileName2;
          console.log("fileNameWithPath");
          console.log(fileNameWithPath);
          if (fs.existsSync(fileNameWithPath))
          {
            fs.unlink(uploadDir + oldFileName, (err) => 
            {
              console.log("unlink file error "+err);
            });
          }
        }
      }else{
        //console.log(allModels);return false;
        if(allModels[0].black_white_image)
        {
          var black_white_image = allModels[0].black_white_image;
        }else{
          var black_white_image = null;
        }
      }
      //return false;
      //mongoose.set("debug",true);
      ModelsModel.findByIdAndUpdate({ _id: edit_id },
        { parent_id,
                attribute_type,
                fileImage,
                black_white_image,
                model_name },
         function (err, user) {
      if (err) {
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Un problème est survenu",
              errorOrignalMessage: err
          });
      } else {
        //console.log("Record updated success-fully");
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Enregistrement mis à jour avec succès"
          });
      }
     });
    }catch(err){
      console.log(err);
    }
  },
  getCarCategory: async function(req, res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    try{
      allModels = await CategoryModel.find({ status: 1 });
      //console.log("allModels"+allModels);
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            result: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },

  updateCatStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
      CategoryModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
  },
  
  getModelAdmin: async function(req, res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    //return false;
    try{
      if(req.body.category)
      {
        var allModels = await ModelsModel.find({ parent_id: null,attribute_type:req.body.category });
      }else{
        if(req.body.parent_id)
        {
          let parent_rec = await ModelsModel.findOne({ _id: req.body.parent_id});
          if(parent_rec)
          {
            var allModels = await ModelsModel.find({ parent_id: null,attribute_type:parent_rec.attribute_type });
          }else{
            var allModels = await ModelsModel.find({ parent_id: null,attribute_type:"Pièces et accessoires" });
          }
          
        }else{
          var allModels = await ModelsModel.find({ parent_id: null,attribute_type:"Pièces et accessoires" });
        }

      }
      
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            result: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },
  getSingleModel: async function(req, res,next)
  {
    //console.log("all data");
    var id = req.body.id;
    //console.log(id);

    try{
      allModels = await ModelsModel.find({ _id: id });
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },
  allModelList: async function(req, res,next)
  {
    //mongoose.set('debug', true);
    //console.log("all data");
    //console.log(req.body);

    var { attribute_type,model_name } = req.body;
    
    var String_qr = {}; 
    //String_qr['firstName'] = firstName;
    if(attribute_type && attribute_type != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['attribute_type'] = attribute_type;
    }
    if(model_name && model_name != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['model_name'] = { $regex: '.*' + model_name + '.*' };
    }
    String_qr['parent_id'] = null;

    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ModelsModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  exportCsvallModelApi: async function(req, res,next)
  {
    //mongoose.set('debug', true);
    //console.log("all data");
    //console.log(req.body);

    var { attribute_type,model_name } = req.body;
    
    var String_qr = {}; 
    //String_qr['firstName'] = firstName;
    /*if(attribute_type && attribute_type != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['attribute_type'] = attribute_type;
    }
    if(model_name && model_name != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['model_name'] = { $regex: '.*' + model_name + '.*' };
    }*/
    String_qr['parent_id'] = null;
    try{
      ModelsModel.find( {parent_id:null} , {_id:1,attribute_type:1,model_name:1}).exec((err, data)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("data"+data);
          //console.log(result);
          var fields = ['id', "attribute_type",'model_name'];
            //const fields = ['field1', 'field2', 'field3'];
            const opts = { fields };
            const parser = new Parser(opts);
            const csv = parser.parse(data);
            //var path=  Date.now()+'.csv'; 
            var path=  'allModelData.csv'; 
             fs.writeFile(path, csv, function(err,data) {
              if (err) {throw err;}
              else{ 
                res.download(path); // This is what you need
              }
          });
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  updateModelStatusApi: async function(req, res,next)
  {
    //console.log(req.body.status);
      ModelsModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
  },
  updateSubModelStatus: async function(req, res,next)
  {
    //console.log(req.body.status);
      ModelsModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
  },
  modelTotalCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("all data");
    //console.log(req.body);
    try{
      ModelsModel.count({ parent_id:null },(err,totalcount)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          total = totalcount;
          totalPageNumber = total / perPageRecord;
          //console.log("totalPageNumber"+totalPageNumber);
          totalPageNumber = Math.ceil(totalPageNumber);
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log("totalcount new fun"+totalcount);
          //console.log("all record count "+totalcount);
          var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  subModelTotalCount: async function(req, res,next)
  {
    //mongoose.set('debug', true);
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      var { attribute_type,model_name } = req.body;
    
      var String_qr = {};
      if(model_name && model_name != "")
      {
        String_qr['model_name'] = { $regex: '.*' + model_name + '.*' };
      }
      String_qr['parent_id'] = { $ne: null };
      //console.log(String_qr);
      ModelsModel.countDocuments(String_qr).exec((err, totalcount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
          total = totalcount;
          totalPageNumber = total / perPageRecord;
          //console.log("totalPageNumber"+totalPageNumber);
          totalPageNumber = Math.ceil(totalPageNumber);
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log("totalcount new fun"+totalcount);
          //console.log("all record count "+totalcount);
          var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
          res.status(200).send(return_response);
        }
      });
       
    }catch(err){
      console.log(err);
    }
  },
  allSubModel: async function(req, res,next)
  {
    
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage)
    {
      if(req.body.numofpage == 0)
      {
        fromindex = 0;
      }else{
        fromindex = perPageRecord * req.body.numofpage
      }
    }else{
      fromindex = 0;
    }
   
    try{
      var { attribute_type,model_name } = req.body;
      var String_qr = {};
      if(model_name && model_name != "")
      {
        String_qr['model_name'] = { $regex: '.*' + model_name + '.*' };
      }
      String_qr['parent_id'] = { $ne: null };
      //console.log(String_qr);
      ModelsModel.find( String_qr , {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {
            var xx = result.length - 1;
            for(let i=0; i<result.length; i++)
            {
              /*result[i].newKey = "value here";
              console.log("in side loop "+result[i]);
              console.log("in side loop "+result[i].model_name);*/
              //console.log("in side loop "+result[i].parent_id);
              var parent_id = result[i].parent_id;
              ModelsModel.findOne({ _id:parent_id }).lean(true).exec((errr, resulttt)=>
              { 
                if(resulttt)
                {
                  //console.log("parent_record "+resulttt.model_name);
                  //console.log("parent_record "+resulttt.model_name);
                  result[i].rootModel = resulttt.model_name;
                }
                if(xx == i)
                {
                  //console.log("inside if");
                  var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
                }
              });
            }
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allVersionSubmodel:async(req,res)=>{
    try{
      //console.log(req.body);
      let all_version = await VersionsModel.find({version:req.body.parent_id});
      if(all_version.length > 0)
      {
        var return_response = {"error":false,errorMessage:"Succès","all_version":all_version};
        return res.status(200).send(return_response);
      }else{
        var return_response = {"error":false,errorMessage:"Pas d'enregistrement","all_version":[]};
        return res.status(200).send(return_response);
      }
    }catch(e){
      var return_response = {"error":false,errorMessage:e.message};
      return res.status(200).send(return_response);
    }
  },
  exportCsvallSubModelApi: async function(req, res,next)
  {

    var String_qr = {};
    String_qr['parent_id'] = { $ne: null };
    try{
      ModelsModel.find( String_qr , {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {
            var xx = result.length - 1;
            for(let i=0; i<result.length; i++)
            {
              var parent_id = result[i].parent_id;
              ModelsModel.findOne({ _id:parent_id }).lean(true).exec((errr, resulttt)=>
              { 
                if(resulttt)
                {
                  result[i].rootModel = resulttt.model_name;
                }
                if(xx == i)
                {
                  var fields = ['id', "rootModel",'model_name'];
                  //const fields = ['field1', 'field2', 'field3'];
                  const opts = { fields };
                  const parser = new Parser(opts);
                  const csv = parser.parse(result);
                  //var path=  Date.now()+'.csv'; 
                  var path=  'allSubModelData.csv'; 
                   fs.writeFile(path, csv, function(err,result){
                    if (err) {throw err;}
                    else{ 
                      res.download(path); // This is what you need
                    }
                  });                  
                }
              });
            }
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  getSingleSubModel: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.id;
      ModelsModel.find({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result.length > 0)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement",
                  record:{}
              });
          }
        }
      });

    }catch(err){
      console.log(err);
    }
  },

  allCategoryCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       CategoryModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allCategory: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      CategoryModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },

  editCategorySubmit: async function(req, res,next)
  {
    try{
      //console.log(req.files);return false;

      var {category} = req.body;
      var images = [];
      //var video = [];
      if(req.files)
      {
        if(req.files.length > 0)
        {
          /*console.log("ifff"); 
          */
          //console.log(req.files.file[0].filename);
          //var p_r_i = req.files;
          //console.log(p_r_i.length);
          for(let m=0; m<1; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
          }
          //console.log(exterior_image);
        }
      }

      /*console.log(req.files);
      console.log(images);
      //console.log(images);

      return false;*/
      CategoryModel.find({ _id:req.body.edit_id }, {images:1}).exec((err, result)=>
      {
        if(err)
        {
          //console.log("herrrrrerr");
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              if(images.length>0)
              {
                //console.log("images is here");
                if(result[0].images.length > 0)
                {
                  //console.log(result[0].images[0]);
                  if(result[0].images[0].path)
                  {
                    if (fs.existsSync(result[0].images[0].path))
                    {
                      fs.unlink(result[0].images[0].path, (err) => 
                      {
                        console.log("unlink file error "+err);
                      });
                    }
                  }
                }
              }else{
                //console.log("images is not here");
                images = result[0].images;
              }
              //console.log(req.body);
              /*console.log(images);
              return false;*/
             

              //console.log("elseee");
              CategoryModel.updateOne({ "_id":req.body.edit_id }, 
              {
                first_heading:req.body.first_heading,
                
                images:images,
              }, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
              }
              else{
                res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Enregistrement complet des succès mis à jour"
                  });
                }
              });
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },


  getSingleCategory: async function(req, res,next)
  {
    try{
      CategoryModel.find({ _id:req.body.id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
};