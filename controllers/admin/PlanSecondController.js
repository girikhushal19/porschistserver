const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");
const ProUserPlanTopSearchModel = require("../../models/ProUserPlanTopSearchModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  addTopListPlanNormalUser:async function(req,res,next)
  {
    //console.log(req.body);return false;
    try{
      var { day_number , price,plan_type,plan_day_type } = req.body;
      plan_day_type = parseFloat(plan_day_type);
      NormalUserTopSearchPlanModel.count({day_number:day_number,price:price,plan_type:plan_type,plan_day_type:plan_day_type},(errs, allReadyLike)=>
      {
        //console.log("allReadyLike"+allReadyLike);
        if(allReadyLike == 0)
        {
          NormalUserTopSearchPlanModel.create({
            plan_day_type:plan_day_type,
            plan_type:plan_type,
            user_type:"normal_user",
            day_number,
            price
        });
        var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
        res.status(200)
                .send(return_response);
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
            res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  allPlanNormalUserTopListCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      NormalUserTopSearchPlanModel.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allPlanNormalUserTopList:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      NormalUserTopSearchPlanModel.find({  }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },

  editPlanNormalUserTopList:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      var { day_number , price ,edit_id,plan_type} = req.body;
      NormalUserTopSearchPlanModel.updateOne({ "_id":edit_id }, 
        {day_number:day_number,price:price,plan_type:plan_type}, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }
        else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement mis à jour avec succès"
            });
          }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },


  addTopUrgentPlanNormalUser:async function(req,res,next)
  {
    //console.log(req.body);return false;
    try{
      var { day_number , price,plan_type } = req.body;
      TopUrgentPlan.count({day_number:day_number,price:price,plan_type:plan_type},(errs, allReadyLike)=>
      {
        //console.log("allReadyLike"+allReadyLike);
        if(allReadyLike == 0)
        {
          TopUrgentPlan.create({
          plan_type:plan_type,
          user_type:"normal_user",
          day_number,
          price
        });
        var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
        res.status(200)
                .send(return_response);
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Même plan déjà disponible"};
            res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      //console.log(error)
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },

  allPlanNormalUserTopUrgentCount:async function(req,res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
      TopUrgentPlan.countDocuments({  }).exec((err, totalcount)=>
        {
          console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allPlanNormalUserTopUrgent:async function(req,res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      TopUrgentPlan.find({  }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },

  getSinglePlanNormalUserTopList: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var id = req.body.edit_id;
      NormalUserTopSearchPlanModel.find({ _id:id } ).exec((err, result)=>
      {
        if(err)
        {
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
        }else{
          if(result.length > 0)
          {
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Succès",
                  record:result
              });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement",
                  record:[]
              });
          }
        }
      });

    }catch(err){
      console.log(err);
    }
  },
};