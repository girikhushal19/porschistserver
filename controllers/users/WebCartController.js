const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const SettingModel = require("../../models/SettingModel");
const CountryModel = require('../../models/CountryModel');

const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const { async } = require('rxjs');
/*/*/
module.exports = {
  webAddToCart:async function(req,res,next)
  {
    //console.log(req.body);
    
    //  delivery_type 
    //CartsModel.find()
    try{
      var tax_rec = await SettingModel.findOne({attribute_key:"access_pur_tax_percent"});
      if(tax_rec)
      {
        var tax = tax_rec.attribute_value;
      }else{
        var tax = 0;
      }
      
      //var quantity = 1;
      var {user_id,ad_id,address_id,shipping_charge,quantity} = req.body;
      if(quantity == "" || quantity == null || quantity == undefined  || quantity == "null" || quantity == "undefined" )
      {
        quantity = 1;
      }
      user_id = user_id.toString();
      AddAdvertisementModel.find({_id:ad_id},{'_id':1,'userId':1,'price':1,'pro_price':1},(errs, record)=>{
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(200)
            .send(return_response);
        }else{
          if(record.length > 0)
          {
            //console.log(record);
            var ad_owner_user_id = record[0].userId.toString();
            console.log("39 line no ad_owner_user_id "+ad_owner_user_id);
            console.log("40 line no user_id "+user_id);
            if(ad_owner_user_id == user_id)
            {
              var return_response = {"error":true,success: false,errorMessage:"Il s'agit de votre propre ajout. Vous ne pouvez pas ajouter votre propre article dans un panier."};
                      res.status(200)
                      .send(return_response);
            }else{
              CartsModel.find({user_id:user_id},{'_id':1,'ad_owner_user_id':1},(errsOldSingle, recordOldSingle)=>{
                if(errsOldSingle)
                {
                  var return_response = {"error":true,success: false,errorMessage:errsOldSingle};
                  res.status(200)
                  .send(return_response);
                }else{
                  if(recordOldSingle.length > 0)
                  {
                    console.log("56 recordOldSingle "+recordOldSingle);
                    var old_ad_owner_user_id = recordOldSingle[0].ad_owner_user_id.toString();

                    console.log("59 old_ad_owner_user_id "+ad_owner_user_id);
                    console.log("57 recordOldSingle "+ad_owner_user_id);
                    if(old_ad_owner_user_id == ad_owner_user_id)
                    {
                      var price = record[0].price;
                      price = price * quantity;
                      CartsModel.find({user_id:user_id,ad_id:ad_id},(errsC, recordOld)=>{
                        if(errs)
                        {
                          var return_response = {"error":true,success: false,errorMessage:errsC};
                          res.status(200)
                            .send(return_response);
                        }else{
                          /*console.log(recordOld);
                          return false;*/
                          if(recordOld.length > 0)
                          {
                            CartsModel.findOneAndUpdate({_id:recordOld[0]._id},{
                              quantity:quantity,
                              address_id:address_id,
                              tax:tax,
                              shipping_charge:shipping_charge,
                              price:price
                            },function(err,result){
                              if(err)
                              {
                                var return_response = {"error":true,success: false,errorMessage:err};
                                  res.status(200)
                                  .send(return_response);
                              }else{
                                //console.log(result);
                                //console.log(result._id);
                                var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                                  res.status(200)
                                  .send(return_response);
                              }
                            });
                          }else{
                            CartsModel.create({
                              user_id:user_id,
                              ad_owner_user_id:ad_owner_user_id,
                              ad_id:ad_id,
                              quantity:quantity,
                              address_id:address_id,
                              tax:tax,
                              shipping_charge:shipping_charge,
                              price:price
                            },function(err,result){
                              if(err)
                              {
                                var return_response = {"error":true,success: false,errorMessage:err};
                                  res.status(200)
                                  .send(return_response);
                              }else{
                                //console.log(result);
                                //console.log(result._id);
                                var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                                  res.status(200)
                                  .send(return_response);
                              }
                            });
                          }
                        }
                      });
                    }else{
                      //console.log("2 diff seller");return false;
                      // console.log("hereeeeeeeeee 120 different seller product");
                      // console.log(old_ad_owner_user_id);
                      // console.log(ad_owner_user_id);
                      // return false;
                      var return_response = {"error":true,success: false,errorMessage:"En même temps, vous ne pouvez pas acheter deux accessoires de vendeur différents. "};
                      res.status(200)
                      .send(return_response);
                    }
                  }else{
                    var price = record[0].price;
                    price = price * quantity;
                    CartsModel.find({user_id:user_id,ad_id:ad_id},(errsC, recordOld)=>{
                      if(errs)
                      {
                        var return_response = {"error":true,success: false,errorMessage:errsC};
                        res.status(200)
                          .send(return_response);
                      }else{
                        /*console.log(recordOld);
                        return false;*/
                        if(recordOld.length > 0)
                        {
                          CartsModel.findOneAndUpdate({_id:recordOld[0]._id},{
                            quantity:quantity,
                            address_id:address_id,
                            tax:tax,
                            shipping_charge:shipping_charge,
                            price:price
                          },function(err,result){
                            if(err)
                            {
                              var return_response = {"error":true,success: false,errorMessage:err};
                                res.status(200)
                                .send(return_response);
                            }else{
                              //console.log(result);
                              //console.log(result._id);
                              var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                                res.status(200)
                                .send(return_response);
                            }
                          });
                        }else{
                          CartsModel.create({
                            user_id:user_id,
                            ad_owner_user_id:ad_owner_user_id,
                            ad_id:ad_id,
                            quantity:quantity,
                            address_id:address_id,
                            tax:tax,
                            shipping_charge:shipping_charge,
                            price:price
                          },function(err,result){
                            if(err)
                            {
                              var return_response = {"error":true,success: false,errorMessage:err};
                                res.status(200)
                                .send(return_response);
                            }else{
                              //console.log(result);
                              //console.log(result._id);
                              var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                                res.status(200)
                                .send(return_response);
                            }
                          });
                        }
                      }
                    });
                  }
                }
              });
            }
          }else{
            var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
            res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error){
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
      res.status(200)
        .send(return_response);
    }
  },


  webAddToCartNew:async function(req,res,next)
  {
    try{
      var tax = 10;
      var quantity = 1;
      var abc_current_seller = "";
      var gggg = "";
      var {user_id,ad_id,address_id,shipping_charge} = req.body;
      AddAdvertisementModel.find({_id:ad_id},{'_id':1,'userId':1,'price':1,'pro_price':1}).exec((errs, record)=>{
        if(errs)
        {
          console.log(errs);
        }else{
          //console.log(record);
          if(record.length > 0)
          {
            var ad_owner_user_id = record[0].userId;
            abc_current_seller = record[0].userId.toString();
            console.log("ad_owner_user_id "+ad_owner_user_id);
            console.log("user_id "+user_id);

            console.log("ad_owner_user_id "+ JSON.stringify(ad_owner_user_id));
            console.log("user_id "+ JSON.stringify(user_id));

            if(ad_owner_user_id == user_id)
            {
              //console.log("your own ad");
              var return_response = {"error":true,success: false,errorMessage:"Il s'agit de votre propre ajout. Vous ne pouvez pas ajouter votre propre article dans un panier."};
              res.status(200)
              .send(return_response);
            }else{
              //console.log("not your own ad");
              CartsModel.find({user_id:user_id},{'_id':1,'ad_owner_user_id':1,'price':1}).exec((errCart, recordCart)=>{
                if(errCart)
                {
                  console.log(errCart);
                }else{
                  console.log(recordCart);
                  if(recordCart.length > 0)
                  {
                    gggg = recordCart[0].ad_owner_user_id.toString();
                    console.log("gggg "+ JSON.stringify(gggg));
                    console.log("abc_current_seller  "+JSON.stringify(abc_current_seller));
                    console.log("both seller are different");
                    if(gggg == abc_current_seller)
                    {
                      //console.log("all same no issue");
                    }else{
                      //console.log("elseeeeeeeeeee issue");
                    }
                  }else{

                  }
                }
              });
            }
          }else{
            var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
            res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error)
    {
      console.log(error);
    }
  },
  webCheckItemInCartApi:async function(req,res,next)
  {
    try{
      var {user_id,ad_id} = req.body;
      CartsModel.count({user_id:user_id,ad_id:ad_id},(errsC, recordOld)=>{
        if(errsC)
        {
          var return_response = {"error":true,success: false,errorMessage:errsC};
          res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:recordOld};
          res.status(200)
            .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
          res.status(200)
            .send(return_response);
    }
  },
  webGetUserCart:async function(req,res,next)
  {
    try{

      var tax_rec = await SettingModel.findOne({attribute_key:"access_pur_tax_percent"});
      if(tax_rec)
      {
        var tax = tax_rec.attribute_value;
      }else{
        var tax = 0;
      }


      let cart_rec = await CartsModel.findOne({user_id:req.body.user_id},{ad_owner_user_id:1});
      if(cart_rec)
      {
        let seller_id = cart_rec.ad_owner_user_id[0].toString();
        if(seller_id)
        {
          let seller_rec = await UsersModel.findOne({_id:seller_id},{country:1});
          
          if(seller_rec)
          {
            if(seller_rec.country)
            {
              let result_country = await CountryModel.findOne({title:seller_rec.country});
              //console.log("result_country ", result_country);
              if(result_country)
              {
                if(result_country.tax)
                {
                  tax = result_country.tax;
                }
              }
            }
          }
        }
        
      }
      //console.log("tax ", tax);
      //return false;

      var self_pickup = 2;
      const queryJoin = [
        {
          path:'ad_id',
          //select:['price','shipping_price','accessoires_image','ad_name','user_type']
        },
      ];
      CartsModel.find({user_id:req.body.user_id},{}).populate(queryJoin).lean().exec((errCart,resultCart)=>{
        if(errCart)
        {
          var return_response = {"error":true,success: false,errorMessage:errCart};
          res.status(200)
          .send(return_response);
        }else{
          // console.log("resultCart ", resultCart);
          // console.log("resultCart ", resultCart[0].ad_owner_user_id[0]);
          // return false;
          if(resultCart.length > 0)
          {
           
            var last_val = resultCart.length - 1;
            var x=0;
            var total_product_price = 0;
            var sub_total = 0;
            var total_shipping_price = 0;
            var final_price = 0;
            var with_out_shipping_final_price = 0;

            resultCart.forEach(object=>{
              //console.log(object);return false;
              self_pickup = object.self_pickup;
              let user_type = object.ad_id[0].user_type;
              //self_pickup
              //console.log(object.ad_id[0].price);
              // console.log("shipping_price " , object.shipping_charge);
              // console.log("price " , object.ad_id[0].price);
              console.log("user_type " , object.ad_id[0].user_type);

               total_product_price = total_product_price + object.ad_id[0].price * object.quantity;
               //total_shipping_price = total_shipping_price + object.ad_id[0].shipping_price;
               total_shipping_price = total_shipping_price + parseFloat(object.shipping_charge);
               object.total_product_price = total_product_price;
              if(last_val == x)
              {
                let taxx = 0;
                if(object.ad_id[0].user_type != "normal_user" )
                {
                  taxx = total_product_price * tax /100;
                }
                
                final_price = total_product_price + total_shipping_price + taxx;
                with_out_shipping_final_price = total_product_price  + taxx;
                sub_total = total_product_price + total_shipping_price;
                //object.total_shipping_price = total_shipping_price;
                //object.final_price = final_price;

                var return_response = {"error":false,success: true,errorMessage:"Succès",record:resultCart,total_product_price:total_product_price,tax_price:taxx,tax_percentage:tax,total_shipping_price:total_shipping_price,sub_total:sub_total,final_price:final_price,with_out_shipping_final_price:with_out_shipping_final_price,self_pickup:self_pickup,user_type:user_type}; 
                res.status(200)
                .send(return_response);

              }  
              x++;
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Pas d'enregistrement",record:resultCart};
            res.status(200)
            .send(return_response);
          }
        }
      })
    }catch(error){
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
      res.status(200)
        .send(return_response);
    }
  },

  webAddDeliveryAddress:async function(req,res,next)
  {
    try{
      var user_id = req.body.user_id;
      DeliveryAddressUser.find({ user_id:user_id,address:req.body.address },(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(200)
                .send(return_response);
        }else{
          if(record.length > 0)
          {
            /*console.log("update id");
            console.log(record);console.log(record[0]._id);return false;*/
            
            DeliveryAddressUser.findOneAndUpdate({_id:record[0]._id},{
              address:req.body.address,
              location: {
               type: "Point",
               coordinates: [req.body.longitude,req.body.latitude]
              }
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Adresser pleinement le succès ajouté",lastInsertId:result._id};
                  res.status(200)
                  .send(return_response);
              }
            });
          }else{
            DeliveryAddressUser.create({
              user_id:req.body.user_id,
              address:req.body.address,
              location: {
               type: "Point",
               coordinates: [req.body.longitude,req.body.latitude]
              }
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                //console.log(result);
                //console.log(result._id);
                var return_response = {"error":false,success: true,errorMessage:"Adresser pleinement le succès ajouté",lastInsertId:result._id};
                  res.status(200)
                  .send(return_response);
              }
            });
          }
        }
      });
      
    }catch(err){
        console.log(err);
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },
  webGetDeliveryAddress:async function(req,res,next)
  {
    try{
      var user_id = req.body.user_id;
      DeliveryAddressUser.find({ user_id:user_id },(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(200)
                .send(return_response);
        }else{
          if(record.length > 0)
          {
            var return_response = {"error":false,success: true,errorMessage:"success",record:record};
            res.status(200)
                .send(return_response);
          }else{
            var return_response = {"error":true,success: false,errorMessage:"No Record"};
            res.status(200)
                .send(return_response);
          }
        }
      });
      
    }catch(err){
        console.log(err);
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },
  webSubmitCart:async function(req,res,next)
  {
    try{
      var {self_pickup,user_id,address_id} = req.body;
      if(self_pickup == "" || self_pickup == undefined || self_pickup == null)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'option de livraison est requise"});
      }
      if(user_id == "" || user_id == undefined || user_id == null)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis"});
      }
      if(address_id == "" || address_id == undefined || address_id == null)
      {
        address_id = null;
      }
      //console.log(req.body);
      CartsModel.updateMany({ user_id:user_id },{
        self_pickup:self_pickup,
        address_id:address_id,
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Succès de la mise à jour de l'identifiant de l'adresse"};
            res.status(200)
            .send(return_response);
        }
      });

    }catch(err){
        console.log(err);
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },
  webRemoveCartItem:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var cart_id = req.body.cart_id;
      CartsModel.deleteOne({_id:cart_id},function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Article supprimé de votre panier"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
        res.status(200)
        .send(return_response);
    }
  },

  testFunction:async function(req,res,next)
  {
    try{
      var obj = [];
      for(var j=0; j<10; j++)
      {
        obj[j] = { name: "etc" };
      }
      console.log(obj);
    }catch(error)
    {
      console.log(error);
    }
    
  },
  updateCartQuantity:async(req,res)=>{
    try{
      var {cart_id,quantity} = req.body;
      var cart_record = await CartsModel.findOne({_id:cart_id});
      if(!cart_record)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"Identifiant de panier invalide"});
      }
      if(quantity < 1)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"Quantité du panier invalide"});
      }
      CartsModel.updateMany({ _id:cart_id },{
        quantity:quantity
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Quantité mise à jour succès complet"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch(error)
    {
      return res.status(200).send({
        error:true,
        errorMessage:error.message
      });
    }
  }
};