const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const AdChatModel = require('../../models/AdChatModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

module.exports = io => {
  io.on("connection", client => {
    console.log("new connection fdgfdgfdg");

    client.on("disconnect", () => {
      console.log("user disconnected");
    });

    client.on("sendMessage", (data) => {

      //console.log("hello sendMessage inside controller");
      //console.log(data);

      try{
        var {table_id,sender_id,newMessage} = data;
        console.log(table_id);console.log(sender_id);console.log(newMessage);
        AdChatModel.findOne({_id:table_id}).lean(true).exec((err,result)=>{
          if(err)
          {
            console.log("ifff "+err);
          }else{
            if(!result)
            {
              var return_response = {"error":true,success: false,errorMessage:"This is invalid chat id"};
              console.log(return_response);
              //res.status(200).send(return_response);
            }else{
              
                //console.log("result "+JSON.stringify(result));
                //console.log("result "+result.ad_id);
                var ad_id = result.ad_id;
                var randomNumber = result.randomNumber;
                if(result.sender_id == sender_id)
                {
                  var message_by = 1;
                  var receiver_id = result.receiver_id;
                }else{
                  var message_by = 2;
                  var receiver_id = result.sender_id;
                }
                /*console.log("else "+result.userId);
                console.log("else "+sender_id);*/
                
                //console.log("iff "+resultCount);
                AdChatModel.create({
                  ad_id,
                  randomNumber,
                  sender_id,
                  receiver_id,
                  message:newMessage,
                  message_by
                },function(errC,resultC){
                  if(errC)
                  {
                    console.log(errC);
                  }else{
                    const queryJoin = [
                      {
                        path:'receiver_id',
                        select:['firstName','lastName']
                      },

                      {
                        path:'sender_id',
                        select:['firstName','lastName']
                      },
                    ];
                    AdChatModel.find({randomNumber:randomNumber},{sender_id:1,receiver_id:1,message_by:1,message:1,created_at:1}).populate(queryJoin).exec((getErr,getResult)=>{
                      if(getErr)
                      {
                        console.log(getErr);
                      }else{
                        if(getResult)
                        {
                          var return_response = {"error":false,success: true,errorMessage:"Succès",record:getResult};
                          io.emit("getMessages", {"error":false,success: true,errorMessage:"Succès",record:getResult});
                        }else{
                          var return_response = {"error":true,success: true,errorMessage:"Pas d'enregistrement",record:getResult};
                          io.emit("getMessages", {"error":false,success: true,errorMessage:"Succès",record:getResult});
                        }
                      }
                    });
                  }
                });
            }
          }
        });
      }catch(err){
        console.log(err);
      }
      
    });
  });
};

