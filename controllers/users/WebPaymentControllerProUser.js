const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();

const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
//const customConstant = require('../../helpers/customConstant');
 

 const paypal = require('paypal-rest-sdk');

 

/*/*/
module.exports = {
  
  webCarAdPaymentProUser:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var base_url = customConstant.base_url;
			/* http://localhost:3001/api/webCarAdPaymentProUser?ad_id=63b7d491e20a3287735f8e45&user_id=62e5204f0817d03eea4de394&basic_main_plan_id=63b6b5a4e6478bcbe1e75043&top_search_plan_id=63b6b652e6478bcbe1e75066&paymentType=Stripe&price=220 */
			var {ad_id,user_id,basic_main_plan_id,top_search_plan_id,paymentType,price} = req.query;
			if(top_search_plan_id == "" || top_search_plan_id == null)
			{
				top_search_plan_id = null;
			}
			UserPurchasedPlan.create({
				ad_type:"car",
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				basic_plan_id_pro_user:basic_main_plan_id,
				top_search_id_pro_user:top_search_plan_id,
				paymentType:paymentType,
				price:price,
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					///console.log(result._id);
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getNorUsrCarAdPaymentPaypal?id="+lastInsertId+"&price="+price;
					}else{
						var redirect_url_web = base_url+"api/getNorUsrCarAdPaymentStripe?id="+lastInsertId+"&price="+price;
					}
					//console.log(redirect_url_web);return false;
					res.redirect(303, redirect_url_web);
				}
			});
		}catch(error){
			//console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
  getProUsrCarAdPaymentPaypal:async function(req,res,next)
  {
    //console.log(req.query);
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    if(req.query.title)
    {
      var title = req.query.title;
    }else{
      var title = "Porschists";
    }
    var paypal_rec = await PaypalModel.findOne({}, {});
		if(!paypal_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		paypal.configure({
			'mode': paypal_rec.mode, //sandbox or live
			'client_id': paypal_rec.client_id,
			'client_secret': paypal_rec.client_secret
		});

    var return_url = base_url+"api/getPaypalProUsrCarAdSuccess";
    var cancel_url = base_url+"api/getPaypalProUsrCarAdCancel";
    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": return_url,
          "cancel_url": cancel_url
      },
      "transactions": [{
          "item_list": {
              "items": [{
                  "name": "Porschists",
                  "sku": lastInsertId,
                  "price": price,
                  "currency": "EUR",
                  "quantity": 1
              }]
          },
          "amount": {
              "currency": "EUR",
              "total": price
          },
          "description": lastInsertId
      }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
          throw error;
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
              res.redirect(payment.links[i].href);
            }
          }
      }
    });
  },
  getPaypalProUsrCarAdSuccess:async function(req,res,next)
  {
		try{
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var updated_at = new Date();
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;
			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			const execute_payment_json = {
				"payer_id": payerId
			};
			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
            //return false;
						UserPurchasedPlan.findOne({_id:payment_table_id},{ }).exec((err, result)=>
						{
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200).send(return_response);
							}else{
								/*console.log(result);
								console.log(result.final_price);
								console.log(result.ad_name);
								console.log(result.payment_type);*/
								if(result)
								{
									if(result.basic_plan_id_pro_user)
									{
										basic_plan_status = 1;
										show_on_web = 1;
										complition_status = 1;
										var maximum_upload = result.maximum_upload - 1;
									}else{
										var maximum_upload = result.maximum_upload - 1;
									}
									if(result.top_search_id_pro_user)
									{
										top_search_payment_status = 1;
										top_search = 1;
										var maximum_upload_top_search =  result.maximum_upload_top_search - 1;
									}else{
										var maximum_upload_top_search =  0;
									}

									UserPurchasedPlan.updateOne({ _id:result._id },{
										payment_status:1,
										maximum_upload:maximum_upload,
										maximum_upload_top_search:maximum_upload_top_search,
										updated_at:updated_at
									},function(err2,result2){
										if(err2)
										{
											var return_response = { "error":true,success: false,errorMessage:err2};
												res.status(200)
												.send(return_response);
										}else{
											//res.redirect(303, session.url);
											AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
												show_on_web:show_on_web,
												complition_status:complition_status,
												top_search:top_search,
												basic_plan_status:basic_plan_status,
												top_search_payment_status:top_search_payment_status,
												updated_at:updated_at
											},function(err,result){
												if(err)
												{
													var return_response = {"error":true,success: false,errorMessage:err};
														res.status(200)
														.send(return_response);
												}else{
													//console.log(result);
													//console.log(result._id);
													// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
													// 	res.status(200)
													// 	.send(return_response);
													var redirect_url_web = base_url_web+"adSuccess";
													res.redirect(303, redirect_url_web);
												}
											});
										}
									});
									
								}else{
                  res.status(200)
                    .send({
                      error: true,
                      success: false,
                      errorMessage: "Non payé"
                  });
                }
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
	getPaypalProUsrCarAdCancel:async function(req,res,next)
	{
    //console.log("pay pal cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  getProUsrCarAdPaymentStripe:async function(req,res,next)
  {
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    //var title = req.query.title;
    /*console.log(lastInsertId);
    console.log(price);
    console.log(title);*/
		var stripe_rec = await StripeModel.findOne({}, {});
		if(!stripe_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		//console.log(stripe_rec);return false;
		const stripe = require('stripe')(stripe_rec.stripe_secret);

    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price_data: {
            currency: 'eur',
            product_data: {
              name: "Porschists",
            },
            unit_amount: price * 100,
          },
          quantity: 1,
          //description: lastInsertId,
        },
      ],
      mode: 'payment',
      //description: 'One-time setup fee',
      success_url: base_url+'api/getProUsrCarAdStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: base_url+'api/getProUsrCarAdStripeCancel',
    });
    //console.log(session);
    //console.log(session.id);
    //console.log(session.payment_intent);
    //res.redirect(303, session.url);
    UserPurchasedPlan.updateOne({ _id:lastInsertId },{
      transaction_id:session.id,
      payment_intent:session.payment_intent,
      updated_at:updated_at
    },function(err,result){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        res.redirect(303, session.url);
      }
    });
  },
	getProUsrCarAdStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{}).exec((err, result)=>
					{
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200).send(return_response);
						}else{
							/*console.log(result);
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
							if(result)
							{
								if(result.basic_plan_id_pro_user)
								{
									basic_plan_status = 1;
									show_on_web = 1;
									complition_status = 1;
									var maximum_upload = result.maximum_upload - 1;
								}else{
									var maximum_upload = result.maximum_upload - 1;
								}
								if(result.top_search_id_pro_user)
								{
									top_search_payment_status = 1;
									top_search = 1;
									var maximum_upload_top_search =  result.maximum_upload_top_search - 1;
								}else{
									var maximum_upload_top_search =  0;
								}

                UserPurchasedPlan.updateOne({ _id:result._id },{
                  payment_status:1,
									maximum_upload:maximum_upload,
									maximum_upload_top_search:maximum_upload_top_search,
                  updated_at:updated_at
                },function(err2,result2){
                  if(err2)
                  {
                    var return_response = { "error":true,success: false,errorMessage:err2};
                      res.status(200)
                      .send(return_response);
                  }else{
                    //res.redirect(303, session.url);
                    AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
											show_on_web:show_on_web,
											complition_status:complition_status,
											top_search:top_search,
                      basic_plan_status:basic_plan_status,
                      top_search_payment_status:top_search_payment_status,
                      updated_at:updated_at
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        //console.log(result);
                        //console.log(result._id);
                        // var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
                        // 	res.status(200)
                        // 	.send(return_response);
                        var redirect_url_web = base_url_web+"adSuccess";
                        res.redirect(303, redirect_url_web);
                      }
                    });
                  }
                });
								
							}else{
                res.status(200)
                  .send({
                    error: true,
                    success: false,
                    errorMessage: "Non payé"
                });
              }
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getProUsrCarAdStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },

};