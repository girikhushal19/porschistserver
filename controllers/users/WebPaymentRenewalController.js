const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const SettingModel = require("../../models/SettingModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
//const customConstant = require('../../helpers/customConstant');
 
const paypal = require('paypal-rest-sdk');
// const paypal = require('paypal-rest-sdk');

 
/*/*/
module.exports = {
  normalUserCarTopList:async(req,res)=>{
    try{

      var base_url = customConstant.base_url;
      //console.log(req.body);return false;
      //mongoose.set("debug",true);
      var tax_percent = 0;
      var final_price = 0;
      var top_search_days = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var top_search_plan_end_date_time = null; 
			var top_search_every_day = 0;
      var updated_at  = new Date();
			var tax_val = await SettingModel.findOne({ attribute_key:"tax_percent" }, {});
			if(tax_val)
			{
				tax_percent = tax_val.attribute_value
			}

      let today_date = new Date();
      var {ad_id,user_id,top_search_plan_id,paymentType,which_week} = req.body;
      //mongoose.set("debug",true);
      var addRecord = await AddAdvertisementModel.findOne({
        _id:ad_id,
        top_search:1,
        top_search_plan_end_date_time:{ $gte:today_date }
      });
      //console.log(addRecord);
      if(addRecord)
      {
        return res.send({
          error:true,
          message:"Cette publicité a déjà un plan de recherche pour la journée à venir.",
          //record:addRecord
        });
      }
      var addRecord_2 = await AddAdvertisementModel.findOne({
        _id:ad_id
      },{category:1});
      
      var access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:top_search_plan_id});
      //console.log(access_top_search_record);return false;
      if(access_top_search_record)
      {
        //console.log(access_top_search_record);
        if(access_top_search_record.day_number == 7)
        { 
          top_search_price = access_top_search_record.price;
          final_price = final_price + top_search_price;
          var d = new Date();
          var date = d.getDate();
          var day = d.getDay();
          var weekOfMonth = Math.ceil((date - 1 - day) / 7);
          if(which_week == "" || which_week == undefined || which_week == null)
          {
            which_week = 1;
          }
          which_week = parseInt(which_week);
            //console.log("weekOfMonth");
            //console.log(weekOfMonth);
            if(weekOfMonth <  which_week)
            {
              //current month
              //console.log("current month");
              var date = new Date();
              var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

              let start_date = weekOfMonth * 7;

              top_search_plan_id = access_top_search_record._id;
              top_search_days = access_top_search_record.day_number;
              
              
              top_search_plan_purchase_date_time = new Date(firstDay);
              top_search_plan_purchase_date_time.setDate(top_search_plan_purchase_date_time.getDate() + start_date);
              top_search_plan_end_date_time = new Date(top_search_plan_purchase_date_time); // Now
              top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
              //console.log(top_search_plan_end_date_time);
              top_search_every_day = 1;
              // console.log("top_search_plan_purchase_date_time "+top_search_plan_purchase_date_time);
              // console.log("top_search_plan_end_date_time "+top_search_plan_end_date_time);
              // final_price = final_price + top_search_price;
              // console.log("which week" + which_week);
              // console.log("firstDay" + firstDay);
            }else{
              //next month
              // console.log("next month");
              // console.log("which week" + which_week);
              const today = new Date();
              // Get next month's index(0 based)
              const nextMonth = today.getMonth() + 1;
              // Get year
              const year = today.getFullYear() + (nextMonth === 12 ? 1: 0);
              // Get first day of the next month
              const firstDay = new Date(year, nextMonth%12, 1);
              //console.log(firstDay);

              let start_date = weekOfMonth * 7;
              top_search_plan_id = access_top_search_record._id;
              top_search_days = access_top_search_record.day_number;
              top_search_price = access_top_search_record.price;
              top_search_plan_purchase_date_time = new Date(firstDay);
              top_search_plan_purchase_date_time.setDate(top_search_plan_purchase_date_time.getDate() + start_date);
              top_search_plan_end_date_time = new Date(top_search_plan_purchase_date_time); // Now
              top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
              //console.log(top_search_plan_end_date_time);
              top_search_every_day = 1;
              //console.log("top_search_plan_purchase_date_time "+top_search_plan_purchase_date_time);
              //console.log("top_search_plan_end_date_time "+top_search_plan_end_date_time);
              // final_price = final_price + top_search_price;
              // console.log("which week" + which_week);
              // console.log("firstDay" + firstDay);
            }
          //return false;
        }else{
          top_search_plan_id = access_top_search_record._id;
          top_search_days = access_top_search_record.day_number;
          top_search_price = access_top_search_record.price;
          top_search_plan_purchase_date_time = new Date();
          top_search_plan_end_date_time = new Date(); // Now
          top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
          //console.log(top_search_plan_end_date_time);
          top_search_every_day = 1;
          final_price = final_price + top_search_price;
        }

        if(addRecord_2.category == "Porsche Voitures")
        {
          let taxx = final_price * tax_percent /100;
          //console.log("final_price "+final_price);
          //console.log("taxx "+taxx);
          final_price = taxx + final_price;
          var ad_type = "car";
        }else{
          var ad_type = "accessories";
        }
        
        
        await AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
          vat_tax:tax_percent,
          top_search_days:top_search_days,
          top_search_price:top_search_price,
          top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
          top_search_plan_end_date_time:top_search_plan_end_date_time,
          top_search_every_day:top_search_every_day,
          updated_at:updated_at
        });
        
        await UserPurchasedPlan.create({
          ad_type:ad_type,
          ad_id:ad_id,
          user_id:user_id,
          maximum_upload:0,
          top_search_plan_id:top_search_plan_id,
          paymentType:paymentType,
          price:final_price,
        }).then((result)=>{
          var lastInsertId = result._id;
          //console.log("hereeee 179");
          //console.log(lastInsertId);
          //return false;
          if(paymentType == "Paypal")
          {
            var redirect_url_web = base_url+"api/getTopSrchNorUsrCarAdPaymentPaypal?id="+lastInsertId+"&price="+final_price;
          }else{
            var redirect_url_web = base_url+"api/getTopSrchNorUsrCarAdPaymentStripe?id="+lastInsertId+"&price="+final_price;
          }
          //console.log(redirect_url_web);return false;
          //res.redirect(303, redirect_url_web);
          var return_response = {"error":false,success: true,errorMessage:"Succès",record:redirect_url_web};
			    return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
			    return res.status(200).send(return_response);
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant de plan invalide"};
			  return res.status(200).send(return_response);
      }
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
			return res.status(200).send(return_response);
    }
  },
  getTopSrchNorUsrCarAdPaymentPaypal:async function(req,res,next)
  {
		try{
			//console.log(req.query);
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			var title = req.query.title;
	
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});
	
				//console.log(stripe_rec);return false; 
	
	
			var return_url = base_url+"api/getPaypalTopSrchNorUsrCarAdSuccess";
			var cancel_url = base_url+"api/getPaypalTopSrchNorUsrCarAdCancel";
			const create_payment_json = {
				"intent": "sale",
				"payer": {
						"payment_method": "paypal"
				},
				"redirect_urls": {
						"return_url": return_url,
						"cancel_url": cancel_url
				},
				"transactions": [{
						"item_list": {
								"items": [{
										"name": "Porschists",
										"sku": lastInsertId,
										"price": price,
										"currency": "EUR",
										"quantity": 1
								}]
						},
						"amount": {
								"currency": "EUR",
								"total": price
						},
						"description": lastInsertId
				}]
			};
			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
						throw error;
				} else {
						for(let i = 0;i < payment.links.length;i++){
							if(payment.links[i].rel === 'approval_url'){
								res.redirect(payment.links[i].href);
							}
						}
				}
			});
		}catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
			return res.status(200).send(return_response);
    }
  },
  getPaypalTopSrchNorUsrCarAdSuccess:async function(req,res,next)
  {
		try{
      //console.log("hereeeeeeeeeeee 277");return false;
			var base_url_web = customConstant.front_end_base_url;
			// var basic_plan_status = 0;
			// var top_urgent_payment_status = 0;
			var top_search_payment_status = 0;
			var updated_at = new Date();
			//var show_on_web = 0;
			//var complition_status = 0;
			var top_search = 0;
			//var top_urgent = 0;

			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
						UserPurchasedPlan.findOne({_id:payment_table_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
						{
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200).send(return_response);
							}else{
								/*console.log(result);
								console.log(result.final_price);
								console.log(result.ad_name);
								console.log(result.payment_type);*/
								if(result)
								{
									if(result.top_search_plan_id)
									{
										top_search_payment_status = 1;
										top_search = 1;
									}

									UserPurchasedPlan.updateOne({ _id:result._id },{
										payment_status:1,
										updated_at:updated_at
									},function(err2,result2){
										if(err2)
										{
											var return_response = { "error":true,success: false,errorMessage:err2};
												res.status(200)
												.send(return_response);
										}else{
											//res.redirect(303, session.url);
											AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
												
												top_search:top_search,
												 
												updated_at:updated_at
											},function(err,result){
												if(err)
												{
													var return_response = {"error":true,success: false,errorMessage:err};
														res.status(200)
														.send(return_response);
												}else{
													//console.log(result);
													//console.log(result._id);
													// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
													// 	res.status(200)
													// 	.send(return_response);
													var redirect_url_web = base_url_web+"adSuccess";
													res.redirect(303, redirect_url_web);
												}
											});
										}
									});
									
								}
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
	getPaypalTopSrchNorUsrCarAdCancel:async function(req,res,next)
	{
    //console.log("pay pal cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  getTopSrchNorUsrCarAdPaymentStripe:async function(req,res,next)
  {
		try{
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//console.log(req.query);return false;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.create({
				line_items: [
					{
						price_data: {
							currency: 'eur',
							product_data: {
								name: "Porschists",
							},
							unit_amount: price * 100,
						},
						quantity: 1,
						//description: lastInsertId,
					},
				],
				mode: 'payment',
				//description: 'One-time setup fee',
				success_url: base_url+'api/getTopSrchNorUsrCarAdStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
				cancel_url: base_url+'api/getTopSrchNorUsrCarAdStripeCancel',
			});
			//console.log(session);
			//console.log(session.id);
			//console.log(session.payment_intent);
			//res.redirect(303, session.url);
			UserPurchasedPlan.updateOne({ _id:lastInsertId },{
				transaction_id:session.id,
				payment_intent:session.payment_intent,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					res.redirect(303, session.url);
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error.message};
			res.status(200).send(return_response);
		}
    
  },
  getTopSrchNorUsrCarAdStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			var base_url_web = customConstant.front_end_base_url;
			var top_search = 0;
			var top_search_payment_status = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
					{
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200).send(return_response);
						}else{
							/*console.log(result);
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
							if(result)
							{
								if(result.top_search_plan_id)
								{
									top_search_payment_status = 1;
									top_search = 1;
								}
								UserPurchasedPlan.updateOne({ _id:result._id },{
									payment_status:1,
									updated_at:updated_at
								},function(err2,result2){
									if(err2)
									{
										var return_response = { "error":true,success: false,errorMessage:err2};
											res.status(200)
											.send(return_response);
									}else{
										//res.redirect(303, session.url);
										AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
											top_search:top_search,
											top_search_payment_status:top_search_payment_status,
											updated_at:updated_at
										},function(err,result){
											if(err)
											{
												var return_response = {"error":true,success: false,errorMessage:err};
													res.status(200)
													.send(return_response);
											}else{
												//console.log(result);
												//console.log(result._id);
												// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
												// 	res.status(200)
												// 	.send(return_response);
												var redirect_url_web = base_url_web+"adSuccess";
												res.redirect(303, redirect_url_web);
											}
										});
									}
								});
								
							}
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getTopSrchNorUsrCarAdStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  normalUserCarTopUrgent:async(req,res)=>{
    try{
      var base_url = customConstant.base_url;
      //console.log(req.body);return false;
			var {ad_id,user_id,top_urgent_plan_id,paymentType} = req.body;
			if(top_urgent_plan_id == "" || top_urgent_plan_id == null || top_urgent_plan_id == undefined)
			{
        console.log("iffffffffff");
        console.log(top_urgent_plan_id);
				top_urgent_plan_id = null;
			}
      var tax_percent = 0;
      var final_price = 0;
      var updated_at  = new Date();
      let today_date = new Date();

      var top_urgent_day_number = 0; 
			var top_urgent_price = 0;
			var top_urgent_plan_purchase_date_time = null;
			var top_urgent_plan_end_date_time = null;


			var tax_val = await SettingModel.findOne({ attribute_key:"tax_percent" }, {});
			if(tax_val)
			{
				tax_percent = tax_val.attribute_value
			}
      //mongoose.set("debug",true);
      var addRecord = await AddAdvertisementModel.findOne({
        _id:ad_id,
        top_urgent:1,
        top_urgent_plan_end_date_time:{ $gte : today_date }
      });
      //console.log(addRecord);
      if(addRecord)
      {
        return res.send({
          error:true,
          message:"Cette annonce est déjà assortie d'un plan d'urgence."
        });
      }
      //mongoose.set("debug",true);
      var resultTopUrgent = await TopUrgentPlan.findOne({_id:top_urgent_plan_id});
      if(resultTopUrgent)
      {
        top_urgent_day_number = resultTopUrgent.day_number;
        top_urgent_price = resultTopUrgent.price;
        top_urgent_plan_purchase_date_time = new Date();
        top_urgent_plan_end_date_time = new Date();
        top_urgent_plan_end_date_time.setDate(top_urgent_plan_end_date_time.getDate() + top_urgent_day_number);
        final_price = final_price + top_urgent_price;
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant de plan invalide"};
			  return res.status(200).send(return_response);
      }

      var addRecord_2 = await AddAdvertisementModel.findOne({
        _id:ad_id
      },{category:1});
      if(addRecord_2.category == "Porsche Voitures")
      {
        let taxx = final_price * tax_percent /100;
        //console.log("final_price "+final_price);
        //console.log("taxx "+taxx);
        final_price = taxx + final_price;
        var ad_type = "car";
      }else{
        var ad_type = "accessories";
      }

      await AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
				top_urgent_day_number:top_urgent_day_number,
				top_urgent_price:top_urgent_price,
				top_urgent_plan_purchase_date_time:top_urgent_plan_purchase_date_time,
				top_urgent_plan_end_date_time:top_urgent_plan_end_date_time,
				updated_at:updated_at
			});

			//console.log(req.query);return false;
			UserPurchasedPlan.create({
				ad_type:ad_type,
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				top_urgent_id:top_urgent_plan_id,
				paymentType:paymentType,
				price:final_price,
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					///console.log(result._id);
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getTopUrgntNorUsrCarAdPaymentPaypal?id="+lastInsertId+"&price="+final_price;
					}else{
						var redirect_url_web = base_url+"api/getTopUrgntNorUsrCarAdPaymentStripe?id="+lastInsertId+"&price="+final_price;
					}
          var return_response = {"error":false,success: true,errorMessage:"Succès",record:redirect_url_web};
			    return res.status(200).send(return_response);
          //return res.status(200).send(return_response);
					//console.log(redirect_url_web);return false;
					//res.redirect(303, redirect_url_web);
				}
			});
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
			return res.status(200).send(return_response);
    }
  },

  getTopUrgntNorUsrCarAdPaymentPaypal:async function(req,res,next)
  {
		try{
			//console.log(req.query);
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			var title = req.query.title;

			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

				//console.log(stripe_rec);return false; 


			var return_url = base_url+"api/getPaypalTopUrgntNorUsrCarAdSuccess";
			var cancel_url = base_url+"api/getPaypalTopUrgntNorUsrCarAdCancel";
			const create_payment_json = {
				"intent": "sale",
				"payer": {
						"payment_method": "paypal"
				},
				"redirect_urls": {
						"return_url": return_url,
						"cancel_url": cancel_url
				},
				"transactions": [{
						"item_list": {
								"items": [{
										"name": "Porschists",
										"sku": lastInsertId,
										"price": price,
										"currency": "EUR",
										"quantity": 1
								}]
						},
						"amount": {
								"currency": "EUR",
								"total": price
						},
						"description": lastInsertId
				}]
			};
			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
						throw error;
				} else {
						for(let i = 0;i < payment.links.length;i++){
							if(payment.links[i].rel === 'approval_url'){
								res.redirect(payment.links[i].href);
							}
						}
				}
			});
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getPaypalTopUrgntNorUsrCarAdSuccess:async function(req,res,next)
  {
		try{
      //console.log("hereeeeeeeeeeee 277");return false;
			var base_url_web = customConstant.front_end_base_url;
			// var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var updated_at = new Date();
			var top_urgent = 0;

			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
						UserPurchasedPlan.findOne({_id:payment_table_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
						{
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200).send(return_response);
							}else{
								/*console.log(result);
								console.log(result.final_price);
								console.log(result.ad_name);
								console.log(result.payment_type);*/
								if(result)
								{
									if(result.top_urgent_id)
									{
										top_urgent_payment_status = 1;
										top_urgent = 1;
									}

									UserPurchasedPlan.updateOne({ _id:result._id },{
										payment_status:1,
										updated_at:updated_at
									},function(err2,result2){
										if(err2)
										{
											var return_response = { "error":true,success: false,errorMessage:err2};
												res.status(200)
												.send(return_response);
										}else{
											//res.redirect(303, session.url);
											AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
												top_urgent_payment_status:top_urgent_payment_status,
												top_urgent:top_urgent,												 
												updated_at:updated_at
											},function(err,result){
												if(err)
												{
													var return_response = {"error":true,success: false,errorMessage:err};
														res.status(200)
														.send(return_response);
												}else{
													//console.log(result);
													//console.log(result._id);
													// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
													// 	res.status(200)
													// 	.send(return_response);
													var redirect_url_web = base_url_web+"adSuccess";
													res.redirect(303, redirect_url_web);
												}
											});
										}
									});
									
								}
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
	getPaypalTopUrgntNorUsrCarAdCancel:async function(req,res,next)
	{
    //console.log("pay pal cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },

  getTopUrgntNorUsrCarAdPaymentStripe:async function(req,res,next)
  {
		try{
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//console.log(req.query);return false;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.create({
				line_items: [
					{
						price_data: {
							currency: 'eur',
							product_data: {
								name: "Porschists",
							},
							unit_amount: price * 100,
						},
						quantity: 1,
						//description: lastInsertId,
					},
				],
				mode: 'payment',
				//description: 'One-time setup fee',
				success_url: base_url+'api/getTopUrgntNorUsrCarAdStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
				cancel_url: base_url+'api/getTopUrgntNorUsrCarAdStripeCancel',
			});
			//console.log(session);
			//console.log(session.id);
			//console.log(session.payment_intent);
			//res.redirect(303, session.url);
			UserPurchasedPlan.updateOne({ _id:lastInsertId },{
				transaction_id:session.id,
				payment_intent:session.payment_intent,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					res.redirect(303, session.url);
				}
			});
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getTopUrgntNorUsrCarAdStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			var base_url_web = customConstant.front_end_base_url;
			var top_urgent = 0;
			var top_urgent_payment_status = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
					{
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200).send(return_response);
						}else{
							/*console.log(result);
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
							if(result)
							{
								if(result.top_urgent_id)
								{
									top_urgent_payment_status = 1;
									top_urgent = 1;
								}
								UserPurchasedPlan.updateOne({ _id:result._id },{
									payment_status:1,
									updated_at:updated_at
								},function(err2,result2){
									if(err2)
									{
										var return_response = { "error":true,success: false,errorMessage:err2};
											res.status(200)
											.send(return_response);
									}else{
										//res.redirect(303, session.url);
										AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
											top_urgent:top_urgent,
											top_urgent_payment_status:top_urgent_payment_status,
											updated_at:updated_at
										},function(err,result){
											if(err)
											{
												var return_response = {"error":true,success: false,errorMessage:err};
                        return res.status(200).send(return_response);
											}else{
												//console.log(result);
												//console.log(result._id);
												// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
												// 	res.status(200)
												// 	.send(return_response);
												var redirect_url_web = base_url_web+"adSuccess";
												res.redirect(303, redirect_url_web);
                         
											}
										});
									}
								});
								
							}
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getTopUrgntNorUsrCarAdStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },

	normalUserCarAdRenewal:async function(req,res,next)
	{
		try{
      var base_url = customConstant.base_url;
      //console.log(req.body);return false;
			var {ad_id,user_id,basic_main_plan_id,paymentType} = req.body;
			if(basic_main_plan_id == "" || basic_main_plan_id == null || basic_main_plan_id == undefined)
			{
        console.log("iffffffffff");
        console.log(basic_main_plan_id);
				basic_main_plan_id = null;
			}
      var tax_percent = 0;
      var final_price = 0;
      var updated_at  = new Date();
      let today_date = new Date();

      var basic_plan_price = 0;
			var showing_number_day = 0;
			var plan_purchase_date_time = null;
			var plan_end_date_time = null;


			var tax_val = await SettingModel.findOne({ attribute_key:"tax_percent" }, {});
			if(tax_val)
			{
				tax_percent = tax_val.attribute_value
			}
      //mongoose.set("debug",true); basic_main_plan_id
      var addRecord = await AddAdvertisementModel.findOne({
        _id:ad_id,
        show_on_web:1,
        plan_end_date_time:{ $gte : today_date }
      });
      //console.log(addRecord);
      if(addRecord)
      {
        return res.send({
          error:true,
          message:"Cette annonce est déjà prévue."
        });
      }

			var access_plan_record = await UserPlanModel.findOne({_id:basic_main_plan_id});
			if(!access_plan_record)
			{
				var return_response = {"error":false,success: true,errorMessage:"L'identifiant du plan n'est pas valide"};
				return res.status(200).send(return_response);
			}
			//console.log("access_plan_record");
			//console.log(access_plan_record);

			basic_plan_price = access_plan_record.price;
			final_price = basic_plan_price;
			showing_number_day = access_plan_record.day_number;
			plan_purchase_date_time = new Date();
			plan_end_date_time = new Date(); // Now
			plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date

			var ad_type = "car";

			var addRecord_2 = await AddAdvertisementModel.findOne({
        _id:ad_id
      },{category:1});
      if(addRecord_2.category == "Porsche Voitures")
      {
        let taxx = final_price * tax_percent /100;
        //console.log("final_price "+final_price);
        //console.log("taxx "+taxx);
        final_price = taxx + final_price;
        
      }
			
			
			await AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
				basic_plan_price:basic_plan_price,
				showing_number_day:showing_number_day,
				plan_purchase_date_time:plan_purchase_date_time,
				plan_end_date_time:plan_end_date_time,
				updated_at:updated_at
			});

			//console.log(req.query);return false;
			UserPurchasedPlan.create({
				ad_type:ad_type,
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				basic_plan_id:basic_main_plan_id,
				paymentType:paymentType,
				price:final_price,
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					///console.log(result._id);
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getNorUsrCarAdMainPlanPaymentPaypal?id="+lastInsertId+"&price="+final_price;
					}else{
						var redirect_url_web = base_url+"api/getNorUsrCarAdMainPlanPaymentStripe?id="+lastInsertId+"&price="+final_price;
					}
          var return_response = {"error":false,success: true,errorMessage:"Succès",record:redirect_url_web};
			    return res.status(200).send(return_response);
          //return res.status(200).send(return_response);
					//console.log(redirect_url_web);return false;
					//res.redirect(303, redirect_url_web);
				}
			});


		}catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
			return res.status(200).send(return_response);
    }
	},


	getNorUsrCarAdMainPlanPaymentStripe:async function(req,res,next)
  {
		try{
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//console.log(req.query);return false;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.create({
				line_items: [
					{
						price_data: {
							currency: 'eur',
							product_data: {
								name: "Porschists",
							},
							unit_amount: price * 100,
						},
						quantity: 1,
						//description: lastInsertId,
					},
				],
				mode: 'payment',
				//description: 'One-time setup fee',
				success_url: base_url+'api/getNorUsrCarAdMainPlanPaymentStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
				cancel_url: base_url+'api/getNorUsrCarAdMainPlanPaymentStripeCancel',
			});
			//console.log(session);
			//console.log(session.id);
			//console.log(session.payment_intent);
			//res.redirect(303, session.url);
			UserPurchasedPlan.updateOne({ _id:lastInsertId },{
				transaction_id:session.id,
				payment_intent:session.payment_intent,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					res.redirect(303, session.url);
				}
			});
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getNorUsrCarAdMainPlanPaymentStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			var base_url_web = customConstant.front_end_base_url;
			var show_on_web = 0;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
					{
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200).send(return_response);
						}else{
							/*console.log(result);
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
							if(result)
							{
								if(result.basic_plan_id)
									{
										basic_plan_status = 1;
										show_on_web = 1;
									}
								 
								UserPurchasedPlan.updateOne({ _id:result._id },{
									payment_status:1,
									updated_at:updated_at
								},function(err2,result2){
									if(err2)
									{
										var return_response = { "error":true,success: false,errorMessage:err2};
											res.status(200)
											.send(return_response);
									}else{
										//res.redirect(303, session.url);
										AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
											show_on_web:show_on_web,
											basic_plan_status:basic_plan_status,
											updated_at:updated_at
										},function(err,result){
											if(err)
											{
												var return_response = {"error":true,success: false,errorMessage:err};
                        return res.status(200).send(return_response);
											}else{
												//console.log(result);
												//console.log(result._id);
												// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
												// 	res.status(200)
												// 	.send(return_response);
												var redirect_url_web = base_url_web+"adSuccess";
												res.redirect(303, redirect_url_web);
                         
											}
										});
									}
								});
								
							}
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getNorUsrCarAdMainPlanPaymentStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },


	getNorUsrCarAdMainPlanPaymentPaypal:async function(req,res,next)
  {
		try{
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//console.log(req.query);return false;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			
			
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(paypal_rec);return false; 
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

				//console.log(paypal_rec);return false; 


			var return_url = base_url+"api/getNorUsrCarAdMainPlanPaymentPaypalSuccess";
			var cancel_url = base_url+"api/getNorUsrCarAdMainPlanPaymentPaypalCancel";
			const create_payment_json = {
				"intent": "sale",
				"payer": {
						"payment_method": "paypal"
				},
				"redirect_urls": {
						"return_url": return_url,
						"cancel_url": cancel_url
				},
				"transactions": [{
						"item_list": {
								"items": [{
										"name": "Porschists",
										"sku": lastInsertId,
										"price": price,
										"currency": "EUR",
										"quantity": 1
								}]
						},
						"amount": {
								"currency": "EUR",
								"total": price
						},
						"description": lastInsertId
				}]
			};
			//console.log(create_payment_json);return false;
			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
						throw error;
				} else {
						for(let i = 0;i < payment.links.length;i++){
							if(payment.links[i].rel === 'approval_url'){
								res.redirect(payment.links[i].href);
							}
						}
				}
			});
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getNorUsrCarAdMainPlanPaymentPaypalSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
      //console.log("hereeeeeeeeeeee 277");return false;
			var base_url_web = customConstant.front_end_base_url;
			// var basic_plan_status = 0;
			var show_on_web = 0;
  		var basic_plan_status = 0;
			//var updated_at = new Date();
			

			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));return false;
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
						UserPurchasedPlan.findOne({_id:payment_table_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1}).exec((err, result)=>
						{
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200).send(return_response);
							}else{
								/*console.log(result);
								console.log(result.final_price);
								console.log(result.ad_name);
								console.log(result.payment_type);*/
								if(result)
								{
									if(result.basic_plan_id)
									{
										basic_plan_status = 1;
										show_on_web = 1;
									}

									UserPurchasedPlan.updateOne({ _id:result._id },{
										payment_status:1,
										updated_at:updated_at
									},function(err2,result2){
										if(err2)
										{
											var return_response = { "error":true,success: false,errorMessage:err2};
												res.status(200)
												.send(return_response);
										}else{
											//res.redirect(303, session.url);
											AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
												show_on_web:show_on_web,
                  			basic_plan_status:basic_plan_status,											 
												updated_at:updated_at
											},function(err,result){
												if(err)
												{
													var return_response = {"error":true,success: false,errorMessage:err};
														res.status(200)
														.send(return_response);
												}else{
													//console.log(result);
													//console.log(result._id);
													// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
													// 	res.status(200)
													// 	.send(return_response);
													var redirect_url_web = base_url_web+"adSuccess";
													res.redirect(303, redirect_url_web);
												}
											});
										}
									});
									
								}
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
  getNorUsrCarAdMainPlanPaymentPaypalCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
	webAcesPhotoPack:async(req,res)=>{
		try{
			console.log(req.files);
			console.log(req.body);
			//var accessoires_image_new = [];
			var exterior_image = [];
			var ad_id = req.body.ad_id;
			var updated_at = new Date();
			var new_images = [];
			if(req.files.accessoires_image_new.length == 0)
			{
				var return_response = {"error":true,success: false,errorMessage:"L'image est requise"};
				return res.status(200).send(return_response);
			}
			var adResultRecord = await AddAdvertisementModel.findOne({ _id:req.body.ad_id },{accessoires_image:1});
			if(!adResultRecord)
			{
				var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
				return res.status(200).send(return_response);
			}
			if(adResultRecord.accessoires_image.length > 10)
			{
				var return_response = {"error":true,success: false,errorMessage:"Vous ne pouvez pas télécharger plus de 10 photos"};
				return res.status(200).send(return_response);
			}
			var remaining_img_length = 11 - adResultRecord.accessoires_image.length;
			
			var p_r_i = req.files.accessoires_image_new;
			var p_r_i_length = p_r_i.length;
			if(remaining_img_length < p_r_i_length)
			{
				p_r_i_length = remaining_img_length;
			}

			for(let m=0; m<p_r_i_length; m++)
			{
				let images_name_obj = { "filename":req.files.accessoires_image_new[m].filename,"path":req.files.accessoires_image_new[m].path  };
					exterior_image.push(images_name_obj);
			}
			// for (let j = 0; j < exterior_image.length; j++)
			// {
			// 	new_images.push(exterior_image[j]);
			// }
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				accessoires_image_new:exterior_image,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err.message};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					//console.log(result._id);
					var return_response = {"error":false,success: true,errorMessage:"Effectuez le paiement et l'image apparaîtra au recto",lastInsertId:ad_id};
						res.status(200)
						.send(return_response);
				}
			});
		}catch(error)
		{
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webPaymentAccessParticularRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.body);return false;
			var {normalUserAd,normalUserAdTopUrgent,ad_id,normalUserAdTopSearch,which_week} = req.body;
			var updated_at  = new Date();
			var final_price = 0;
			var top_urgent_day_number = 0; var top_urgent_price = 0;
			var top_urgent_plan_purchase_date_time = null;var top_urgent_plan_end_date_time = null;
			var access_top_urgent_record = "";var basic_plan_price = 0;
			var access_top_search_record = "";var accessories_max_photo = 0;
			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var top_search_plan_end_date_time = null; 
			var top_search_every_day = 0;
			var show_on_web = 1;
			/*
				basic_plan_price: { type: Number, default: null },
				showing_number_day: { type: Number, default: 0 },
				plan_purchase_date_time: { type: Date, default: null },
				plan_end_date_time: { type: Date, default: null },

				top_urgent: { type: Number, default: 0 },
				top_urgent_price: { type: Number, default: null },
				top_urgent_payment_status: { type: Number, default: 0 },
				top_urgent_plan_purchase_date_time: { type: Date, default: null },
				top_urgent_plan_end_date_time: { type: Date, default: null },
			*/
			var addResult = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image:1,accessoires_image_new:1});
			//console.log(addResult);
			if(!addResult)
			{
				var return_response = {"error":true,success: false,errorMessage:"Identifiant d'annonce invalide"};
				return res.status(200).send(return_response);
			}
			
			var upload_iamge_length = addResult.accessoires_image_new.length;
			 

			if(normalUserAd)
			{
				var access_plan_record = await UseraccessoriesplanModel.findOne({_id:normalUserAd});
				if(access_plan_record)
				{
					if(upload_iamge_length > access_plan_record.photo_number)
					{
						let messg = "Vous avez téléchargé "+upload_iamge_length+" images Veuillez choisir le plan en fonction de votre téléchargement.";
						var return_response = {"error":true,success: false,errorMessage:messg};
						return res.status(200).send(return_response);
					}else{
						//console.log("continue");
						basic_plan_price = access_plan_record.price;
						accessories_max_photo = access_plan_record.photo_number;
						final_price = basic_plan_price;
					}
				}else{
					let messg = "Vous avez téléchargé "+upload_iamge_length+" images Veuillez choisir le plan en fonction de votre téléchargement.";
						var return_response = {"error":true,success: false,errorMessage:messg};
					return res.status(200).send(return_response);
				}
			}else{
				var return_response = {"error":true,success: false,errorMessage:"L'identifiant du plan est requis"};
				 return res.status(200).send(return_response);
			}

			var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id,price:final_price};
				return res.status(200).send(return_response);

		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},

	webAccessAdPaymentRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);return false;
			var base_url = customConstant.base_url;
			/* http://localhost:3001/api/webCarAdPayment?ad_id=6398854fb434cbf8981ae746&user_id=62e4f2a998c8e35854b2dba0&basic_main_plan_id=6394447a2535bcdc27eb3898&top_urgent_plan_id=62e3e22fba3fb765e8c6a615&paymentType=Paypal&price=undefined */
			var {ad_id,user_id,basic_main_plan_id,top_urgent_plan_id,paymentType,price,normalUserAdTopSearch} = req.query;
			if(top_urgent_plan_id == "" || top_urgent_plan_id == null || top_urgent_plan_id == undefined)
			{
				top_urgent_plan_id = null;
			}
			if(normalUserAdTopSearch == "" || normalUserAdTopSearch == null || normalUserAdTopSearch == undefined)
			{
				normalUserAdTopSearch = null;
			}
			UserPurchasedPlan.create({
				ad_type:"accessories",
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				basic_plan_id:basic_main_plan_id,
				top_urgent_id:top_urgent_plan_id,
				top_search_plan_id:normalUserAdTopSearch,
				paymentType:paymentType,
				price:price,
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					///console.log(result._id);
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getNorUsrAccessPaymentPaypalRenewal?id="+lastInsertId+"&price="+price;
					}else{
						var redirect_url_web = base_url+"api/getNorUsrAccessPaymentStripeRenewal?id="+lastInsertId+"&price="+price;
					}
					//console.log(redirect_url_web);return false;
					res.redirect(303, redirect_url_web);
				}
			});
		}catch(error){
			//console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},


	getNorUsrAccessPaymentStripeRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);

			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.create({
				line_items: [
					{
						price_data: {
							currency: 'eur',
							product_data: {
								name: "Porschists",
							},
							unit_amount: price * 100,
						},
						quantity: 1,
						//description: lastInsertId,
					},
				],
				mode: 'payment',
				//description: 'One-time setup fee',
				success_url: base_url+'api/getNorUsrAccessStripeSuccessRenewal?session_id={CHECKOUT_SESSION_ID}',
				cancel_url: base_url+'api/getNorUsrAccessStripeCancelRenewal',
			});
			//console.log(session);
			//console.log(session.id);
			//console.log(session.payment_intent);
			//res.redirect(303, session.url);
			UserPurchasedPlan.updateOne({ _id:lastInsertId },{
				transaction_id:session.id,
				payment_intent:session.payment_intent,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					res.redirect(303, session.url);
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},

	getNorUsrAccessStripeSuccessRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_urgent = 0;
			var top_search_payment_status = 0;
			var top_search = 0;


			var updated_at = new Date();
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{}).exec((err, result)=>
					{
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200).send(return_response);
						}else{
							/*console.log(result);
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
							if(result)
							{
								if(result.basic_plan_id)
								{
									basic_plan_status = 1;
								}
								

								UserPurchasedPlan.updateOne({ _id:result._id },{
									payment_status:1,
									updated_at:updated_at
								},function(err2,result2){
									if(err2)
									{
										var return_response = { "error":true,success: false,errorMessage:err2};
											res.status(200)
											.send(return_response);
									}else{
										//res.redirect(303, session.url);
										AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
											accessories_max_photo_status:basic_plan_status,
											
											updated_at:updated_at
										},function(err,result){
											if(err)
											{
												var return_response = {"error":true,success: false,errorMessage:err};
													res.status(200)
													.send(return_response);
											}else{
												//console.log(result);
												//console.log(result._id);
												// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
												// 	res.status(200)
												// 	.send(return_response);
												var redirect_url_web = base_url_web+"adSuccess";
												res.redirect(303, redirect_url_web);
											}
										});
									}
								});
								
							}
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},

	getNorUsrAccessStripeCancelRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
			res.status(200).send(return_response);
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getNorUsrAccessPaymentPaypalRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			//console.log(req.query);
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			var title = req.query.title;
			var return_url = base_url+"api/getPaypalNorUsrAccessSuccessRenewal";
			var cancel_url = base_url+"api/getPaypalNorUsrAccessCancelRenewal";
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			const create_payment_json = {
				"intent": "sale",
				"payer": {
						"payment_method": "paypal"
				},
				"redirect_urls": {
						"return_url": return_url,
						"cancel_url": cancel_url
				},
				"transactions": [{
						"item_list": {
								"items": [{
										"name": "Porschists",
										"sku": lastInsertId,
										"price": price,
										"currency": "EUR",
										"quantity": 1
								}]
						},
						"amount": {
								"currency": "EUR",
								"total": price
						},
						"description": lastInsertId
				}]
			};
			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
						throw error;
				} else {
						for(let i = 0;i < payment.links.length;i++){
							if(payment.links[i].rel === 'approval_url'){
								res.redirect(payment.links[i].href);
							}
						}
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getPaypalNorUsrAccessSuccessRenewal:async function(req,res,next)
	{
		try{
			//console.log("success paypal");
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_urgent = 0;
			var updated_at = new Date();
			var top_search_payment_status = 0;
			var top_search = 0;


			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
						UserPurchasedPlan.findOne({_id:payment_table_id},{}).exec((err, result)=>
						{
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200).send(return_response);
							}else{
								// console.log(result);
								// console.log(result.ad_id);
								// console.log(result.ad_name);
								// console.log(result.payment_type);
								// return false;

								//result.ad_id;
								if(result)
								{

									AddAdvertisementModel.findOne({ _id:result.ad_id },{
										accessoires_image:1,
										accessoires_image_new:1
									},function(errAd,resultAd){
										if(errAd)
										{
											var return_response = { "error":true,success: false,errorMessage:errAd};
												res.status(200)
												.send(return_response);
										}else{
											console.log(resultAd);
											if(!resultAd)
											{
												var return_response = { "error":true,success: false,errorMessage:"ID d'annonce invalide"};
												return res.status(200)
												.send(return_response);
											}
											var accessoires_image = resultAd.accessoires_image;
											var accessoires_image_new = resultAd.accessoires_image_new;
											if(accessoires_image_new.length > 0)
											{
												for (let j = 0; j < accessoires_image_new.length; j++)
												{
													accessoires_image.push(accessoires_image_new[j]);
												}
											}
											// console.log(accessoires_image);
											// return false;
												if(result.basic_plan_id)
												{
													basic_plan_status = 1;
												}
												UserPurchasedPlan.updateOne({ _id:result._id },{
													payment_status:1,
													updated_at:updated_at
												},function(err2,result2){
													if(err2)
													{
														var return_response = { "error":true,success: false,errorMessage:err2};
															res.status(200)
															.send(return_response);
													}else{
														//res.redirect(303, session.url);
														AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
															accessories_max_photo_status:basic_plan_status,
															accessoires_image:accessoires_image,
															accessoires_image_new:[],
															updated_at:updated_at
														},function(err,result){
															if(err)
															{
																var return_response = {"error":true,success: false,errorMessage:err};
																	res.status(200)
																	.send(return_response);
															}else{
																//console.log(result);
																//console.log(result._id);
																// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
																// 	res.status(200)
																// 	.send(return_response);
																var redirect_url_web = base_url_web+"adSuccess";
																res.redirect(303, redirect_url_web);
															}
														});
													}
												});

										}
									})
									
									
								}
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getPaypalNorUsrAccessCancelRenewal:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
			res.status(200).send(return_response);
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	normalUserReactivate:async function(req,res,next)
	{
		try{
			console.log("req.body ", req.body);
			var show_on_web = 1;
			var updated_at = new Date();
			var plan_purchase_date_time = null;
			var plan_end_date_time = null;
			var showing_number_day = 90;
			plan_purchase_date_time = new Date();
			plan_end_date_time = new Date(); // Now
			plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date

			await AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				complition_status:1,
				show_on_web:show_on_web,
				showing_number_day:showing_number_day,
				plan_purchase_date_time:plan_purchase_date_time,
				plan_end_date_time:plan_end_date_time,
				updated_at:updated_at
			}).then((result)=>{
				var return_response = {"error":false,success: true,errorMessage:"Veuillez effectuer votre paiement pour valider votre annonce"};
				return res.status(200).send(return_response);
			}).catch((error)=>{
				 var return_response = {"error":true,success: false,errorMessage:error.message};
				 return res.status(200).send(return_response);
			});

		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error.message};
			return res.status(200).send(return_response);
		}
	}
};