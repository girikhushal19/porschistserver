const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const OrderModel = require("../../models/OrderModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  checkout:async function(req,res,next)
  {

      //var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var random_str = Math.floor(10000000 + Math.random() * 90000000);
      // for (var i = 32; i > 0; --i)
      // {
      //   random_str += chars[Math.floor(Math.random() * chars.length)];
      // }
      // //console.log(random_str);

      var user_id = req.body.user_id;
      var multi_price = 0;var final_price = 0;var shipping_charge = 0;
      CartsModel.find({user_id:user_id},(err, recordOld)=>{
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:errsC};
        res.status(400)
          .send(return_response);
      }else{
        //console.log(recordOld);
        if(recordOld.length > 0)
        {
          //console.log(recordOld);
          var break_loop = recordOld.length - 1;
          for (let i=0; i <recordOld.length; i++)
          {
            //console.log(recordOld[i]._id);
            //console.log(recordOld[i].user_id);
            var ad_owner_user_id = recordOld[i].ad_owner_user_id;
            var ad_id = recordOld[i].ad_id;
            var payment_type = req.body.payment_type;
            var quantity = recordOld[i].quantity;
            //var price = recordOld[i].price;
            var tax = recordOld[i].tax;
            //var shipping_charge = recordOld[i].shipping_charge;
            var address_id = recordOld[i].address_id;

            
            AddAdvertisementModel.findOne({_id:ad_id},{"ad_name":1,"price":1, 'pro_price':1,'category':1,'user_type':1,'registration_year':1,'price_for_pors':1,'description':1,'address':1,'location':1,'state':1,'OEM':1,'type_de_piece':1,'delivery_price':1,'contact_number':1}).exec((err, Advertresult)=>
            {
              //console.log("Advertresult"+Advertresult);
              var price = Advertresult.price;
              multi_price = (price * quantity)+multi_price;
              var ad_name = Advertresult.ad_name;
              var category = Advertresult.category;
              var registration_year = Advertresult.registration_year;
              var description = Advertresult.description;
              var pick_up_address = Advertresult.address;
              var contact_number = Advertresult.contact_number;
              var pick_up_location = Advertresult.location;
              var state = Advertresult.state;
              var OEM = Advertresult.OEM;
              /*console.log("pick_up_location "+pick_up_location);
              console.log("pick_up_location "+pick_up_location.type);
              console.log("pick_up_location "+pick_up_location.coordinates[0]);
              console.log("pick_up_location "+pick_up_location.coordinates[1]);*/

              shipping_charge = parseFloat(Advertresult.delivery_price) + parseFloat(shipping_charge);
              //console.log("shipping_charge "+shipping_charge);
              AttributeModel.findOne({_id:Advertresult.type_de_piece},{"attribute_name":1}).exec((errs, AttributeResult)=>
              {
                //console.log(AttributeResult);
                //console.log(AttributeResult.attribute_name);
                var type_de_piece = AttributeResult.attribute_name;
                //console.log("multi_price "+multi_price);
                var tax_price = multi_price * tax / 100;
                //console.log("tax_price "+tax_price);
                final_price = parseFloat(multi_price) + parseFloat(tax_price) + parseFloat(shipping_charge);
                //console.log("final_price "+final_price);

                DeliveryAddressUser.find({ _id:address_id },(AddressErr, AddRecord)=>
                {
                  if(AddressErr)
                  {
                    console.log("AddressErr");
                    console.log(AddressErr);
                  }else{
                    /*console.log("else no error AddRecord"+AddRecord);
                    console.log("else no error AddRecord"+AddRecord._id);
                    console.log("else no error AddRecord"+AddRecord.address);*/
                    //var delivery_address = AddRecord.address;
                    //var delivery_location = AddRecord.location;
                    //console.log("delivery_location"+AddRecord._id);
                    //return false;
                    /*console.log(AddRecord.location.type);
                    console.log(delivery_location);return false;*/
                    /*var delivery_location.type;
                    var delivery_location.coordinates[0];
                    var delivery_location.coordinates[1];*/
                    OrderModel.create({
                      order_id:random_str,
                      user_id:user_id,
                      ad_owner_user_id:ad_owner_user_id,
                      ad_id:ad_id,
                      quantity:quantity,
                      tax:tax,
                      shipping_charge:Advertresult.delivery_price,
                      price:price,
                      final_price:final_price,
                      pick_up_address:pick_up_address,
                      location: {
                       type: pick_up_location.type,
                       coordinates: [pick_up_location.coordinates[0],pick_up_location.coordinates[1]]
                      },
                      ad_name:ad_name,
                      category:category,
                      registration_year:registration_year,
                      description:description,
                      state:state,
                      OEM:OEM,
                      type_de_piece:type_de_piece,
                      contact_number:contact_number,
                      payment_type:req.body.payment_type
                    },function(err,Finalresult){
                      if(err)
                      {
                        //console.log("iffff");
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200).send(return_response);
                      }else{
                        if(i == break_loop)
                        {
                          //console.log("here");
                          //console.log(Finalresult);
                          //console.log(Finalresult._id);

                          var return_response = {"error":false,success: true,errorMessage:"Please do payment",order_id:random_str};
                          res.status(200).send(return_response);
                        }
                        
                      }
                    });
                  }
                });
              });
            });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"No item in your cart"};
          res.status(200)
            .send(return_response);
        }
      }
    }); 
  },

};