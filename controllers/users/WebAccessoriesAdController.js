const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const UsersModel = require("../../models/UsersModel");
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const OrderModel = require('../../models/OrderModel');

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();


module.exports = {
  webGetAccessoriesPurchase:async function(req,res,next)
  {
    try{
      OrderModel.find({user_id:req.body.user_id,payment_status:1},{}).exec((err,result)=>{
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
        }else{
          if(result.length > 0)
          {
            var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
            res.status(200)
            .send(return_response);
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Pas d'enregistrement",record:result};
            res.status(200)
            .send(return_response);
          }
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200)
      .send(return_response);
    }
  },
  webGetAccessoriesPurchaseSingle:async function(req,res,next)
  {
    try{
      let id = req.body.id;
      if(!req.body.id)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant est requis"});
      }

      let result = await OrderModel.aggregate([
        {
          $match:{
            _id:mongoose.Types.ObjectId(req.body.id)
          }
        },
        {
            $addFields:{
                "mcatone":{$arrayElemAt: ["$ad_id", 0]}
            }
        },
        {
            $addFields:{
                "ad_idd":{"$toString": "$mcatone" }
            }
        },
        {
          $lookup:
          {
            from:"advertisements",
            let:{"addd_id":{"$toObjectId":"$ad_idd"}},
            pipeline:[
              {
                $match:{
                  $expr:{$eq:["$_id","$$addd_id"]}
                }
              }
            ],
            as:"adData"
          }
        }
      ]);
      if(result)
      {
        var return_response = {"error":false,success: true,errorMessage:"Succès",record:result};
        res.status(200)
        .send(return_response);
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Pas d'enregistrement",record:result};
        res.status(200)
        .send(return_response);
      }
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSalesCount:async (req,res)=>{
    try{
      var user_id = req.params.id;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              }
            ]
          }
        },
        {
          $group:{
            "_id":"$order_id"
          }
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result.length};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSales:async (req,res)=>{
    try{
     // mongoose.set("debug",true);
      var user_id = req.body.user_id;
      var pageNo = req.body.pageNo;
      var from_date = req.body.from_date;
      var to_date = req.body.to_date;
      var search_text = req.body.search_text;
      if(pageNo == null || pageNo == undefined)
      {
        pageNo = 0;
      }
      var limitt = 10;
      var sortt = limitt * pageNo;
      var String_qr = { "$match":{"payment_status":1 } };
      String_qr["$match"]['payment_status'] = 1;
      String_qr["$match"]['ad_owner_user_id'] =  mongoose.Types.ObjectId(user_id);

      if(from_date && from_date != "" && from_date != null)
      {
        String_qr["$match"]['payment_success_date'] = {  $gte:from_date } 
      }
      if(to_date  && to_date != ""  && to_date != null)
      {
        String_qr["$match"]['payment_success_date'] = {  $lte: to_date } 
      }
      if(from_date && from_date != "" && from_date != null && to_date  && to_date != ""  && to_date != null)
      {
        String_qr["$match"]['payment_success_date'] = { $gte:from_date , $lte:to_date }
      }
      if(search_text && search_text != "" && search_text != null)
      {
        String_qr["$match"]['order_id'] = {'$regex' : '^'+search_text, '$options' : 'i'}
      }
      console.log(String_qr);
      //mongoose.set("debug",true);
      await OrderModel.aggregate([
        // {
        //   $match:{
        //       $and:[
        //       {
        //         ad_owner_user_id: mongoose.Types.ObjectId(user_id)
        //       },
        //       {
        //         payment_status:1
        //       }
        //     ]
        //   }
        // },
        String_qr,
        {
          $group:{
            "_id":"$order_id",
            "t_id":{"$first":"$_id"},
            "user_id":{"$first":"$user_id"},
            "ad_owner_user_id":{"$first":"$ad_owner_user_id"},
            "ad_id":{"$first":"$ad_id"},
            "quantity":{"$first":"$quantity"},
            "tax":{"$first":"$tax"},
            "shipping_charge":{"$first":"$shipping_charge"},
            "price":{"$first":"$price"},
            "final_price":{"$first":"$final_price"},
            "pick_up_address":{"$first":"$pick_up_address"},
            "pick_up_location":{"$first":"$pick_up_location"},
            "address":{"$first":"$address"},
            "location":{"$first":"$location"},
            "ad_name":{"$first":"$ad_name"},
            "category":{"$first":"$category"},
            "registration_year":{"$first":"$registration_year"},
            "state":{"$first":"$state"},
            "OEM":{"$first":"$OEM"},
            "type_de_piece":{"$first":"$type_de_piece"},
            "contact_number":{"$first":"$contact_number"},
            "payment_type":{"$first":"$payment_type"},
            "shipping_status":{"$first":"$shipping_status"},
            "payment_success_date":{"$first":"$payment_success_date"},
            "payment_time":{"$first":"$payment_time"},
            "firstName":{"$first":"$firstName"},
            "lastName":{"$first":"$lastName"},
            "mobileNumber":{"$first":"$mobileNumber"},
            "email":{"$first":"$email"},
          }
        },
        {
          $project:{
            "_id":1,
            "t_id":1,
            "user_id":1,
            "ad_owner_user_id":1,
            "ad_id":1,
            "quantity":1,
            "tax":1,
            "shipping_charge":1,
            "price":1,
            "final_price":1,
            "pick_up_address":1,
            "pick_up_location":1,
            "address":1,
            "location":1,
            "ad_name":1,
            "price":1,
            "category":1,
            "registration_year":1,
            "state":1,
            "OEM":1,
            "type_de_piece":1,
            "contact_number":1,
            "payment_type":1,
            "shipping_status":1,
            "payment_success_date":1,
            "payment_time":1,
            "firstName":1,
            "lastName":1,
            "mobileNumber":1,
            "email":1
          }
        },
        {
          $skip:sortt
        },
        {
          $limit:limitt
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },



  webGetOwnSalesProcessCount:async (req,res)=>{
    try{
      var user_id = req.params.id;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:1
              }
            ]
          }
        },
        {
          $group:{
            "_id":"$order_id"
          }
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result.length};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSalesProcess:async (req,res)=>{
    try{
     // mongoose.set("debug",true);
      var user_id = req.body.user_id;
      var pageNo = req.body.pageNo;
      if(pageNo == null || pageNo == undefined)
      {
        pageNo = 0;
      }
      var limitt = 10;
      var sortt = limitt * pageNo;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:1
              }
              
            ]
          }
        },
        {
          $addFields:{
            user_idd:{$arrayElemAt:["$user_id",0]}
          }
        },
        {
          $lookup:{
            from:"users",
            let:{ "usrObjId":{"$toObjectId":"$user_idd"} },
            pipeline:[
              {
                $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
              }
            ],
            as:"userDetail"
          }
        },
        {
          $group:{
            "_id":"$order_id",
            "t_id":{"$first":"$_id"},
            "user_id":{"$first":"$user_id"},
            "ad_owner_user_id":{"$first":"$ad_owner_user_id"},
            "ad_id":{"$first":"$ad_id"},
            "quantity":{"$first":"$quantity"},
            "tax":{"$first":"$tax"},
            "shipping_charge":{"$first":"$shipping_charge"},
            "price":{"$first":"$price"},
            "final_price":{"$first":"$final_price"},
            "pick_up_address":{"$first":"$pick_up_address"},
            "pick_up_location":{"$first":"$pick_up_location"},
            "address":{"$first":"$address"},
            "location":{"$first":"$location"},
            "ad_name":{"$first":"$ad_name"},
            "category":{"$first":"$category"},
            "registration_year":{"$first":"$registration_year"},
            "state":{"$first":"$state"},
            "OEM":{"$first":"$OEM"},
            "type_de_piece":{"$first":"$type_de_piece"},
            "contact_number":{"$first":"$contact_number"},
            "payment_type":{"$first":"$payment_type"},
            "shipping_status":{"$first":"$shipping_status"},
            "payment_success_date":{"$first":"$payment_success_date"},
            "payment_time":{"$first":"$payment_time"},
            "firstName":{"$first":"$firstName"},
            "lastName":{"$first":"$lastName"},
            "mobileNumber":{"$first":"$mobileNumber"},
            "email":{"$first":"$email"},
          }
        },
        {
          $project:{
            "_id":1,
            "t_id":1,
            "user_id":1,
            "ad_owner_user_id":1,
            "ad_id":1,
            "quantity":1,
            "tax":1,
            "shipping_charge":1,
            "price":1,
            "final_price":1,
            "pick_up_address":1,
            "pick_up_location":1,
            "address":1,
            "location":1,
            "ad_name":1,
            "price":1,
            "category":1,
            "registration_year":1,
            "state":1,
            "OEM":1,
            "type_de_piece":1,
            "contact_number":1,
            "payment_type":1,
            "shipping_status":1,
            "payment_success_date":1,
            "payment_time":1,
            "firstName":1,
            "lastName":1,
            "mobileNumber":1,
            "email":1
          }
        },
        {
          $skip:sortt
        },
        {
          $limit:limitt
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },

  webGetOwnSalesCancelCount:async (req,res)=>{
    try{
      var user_id = req.params.id;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:2
              }
            ]
          }
        },
        {
          $group:{
            "_id":"$order_id"
          }
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result.length};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSalesCancel:async (req,res)=>{
    try{
     // mongoose.set("debug",true);
      var user_id = req.body.user_id;
      var pageNo = req.body.pageNo;
      if(pageNo == null || pageNo == undefined)
      {
        pageNo = 0;
      }
      var limitt = 10;
      var sortt = limitt * pageNo;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:2
              }
              
            ]
          }
        },
        {
          $addFields:{
            user_idd:{$arrayElemAt:["$user_id",0]}
          }
        },
        {
          $lookup:{
            from:"users",
            let:{ "usrObjId":{"$toObjectId":"$user_idd"} },
            pipeline:[
              {
                $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
              }
            ],
            as:"userDetail"
          }
        },
        {
          $group:{
            "_id":"$order_id",
            "t_id":{"$first":"$_id"},
            "user_id":{"$first":"$user_id"},
            "ad_owner_user_id":{"$first":"$ad_owner_user_id"},
            "ad_id":{"$first":"$ad_id"},
            "quantity":{"$first":"$quantity"},
            "tax":{"$first":"$tax"},
            "shipping_charge":{"$first":"$shipping_charge"},
            "price":{"$first":"$price"},
            "final_price":{"$first":"$final_price"},
            "pick_up_address":{"$first":"$pick_up_address"},
            "pick_up_location":{"$first":"$pick_up_location"},
            "address":{"$first":"$address"},
            "location":{"$first":"$location"},
            "ad_name":{"$first":"$ad_name"},
            "category":{"$first":"$category"},
            "registration_year":{"$first":"$registration_year"},
            "state":{"$first":"$state"},
            "OEM":{"$first":"$OEM"},
            "type_de_piece":{"$first":"$type_de_piece"},
            "contact_number":{"$first":"$contact_number"},
            "payment_type":{"$first":"$payment_type"},
            "shipping_status":{"$first":"$shipping_status"},
            "payment_success_date":{"$first":"$payment_success_date"},
            "payment_time":{"$first":"$payment_time"},
            "firstName":{"$first":"$firstName"},
            "lastName":{"$first":"$lastName"},
            "mobileNumber":{"$first":"$mobileNumber"},
            "email":{"$first":"$email"},
          }
        },
        {
          $project:{
            "_id":1,
            "t_id":1,
            "user_id":1,
            "ad_owner_user_id":1,
            "ad_id":1,
            "quantity":1,
            "tax":1,
            "shipping_charge":1,
            "price":1,
            "final_price":1,
            "pick_up_address":1,
            "pick_up_location":1,
            "address":1,
            "location":1,
            "ad_name":1,
            "price":1,
            "category":1,
            "registration_year":1,
            "state":1,
            "OEM":1,
            "type_de_piece":1,
            "contact_number":1,
            "payment_type":1,
            "shipping_status":1,
            "payment_success_date":1,
            "payment_time":1,
            "firstName":1,
            "lastName":1,
            "mobileNumber":1,
            "email":1
          }
        },
        {
          $skip:sortt
        },
        {
          $limit:limitt
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSalesCompleteCount:async (req,res)=>{
    try{
      var user_id = req.params.id;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:3
              }
            ]
          }
        },
        {
          $group:{
            "_id":"$order_id"
          }
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result.length};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSalesComplete:async (req,res)=>{
    try{
     // mongoose.set("debug",true);
      var user_id = req.body.user_id;
      var pageNo = req.body.pageNo;
      if(pageNo == null || pageNo == undefined)
      {
        pageNo = 0;
      }
      var limitt = 10;
      var sortt = limitt * pageNo;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                ad_owner_user_id: mongoose.Types.ObjectId(user_id)
              },
              {
                payment_status:1
              },
              {
                shipping_status:3
              }
              
            ]
          }
        },
        {
          $addFields:{
            user_idd:{$arrayElemAt:["$user_id",0]}
          }
        },
        {
          $lookup:{
            from:"users",
            let:{ "usrObjId":{"$toObjectId":"$user_idd"} },
            pipeline:[
              {
                $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
              }
            ],
            as:"userDetail"
          }
        },
        {
          $group:{
            "_id":"$order_id",
            "t_id":{"$first":"$_id"},
            "user_id":{"$first":"$user_id"},
            "ad_owner_user_id":{"$first":"$ad_owner_user_id"},
            "ad_id":{"$first":"$ad_id"},
            "quantity":{"$first":"$quantity"},
            "tax":{"$first":"$tax"},
            "shipping_charge":{"$first":"$shipping_charge"},
            "price":{"$first":"$price"},
            "final_price":{"$first":"$final_price"},
            "pick_up_address":{"$first":"$pick_up_address"},
            "pick_up_location":{"$first":"$pick_up_location"},
            "address":{"$first":"$address"},
            "location":{"$first":"$location"},
            "ad_name":{"$first":"$ad_name"},
            "category":{"$first":"$category"},
            "registration_year":{"$first":"$registration_year"},
            "state":{"$first":"$state"},
            "OEM":{"$first":"$OEM"},
            "type_de_piece":{"$first":"$type_de_piece"},
            "contact_number":{"$first":"$contact_number"},
            "payment_type":{"$first":"$payment_type"},
            "shipping_status":{"$first":"$shipping_status"},
            "payment_success_date":{"$first":"$payment_success_date"},
            "payment_time":{"$first":"$payment_time"},
            "firstName":{"$first":"$firstName"},
            "lastName":{"$first":"$lastName"},
            "mobileNumber":{"$first":"$mobileNumber"},
            "email":{"$first":"$email"},
          }
        },
        {
          $project:{
            "_id":1,
            "t_id":1,
            "user_id":1,
            "ad_owner_user_id":1,
            "ad_id":1,
            "quantity":1,
            "tax":1,
            "shipping_charge":1,
            "price":1,
            "final_price":1,
            "pick_up_address":1,
            "pick_up_location":1,
            "address":1,
            "location":1,
            "ad_name":1,
            "price":1,
            "category":1,
            "registration_year":1,
            "state":1,
            "OEM":1,
            "type_de_piece":1,
            "contact_number":1,
            "payment_type":1,
            "shipping_status":1,
            "payment_success_date":1,
            "payment_time":1,
            "firstName":1,
            "lastName":1,
            "mobileNumber":1,
            "email":1
          }
        },
        {
          $skip:sortt
        },
        {
          $limit:limitt
        }
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },


  webShippingProcess:async(req,res)=>{
    try{
      var id = req.params.id;
      var orders = await OrderModel.findOne({order_id:id,payment_status:1},{_id:1});
      if(orders)
      {
        await OrderModel.updateMany({order_id:id},{shipping_status:1}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"La commande est en cours de traitement"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Le paiement de la commande n'est pas encore effectué"};
          res.status(200)
          .send(return_response);
      }      
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webShippingCancel:async(req,res)=>{
    try{
      var id = req.params.id;
      var orders = await OrderModel.findOne({order_id:id,payment_status:1},{_id:1});
      if(orders)
      {
        await OrderModel.updateMany({order_id:id},{shipping_status:2}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"La commande est en cours de traitement"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Le paiement de la commande n'est pas encore effectué"};
          res.status(200)
          .send(return_response);
      }      
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webShippingComplete:async(req,res)=>{
    try{
      var id = req.params.id;
      var orders = await OrderModel.findOne({order_id:id,payment_status:1},{_id:1,shipping_status:1,seller_profit:1,ad_owner_user_id:1});
      if(orders)
      {
        if(orders.shipping_status == 2)
        {
          var return_response = {"error":true,success: false,errorMessage:"Cette commande a été annulée vous ne pouvez pas compléter cette commande contactez l'administrateur"};
            res.status(200)
            .send(return_response);
        }else{
          let seller_profit = orders.seller_profit;
          let ad_owner_user_id = orders.ad_owner_user_id;
          ad_owner_user_id = ad_owner_user_id.toString();
          console.log("seller_profit ", seller_profit);
          console.log("ad_owner_user_id ", ad_owner_user_id);
          let user_record = await UsersModel.findOne({_id:ad_owner_user_id},{seller_pending_amount:1});

          //seller_pending_amount
          //return false;
          if(orders.shipping_status == 3)
          {
            var return_response = {"error":true,success: false,errorMessage:"Commande déjà livrée"};
            return res.status(200).send(return_response);
          }else{
            await OrderModel.updateMany({order_id:id},{shipping_status:3}).then(async (result)=>{
              let seller_pending_amount = parseFloat(seller_profit) + parseFloat(user_record.seller_pending_amount);
               console.log("seller_pending_amount ", seller_pending_amount);
              // return false;
              //mongoose.set("debug",true);
              await UsersModel.updateOne(
                {_id:ad_owner_user_id},
                {seller_pending_amount:seller_pending_amount}).then((newResult)=>{
                  var return_response = {"error":false,success: true,errorMessage:"La commande est en cours de traitement"};
                  return res.status(200).send(return_response);
                }).catch((error)=>{
                  var return_response = {"error":true,success: false,errorMessage:error.message};
                  res.status(200)
                  .send(return_response);
                });
            }).catch((error)=>{
              var return_response = {"error":true,success: false,errorMessage:error.message};
              res.status(200)
              .send(return_response);
            });
          }
        }
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Le paiement de la commande n'est pas encore effectué"};
          res.status(200)
          .send(return_response);
      }      
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  webGetOwnSingleSales:async (req,res)=>{
    try{
      var order_id = req.params.id;
      await OrderModel.aggregate([
        {
          $match:{
              $and:[
              {
                order_id: order_id
              }
            ]
          }
        },
        {
          $addFields:{
            user_idd:{$arrayElemAt:["$user_id",0]}
          }
        },
        {
          $lookup:{
            from:"users",
            let:{ "usrObjId":{"$toObjectId":"$user_idd"} },
            pipeline:[
              {
                $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
              }
            ],
            as:"userDetail"
          }
        },
        
        
      ]).then((result)=>{
        //console.log(result);
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  },
  
};