const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const UserfavadsModel = require("../../models/UserfavadsModel");
const SearchHistoryModel = require("../../models/SearchHistoryModel");
const FollowSellerModel = require("../../models/FollowSellerModel");
const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const OrderModel = require("../../models/OrderModel");
const RatingModel = require("../../models/RatingModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const { parse } = require('path');
const app = express();
//const customConstant = require('../../helpers/customConstant');
 
/*/*/
module.exports = {

	getWebHomeCarAd_TEST:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15000000;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    try{
      var  user_id= req.body.user_id;
      //console.log(user_id);
      //console.log(req.body);
      //console.log(req.query.category);
      var String_qr = {};
      //Porsche Voitures
      String_qr['category'] = "Porsche Voitures";
      String_qr['show_on_web'] = 1;
      String_qr['complition_status'] = 1;
      String_qr["delete_at"] = 0;
      String_qr["activation_status"] = 1;
      //mongoose.set("debug",true);
      let logged_in_user_id = req.body.user_id;
      if(logged_in_user_id)
      {
        logged_in_user_id = mongoose.Types.ObjectId(logged_in_user_id);
      }
      await AddAdvertisementModel.aggregate([
        {
          $match:String_qr
        },
        {
          $addFields:{
            "ad_id":"$_id"
          }
        },
        {
          $lookup:{
            from:"userfavads",
            let:{"add_id":{"$toObjectId":"$ad_id"} },
            pipeline:[
                {
                  $match:{$expr:{
                    $and:[
                      {$eq:["$user_id", logged_in_user_id ] },
                      {$eq:["$ad_id","$$add_id"]}
                    ]
                    }
                  }
                }
            ],
            "as":"favAd"
          }
        },
        {
          $addFields:{
            "isFav":{$size:"$favAd"}
          }
        },
        // {
        //   $addFields:{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $project:{
            'top_urgent':1,
            'ad_name':1,
            '_id':1,
            'exterior_image':1,
            'address':1,
            'price':1,
            'pro_price':1,
            'userId':1,
            'created_at':1,
            'user_id':1,
            'top_search':1,
            'isFav':1,
            //'tmpOrder':1
          }
        },
        {
          $sort:{
            'top_search':-1,
            //'tmpOrder':1
          }
        }
      ]).then((result)=>{
        return res.status(200).send({
          error:false,
          message:"Success",
          record:result
        });
      }).catch((error)=>{
        return res.status(200).send({
          error:true,
          message:error.message
        });
      })
    }catch(err){
      return res.status(200).send({
        error:true,
        message:err.message
      });
    }
  
	},
  getWebHomeAccessAd_TEST:async function(req,res,next)
  {
    try{
      const { user_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      //,"complition_status":1,"activation_status":1
      var imagePath = customConstant.base_url;
      
      var String_qr = {};
      String_qr['category'] = "Pièces et accessoires";
      String_qr['show_on_web'] = 1;
      String_qr['complition_status'] = 1;
      String_qr["delete_at"] = 0;
      String_qr["activation_status"] = 1;
      //mongoose.set("debug",true);
      
      if(req.body.user_id)
      {
        await AddAdvertisementModel.aggregate([
          {
            $match:String_qr
          },
          {
            $addFields:{
              "ad_id":"$_id"
            }
          },
          {
            $lookup:{
              from:"userfavads",
              let:{"add_id":{"$toObjectId":"$ad_id"} },
              pipeline:[
                  {
                    $match:{$expr:{
                      $and:[
                        {$eq:["$user_id", mongoose.Types.ObjectId(req.body.user_id)] },
                        {$eq:["$ad_id","$$add_id"]}
                      ]
                      }
                    }
                  }
              ],
              "as":"favAd"
            }
          },
          {
            $addFields:{
              "isFav":{$size:"$favAd"}
            }
          },
          // {
          //   $addFields:{
          //     "tmpOrder":{'$rand':{} }
          //   }
          // },
          {
            $project:{
              'top_urgent':1,
              'ad_name':1,
              '_id':1,
              'address':1,
              'price':1,
              'pro_price':1,
              'userId':1,
              'created_at':1,
              'user_id':1,
              'top_search':1,
              'isFav':1,
              'accessoires_image':1,
              //'tmpOrder':1
            }
          },
          {
            $sort:{
              'top_search':-1,
              //'tmpOrder':1
            }
          }
        ]).then((result)=>{
          return res.status(200).send({
            error:false,
            message:"Success",
            record:result
          });
        }).catch((error)=>{
          return res.status(200).send({
            error:true,
            message:error.message
          });
        })
      }else{
        await AddAdvertisementModel.aggregate([
          {
            $match:String_qr
          },
          {
            $addFields:{
              "ad_id":"$_id"
            }
          },
          {
            $lookup:{
              from:"userfavads",
              let:{"add_id":{"$toObjectId":"$ad_id"} },
              pipeline:[
                  {
                    $match:{$expr:{
                      $and:[
                        {$eq:["$user_id", "$$add_id"] },
                        {$eq:["$ad_id","$$add_id"]}
                      ]
                      }
                    }
                  }
              ],
              "as":"favAd"
            }
          },
          {
            $addFields:{
              "isFav":{$size:"$favAd"}
            }
          },
          // {
          //   $addFields:{
          //     "tmpOrder":{'$rand':{} }
          //   }
          // },
          {
            $project:{
              'top_urgent':1,
              'ad_name':1,
              '_id':1,
              'address':1,
              'price':1,
              'pro_price':1,
              'userId':1,
              'created_at':1,
              'user_id':1,
              'top_search':1,
              'isFav':1,
              'accessoires_image':1,
              //'tmpOrder':1
            }
          },
          {
            $sort:{
              'top_search':-1,
              //'tmpOrder':1
            }
          }
        ]).then((result)=>{
          return res.status(200).send({
            error:false,
            message:"Success",
            record:result
          });
        }).catch((error)=>{
          return res.status(200).send({
            error:true,
            message:error.message
          });
        })
      }
    }catch(err){
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: err
      });
    }
  },
  getWebProductList:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    try{
      //console.log("hereee");
      var  user_id= req.body.user_id;
      //console.log(user_id);
      //console.log(req.body);
      //console.log("hereeeeeeee "+JSON.stringify(req.body));
      //return false;
      //console.log(req.query.category);
      let dquery={};
      
      
      

      //var { category,carModelArray,fuel,vehicle_condition,type_of_gearbox,colorId,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior ,minPrice ,maxPrice,yearArray,yearArray_2,chahisArray,deductible_VAT,minKilometer,maxKilometer,model_variant,colorArray,accessModel,subModel,type_de_piece,state,OEM,minAccessPrice,maxAccessPrice,yearArrayAccess,userAddress,userLatitude,userLongitude,ad_name,pageNo,sorting_var,carVersion,report_piwi,pors_warranty,vendeurType,onlineSince,nearByKm,zip_code,city,country } = req.body;
      var { category,carModelArray,fuel,vehicle_condition,type_of_gearbox,colorId,colorInteriorId,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior ,minPrice ,maxPrice,yearArray,yearArray_2,chahisArray,deductible_VAT,minKilometer,maxKilometer,model_variant,colorArray,accessModel,subModel,type_de_piece,state,OEM,minAccessPrice,maxAccessPrice,yearArrayAccess,userAddress,userLatitude,userLongitude,ad_name,user_id,nearByKm,onlineSince,vendeurType,pors_warranty,report_piwi,carVersion,zip_code,city,country,accessRegYear,accessRegYear_2,pageNo,sorting_var } = req.body;

      let datee = new Date();
      let today_day = datee.getDay();
      console.log("today_day ---->>>>>   ,,  ", today_day);
      today_day = today_day + 1;

      var limitt = 4;
      if(pageNo == "" || pageNo == null || pageNo == undefined)
      {
        pageNo = 0;
        var skip = limitt*pageNo;
      }else{
        var skip = limitt*pageNo;
      }
      console.log("pageNo "+pageNo);
      if(yearArray)
      {
        yearArray = parseFloat(yearArray);
      }
      if(yearArray_2)
      {
        yearArray_2 = parseFloat(yearArray_2);
      }
      if(accessRegYear)
      {
        accessRegYear = parseFloat(accessRegYear);
      }
      if(accessRegYear_2)
      {
        accessRegYear_2 = parseFloat(accessRegYear_2);
      }

      if(minPrice)
      {
        minPrice = parseFloat(minPrice);
      }
      if(maxPrice)
      {
        maxPrice = parseFloat(maxPrice);
      }
      if(minKilometer)
      {
        minKilometer = parseFloat(minKilometer);
      }
      if(maxKilometer)
      {
        maxKilometer = parseFloat(maxKilometer);
      }

       
      
      
      var String_qr = {$match:{"show_on_web":1} };
      //var String_qr = {};
		  //console.log("String_qr 569");
      //console.log(String_qr);
      var query_array = [];
      //Porsche Voitures
      //String_qr['category'] = "Porsche Voitures";
      if(category && category != "" && category == "Porsche Voitures")
      {
        String_qr["$match"]['category'] = category;

        if(colorId && colorId != "")
        {
          String_qr["$match"]['colorId'] = mongoose.Types.ObjectId(colorId);
        }
        if(colorInteriorId && colorInteriorId != "")
        {
          String_qr["$match"]['colorIdInterior'] = mongoose.Types.ObjectId(colorInteriorId);
        }
        console.log("carModelArray -------406------------  ", carModelArray);
        if(carModelArray && carModelArray != ""  && carModelArray != "undefined")
        {
          console.log("carModelArray ", carModelArray);
          //mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          //console.log(newArr);
          String_qr["$match"]['modelId'] = mongoose.Types.ObjectId(carModelArray);
          //String_qr = { "$match":{ "modelId": { "$in":newArr } } }
        }
        if(yearArray && yearArray != "" && yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 967");
          String_qr["$match"]['registration_year'] = {  $gte:yearArray,$lte:yearArray_2 };
          //String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
          //String_qr = { "$match":{ "registration_year":{ "$in":yearArray } } }
        }else if(yearArray && yearArray != "")
        {
          //console.log("iffff 973");
          String_qr["$match"]['registration_year'] = yearArray;
        }else if(yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 977");
          String_qr["$match"]['registration_year'] = yearArray_2;
        }
        if(chahisArray && chahisArray != "")
        {
          // const newArr = chahisArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // }); chahisArray
          String_qr["$match"]['type_of_chassis'] = mongoose.Types.ObjectId(chahisArray);
          //String_qr = { "$match":{ "type_of_chassis": { "$in":newArr } } }
        }
        if(fuel && fuel != "")
        {
          String_qr["$match"]['fuel'] = fuel;
        }
        if(vehicle_condition && vehicle_condition != "")
        {
          String_qr["$match"]['vehicle_condition'] = vehicle_condition;
        }
        if(type_of_gearbox && type_of_gearbox != "")
        {
          String_qr["$match"]['type_of_gearbox'] = type_of_gearbox;
        }
        if(warranty && warranty != "")
        {
          String_qr["$match"]['warranty'] = warranty;
        }
        if(maintenance_booklet && maintenance_booklet != "")
        {
          String_qr["$match"]['maintenance_booklet'] = maintenance_booklet;
        }
        if(maintenance_invoice && maintenance_invoice != "")
        {
          String_qr["$match"]['maintenance_invoice'] = maintenance_invoice;
        }
        if(accidented && accidented != "")
        {
          String_qr["$match"]['accidented'] = accidented;
        }
        if(original_paint && original_paint != "")
        {
          String_qr["$match"]['original_paint'] = original_paint;
        }
        if(matching_number && matching_number != "")
        {
          String_qr["$match"]['matching_number'] = matching_number;
        }
        if(matching_color_paint && matching_color_paint != "")
        {
          String_qr["$match"]['matching_color_paint'] = matching_color_paint;
        }
        if(matching_color_interior && matching_color_interior != "")
        {
          String_qr["$match"]['matching_color_interior'] = matching_color_interior;
        }
        if(deductible_VAT && deductible_VAT != "")
        {
          String_qr["$match"]['deductible_VAT'] = deductible_VAT;
        }
        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }
        if(minKilometer && minKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $gte:minKilometer } 
        }
        if(maxKilometer && maxKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $lte:maxKilometer } 
        }
        if(minKilometer && minKilometer != "" && maxKilometer && maxKilometer != "")
        {
          //String_qr = { $gte:minKilometer , $lte:maxKilometer };
          String_qr["$match"]['mileage_kilometer'] = { $gte:minKilometer , $lte:maxKilometer };
        }
        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
        

        if(pors_warranty && pors_warranty != "")
        {
          String_qr["$match"]['pors_warranty'] = pors_warranty;
        }

        if(report_piwi && report_piwi != "")
        {
          String_qr["$match"]['report_piwi'] = report_piwi;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(carVersion && carVersion != "")
        {
          String_qr["$match"]['model_variant'] = carVersion;
        }
        if(onlineSince)
        {
          onlineSince = parseFloat(onlineSince);
          let today_date = new Date();
          let previous_date = today_date.setDate(today_date.getDate() - onlineSince);
         
          previous_date = today_date.toISOString();
          //console.log("previous_date ", previous_date); 
          let dsdsd = new Date(previous_date);
           //console.log("dsdsd ", dsdsd);
          // console.log("today_date ", today_date);
          //plan_end_date_time:{ $lte:today_date }
          String_qr["$match"]['created_at'] = { $gte:dsdsd  };
        }
        if(model_variant && model_variant != "")
        {
          String_qr["$match"]['subModelId'] = mongoose.Types.ObjectId(model_variant)

          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          // //console.log(newArr);
          // String_qr["$match"]['modelId'] = {  $in:newArr };


        }
        if(city && city != "")
        {
          String_qr["$match"]['city'] = {'$regex' : '^'+city, '$options' : 'i'}
        }
        if(country && country != "")
        {
          String_qr["$match"]['country'] = {'$regex' : '^'+country, '$options' : 'i'}
        }
        if(zip_code && zip_code != "")
        {
          String_qr["$match"]['zip_code'] = zip_code;
        }

        //vendeurType  , carVersion
        
      }else{
        //console.log("hereeeeee");
        
        if(category && category != "")
        {
          String_qr['$match']['category'] = category;
          //String_qr = {$match:{'category':category}};
        }
        if(accessModel && accessModel != "")
        {
          String_qr['$match']['modelId'] = mongoose.Types.ObjectId(accessModel);
        }
        if(subModel && subModel != "")
        {
          String_qr['$match']['subModelId'] = mongoose.Types.ObjectId(subModel);
        }
        if(type_de_piece && type_de_piece != "")
        {
          String_qr['$match']['type_de_piece'] = mongoose.Types.ObjectId(type_de_piece);
        }
        if(state && state != "")
        {
          String_qr['$match']['state'] = state;
        }
        if(OEM && OEM != "")
        {
          String_qr['$match']['OEM'] = OEM;
        }
        // if(minAccessPrice && minAccessPrice != "")
        // {
        //   //console.log("minAccessPrice "+minAccessPrice);
        //   String_qr['$match']['price'] = {  $gte:minAccessPrice } 
        // }
        // if(maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice } 
        // }
        // if(minAccessPrice && minAccessPrice != "" && maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice  , $gte:minAccessPrice } 
        // }


        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }


        if(accessRegYear && accessRegYear != "" && accessRegYear_2 && accessRegYear_2 != "")
        {
          String_qr["$match"]['registration_year'] = {  $gte:accessRegYear,$lte:accessRegYear_2 };
        }else if(accessRegYear && accessRegYear != "")
        {
          console.log("iffff 973");
          String_qr["$match"]['registration_year'] = accessRegYear;
        }else if(accessRegYear_2 && accessRegYear_2 != "")
        {
          console.log("iffff 977");
          String_qr["$match"]['registration_year'] = accessRegYear_2;
        }

        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
      }
      if(userAddress != '' && userLatitude != ''  && userLongitude != ''  && userLatitude != 0  && userLongitude != 0 && userAddress != undefined && userLatitude != undefined  && userLongitude != undefined)
      {
         console.log("hereee ifff 1344   ");
        console.log("userAddress "+userAddress);
        console.log("userLatitude "+userLatitude);
        console.log("userLongitude "+userLongitude);
        let newradius=nearByKm*1000;
        dquery ={
          $geoNear:{
            near:{
              type:"Point",coordinates:[ userLongitude,userLatitude ]
            },
            distanceMultiplier: 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
          }
        }
        query_array.push(dquery);
      }
      
      
		  //console.log(req.body);
		  //console.log("String_qr 763");
      //console.log(String_qr);
      //return false;
      query_array.push(String_qr);
      //console.log(query_array);
      // query_array.push(
      //   {
      //     $count: "totalRec"
      //   });
       

      var sortt = {
        
          "$sort":{
            'top_search':-1
          }
        
      };
      if(sorting_var != "")
      {
        if(sorting_var == "price_asc")
        {
          sortt = { "$sort":{ 'price': 1 } };
        }else if(sorting_var == "price_desc")
        {
          sortt = { "$sort":{ 'price': -1 } };
        }else if(sorting_var == "registration_year_asc")
        {
          sortt = { "$sort":{ 'registration_year': 1 } };
        }else if(sorting_var == "registration_year_desc")
        {
          sortt = { "$sort":{ 'registration_year': -1 } };
        }else if(sorting_var == "kilometer_asc")
        {
          sortt = { "$sort":{ 'mileage_kilometer': 1 } };
        }else if(sorting_var == "kilometer_desc")
        {
          sortt = { "$sort":{  'mileage_kilometer': -1 } };
        }
      }
      
      query_array.push(

        {
          $addFields:{
            "ad_id":"$_id"
          }
        },
        {
          $addFields:{
            "usssssrrrrr_id":user_id
          }
        },
        {
          "$lookup":{
            from:"userfavads",
            let:{"pro_id":{"$toObjectId":"$_id"}},
                  pipeline:[
                      {
                      $match:{
                          $expr:{
                              $and:[
                                  {$eq:["$user_id","$user_id"]},
                                  {$eq:["$ad_id","$$pro_id"]}
                              ]
                          }
                      }
                  }
              ],
              as:"favProduct"
          }
        },
        {
          $addFields:{
            "isFav":{$size:"$favProduct"}
          }
        },
        {
          $addFields:{
            top_search:{
              $cond:{
                if:{$eq:["$top_search_which_day",today_day]},
                then:1,
                else:{
                  $cond:{
                    if:{$eq:["$top_search",1]},
                    then:1,
                    else:0
                  }
                }
              }
            }
          }
        },
        // {
        //   "$addFields":{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          "$project":{
            'top_urgent':1,
            'ad_name':1,
            '_id':1,
            'exterior_image':1,
            'category':1,
            'accessoires_image':1,
            'address':1,
            'price':1,
            'pro_price':1,
            'userId':1,
            'created_at':1,
            'user_id':1,
            'top_search':1,
            'isFav':1,
            'registration_year':1,
            'mileage_kilometer':1
          }
        },
        {
          '$skip':skip
        },
        {
          '$limit':limitt
        },
        sortt
      );
      // console.log("query_array");
      // console.log(JSON.stringify(query_array));
      //var cc = JSON.stringify(query_array);
      console.log("ad queryyyyyyyyy")
      //mongoose.set("debug",true);
      let a_dist = 100*1000;
      await AddAdvertisementModel.aggregate(
          query_array
        ).then((result)=>{
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"No Record","record":result};
            res.status(200).send(return_response);
          }
        // var return_response = {"error":false,errorMessage:"success","record":result};
        // res.status(200).send(return_response);
      }).catch((error)=>{
        //console.log(error);
        var return_response = {"error":true,errorMessage:error.message};
        res.status(200).send(return_response);
      });
      // AddAdvertisementModel.count( String_qr , (err, result)=>
      // {
      //   if(err)
      //   {
      //     console.log(err);
      //   }else{
      //     /*console.log("result ");
      //     console.log(typeof result);*/
      //     //console.log("result"+result);
      //     var return_response = {"error":false,errorMessage:"success","record":result};
      //     res.status(200).send(return_response);
      //   }
      // });
    }catch(err){
      console.log(err);
    }
	},

	getWebHomeProductCount:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    try{
      //console.log("hereee");
      // var  user_id= req.body.user_id;
      //console.log(user_id);
      //console.log(req.body);return false;
      //console.log("hereeeeeeee "+JSON.stringify(req.body));
      //
      //console.log(req.query.category);
      let dquery={};
      // report_piwi: 'yes',category: 'Porsche Voitures',pors_warranty: 'yes',warranty: 'yes',
      // maintenance_booklet: 'yes',maintenance_invoice: 'no',original_paint: 'yes',
      // matching_color_paint: 'yes',onlineSince: '7',vendeurType: 'Particulier',
      // model_variant: 'sdsad',user_id: '6537a36ffeadb9c4b500654c'

      
      var { category,carModelArray,fuel,vehicle_condition,type_of_gearbox,colorId,colorInteriorId,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior ,minPrice ,maxPrice,yearArray,yearArray_2,chahisArray,deductible_VAT,minKilometer,maxKilometer,model_variant,colorArray,accessModel,subModel,type_de_piece,state,OEM,minAccessPrice,maxAccessPrice,yearArrayAccess,userAddress,userLatitude,userLongitude,ad_name,user_id,nearByKm,onlineSince,vendeurType,pors_warranty,report_piwi,carVersion,zip_code,city,country,accessRegYear,accessRegYear_2 } = req.body;

      //console.log("onlineSince ", onlineSince);
      if(yearArray)
      {
        yearArray = parseFloat(yearArray);
      }
      if(yearArray_2)
      {
        yearArray_2 = parseFloat(yearArray_2);
      }
      if(accessRegYear)
      {
        accessRegYear = parseFloat(accessRegYear);
      }
      if(accessRegYear_2)
      {
        accessRegYear_2 = parseFloat(accessRegYear_2);
      }

      // First of month
       
      // top_search_plan_purchase_date_time:{ $lte:today_date },
      // top_search_plan_end_date_time:{ $gte:today_date }


      //return false;
      


      if(!nearByKm)
      {
        nearByKm = 100;
      }
      var String_qr = { "$match":{"show_on_web":1 } };
      //String_qr["$match"]["show_on_web"] = 1;
      var query_array = [];
      //Porsche Voitures
      //String_qr['category'] = "Porsche Voitures";
      if(category && category != "" && category == "Porsche Voitures")
      {
        String_qr["$match"]['category'] = category;

        if(colorId && colorId != "")
        {
          String_qr["$match"]['colorId'] = mongoose.Types.ObjectId(colorId);
        }
        if(colorInteriorId && colorInteriorId != "")
        {
          String_qr["$match"]['colorIdInterior'] = mongoose.Types.ObjectId(colorInteriorId);
        }
        
        if(carModelArray && carModelArray != "")
        {
          
          //mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          //console.log(newArr);
          String_qr["$match"]['modelId'] = mongoose.Types.ObjectId(carModelArray);
          //String_qr = { "$match":{ "modelId": { "$in":newArr } } }
        }
        if(yearArray && yearArray != "" && yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 967");
          String_qr["$match"]['registration_year'] = {  $gte:yearArray,$lte:yearArray_2 };
          //String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
          //String_qr = { "$match":{ "registration_year":{ "$in":yearArray } } }
        }else if(yearArray && yearArray != "")
        {
          //console.log("iffff 973");
          String_qr["$match"]['registration_year'] = yearArray;
        }else if(yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 977");
          String_qr["$match"]['registration_year'] = yearArray_2;
        }
        if(chahisArray && chahisArray != "")
        {
          // const newArr = chahisArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // }); chahisArray
          String_qr["$match"]['type_of_chassis'] = mongoose.Types.ObjectId(chahisArray);
          //String_qr = { "$match":{ "type_of_chassis": { "$in":newArr } } }
        }
        if(fuel && fuel != "")
        {
          String_qr["$match"]['fuel'] = fuel;
        }
        if(vehicle_condition && vehicle_condition != "")
        {
          String_qr["$match"]['vehicle_condition'] = vehicle_condition;
        }
        if(type_of_gearbox && type_of_gearbox != "")
        {
          String_qr["$match"]['type_of_gearbox'] = type_of_gearbox;
        }
        if(warranty && warranty != "")
        {
          String_qr["$match"]['warranty'] = warranty;
        }
        if(maintenance_booklet && maintenance_booklet != "")
        {
          String_qr["$match"]['maintenance_booklet'] = maintenance_booklet;
        }
        if(maintenance_invoice && maintenance_invoice != "")
        {
          String_qr["$match"]['maintenance_invoice'] = maintenance_invoice;
        }
        if(accidented && accidented != "")
        {
          String_qr["$match"]['accidented'] = accidented;
        }
        if(original_paint && original_paint != "")
        {
          String_qr["$match"]['original_paint'] = original_paint;
        }
        if(matching_number && matching_number != "")
        {
          String_qr["$match"]['matching_number'] = matching_number;
        }
        if(matching_color_paint && matching_color_paint != "")
        {
          String_qr["$match"]['matching_color_paint'] = matching_color_paint;
        }
        if(matching_color_interior && matching_color_interior != "")
        {
          String_qr["$match"]['matching_color_interior'] = matching_color_interior;
        }
        if(deductible_VAT && deductible_VAT != "")
        {
          String_qr["$match"]['deductible_VAT'] = deductible_VAT;
        }
        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }
        if(minKilometer && minKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $gte:minKilometer } 
        }
        if(maxKilometer && maxKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $lte:maxKilometer } 
        }
        if(minKilometer && minKilometer != "" && maxKilometer && maxKilometer != "")
        {
          //String_qr = { $gte:minKilometer , $lte:maxKilometer };
          String_qr["$match"]['mileage_kilometer'] = { $gte:minKilometer , $lte:maxKilometer };
        }
        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
        

        if(pors_warranty && pors_warranty != "")
        {
          String_qr["$match"]['pors_warranty'] = pors_warranty;
        }

        if(report_piwi && report_piwi != "")
        {
          String_qr["$match"]['report_piwi'] = report_piwi;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(carVersion && carVersion != "")
        {
          String_qr["$match"]['model_variant'] = carVersion;
        }
        if(onlineSince)
        {
          onlineSince = parseFloat(onlineSince);
          let today_date = new Date();
          let previous_date = today_date.setDate(today_date.getDate() - onlineSince);
         
          previous_date = today_date.toISOString();
          //console.log("previous_date ", previous_date); 
          let dsdsd = new Date(previous_date);
           //console.log("dsdsd ", dsdsd);
          // console.log("today_date ", today_date);
          //plan_end_date_time:{ $lte:today_date }
          String_qr["$match"]['created_at'] = { $gte:dsdsd  };
        }
        if(model_variant && model_variant != "")
        {
          String_qr["$match"]['subModelId'] = mongoose.Types.ObjectId(model_variant)

          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          // //console.log(newArr);
          // String_qr["$match"]['modelId'] = {  $in:newArr };


        }
        if(city && city != "")
        {
          String_qr["$match"]['city'] = {'$regex' : '^'+city, '$options' : 'i'}
        }
        if(country && country != "")
        {
          String_qr["$match"]['country'] = {'$regex' : '^'+country, '$options' : 'i'}
        }
        if(zip_code && zip_code != "")
        {
          String_qr["$match"]['zip_code'] = zip_code;
        }

        //vendeurType  , carVersion
        
      }else{
        //console.log("hereeeeee");
        
        if(category && category != "")
        {
          String_qr['$match']['category'] = category;
          //String_qr = {$match:{'category':category}};
        }
        if(accessModel && accessModel != "")
        {
          String_qr['$match']['modelId'] = mongoose.Types.ObjectId(accessModel);
        }
        if(subModel && subModel != "")
        {
          String_qr['$match']['subModelId'] = mongoose.Types.ObjectId(subModel);
        }
        if(type_de_piece && type_de_piece != "")
        {
          String_qr['$match']['type_de_piece'] = mongoose.Types.ObjectId(type_de_piece);
        }
        if(state && state != "")
        {
          String_qr['$match']['state'] = state;
        }
        if(OEM && OEM != "")
        {
          String_qr['$match']['OEM'] = OEM;
        }
        // if(minAccessPrice && minAccessPrice != "")
        // {
        //   //console.log("minAccessPrice "+minAccessPrice);
        //   String_qr['$match']['price'] = {  $gte:minAccessPrice } 
        // }
        // if(maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice } 
        // }
        // if(minAccessPrice && minAccessPrice != "" && maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice  , $gte:minAccessPrice } 
        // }


        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }


        if(accessRegYear && accessRegYear != "" && accessRegYear_2 && accessRegYear_2 != "")
        {
          String_qr["$match"]['registration_year'] = {  $gte:accessRegYear,$lte:accessRegYear_2 };
        }else if(accessRegYear && accessRegYear != "")
        {
          console.log("iffff 973");
          String_qr["$match"]['registration_year'] = accessRegYear;
        }else if(accessRegYear_2 && accessRegYear_2 != "")
        {
          console.log("iffff 977");
          String_qr["$match"]['registration_year'] = accessRegYear_2;
        }

        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
      }
      if(userAddress != '' && userLatitude != ''  && userLongitude != ''  && userLatitude != 0  && userLongitude != 0 && userAddress != undefined && userLatitude != undefined  && userLongitude != undefined)
      {
         console.log("hereee ifff 1344   ");
        console.log("userAddress "+userAddress);
        console.log("userLatitude "+userLatitude);
        console.log("userLongitude "+userLongitude);
        let newradius=nearByKm*1000;
        dquery ={
          $geoNear:{
            near:{
              type:"Point",coordinates:[ userLongitude,userLatitude ]
            },
            distanceMultiplier: 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
          }
        }
        query_array.push(dquery);
      }
      
      

		  //console.log(req.body);
		  //console.log(String_qr);
      //return false;
      query_array.push(String_qr);
      //console.log(query_array);
      query_array.push(
        {
          $count: "totalRec"
        });
        //console.log(query_array);
      //mongoose.set("debug",true);
      let a_dist = 100*1000;
      var result = await AddAdvertisementModel.aggregate(query_array);
        //console.log(result);
        let count_rec = 0;
        if(result.length == 0)
        {
          count_rec = 0;
        }else{
          count_rec = result[0].totalRec;
        }
        if(count_rec > 0)
        {
          var date_time = new Date();
          if(user_id != "" && user_id != null && user_id != undefined)
          {
            if(ad_name && ad_name != ""  && ad_name != undefined && userAddress != '' && userLatitude != ''  && userLongitude != ''  && userLatitude != 0  && userLongitude != 0)
            {
              var is_exist = await SearchHistoryModel.findOne({user_id:user_id,ad_name:ad_name,category:category,address:userAddress},{ad_name:1});
              if(!is_exist)
              {
                await SearchHistoryModel.create(
                  {
                    user_id:user_id,
                    ad_name:ad_name,
                    category:category,
                    address:userAddress,
                    location: {
                      type: "Point",
                      coordinates: [userLongitude,userLatitude]
                    },
                    searched_time:date_time,
                    item_count:count_rec,
                  });
              }
            }else if(ad_name && ad_name != ""  && ad_name != undefined)
            {
              var is_exist = await SearchHistoryModel.findOne({user_id:user_id,ad_name:ad_name,category:category},{ad_name:1});
              if(!is_exist)
              {
                await SearchHistoryModel.create(
                  {
                    user_id:user_id,
                    ad_name:ad_name,
                    category:category,
                    searched_time:date_time,
                    item_count:count_rec,
                  });
              }
            }else if(userAddress != '' && userLatitude != ''  && userLongitude != ''  && userLatitude != 0  && userLongitude != 0)
            {
              var is_exist = await SearchHistoryModel.findOne({user_id:user_id,category:category,address:userAddress},{ad_name:1});
              if(!is_exist)
              {
                await SearchHistoryModel.create(
                {
                  user_id:user_id,
                  category:category,
                  address:userAddress,
                  location: {
                    type: "Point",
                    coordinates: [userLongitude,userLatitude]
                  },
                  searched_time:date_time,
                  item_count:count_rec,
                });
              }
            }
          }
        }
        

        var return_response = {"error":false,errorMessage:"success","record":count_rec};
        res.status(200).send(return_response);
      
      
    }catch(err){
      console.log(err);
    }
	},
  getWebProductListCount:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    try{
      //console.log("hereee");
      var  user_id= req.body.user_id;
      //console.log(user_id);
      //console.log(req.body);
      //console.log("hereeeeeeee "+JSON.stringify(req.body));
      //return false;
      //console.log(req.query.category);
      let dquery={};
      
      
      
      var { category,carModelArray,fuel,vehicle_condition,type_of_gearbox,colorId,colorInteriorId,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior ,minPrice ,maxPrice,yearArray,yearArray_2,chahisArray,deductible_VAT,minKilometer,maxKilometer,model_variant,colorArray,accessModel,subModel,type_de_piece,state,OEM,minAccessPrice,maxAccessPrice,yearArrayAccess,userAddress,userLatitude,userLongitude,ad_name,pageNo,carVersion,report_piwi,pors_warranty,vendeurType,onlineSince,nearByKm,zip_code,city,country,accessRegYear,accessRegYear_2 } = req.body;
      var limitt = 12;
        
      
      if(yearArray)
      {
        yearArray = parseFloat(yearArray);
      }
      if(yearArray_2)
      {
        yearArray_2 = parseFloat(yearArray_2);
      }
      if(accessRegYear)
      {
        accessRegYear = parseFloat(accessRegYear);
      }
      if(accessRegYear_2)
      {
        accessRegYear_2 = parseFloat(accessRegYear_2);
      }

      // First of month
       
      // top_search_plan_purchase_date_time:{ $lte:today_date },
      // top_search_plan_end_date_time:{ $gte:today_date }


      //return false;
      


      if(!nearByKm)
      {
        nearByKm = 100;
      }
      var String_qr = { "$match":{"show_on_web":1 } };
      //String_qr["$match"]["show_on_web"] = 1;
      var query_array = [];
      //Porsche Voitures
      //String_qr['category'] = "Porsche Voitures";
      if(category && category != "" && category == "Porsche Voitures")
      {
        String_qr["$match"]['category'] = category;

        if(colorId && colorId != "")
        {
          String_qr["$match"]['colorId'] = mongoose.Types.ObjectId(colorId);
        }
        if(colorInteriorId && colorInteriorId != "")
        {
          String_qr["$match"]['colorIdInterior'] = mongoose.Types.ObjectId(colorInteriorId);
        }
        
        if(carModelArray && carModelArray != "")
        {
          
          //mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          //console.log(newArr);
          String_qr["$match"]['modelId'] = mongoose.Types.ObjectId(carModelArray);
          //String_qr = { "$match":{ "modelId": { "$in":newArr } } }
        }
        if(yearArray && yearArray != "" && yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 967");
          String_qr["$match"]['registration_year'] = {  $gte:yearArray,$lte:yearArray_2 };
          //String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
          //String_qr = { "$match":{ "registration_year":{ "$in":yearArray } } }
        }else if(yearArray && yearArray != "")
        {
          //console.log("iffff 973");
          String_qr["$match"]['registration_year'] = yearArray;
        }else if(yearArray_2 && yearArray_2 != "")
        {
          //console.log("iffff 977");
          String_qr["$match"]['registration_year'] = yearArray_2;
        }
        if(chahisArray && chahisArray != "")
        {
          // const newArr = chahisArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // }); chahisArray
          String_qr["$match"]['type_of_chassis'] = mongoose.Types.ObjectId(chahisArray);
          //String_qr = { "$match":{ "type_of_chassis": { "$in":newArr } } }
        }
        if(fuel && fuel != "")
        {
          String_qr["$match"]['fuel'] = fuel;
        }
        if(vehicle_condition && vehicle_condition != "")
        {
          String_qr["$match"]['vehicle_condition'] = vehicle_condition;
        }
        if(type_of_gearbox && type_of_gearbox != "")
        {
          String_qr["$match"]['type_of_gearbox'] = type_of_gearbox;
        }
        if(warranty && warranty != "")
        {
          String_qr["$match"]['warranty'] = warranty;
        }
        if(maintenance_booklet && maintenance_booklet != "")
        {
          String_qr["$match"]['maintenance_booklet'] = maintenance_booklet;
        }
        if(maintenance_invoice && maintenance_invoice != "")
        {
          String_qr["$match"]['maintenance_invoice'] = maintenance_invoice;
        }
        if(accidented && accidented != "")
        {
          String_qr["$match"]['accidented'] = accidented;
        }
        if(original_paint && original_paint != "")
        {
          String_qr["$match"]['original_paint'] = original_paint;
        }
        if(matching_number && matching_number != "")
        {
          String_qr["$match"]['matching_number'] = matching_number;
        }
        if(matching_color_paint && matching_color_paint != "")
        {
          String_qr["$match"]['matching_color_paint'] = matching_color_paint;
        }
        if(matching_color_interior && matching_color_interior != "")
        {
          String_qr["$match"]['matching_color_interior'] = matching_color_interior;
        }
        if(deductible_VAT && deductible_VAT != "")
        {
          String_qr["$match"]['deductible_VAT'] = deductible_VAT;
        }
        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }
        if(minKilometer && minKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $gte:minKilometer } 
        }
        if(maxKilometer && maxKilometer != "")
        {
          String_qr["$match"]['mileage_kilometer'] = {  $lte:maxKilometer } 
        }
        if(minKilometer && minKilometer != "" && maxKilometer && maxKilometer != "")
        {
          //String_qr = { $gte:minKilometer , $lte:maxKilometer };
          String_qr["$match"]['mileage_kilometer'] = { $gte:minKilometer , $lte:maxKilometer };
        }
        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
        

        if(pors_warranty && pors_warranty != "")
        {
          String_qr["$match"]['pors_warranty'] = pors_warranty;
        }

        if(report_piwi && report_piwi != "")
        {
          String_qr["$match"]['report_piwi'] = report_piwi;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(vendeurType && vendeurType != "")
        {
          String_qr["$match"]['user_type'] = vendeurType;
        }
        if(carVersion && carVersion != "")
        {
          String_qr["$match"]['model_variant'] = carVersion;
        }
        if(onlineSince)
        {
          onlineSince = parseFloat(onlineSince);
          let today_date = new Date();
          let previous_date = today_date.setDate(today_date.getDate() - onlineSince);
         
          previous_date = today_date.toISOString();
          //console.log("previous_date ", previous_date); 
          let dsdsd = new Date(previous_date);
           //console.log("dsdsd ", dsdsd);
          // console.log("today_date ", today_date);
          //plan_end_date_time:{ $lte:today_date }
          String_qr["$match"]['created_at'] = { $gte:dsdsd  };
        }
        if(model_variant && model_variant != "")
        {
          String_qr["$match"]['subModelId'] = mongoose.Types.ObjectId(model_variant)

          // const newArr = carModelArray.map((val)=>{
          //   //console.log(val);
          //   return mongoose.Types.ObjectId(val);
          // });
          // //console.log(newArr);
          // String_qr["$match"]['modelId'] = {  $in:newArr };


        }
        if(city && city != "")
        {
          String_qr["$match"]['city'] = {'$regex' : '^'+city, '$options' : 'i'}
        }
        if(country && country != "")
        {
          String_qr["$match"]['country'] = {'$regex' : '^'+country, '$options' : 'i'}
        }
        if(zip_code && zip_code != "")
        {
          String_qr["$match"]['zip_code'] = zip_code;
        }

        //vendeurType  , carVersion
        
      }else{
        //console.log("hereeeeee");
        
        if(category && category != "")
        {
          String_qr['$match']['category'] = category;
          //String_qr = {$match:{'category':category}};
        }
        if(accessModel && accessModel != "")
        {
          String_qr['$match']['modelId'] = mongoose.Types.ObjectId(accessModel);
        }
        if(subModel && subModel != "")
        {
          String_qr['$match']['subModelId'] = mongoose.Types.ObjectId(subModel);
        }
        if(type_de_piece && type_de_piece != "")
        {
          String_qr['$match']['type_de_piece'] = mongoose.Types.ObjectId(type_de_piece);
        }
        if(state && state != "")
        {
          String_qr['$match']['state'] = state;
        }
        if(OEM && OEM != "")
        {
          String_qr['$match']['OEM'] = OEM;
        }
        // if(minAccessPrice && minAccessPrice != "")
        // {
        //   //console.log("minAccessPrice "+minAccessPrice);
        //   String_qr['$match']['price'] = {  $gte:minAccessPrice } 
        // }
        // if(maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice } 
        // }
        // if(minAccessPrice && minAccessPrice != "" && maxAccessPrice  && maxAccessPrice != "" )
        // {
        //   String_qr['$match']['price'] = {  $lte: maxAccessPrice  , $gte:minAccessPrice } 
        // }


        if(minPrice && minPrice != "")
        {
          String_qr["$match"]['price'] = {  $gte:minPrice } 
        }
        if(maxPrice  && maxPrice != "" )
        {
          String_qr["$match"]['price'] = {  $lte: maxPrice } 
        }
        if(minPrice && minPrice != "" && maxPrice  && maxPrice != "")
        {
          String_qr["$match"]['price'] = { $lte:maxPrice , $gte:minPrice }
        }


        if(accessRegYear && accessRegYear != "" && accessRegYear_2 && accessRegYear_2 != "")
        {
          String_qr["$match"]['registration_year'] = {  $gte:accessRegYear,$lte:accessRegYear_2 };
        }else if(accessRegYear && accessRegYear != "")
        {
          console.log("iffff 973");
          String_qr["$match"]['registration_year'] = accessRegYear;
        }else if(accessRegYear_2 && accessRegYear_2 != "")
        {
          console.log("iffff 977");
          String_qr["$match"]['registration_year'] = accessRegYear_2;
        }

        if(ad_name && ad_name != "")
        {
          String_qr["$match"]['ad_name'] = {'$regex' : '^'+ad_name, '$options' : 'i'}
        }
        String_qr['$match']['show_on_web'] = 1;
        String_qr['$match']['complition_status'] = 1;
        String_qr['$match']["delete_at"] = 0;
        String_qr['$match']["activation_status"] = 1;
      }
      if(userAddress != '' && userLatitude != ''  && userLongitude != ''  && userLatitude != 0  && userLongitude != 0 && userAddress != undefined && userLatitude != undefined  && userLongitude != undefined)
      {
         console.log("hereee ifff 1344   ");
        console.log("userAddress "+userAddress);
        console.log("userLatitude "+userLatitude);
        console.log("userLongitude "+userLongitude);
        let newradius=nearByKm*1000;
        dquery ={
          $geoNear:{
            near:{
              type:"Point",coordinates:[ userLongitude,userLatitude ]
            },
            distanceMultiplier: 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
          }
        }
        query_array.push(dquery);
      }
      
      
      if(userLatitude != ''  && userLongitude != '' && userLatitude != undefined  && userLatitude != 0  && userLongitude != 0 && userLongitude != undefined)
      {
         console.log("hereee ifff");
        // console.log("userLatitude "+userLatitude);
        // console.log("userLongitude "+userLongitude);
        let newradius=100*1000;
        dquery ={
          $geoNear:{
            near:{
              type:"Point",coordinates:[ userLongitude,userLatitude ]
            },
            distanceMultiplier: 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
          }
        }
        query_array.push(dquery);
      }
      
      
		  //console.log(req.body);
		  //console.log("String_qr 763");
      //console.log(String_qr);
      //return false;
      query_array.push(String_qr);
      //console.log(query_array);
      // query_array.push(
      //   {
      //     $count: "totalRec"
      //   });

      
      query_array.push(
       
        {
          "$project":{
            '_id':1,
          }
        }
      );
      // console.log("query_array");
      // console.log(JSON.stringify(query_array));
      //var cc = JSON.stringify(query_array);
      //mongoose.set("debug",true);
      let a_dist = 100*1000;
      await AddAdvertisementModel.aggregate(
          query_array
        ).then((result)=>{
          let total = result.length;
          var totalPageNumber = 1;
          var perPageRecord = 4;
          totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);

          var return_response = {"error":false,errorMessage:"No Record","record":result.length,totalPageNumber:totalPageNumber};
            res.status(200).send(return_response);

        // var return_response = {"error":false,errorMessage:"success","record":result};
        // res.status(200).send(return_response);
      }).catch((error)=>{
        //console.log(error);
        var return_response = {"error":true,errorMessage:error.message};
        res.status(200).send(return_response);
      });
      // AddAdvertisementModel.count( String_qr , (err, result)=>
      // {
      //   if(err)
      //   {
      //     console.log(err);
      //   }else{
      //     /*console.log("result ");
      //     console.log(typeof result);*/
      //     //console.log("result"+result);
      //     var return_response = {"error":false,errorMessage:"success","record":result};
      //     res.status(200).send(return_response);
      //   }
      // });
    }catch(err){
      console.log(err);
    }
	},
	webSingleProduct:async function(req,res,next)
	{
    
		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15000000;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    //,show_on_web:1,complition_status:1,activation_status:1,delete_at:0
  
    try{
      var base_url = customConstant.base_url;
      var  user_id= req.body.user_id;
      var id = req.body.ad_id; 
      
      let rating_rec = await RatingModel.aggregate(
        [
          {
            $match:{
              ad_id:id
            }
          },
          {
            $group:
              {
                "_id": "$id",
                avgRating: { $avg: "$rating" },
                totalRating: { $sum: 1 }
              }
          }
        ]
     );

     var modelRec = [];
     var userIdd = "";
     let add_recc = await AddAdvertisementModel.findOne({"_id":id},{userId:1});
     if(add_recc)
     {
       if(add_recc.userId)
       {
         userIdd = add_recc.userId.toString();
       }
     }
     if(userIdd)
     {
        modelRec = await AddAdvertisementModel.aggregate([
          { 
            $match : 
            { 
              userId: mongoose.Types.ObjectId(userIdd) 
            }
          },
          {
            $match:{show_on_web:1}
          },
          {
            $match:
            {
              delete_at:0
            }
          },
          {
            $match:
            {
              activation_status:1
            }
          },
          {
            $addFields:{
              modelIdd:{$arrayElemAt:["$modelId",0]}
            }
          },
          {
            $lookup:{
              from:"models",
              let:{ "usrObjId":{"$toObjectId":"$modelIdd"} },
              pipeline:[
                {
                  $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
                }
              ],
              as:"modelDetail"
            }
          },
          {
            $group : 
            { 
                _id : "$modelId",
                "model_name":{"$first":"$modelDetail.model_name"}
              } 
          }
        ]);
     }
     //console.log("modelRec ",modelRec);
    //  String_qr['category'] = "Porsche Voitures";
    //  String_qr['show_on_web'] = 1;
    //  String_qr['complition_status'] = 1;
    //  String_qr["delete_at"] = 0;
    //  String_qr["activation_status"] = 1;
      //  console.log("totalRating ", rating_rec);
      //console.log(user_id);
    	const queryJoin = [
					{
		        path:'userId',
		        select:['firstName','lastName','mobileNumber','userImage','companyImage','created_at','enterprise','siret','website_url','email','user_type','userName']
			    },
			    {
			    	path:'modelId',
			        select:['model_name']
			    },
			    {
			    	path:'subModelId',
			        select:['model_name']
			    },
			    {
			    	path:'fuel',
			        select:['attribute_name']
			    },
			    {
			    	path:'vehicle_condition',
			        select:['attribute_name']
			    },
			    {
			    	path:'type_of_gearbox',
			        select:['attribute_name']
			    },
			    {
			    	path:'colorId',
			        select:['color_name']
			    },
          {
            path:'type_de_piece',
              select:['attribute_name']
          },
		    ];
      var String_qr = {};
      //Porsche Voitures
      String_qr['_id'] = id;
		  //console.log(String_qr);
      //,"complition_status":1,"activation_status":1
      AddAdvertisementModel.find( String_qr , {}).populate(queryJoin).sort({ }).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
              var x=0;
              var mm = result.length - 1;
              var obj = [];
              var arr = [];
              result.forEach(objectAd=>{
                //console.log(objectAd);
                //console.log("user_id"+user_id);

                var String_qr = {};
                String_qr["userId"] = objectAd.userId._id;
                String_qr["complition_status"] = 1;
                String_qr["show_on_web"] = 1;
                String_qr["delete_at"] = 0;
                
                AddAdvertisementModel.count(String_qr).exec((errAdCount, resultAdCount)=>
                {
                  
                  if(errAdCount)
                  {
                    console.log(errAdCount);
                  }else{
                    objectAd.total_ad_count = resultAdCount;
                    FollowSellerModel.count({user_id:user_id,seller_id:objectAd.userId._id}).exec((er,rc)=>{
                      if(er)
                      {
                        console.log(errFav);
                      }else{
                        // console.log("rc ",rc);
                        // console.log("allReadyLike ", allReadyLike);
                        objectAd.is_follow = rc;
                        if(objectAd.accessoires_image.length>0)
                        {
                          var accessendLoop = objectAd.accessoires_image.length - 1; 
                          
                          for(var j=0; j<objectAd.accessoires_image.length; j++)
                          {
                            obj[j] = {
                              image: base_url+objectAd.accessoires_image[j].path,
                              thumbImage: base_url+objectAd.accessoires_image[j].path,
                              title: "Cliquez ici et voyez toutes les images"
                            };
                            if(accessendLoop == j)
                            {
                              //arr.push(obj);
                              objectAd.accessoires_image = obj;

                              // String_qr['show_on_web'] = 1;
                              // String_qr['complition_status'] = 1;
                              // String_qr["delete_at"] = 0;
                              // String_qr["activation_status"] = 1;

                              AddAdvertisementModel.find({
                                category:objectAd.category,
                                show_on_web:1,
                                complition_status:1,
                                delete_at:0,
                                activation_status:1
                              }).lean(true).exec((errFav,recordSimilar)=>{
                                if(errFav)
                                {
                                  console.log(errFav);
                                }else{
                                  if(recordSimilar)
                                  {
                                    var p = 0;
                                    var loopEnd = recordSimilar.length - 1;
                                    recordSimilar.forEach(objectFavAd=>{
                                      UserfavadsModel.findOne({user_id:user_id,ad_id:objectFavAd._id}).exec((errFav,resultFav)=>{
                                        if(errFav)
                                        {
                                          console.log(errFav);
                                        }else{
                                          if(resultFav)
                                          {
                                            objectFavAd.isFav = 1;
                                          }else{
                                            objectFavAd.isFav = 0;
                                          }
                                          if(loopEnd == p)
                                          {
                                            //console.log(recordSimilar);
                                            var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                              res.status(200).send(return_response);
                                          }
                                          p++;
                                        }
                                      });
                                      
                                    });
                                    
                                  }else{
                                    var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                          res.status(200).send(return_response);
                                  }
                                }
                              });

                            }
                          }
                          //console.log(obj);
                        }else{
                          obj[0] = {
                            image: base_url+"public/uploads/no-image.png",
                            thumbImage: base_url+"public/uploads/no-image.png",
                            title: "Cliquez ici et voyez toutes les images"
                          };
                          objectAd.accessoires_image = obj;
                          AddAdvertisementModel.find({category:objectAd.category}).lean(true).exec((errFav,recordSimilar)=>{
                            if(errFav)
                            {
                              console.log(errFav);
                            }else{
                              if(recordSimilar)
                              {
                                var p = 0;
                                var loopEnd = recordSimilar.length - 1;
                                recordSimilar.forEach(objectFavAd=>{
                                  UserfavadsModel.findOne({user_id:user_id,ad_id:objectFavAd._id}).exec((errFav,resultFav)=>{
                                    if(errFav)
                                    {
                                      console.log(errFav);
                                    }else{
                                      if(resultFav)
                                      {
                                        objectFavAd.isFav = 1;
                                      }else{
                                        objectFavAd.isFav = 0;
                                      }
                                      if(loopEnd == p)
                                      {
                                        //console.log(recordSimilar);
                                        var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                          res.status(200).send(return_response);
                                      }
                                      p++;
                                    }
                                  });
                                  
                                });
                                
                              }else{
                                var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                      res.status(200).send(return_response);
                              }
                            }
                          });
                        }
                      }

                    });

                  }

                });

                
                

              });
            
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result,"resultForModel":modelRec};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err};
      res.status(200).send(return_response);
    }
  
	},
	webSingleProductCar:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    //companyImage
    var xx = 0;
    var total = 0;
    var perPageRecord = 15000000;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    try{
      var base_url = customConstant.base_url;
      var  user_id= req.body.user_id;
      var id = req.body.ad_id;


      let rating_rec = await RatingModel.aggregate(
        [
          {
            $match:{
              ad_id:id
            }
          },
          {
            $group:
              {
                "_id": "$id",
                avgRating: { $avg: "$rating" },
                totalRating: { $sum: 1 }
              }
          }
        ]
     );
     //console.log("rating_rec ", rating_rec);
     var modelRec = [];
     var userIdd = "";
     let add_recc = await AddAdvertisementModel.findOne({"_id":id},{userId:1});
     if(add_recc)
     {
       if(add_recc.userId)
       {
         userIdd = add_recc.userId.toString();
       }
     }
     if(userIdd)
     {
        modelRec = await AddAdvertisementModel.aggregate([
          { 
            $match : 
            { 
              userId: mongoose.Types.ObjectId(userIdd) 
            }
          },
          {
            $match:{show_on_web:1}
          },
          {
            $match:
            {
              delete_at:0
            }
          },
          {
            $match:
            {
              activation_status:1
            }
          },
          {
            $addFields:{
              modelIdd:{$arrayElemAt:["$modelId",0]}
            }
          },
          {
            $lookup:{
              from:"models",
              let:{ "usrObjId":{"$toObjectId":"$modelIdd"} },
              pipeline:[
                {
                  $match:{ $expr:{ $eq:["$_id","$$usrObjId"] }  }
                }
              ],
              as:"modelDetail"
            }
          },
          {
            $group : 
            { 
                _id : "$modelId",
                "model_name":{"$first":"$modelDetail.model_name"}
              } 
          }
        ]);
     }
      //console.log(user_id);
      //RatingModel
    	const queryJoin = [
					{
		        path:'userId',
		        select:['firstName','lastName','mobileNumber','userImage','companyImage','created_at','enterprise','siret','website_url','email','user_type','userName']
			    },
			    {
			    	path:'modelId',
			        select:['model_name']
			    },
			    {
			    	path:'subModelId',
			        select:['model_name']
			    },
			    {
			    	path:'fuel',
			        select:['attribute_name']
			    },
			    {
			    	path:'vehicle_condition',
			        select:['attribute_name']
			    },
			    {
			    	path:'type_of_gearbox',
			        select:['attribute_name']
			    },
			    {
			    	path:'colorId',
			        select:['color_name']
			    },
          {
			    	path:'colorIdInterior',
			        select:['color_name']
			    },
          {
            path:'type_de_piece',
              select:['attribute_name']
          },
          {
            path:'type_of_chassis',
              select:['attribute_name']
          }
		    ];
      var String_qr = {};
      //Porsche Voitures
      String_qr['_id'] = id;
		  //console.log(String_qr);
      //,"complition_status":1,"activation_status":1
      AddAdvertisementModel.find( String_qr , {}).populate(queryJoin).sort({ }).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
              var x=0;
              var mm = result.length - 1;
              var obj = [];
              var arr = [];
              result.forEach(objectAd=>{
                //console.log("objectAd.userId ",objectAd.userId._id);
               // mongoose.set("debug",true);

               var String_qr = {};
                String_qr["userId"] = objectAd.userId._id;
                String_qr["complition_status"] = 1;
                String_qr["show_on_web"] = 1;
                String_qr["delete_at"] = 0;
                
                AddAdvertisementModel.count(String_qr).exec((errAdCount, resultAdCount)=>
                {
                  
                  if(errAdCount)
                  {
                    console.log(errAdCount);
                  }else{
                    objectAd.total_ad_count = resultAdCount;
                    FollowSellerModel.count({user_id:user_id,seller_id:objectAd.userId._id}).exec((er,rc)=>{
                      if(er)
                      {
                        console.log(errFav);
                      }else{
                        // console.log("rc ",rc);
                        // console.log("allReadyLike ", allReadyLike);
                        objectAd.is_follow = rc;
                        //console.log(objectAd);
                        //console.log("user_id"+user_id);
                        if(objectAd.exterior_image.length>0)
                        {
                          var accessendLoop = objectAd.exterior_image.length - 1; 
                          
                          for(var j=0; j<objectAd.exterior_image.length; j++)
                          {
                            obj[j] = {
                              image: base_url+objectAd.exterior_image[j].path,
                              thumbImage: base_url+objectAd.exterior_image[j].path,
                              title: "Cliquez ici et voyez toutes les images"
                            };
                            if(accessendLoop == j)
                            {
                              //arr.push(obj);
                              objectAd.exterior_image = obj;
    
                              AddAdvertisementModel.find({
                                category:objectAd.category,
                                show_on_web:1,
                                complition_status:1,
                                delete_at:0,
                                activation_status:1}).sort({top_search:-1}).lean(true).exec((errFav,recordSimilar)=>{
                                if(errFav)
                                {
                                  console.log(errFav);
                                }else{
                                  if(recordSimilar)
                                  {
                                    var p = 0;
                                    var loopEnd = recordSimilar.length - 1;
                                    recordSimilar.forEach(objectFavAd=>{
                                      UserfavadsModel.findOne({user_id:user_id,ad_id:objectFavAd._id}).exec((errFav,resultFav)=>{
                                        if(errFav)
                                        {
                                          console.log(errFav);
                                        }else{
                                          if(resultFav)
                                          {
                                            objectFavAd.isFav = 1;
                                          }else{
                                            objectFavAd.isFav = 0;
                                          }
                                          if(loopEnd == p)
                                          {
                                            console.log("hereeeeeeeeeeeeeeeeeeeeee 2111");
                                            //console.log(recordSimilar);
                                            var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                              res.status(200).send(return_response);
                                          }
                                          p++;
                                        }
                                      });
                                      
                                    });
                                    
                                  }else{
                                    var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                          res.status(200).send(return_response);
                                  }
                                }
                              });
    
                            }
                          }
                          //console.log(obj);
                        }else{
                          obj[0] = {
                            image: base_url+"public/uploads/no-image.png",
                            thumbImage: base_url+"public/uploads/no-image.png",
                            title: "Cliquez ici et voyez toutes les images"
                          };
                          objectAd.exterior_image = obj;
                          AddAdvertisementModel.find({category:objectAd.category}).lean(true).exec((errFav,recordSimilar)=>{
                            if(errFav)
                            {
                              console.log(errFav);
                            }else{
                              if(recordSimilar)
                              {
                                var p = 0;
                                var loopEnd = recordSimilar.length - 1;
                                recordSimilar.forEach(objectFavAd=>{
                                  UserfavadsModel.findOne({user_id:user_id,ad_id:objectFavAd._id}).exec((errFav,resultFav)=>{
                                    if(errFav)
                                    {
                                      console.log(errFav);
                                    }else{
                                      if(resultFav)
                                      {
                                        objectFavAd.isFav = 1;
                                      }else{
                                        objectFavAd.isFav = 0;
                                      }
                                      if(loopEnd == p)
                                      {
                                        //console.log(recordSimilar);
                                        var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                          res.status(200).send(return_response);
                                      }
                                      p++;
                                    }
                                  });
                                  
                                });
                                
                              }else{
                                var return_response = {"error":false,errorMessage:"success","rating_rec":rating_rec,"record":result,"recordSimilar":recordSimilar,"resultForModel":modelRec};
                                      res.status(200).send(return_response);
                              }
                            }
                          });
                        }
                      }
                    });
                  }
                });


              });
            
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result,"resultForModel":modelRec};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err};
      res.status(200).send(return_response);
    }
  
	},
	
  getWebHomeUserFavAd:async function(req,res,next)
  {
    try{
      var user_id= req.body.user_id;
      if(user_id == "" || user_id == null || user_id == undefined)
      {
        var return_response = {"error":true,errorMessage:"Pas d'enregistrement",record:[]};
        return res.status(200).send(return_response);
      }
      await UserfavadsModel.aggregate([
        {
          $match:{
            "user_id": mongoose.Types.ObjectId(user_id)
          }
        },
        {
          $addFields:{
            "addd_id":"$ad_id"
          }
        },
        // {
        //   $addFields:{
            
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $sort:{
            "top_search":-1,
            //'tmpOrder':1
          }
        },
        {
          $lookup:{
            from:"advertisements",
            let:{ "add_id":{"$toObjectId":"$addd_id"} },
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$add_id"]
                  }
                }
              },
              {
                $match:{
                  $and:[
                    {show_on_web:1},
                    {complition_status:1},
                    {activation_status:1},
                    {delete_at:0},
                  ]
                }
              }
            ],
            as:"allAd"
          }
        },
        {
          $match:{
            "allAd":{$ne:[]}
          }
        },
        {
          $project:{
            'created_at':1,
            'allAd.top_urgent':1,
            'allAd.address':1,
            'allAd.top_search':1,
            'allAd.ad_name':1,
            'allAd._id':1,
            'allAd.exterior_image':1,
            'allAd.accessoires_image':1,
            'allAd.price':1,
            'allAd.pro_price':1,
            'allAd.created_at':1,
            "allAd.price_for_pors":1,
            "allAd.category":1,
            //'tmpOrder':1
          }
        }
      ]).then((result)=>{
        var return_response = {"error":false,errorMessage:"Success",record:result};
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,errorMessage:error.message};
        return res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  updateAccessNumberCount:async(req,res)=>{
    try{
      var {user_id,ad_id} = req.body;
      var countRecord = await AddAdvertisementModel.count({_id:ad_id,userId:user_id});
      console.log(countRecord);
      if(countRecord == 0)
      {
        await AddAdvertisementModel.updateOne({_id:ad_id},{ $inc:{"mobile_count":1} }).then((result)=>{
          var return_response = {"error":false,errorMessage:"Succès"};
          return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,errorMessage:error.message};
          return res.status(200).send(return_response);
        })
      }
    }catch(error){
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  updateAdViewCount:async(req,res)=>{
    try{
      var {user_id,ad_id} = req.body;
      var countRecord = await AddAdvertisementModel.count({_id:ad_id,userId:user_id});
      console.log(countRecord);
      if(countRecord == 0)
      {
        await AddAdvertisementModel.updateOne({_id:ad_id},{ $inc:{"view_count":1} }).then((result)=>{
          var return_response = {"error":false,errorMessage:"Succès"};
          return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,errorMessage:error.message};
          return res.status(200).send(return_response);
        })
      }
    }catch(error){
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  




	getWebHomeCarAd:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15000000;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    try{
      let datee = new Date();
      let today_day = datee.getDay();
      console.log("today_day ---->>>>>   ,,  ", today_day);
      today_day = today_day + 1;



      var  user_id= req.body.user_id;
      //console.log(user_id);
      //console.log(req.body);
      //console.log(req.query.category);
      var String_qr = {};
      //Porsche Voitures
      String_qr['category'] = "Porsche Voitures";
      String_qr['show_on_web'] = 1;
      String_qr['complition_status'] = 1;
      String_qr["delete_at"] = 0;
      String_qr["activation_status"] = 1;
      //mongoose.set("debug",true);
      let logged_in_user_id = req.body.user_id;
      if(logged_in_user_id)
      {
        logged_in_user_id = mongoose.Types.ObjectId(logged_in_user_id);
      }
      await AddAdvertisementModel.aggregate([
        {
          $match:String_qr
        },
        {
          $addFields:{
            "ad_id":"$_id"
          }
        },
        {
          $lookup:{
            from:"userfavads",
            let:{"add_id":{"$toObjectId":"$ad_id"} },
            pipeline:[
                {
                  $match:{$expr:{
                    $and:[
                      {$eq:["$user_id", logged_in_user_id ] },
                      {$eq:["$ad_id","$$add_id"]}
                    ]
                    }
                  }
                }
            ],
            "as":"favAd"
          }
        },
        {
          $addFields:{
            "isFav":{$size:"$favAd"}
          }
        },
        {
          $addFields:{
            top_search:{
              $cond:{
                if:{$eq:["$top_search_which_day",today_day]},
                then:1,
                else:{
                  $cond:{
                    if:{$eq:["$top_search",1]},
                    then:1,
                    else:0
                  }
                }
              }
            }
          }
        },
        // {
        //   $addFields:{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $project:{
            'top_urgent':1,
            'ad_name':1,
            '_id':1,
            'exterior_image':1,
            'address':1,
            'price':1,
            'pro_price':1,
            'userId':1,
            'created_at':1,
            'user_id':1,
            'top_search':1,
            'isFav':1,
            //'tmpOrder':1
          }
        },
        {
          $sort:{
            'top_search':-1,
            //'tmpOrder':1
          }
        }
      ]).then((result)=>{
        return res.status(200).send({
          error:false,
          message:"Success",
          record:result
        });
      }).catch((error)=>{
        return res.status(200).send({
          error:true,
          message:error.message
        });
      })
    }catch(err){
      return res.status(200).send({
        error:true,
        message:err.message
      });
    }
  
	},
  getWebHomeAccessAd:async function(req,res,next)
  {
    try{
      const { user_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      //,"complition_status":1,"activation_status":1
      var imagePath = customConstant.base_url;
      
      var String_qr = {};
      String_qr['category'] = "Pièces et accessoires";
      String_qr['show_on_web'] = 1;
      String_qr['complition_status'] = 1;
      String_qr["delete_at"] = 0;
      String_qr["activation_status"] = 1;
      //mongoose.set("debug",true);
      let logged_in_user_id = req.body.user_id;
      if(logged_in_user_id)
      {
        logged_in_user_id = mongoose.Types.ObjectId(logged_in_user_id);
      }
      await AddAdvertisementModel.aggregate([
        {
          $match:String_qr
        },
        {
          $addFields:{
            "ad_id":"$_id"
          }
        },
        {
          $lookup:{
            from:"userfavads",
            let:{"add_id":{"$toObjectId":"$ad_id"} },
            pipeline:[
                {
                  $match:{$expr:{
                    $and:[
                      {$eq:["$user_id", logged_in_user_id ] },
                      {$eq:["$ad_id","$$add_id"]}
                    ]
                    }
                  }
                }
            ],
            "as":"favAd"
          }
        },
        {
          $addFields:{
            "isFav":{$size:"$favAd"}
          }
        },
        // {
        //   $addFields:{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $project:{
            'top_urgent':1,
            'ad_name':1,
            '_id':1,
            'address':1,
            'price':1,
            'pro_price':1,
            'userId':1,
            'created_at':1,
            'user_id':1,
            'top_search':1,
            'isFav':1,
            'accessoires_image':1,
            //'tmpOrder':1
          }
        },
        {
          $sort:{
            'top_search':-1,
            //'tmpOrder':1
          }
        }
      ]).then((result)=>{
        return res.status(200).send({
          error:false,
          message:"Success",
          record:result
        });
      }).catch((error)=>{
        return res.status(200).send({
          error:true,
          message:error.message
        });
      })
    }catch(err){
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: err
      });
    }
  }
};