const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const AdChatModel = require('../../models/AdChatModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
module.exports = {
  webInitiateChatSubmit:async function(req,res,next)
  {
    try{
			var {ad_id,sender_id,message} = req.body;
			if(!sender_id)
			{
				var return_response = {"error":true,success: false,errorMessage:"Sans login, vous ne pouvez pas envoyer de message au vendeur."};
				return res.status(200).send(return_response);
				 
			}
			//var randomNumber = Math.random().toString(36).slice(2); 
			var randomNumber = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
			//console.log("randomNumber "+Math.random().toString(36).slice(4));
			AddAdvertisementModel.findOne({_id:ad_id} , { userId:1 }).lean(true).exec((err,result)=>{
				if(err)
				{
					//console.log("ifff "+err);
          var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200)
							    .send(return_response);
				}else{
					if(sender_id == result.userId)
					{
						//console.log("iffff "+result.userId);
						var return_response = {"error":true,success: false,errorMessage:"C'est votre propre ajout"};
						res.status(200)
						.send(return_response);
					}else{
						/*console.log("else "+result.userId);
						console.log("else "+sender_id);*/
						AdChatModel.count({ad_id,sender_id},(err,resultCount)=>{
							if(err)
							{
								//console.log(err);
                var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200)
							    .send(return_response);
							}else{
								//console.log("resultCount "+resultCount);
								if(resultCount === 0)
								{
									AddAdvertisementModel.updateOne({_id:ad_id},{ $inc:{"chat_count":1} }).then((result)=>{
										let receiver_id = result.userId;
										let message_by = 1;
										//console.log(receiver_id);
										//return false;
										//console.log("iff "+resultCount);
										AdChatModel.create({
											ad_id,
											randomNumber,
											sender_id,
											receiver_id,
											message,
											message_by
										});
										var return_response = {"error":false,success: true,errorMessage:"Message envoyé"};
										res.status(200)
										.send(return_response);
									}).catch((error)=>{
										var return_response = {"error":true,errorMessage:error.message};
										return res.status(200).send(return_response);
									});
								}else{
									var return_response = {"error":true,success: false,errorMessage:"Le chat a commencé pour cette annonce avec vous"};
									res.status(200)
							    .send(return_response);
								}
							}
						});
					}
				}
			});
		}catch(error){
      console.log(error);
    }
  },

	webAllAdChatGroup:async function(req,res,next)
	{
		/*
			Very important line
			$match: { receiver_id : new mongoose.Types.ObjectId(currentUserId)  },
		*/
		try {
			//mongoose.set('debug', true);
		    const currentUserId = req.body.user_id;
		    const stats = await AdChatModel.aggregate([
		      {
		        $match: {
			        $or:[{ sender_id : new mongoose.Types.ObjectId(currentUserId)  },
			             { receiver_id : new mongoose.Types.ObjectId(currentUserId)  }] 
			    },
		      },
		      {
		        "$group": {
		         "_id": "$randomNumber",
						 "created_at":{ "$first": "$created_at" },
						 "message_by": { "$first": "$message_by" },
						 "message": { "$first": "$message" },
		         "chat_id": { "$first": "$_id" },
		         "ad_id": { "$first": "$ad_id" },
		         "sender_id": { "$first": "$sender_id" },
		         "receiver_id": { "$first": "$receiver_id" }
		          }, 
		      },
		      { 
				   $lookup:{ 
				      "from":"users",
				      "localField":"sender_id",
				      "foreignField":"_id",
				      "as":"userDetail"
				   }
				},
		      { 
				   $lookup:{ 
				      "from":"users",
				      "localField":"receiver_id",
				      "foreignField":"_id",
				      "as":"userDetail2"
				   }
				},
				{ 
					$lookup:{ 
						 "from":"advertisements",
						 "localField":"ad_id",
						 "foreignField":"_id",
						 "as":"addDetail"
					}
			 	},
				{ 
				   $unwind:"$userDetail"
				},
				{ 
				   $unwind:"$userDetail2"
				},
				{
					"$project": {
						"created_at":1,
						"ad_id":1,
						"chat_id":1,
						"randomNumber":1,
						"addDetail.ad_name":1,
						"addDetail.category":1,
						"addDetail.exterior_image":1,
						"addDetail.accessoires_image":1,
					}
				},

				//{ "$sort": { "payment_time": -1 } },

		    ]);
		    //console.log(stats);
				if(stats)
				{
					var return_response = {"error":false,errorMessage:"Succès","record":stats};
					res.status(200).send(return_response);
				}else{
					var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":stats};
					res.status(200).send(return_response);
				}
		  } catch (error) {
		  	var return_response = {"error":true,errorMessage:error};
				res.status(200).send(return_response);
		  }
	},
	webGetAllAdSingleChat:async function(req,res,next)
	{
		try{
			/*var ran_number = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
			console.log(ran_number);*/
			var {chat_id} = req.body;
			const queryJoinAd = [
				{
					path:'userId',
					select:['firstName','lastName','mobileNumber','userImage','created_at']
				},
				{
					path:'modelId',
						select:['model_name']
				},
				{
					path:'subModelId',
						select:['model_name']
				},
				{
					path:'fuel',
						select:['attribute_name']
				},
				{
					path:'vehicle_condition',
						select:['attribute_name']
				},
				{
					path:'type_of_gearbox',
						select:['attribute_name']
				},
				{
					path:'colorId',
						select:['color_name']
				},
				{
					path:'type_de_piece',
						select:['attribute_name']
				},
				{
					path:'type_of_chassis',
						select:['attribute_name']
				},
			];

			const queryJoin = [
				{
          path:'receiver_id',
          select:['firstName','lastName']
        },
				{
          path:'sender_id',
          select:['firstName','lastName']
        },
			];
			AdChatModel.find( { randomNumber:chat_id },{ad_id:1,sender_id:1,receiver_id:1,message_by:1,message:1,created_at:1} ).populate(queryJoin).lean(true).exec((err, result)=>
			{
				if(err)
				{
					console.log(err);
				}else{
					/*console.log("result ");
					console.log(typeof result);*/
					//console.log("result"+result);
					if(result.length > 0)
					{
						//console.log("result"+JSON.stringify(result[0]));
						var String_qr = {};
						//Porsche Voitures
						String_qr['_id'] = result[0].ad_id;
						//console.log(String_qr);
						//,"complition_status":1,"activation_status":1
						AddAdvertisementModel.find( String_qr , {}).populate(queryJoinAd).lean(true).exec((errAd, resultAd)=>
						{
							if(errAd)
							{
								console.log(errAd);
								var return_response = {"error":true,errorMessage:errAd};
								res.status(200).send(return_response);
							}else{
								/*console.log("resultAd ");
								console.log(typeof resultAd);*/
								//console.log("resultAd"+resultAd);

								var return_response = {"error":false,errorMessage:"success","record":result,"resultAd":resultAd};
								res.status(200).send(return_response);
							}
						});
						
					}else{
						var return_response = {"error":false,errorMessage:"No Record","record":result};
						res.status(200).send(return_response);
					}

				}
			});
		}catch (error) {
			//console.log(error)
			var return_response = {"error":true,errorMessage:error};
			res.status(200).send(return_response);
		}
	},

	sendChatMessageSubmit:async function(req,res,next)
	{
		try{
			console.log("hereeeeeeeeeeeeee");
			console.log(req.body);
		}catch(error){
			console.log(error);
		}
	}
};