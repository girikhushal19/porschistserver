const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {

  normalUserBasicMainPlan:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          UserPlanModel.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"record",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
  normalUserTopUrgent:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          TopUrgentPlan.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"record",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
  professionalUserBasicMainPlan:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          ProUserPlanModel.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"record",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
};