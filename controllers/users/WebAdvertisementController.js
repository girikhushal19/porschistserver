const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const ProUserPlanTopSearchModel = require("../../models/ProUserPlanTopSearchModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");
const SettingModel = require("../../models/SettingModel");
const UsersModel = require("../../models/UsersModel");
const VersionsModel = require("../../models/VersionsModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
//const customConstant = require('../../helpers/customConstant');

/*/*/
module.exports = {

	allWebSubModel: async function(req,res,next)
  {
    try{
      const { parent_id } = req.body;
			let all_version = [];
			if(parent_id)
			{
				
				let m_name_rec = await ModelsModel.findOne({_id:parent_id},{model_name:1});
				if(m_name_rec)
				{
					all_version = await VersionsModel.find({model_id:m_name_rec.model_name});
				}
				ModelsModel.find({parent_id:parent_id,status: 1}, {model_name:1,parent_id:1}).exec((err, result)=>
				{
					//console.log("err "+err);
					//console.log("result "+result);
					if(err)
					{
						//console.log(err);
						res.status(200)
							.send({
									error: true,
									success: false,
									errorMessage: err,
							});

					}else{
						if(result)
						{
							res.status(200)
							.send({
									error: false,
									success: true,
									errorMessage: "Dossier des modèles",
									m_name_rec:m_name_rec,
									all_version:all_version,
									record:result,
							});
						}else{
							res.status(200)
							.send({
									error: true,
									success: false,
									errorMessage: "Dossier des modèles",
									m_name_rec:m_name_rec,
									all_version:all_version,
									record:[],
							});
						}
						
					}
				});
			}else{
				res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "L'identifiant du parent est requis",
						all_version:all_version,
						m_name_rec:[],
						record:[],
				});
			}
      
    }catch(err){
        console.log(err);
    }
  },
	nameOfModel: async function(req,res,next)
  {
    try{
      const { parent_id } = req.body;
			if(parent_id)
			{
				let m_name_rec = await ModelsModel.findOne({_id:parent_id},{model_name:1});
				
				return res.status(200).send({
						error: false,
						success: true,
						errorMessage: "Success",
						m_name_rec:m_name_rec
				});
			}else{
				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "L'identifiant du parent est requis",
				});
			}
    }catch(err){
        console.log(err);
    }
  },
	allWebSubModelByName: async function(req,res,next)
  {
    try{
      const { parent_id } = req.body;
			if(parent_id)
			{
				//mongoose.set("debug",true);
				let abc = await ModelsModel.findOne({model_name:parent_id,status:1}, {_id:1}).skip(0).limit(1).sort({created_at:-1});
				//console.log("abc ",abc);
				if(!abc)
				{
					return res.status(200)
					.send({
							error: true,
							success: false,
							errorMessage: "Aucune donnée n'a été trouvée",
					});
				}else{
					//mongoose.set("debug",true);
					ModelsModel.find({parent_id:abc._id,status:1}, {model_name:1,parent_id:1}).exec((err, result)=>
					{
						//console.log("err "+err);
						//console.log("result "+result);
						if(err)
						{
							//console.log(err);
							res.status(200)
								.send({
										error: true,
										success: false,
										errorMessage: err,
								});

						}else{
							if(result)
							{
								res.status(200)
								.send({
										error: false,
										success: true,
										errorMessage: "Dossier des modèles",
										record:result,
								});
							}else{
								res.status(200)
								.send({
										error: true,
										success: false,
										errorMessage: "Dossier des modèles",
										record:[],
								});
							}
							
						}
					});
				}
				
			}else{
				res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "L'identifiant du parent est requis",
						record:[],
				});
			}
      
    }catch(err){
        console.log(err);
    }
  },
	allWebVersion:async(req,res)=>{
		try{
			console.log(req.body);
			let {parent_id,generation} = req.body;
			let model_id_rec = await ModelsModel.findOne({_id:parent_id},{model_name:1});
			let version_rec = await ModelsModel.findOne({_id:generation},{model_name:1});
			if(!model_id_rec || !version_rec)
			{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: "Aucune donnée n'a été trouvée",
						record:[],
				});
			}else{
				// console.log("model_id_rec ", model_id_rec);
				// console.log("version_rec ", version_rec);
				let all_version = await VersionsModel.find({model_id:model_id_rec.model_name, version:version_rec.model_name});
				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "Succès",
						all_version:all_version,
				});
			}
		}catch(e)
		{
			return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: e.message,
						record:[],
				});
		}
	},
	webStep1CarAdName:async function(req,res,next)
	{
		//console.log("here");
		try{

			var updated_at = new Date();
			//var { user_id,model_id,maintenance_booklet,maintenance_invoice,accidented  } = req.body;
			var ad_id = req.body.ad_id;
				//console.log(req.body);return false;
				//console.log("all data  "+JSON.stringify(req.body));return false;
	      //var fileImage = req.file.modelFile;
	      
	     	if(req.body.user_type == "normal_user")
	     	{
	     		var show_on_web = 1;
	     	}else{
	     		var show_on_web = 0;
	     	}
	     	//return false;
	     	if(ad_id)
	     	{
	     		AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
			      ad_name:req.body.ad_name,
			      updated_at:updated_at
			    },function(err,result){
				    if(err)
				    {
				      var return_response = {"error":true,success: false,errorMessage:err};
				        res.status(200)
				        .send(return_response);
				    }else{
				      //console.log(result);
				      //console.log(result._id);

				      var return_response = {"error":false,success: true,errorMessage:"Vous pouvez continuer la première étape initiée",lastInsertId:ad_id};
				        res.status(200)
				        .send(return_response);
				    }
				  });
	     	}else{
	     		AddAdvertisementModel.create({
	     		  ad_name:req.body.ad_name,
	     		  category:req.body.category,
            user_type:req.body.user_type,
            userId:req.body.user_id,
            show_on_web:show_on_web,
					},function(err,result){
            if(err)
            {
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
              //console.log(result);
              //console.log(result._id);
              var lastInsertId = result._id;
              var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:lastInsertId};
                res.status(200)
                .send(return_response);
            }
          });
	     	}
	     	
           
		}catch(err){
	  		console.log(err);
	  }
	},
	webStep1CarEditAdName:async function(req,res,next)
	{
		//console.log("here");
		try{

			var updated_at = new Date();
			//var { user_id,model_id,maintenance_booklet,maintenance_invoice,accidented  } = req.body;
			var ad_id = req.body.ad_id;
				//console.log(req.body);return false;
				//console.log("all data  "+JSON.stringify(req.body));return false;
	      //var fileImage = req.file.modelFile;
	      
	     	if(req.body.user_type == "normal_user")
	     	{
	     		var show_on_web = 1;
	     	}else{
	     		var show_on_web = 0;
	     	}
	     	//return false;
	     	if(ad_id)
	     	{
	     		AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
			      ad_name:req.body.ad_name,
			      updated_at:updated_at
			    },function(err,result){
				    if(err)
				    {
				      var return_response = {"error":true,success: false,errorMessage:err};
				        res.status(200)
				        .send(return_response);
				    }else{
				      //console.log(result);
				      //console.log(result._id);
				      var return_response = {"error":false,success: true,errorMessage:"Vous pouvez continuer la première étape initiée",lastInsertId:ad_id};
				        res.status(200)
				        .send(return_response);
				    }
				  });
	     	}else{
					var return_response = {"error":true,success: false,errorMessage:"Détail non valide"};
					res.status(200)
					.send(return_response);
				}
	     	
           
		}catch(err){
	  		console.log(err);
	  }
	},
	webStep2CarAdName:async function(req,res,next)
	{
    // console.log("hereeee");
		 //console.log(req.body);
		// console.log("fileseee");
		// console.log(req.files);
		// return false;
		try{
			var ad_id = req.body.ad_id;
				var updated_at = new Date();
		    var piwi_report_image =  [];
        var piwi_report_date =  [];
        var mileage_maintanance_report_date = [];
        var mileage_maintanance_report_kilometer = [];
		    var mileage_maintanance_report_image = [];
        var piwi_checkbox = [];
        var paint_thickness_parts = [];
        var body_repainted = [];
				var engine_operation_hour_img = [];
				if(req.files.piwi_report_image)
				{
					var p_r_i = req.files.piwi_report_image;
					/*console.log("piwi image");
					console.log(req.files.piwi_report_image);*/
					for(let m=0; m<p_r_i.length; m++)
					{
						/*console.log(req.files.piwi_report_image[0]);
						console.log(req.files.piwi_report_image[1]);
						console.log(req.files.piwi_report_image[m].filename);
						console.log(req.files.piwi_report_image[m].path);*/
						let images_name_obj = { "filename":req.files.piwi_report_image[m].filename,"path":req.files.piwi_report_image[m].path  };
						 piwi_report_image.push(images_name_obj);
						}
						//console.log(piwi_report_image);
				}
        if(req.files.mileage_maintanance_report_image)
        {
          /*console.log("mileage image");
          console.log(req.files.mileage_maintanance_report_image);*/
          for(let m=0; m<req.files.mileage_maintanance_report_image.length; m++)
          {
            /*console.log(req.files.piwi_report_image[0]);
            console.log(req.files.piwi_report_image[1]);
            console.log(req.files.piwi_report_image[m].filename);
            console.log(req.files.piwi_report_image[m].path);*/
            let images_name_obj = { "filename":req.files.mileage_maintanance_report_image[m].filename,"path":req.files.mileage_maintanance_report_image[m].path  };
              mileage_maintanance_report_image.push(images_name_obj);
            }
            //console.log(mileage_maintanance_report_image);
        }
        if(req.files.engine_operation_hour_image)
        {
          /*console.log("mileage image");
          console.log(req.files.engine_operation_hour_image);*/
          for(let m=0; m<req.files.engine_operation_hour_image.length; m++)
          {
            /*console.log(req.files.piwi_report_image[0]);
            console.log(req.files.piwi_report_image[1]);
            console.log(req.files.piwi_report_image[m].filename);
            console.log(req.files.piwi_report_image[m].path);*/
            let images_name_obj = { "filename":req.files.engine_operation_hour_image[m].filename,"path":req.files.engine_operation_hour_image[m].path  };
						engine_operation_hour_img.push(images_name_obj);
            }
            //console.log(engine_operation_hour_image);
        }

				if(req.body.model_variant)
		   	{
		   		var model_variant = req.body.model_variant;
		   	}else{
		   		var model_variant = null;
		   	}
		   	if(req.body.subModelId)
		   	{
		   		var sub_model_id = req.body.subModelId;
		   	}else{
		   		var sub_model_id = null;
		   	}
		   	if(req.body.report_piwi)
		   	{
		   		var report_piwi = req.body.report_piwi;
		   	}else{
		   		var report_piwi = null;
		   	}
		   	//console.log("sub_model_id "+sub_model_id);
		   	//return false;
        
        // piwi_report_date.push(req.body.piwi_report_date1); 
        // piwi_report_date.push(req.body.piwi_report_date2); 
        // piwi_report_date.push(req.body.piwi_report_date3); 
        // piwi_report_date.push(req.body.piwi_report_date4); 
        // piwi_report_date.push(req.body.piwi_report_date5); 
        // piwi_report_date.push(req.body.piwi_report_date6); 

        mileage_maintanance_report_date.push(req.body.service_date1);
        mileage_maintanance_report_date.push(req.body.service_date2); 
        mileage_maintanance_report_date.push(req.body.service_date3); 
        mileage_maintanance_report_date.push(req.body.service_date4); 

        mileage_maintanance_report_date.push(req.body.service_date5); 
        mileage_maintanance_report_date.push(req.body.service_date6); 
        mileage_maintanance_report_date.push(req.body.service_date7); 
        mileage_maintanance_report_date.push(req.body.service_date8); 
        mileage_maintanance_report_date.push(req.body.service_date9); 
        mileage_maintanance_report_date.push(req.body.service_date10); 

        mileage_maintanance_report_kilometer.push(req.body.service_km1);
        mileage_maintanance_report_kilometer.push(req.body.service_km2); 
        mileage_maintanance_report_kilometer.push(req.body.service_km3); 
        mileage_maintanance_report_kilometer.push(req.body.service_km4); 
        mileage_maintanance_report_kilometer.push(req.body.service_km5); 
        mileage_maintanance_report_kilometer.push(req.body.service_km6);  
        mileage_maintanance_report_kilometer.push(req.body.service_km7);  
        mileage_maintanance_report_kilometer.push(req.body.service_km8);  
        mileage_maintanance_report_kilometer.push(req.body.service_km9);  
        mileage_maintanance_report_kilometer.push(req.body.service_km10);  

        piwi_checkbox.push(req.body.report_piwi_checkbox_1);  
        piwi_checkbox.push(req.body.report_piwi_checkbox_2);
        piwi_checkbox.push(req.body.report_piwi_checkbox_3);

				 

        paint_thickness_parts.push(req.body.porte);
        paint_thickness_parts.push(req.body.ailes);
        paint_thickness_parts.push(req.body.porte_ar_gauche);
        paint_thickness_parts.push(req.body.toit);
        paint_thickness_parts.push(req.body.capot);
        paint_thickness_parts.push(req.body.body_porte);
        paint_thickness_parts.push(req.body.body_ailes);
        paint_thickness_parts.push(req.body.body_capot);
        paint_thickness_parts.push(req.body.body_ar_dorite);
        paint_thickness_parts.push(req.body.body_toit);
        paint_thickness_parts.push(req.body.body_hayon);
				
				if(req.body.warranty_month != "" && req.body.warranty_month != null && req.body.warranty_month != undefined && req.body.warranty_month != 'null')
				{
					console.log("iffff ",warranty_month);
					var warranty_month = req.body.warranty_month;
				}else{
					var warranty_month = 0;
				}
				if(req.body.pors_warranty_month != "" && req.body.pors_warranty_month != null && req.body.pors_warranty_month != undefined && req.body.pors_warranty_month != 'null')
				{
					console.log("iffff ",pors_warranty_month);
					var pors_warranty_month = req.body.pors_warranty_month;
				}else{
					var pors_warranty_month = 0;
				}

				// color_interior_name: 'sdf  wqe vxv',
				// color_interior: '6565ce24d733202e8ba192ae',
				// color_exterieur: '62e3d574ec6f493144a253e4',
				// color_exterieur_name: 'vcb',
				let color_interior = req.body.color_interior;
				let color_exterieur = req.body.color_exterieur;
				let color_exterieur_name = req.body.color_exterieur_name;
				let pors_warranty = req.body.pors_warranty;
				let optionsArray = req.body.optionsArray;
				let vehicle_condition = req.body.vehicle_condition;
				if(!req.body.vehicle_condition)
				{
					vehicle_condition = null;
				}
				//console.log("warranty_month ",warranty_month);
				//mongoose.set("debug",true);
				/*
				color_interior_name: 'sdf  wqe vxv',
				color_interior: '6565ce24d733202e8ba192ae',
				color_exterieur: '62e3d574ec6f493144a253e4',
				color_exterieur_name: 'vcb',

				colorId: [{ type: Schema.Types.ObjectId,ref:"colors", default: null  }],
				color_exterieur: { type: String, default: null },
				colorIdInterior: [{ type: Schema.Types.ObjectId,ref:"colorinteriors", default: null  }],
				color_name: { type: String, default: null },
				*/

				
				let registration_year = parseFloat(req.body.registration_year);
		   	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
					pors_warranty:pors_warranty,
					optionsArray:optionsArray,
		      colorId:req.body.color_exterieur,
					colorIdInterior:color_interior,
					color_name:req.body.color_interior_name,
					color_exterieur:req.body.color_exterieur_name,
		      modelId:req.body.model_id,
		      subModelId:sub_model_id,
		      model_variant:model_variant,
		      registration_year:registration_year,
					registration_month:req.body.registration_month,
		      type_of_chassis:req.body.type_of_chassis,
		      fuel:req.body.fuel,
		      type_of_gearbox:req.body.type_of_gearbox,
		      vehicle_condition:vehicle_condition,
		      attribute_air_rim:req.body.attribute_air_rim,
		      warranty:req.body.warranty,
		      warranty_month:warranty_month,
					pors_warranty_month:pors_warranty_month,
		      maintenance_booklet:req.body.maintenance_booklet,
		      maintenance_invoice:req.body.maintenance_invoice,
		      accidented:req.body.accidented,
		      original_paint:req.body.original_paint,
		      matching_number:req.body.matching_number,
		      matching_color_paint:req.body.matching_color_paint,
		      matching_color_interior:req.body.matching_color_interior,
		      //price_for_pors:req.body.price_for_pors,
		      //deductible_VAT:req.body.deductible_VAT,
		      number_of_owners:req.body.number_of_owners,
		      //cylinder_capacity:req.body.cylinder_capacity,
		      mileage_kilometer:req.body.mileage_kilometer,
		      engine_operation_hour:req.body.engine_operation_hour,

		      paint_thickness_parts:paint_thickness_parts,
		      body_repainted:body_repainted,
		      piwi_checkbox:piwi_checkbox,
		      piwi_report_date:piwi_report_date,
		      piwi_report_image:piwi_report_image,
		      mileage_maintanance_report_date:mileage_maintanance_report_date,
		      mileage_maintanance_report_kilometer:mileage_maintanance_report_kilometer,
		      mileage_maintanance_report_image:mileage_maintanance_report_image,
					engine_operation_hour_image:engine_operation_hour_img,
					report_piwi:report_piwi,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      // console.log(result);
			      // console.log(result._id);
						// console.log(req.body);
						 //return false;
			      var return_response = {"error":false,success: true,errorMessage:"Vous pouvez continuer la deuxième étape initiée",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
	  }
		
	},
	webStep3CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
				AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      ad_name:req.body.ad_name,
		      description:req.body.description,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);
			      var return_response = {"error":false,success: true,errorMessage:"Vous pouvez continuer la troisième étape initiée",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
  webStep4CarAdName:async function(req,res,next)
	{
		// console.log(req.body);
    // console.log(req.files);
		var ad_id = req.body.ad_id;
		//console.log(ad_id);
		//return false;
		var updated_at = new Date();
		try{
			var new_images = [];
			var exterior_image = [];
			var interior_image = [];
			var trunk_engine_image = [];
			if(req.files.exterior_image)
			{
				// var p_r_i = req.files.exterior_image;
				// for(let m=0; m<p_r_i.length; m++)
				// {
				// 	let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
				// 		exterior_image.push(images_name_obj);
				// }

				let length_check = 0;
				var p_r_i = req.files.exterior_image;
				if(req.files.exterior_image.length > 3)
				{
					length_check = 3;
				}else{
					length_check = req.files.exterior_image.length;
				}
				//console.log("p_r_i ", p_r_i.length);
				//return false;
				for(let m=0; m<length_check; m++)
				{
					let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
						exterior_image.push(images_name_obj);
				}

				for (let j = 3; j < req.files.exterior_image.length; j++)
				{
					let images_name_obj = { "filename":req.files.exterior_image[j].filename,"path":req.files.exterior_image[j].path  };
					//new_images.push(images_name_obj);
					new_images.push(images_name_obj);
				}



					//console.log(exterior_image);
				/*console.log(exterior_image);
				console.log(interior_image);
				console.log(trunk_engine_image);
				return false;*/
				AddAdvertisementModel.findOne({ _id:req.body.ad_id },{
		      exterior_image:1,
		    },function(errAd,resultAd){
			    if(errAd)
			    {
			      var return_response = {"error":true,success: false,errorMessage:errAd};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(resultAd);
			      //console.log(result._id);
						if(resultAd)
						{
							AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
								exterior_image:exterior_image,
								accessoires_image_new:new_images,
								interior_image:interior_image,
								trunk_engine_image:trunk_engine_image,
								updated_at:updated_at
							},function(err,result){
								if(err)
								{
									var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200)
										.send(return_response);
								}else{
									//console.log(result);
									//console.log(result._id);
			
									var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
										res.status(200)
										.send(return_response);
								}
							});
						}else{
							var return_response = {"error":true,success: false,errorMessage:"ID invalide"};
							res.status(200)
							.send(return_response);
						}
			    }
			  });
			}else{
				var return_response = {"error":false,success: true,errorMessage:"No image",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			}
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStepBefore4CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var price = req.body.price;
		var pro_price = req.body.pro_price;
		//price
		var updated_at = new Date();
		try{
				AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      price:price,
		      pro_price:pro_price,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);
			      var return_response = {"error":false,success: true,errorMessage:"Vous pouvez continuer la troisième étape initiée",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStep5CarAdName:async function(req,res,next)
	{
		// console.log(req.body);
		// return false;
		//mongoose.set('debug', true);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		let {zip_code,city,country} = req.body;
		zip_code = parseFloat(zip_code);
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        address:req.body.address,
        location: {
			   type: "Point",
			   coordinates: [req.body.longitude,req.body.latitude]
			  },
				zip_code:zip_code,
				country:country,
				city:city,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStep6CarAdName:async function(req,res,next)
	{
		try{
			//console.log("hereee");
			//console.log(req.body);
			if(req.body.hideNumber == "" || req.body.hideNumber == null)
			{
				var hideNumber = false;
			}else{
				var hideNumber = req.body.hideNumber;
			}

			var updated_at = new Date();
			var ad_id = req.body.ad_id;
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        contact_number:req.body.mobileNumber,
				contact_show:hideNumber,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Vérifier l'étape suivante et continuer",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });

		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webStep2AccessoriesAdName:async function(req,res,next)
	{
		try{
			var ad_id = req.body.ad_id;
			let registration_year = parseFloat(req.body.registration_year);
			var updated_at = new Date();
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        modelId:req.body.model_id,
				subModelId:req.body.subModelId,
        registration_year:registration_year,
				registration_month:req.body.registration_month,
        type_de_piece:req.body.type_de_piece,
        state:req.body.state,
        OEM:req.body.OEM,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(error)
		{
			console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webStep3AccessoriesAdName:async function(req,res,next)
	{
		//console.log(req.body);return false;
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				//ad_name:req.body.ad_name,
				description:req.body.description,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					//console.log(result._id);

					var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
						res.status(200)
						.send(return_response);
				}
			});
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webGetAdTitle:async function(req,res,next)
	{
		try{
		AddAdvertisementModel.findOne({
		   		_id:req.body.ad_id
			},{ad_name:1,accessoires_image:1,accessoires_image_new:1}).exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			      if(result)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":true,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  		console.log(err);
	  	}
	},
	webStep4AccessoriesAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var pro_price = req.body.pro_price;
		var updated_at = new Date();
		try{
      /*console.log("record");
      console.log(record);
      console.log(record[0].price_for_pors);*/
    	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
	      price:req.body.price,
				pro_price:req.body.pro_price,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);

		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},


  webStep5AccessoriesAdNameNormal:async function(req,res,next)
	{
		//console.log(req.body);
    //console.log(req.files);
		//return false;
		var ad_id = req.body.ad_id;
		//console.log(ad_id);
		//return false;
		var updated_at = new Date();
		try{
			var exterior_image = [];
			var interior_image = [];
			var new_images = [];
			var trunk_engine_image = [];
			if(req.files.exterior_image)
			{
				let length_check = 0;
				var p_r_i = req.files.exterior_image;
				if(req.files.exterior_image.length > 3)
				{
					length_check = 3;
				}else{
					length_check = req.files.exterior_image.length;
				}
				//console.log("p_r_i ", p_r_i.length);
				//return false;
				for(let m=0; m<length_check; m++)
				{
					let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
						exterior_image.push(images_name_obj);
				}

				for (let j = 3; j < req.files.exterior_image.length; j++)
				{
					let images_name_obj = { "filename":req.files.exterior_image[j].filename,"path":req.files.exterior_image[j].path  };
					//new_images.push(images_name_obj);
					new_images.push(images_name_obj);
				}

				//console.log("new_images ", new_images);
				/*console.log(exterior_image);
				console.log(interior_image);
				console.log(trunk_engine_image);
				return false;*/
				AddAdvertisementModel.findOne({ _id:req.body.ad_id },{accessoires_image:1}).exec((errAd,resultAd)=>{
					if(errAd)
					{
						var return_response = {"error":true,success: false,errorMessage:errAd};
			        res.status(200)
			        .send(return_response);
					}else{
						if(resultAd)
						{
							if(resultAd.accessoires_image.length > 0)
							{
								AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
									accessoires_image_new:new_images,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(result);
										//console.log(result._id);
				
										var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
											res.status(200)
											.send(return_response);
									}
								});
							}else{
								console.log("hereeeeeeeeeeeee 758");
								AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
									accessoires_image:exterior_image,
									accessoires_image_new:new_images,
									interior_image:interior_image,
									trunk_engine_image:trunk_engine_image,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(result);
										//console.log(result._id);
				
										var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
											res.status(200)
											.send(return_response);
									}
								});
							}
						}else{
							var return_response = {"error":true,success: false,errorMessage:"ID non valide"};
							res.status(200)
							.send(return_response);
						}
					}
				});
				
			}else{
				var return_response = {"error":false,success: true,errorMessage:"No image",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			}
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},


  webStep5AccessoriesAdName:async function(req,res,next)
	{
		//console.log(req.body);
    //console.log(req.files);
		var ad_id = req.body.ad_id;
		//console.log(ad_id);
		//return false;
		var updated_at = new Date();
		try{
			var exterior_image = [];
			var interior_image = [];
			var new_images = [];
			var trunk_engine_image = [];
			if(req.files.exterior_image)
			{
				
				// var p_r_i = req.files.exterior_image;
				// for(let m=0; m<p_r_i.length; m++)
				// {
				// 	let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
				// 		exterior_image.push(images_name_obj);
				// }

				// for (let j = 0; j < exterior_image.length; j++)
				// {
				// 	new_images.push(exterior_image[j]);
				// }


				var adResultRecord = await AddAdvertisementModel.findOne({ _id:req.body.ad_id },{accessoires_image:1});
				if(!adResultRecord)
				{
					var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
					return res.status(200).send(return_response);
				}
				if(adResultRecord.accessoires_image.length > 10)
				{
					var return_response = {"error":false,success: true,errorMessage:"Vous ne pouvez pas télécharger plus de 10 photos"};
					return res.status(200).send(return_response);
				}
				var remaining_img_length = 11 - adResultRecord.accessoires_image.length;
				console.log("remaining_img_length"+remaining_img_length);
				var p_r_i = req.files.exterior_image;
				var p_r_i_length = p_r_i.length;
				if(remaining_img_length < p_r_i_length)
				{
					p_r_i_length = remaining_img_length;
				}
				console.log("p_r_i_length"+p_r_i_length);
				for(let m=0; m<p_r_i_length; m++)
				{
					let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
						exterior_image.push(images_name_obj);
				}
				for (let j = 0; j < exterior_image.length; j++)
				{
					new_images.push(exterior_image[j]);
				}


				//console.log(exterior_image);
				/*console.log(exterior_image);
				console.log(interior_image);
				console.log(trunk_engine_image);
				return false;*/
				AddAdvertisementModel.findOne({ _id:req.body.ad_id },{accessoires_image:1}).exec((errAd,resultAd)=>{
					if(errAd)
					{
						console.log(errAd);
						var return_response = {"error":true,success: false,errorMessage:errAd};
			        res.status(200)
			        .send(return_response);
					}else{
						if(resultAd)
						{
							if(resultAd.accessoires_image.length > 0)
							{
								//console.log("hereeeeeeee 724 "+new_images);return false;
								//exterior_image.push(resultAd.accessoires_image);
								var mm = 0;
								var nn = resultAd.accessoires_image.length - 1;
								for (let i = 0; i < resultAd.accessoires_image.length; i++)
                {
                  new_images.push(resultAd.accessoires_image[i]);
									//console.log("hereeeeeeee 732 "+JSON.stringify(new_images));
									//return false;
									if(mm == nn)
									{
										AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
											accessoires_image:new_images,
											interior_image:interior_image,
											trunk_engine_image:trunk_engine_image,
											updated_at:updated_at
										},function(err,result){
											if(err)
											{
												var return_response = {"error":true,success: false,errorMessage:err};
													res.status(200)
													.send(return_response);
											}else{
												//console.log(result);
												//console.log(result._id);
						
												var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
													res.status(200)
													.send(return_response);
											}
										});
									}
									mm++;
                }
							}else{
								console.log("hereeeeeeeeeeeee 758");
								AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
									accessoires_image:exterior_image,
									interior_image:interior_image,
									trunk_engine_image:trunk_engine_image,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(result);
										//console.log(result._id);
				
										var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
											res.status(200)
											.send(return_response);
									}
								});
							}
						}else{
							var return_response = {"error":true,success: false,errorMessage:"ID non valide"};
							res.status(200)
							.send(return_response);
						}
					}
				});
				
			}else{
				var return_response = {"error":false,success: true,errorMessage:"No image",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			}
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStep6AccessoriesAdName:async function(req,res,next)
	{
		//console.log(req.body);
		//mongoose.set('debug', true);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        address:req.body.address,
        location: {
			   type: "Point",
			   coordinates: [req.body.longitude,req.body.latitude]
			  },
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStep6BeforeAccessoriesAdName:async function(req,res,next)
	{
		console.log("req.body hereeeeeeee");
		console.log(req.body);
		
		//mongoose.set('debug', true);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();

		try{
			let shipping_price = [];
			let {shipping_price_1,shipping_price_2,shipping_price_4,shipping_type_1,shipping_type_2,shipping_type_3,shipping_type_4,shipping_type_1_text,shipping_type_2_text,shipping_type_3_text,shipping_type_4_text,shipping_price_5,shipping_type_5,shipping_type_5_text} = req.body;
			if(  shipping_type_1 == "" && shipping_type_2 == "" && shipping_type_3 == "" && shipping_type_4  == ""   && shipping_type_5  == "")
			{
				var return_response = {"error":true,success: false,errorMessage:"Veuillez sélectionner un mode de livraison."};
		    return res.status(200).send(return_response);
			}
			shipping_price_5 = 0;
			if(shipping_price_1 == '')
			{
				shipping_price_1 = 0;
			}
			if(shipping_price_2 == '')
			{
				shipping_price_2 = 0;
			}
			if(shipping_price_4 == '')
			{
				shipping_price_4 = 0;
			}
			if(shipping_type_1 != "")
			{
				if(shipping_price_1 == "")
				{
					var return_response = {"error":true,success: false,errorMessage:"Veuillez choisir un mode et un montant de livraison"};
		    	return res.status(200).send(return_response);
				}
			}
			if(shipping_type_2 != "")
			{
				if(shipping_price_2 == "")
				{
					var return_response = {"error":true,success: false,errorMessage:"Veuillez choisir un mode et un montant de livraison"};
		    	return res.status(200).send(return_response);
				}
			}
			if(shipping_type_4 != "")
			{
				if(shipping_price_4 == "")
				{
					var return_response = {"error":true,success: false,errorMessage:"Veuillez choisir un mode et un montant de livraison"};
		    	return res.status(200).send(return_response);
				}
			}
			let dataa = await AddAdvertisementModel.findOne({ _id:req.body.ad_id });
			if(dataa)
			{
				if(shipping_type_1 == "")
				{
					shipping_type_1 = dataa.shipping_type_1;
				}
				if(shipping_type_2 == "")
				{
					shipping_type_2 = dataa.shipping_type_2;
				}
				if(shipping_type_3 == "")
				{
					shipping_type_3 = dataa.shipping_type_3;
				}
				if(shipping_type_4 == "")
				{
					shipping_type_4 = dataa.shipping_type_4;
				}
				if(shipping_type_5 == "")
				{
					shipping_type_5 = dataa.shipping_type_5;
				}
				
			}
			//console.log("shipping_type_4 ", shipping_type_4);
			//console.log("shipping_type_5 ", shipping_type_5);
			//return false;
			// shipping_price.push(shipping_price_1);
			// shipping_price.push(shipping_price_2);
			// shipping_price.push(shipping_price_4);
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				shipping_price_1:shipping_price_1,
				shipping_price_2:shipping_price_2,
				shipping_price_4:shipping_price_4,
				shipping_price_5:shipping_price_5,
				shipping_type_1:shipping_type_1,
				shipping_type_2:shipping_type_2,
				shipping_type_3:shipping_type_3,
				shipping_type_4:shipping_type_4,
				shipping_type_5:shipping_type_5,
				shipping_type_1_text:shipping_type_1_text,
				shipping_type_2_text:shipping_type_2_text,
				shipping_type_3_text:shipping_type_3_text,
				shipping_type_4_text:shipping_type_4_text,
				shipping_type_5_text:shipping_type_5_text,
				updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      // console.log(result);
					// return false;
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	webStep7AccessoriesAdName:async function(req,res,next)
	{
		try{
			//console.log("hereee");
			//console.log(req.body);
			if(req.body.hideNumber == "" || req.body.hideNumber == null)
			{
				var hideNumber = false;
			}else{
				var hideNumber = req.body.hideNumber;
			}
			var updated_at = new Date();
			var ad_id = req.body.ad_id;
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        contact_number:req.body.mobileNumber,
				contact_show:hideNumber,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });

		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webPaymentStepCarAdName:async function(req,res,next)
	{
		try{
			//console.log(req.body);return false;
			var {normalUserAd,normalUserAdTopUrgent,ad_id,normalUserTopSearch,which_week,user_id,additional_photo_check} = req.body;
			/*
			*/
			var basic_plan_status = 0;
			var show_on_web = 0;
			var complition_status = 0;

			var tax_percent = 0;
			var tax_val = await SettingModel.findOne({ attribute_key:"tax_percent" }, {});
			if(tax_val)
			{
				tax_percent = tax_val.attribute_value
			}
			var final_price = 0;
			var top_search_days = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var top_search_plan_end_date_time = null; 
			var top_search_every_day = 0;

			var top_search_once_week = 0;
			var top_search_which_day = 0;
			let datee = new Date();
			let today_day = datee.getDay();
      console.log("today_day ---->>>>>   ,,  ", today_day);
      today_day = today_day + 1;
			

			var basic_plan_price = 0;
			var showing_number_day = 0;
			var plan_purchase_date_time = null;
			var plan_end_date_time = null;
			
			var top_urgent_day_number = 0; 
			var top_urgent_price = 0;
			var top_urgent_plan_purchase_date_time = null;
			var top_urgent_plan_end_date_time = null;
			var updated_at  = new Date();

			var addResult = await AddAdvertisementModel.findOne({_id:ad_id},{exterior_image:1,accessoires_image_new:1});
			if(!addResult)
			{
				var return_response = {"error":true,success: false,errorMessage:"Identifiant d'annonce invalide"};
				return res.status(200).send(return_response);
			}
			var user_record = await UsersModel.findOne({_id:user_id},{created_at:1});
			//console.log(user_record)
			if(!user_record)
			{
				var return_response = {"error":true,success: false,errorMessage:"ID d'utilisateur non valide"};
				return res.status(200).send(return_response);
			}
			//var user_crea_date = user_record.created_at;

			if(additional_photo_check == false)
				{
					let new_image = addResult.accessoires_image_new.length;
					if(new_image > 0)
					{
						final_price = 9.11;
					}else{
						var upload_iamge_length = addResult.exterior_image.length;
						if(upload_iamge_length > 3)
						{
							var return_response = {"error":true,success: false,errorMessage:"Veuillez choisir un plan de photos"};
							return res.status(200).send(return_response);
						}
					}
				}else{
					final_price = 9.11;
				}


			const date1 = user_record.created_at;
			const date2 = new Date( );
			const diffTime = Math.abs(date2 - date1);
			const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
			

			basic_plan_price = 0;
			final_price = final_price + basic_plan_price;
			showing_number_day = 90;
			plan_purchase_date_time = new Date();
			plan_end_date_time = new Date(); // Now
			plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
			basic_plan_status = 1;
			show_on_web = 1;
			complition_status = 1;
			
			//console.log("access_plan_record");
			//console.log(access_plan_record);

			
			if(normalUserAdTopUrgent)
			{
				var resultTopUrgent = await TopUrgentPlan.findOne({_id:normalUserAdTopUrgent});
				if(resultTopUrgent)
				{
					top_urgent_day_number = resultTopUrgent.day_number;
					top_urgent_price = resultTopUrgent.price;
					top_urgent_plan_purchase_date_time = new Date();
					top_urgent_plan_end_date_time = new Date();
					top_urgent_plan_end_date_time.setDate(top_urgent_plan_end_date_time.getDate() + top_urgent_day_number);
					final_price = final_price +top_urgent_price;
				}
			}
			
			
			if(normalUserTopSearch)
			{
				var access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:normalUserTopSearch});
				if(access_top_search_record)
				{
					console.log("top_search_record ", access_top_search_record);
					
					let plan_day_type = access_top_search_record.plan_day_type;
					console.log("plan_day_type ", plan_day_type);

					top_search_plan_id = access_top_search_record._id;
					top_search_days = access_top_search_record.day_number;
					top_search_price = access_top_search_record.price;
					top_search_plan_purchase_date_time = new Date();
					top_search_plan_end_date_time = new Date(); // Now
					top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
					//console.log(top_search_plan_end_date_time);
					
					final_price = final_price + top_search_price;
					if(plan_day_type == 1)
					{
						top_search_every_day = 1;
					}else{
						//top_search_once_week = 1;
						//top_search_which_day = today_day;
					}
					//return false;
					
				}
			}
			if(final_price != 9.11)
			{
				let taxx = final_price * tax_percent /100;
				final_price = taxx + final_price;
			}
			final_price = final_price.toFixed(2);
			await AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
				complition_status:1,
				show_on_web:show_on_web,
				basic_plan_status:basic_plan_status,
				vat_tax:tax_percent,
				top_search_days:top_search_days,
				top_search_price:top_search_price,
				top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
				top_search_plan_end_date_time:top_search_plan_end_date_time,
				top_search_every_day:top_search_every_day,
				top_search_which_day:top_search_which_day,
				basic_plan_price:basic_plan_price,
				showing_number_day:showing_number_day,
				plan_purchase_date_time:plan_purchase_date_time,
				plan_end_date_time:plan_end_date_time,
				top_urgent_day_number:top_urgent_day_number,
				top_urgent_price:top_urgent_price,
				top_urgent_plan_purchase_date_time:top_urgent_plan_purchase_date_time,
				top_urgent_plan_end_date_time:top_urgent_plan_end_date_time,
				additional_photo_check:additional_photo_check,
				updated_at:updated_at
			}).then((result)=>{
				//console.log("hereeee 1558");
				var return_response = {"error":false,success: true,errorMessage:"Veuillez effectuer votre paiement pour valider votre annonce",lastInsertId:ad_id,price:final_price};
				return res.status(200).send(return_response);
			}).catch((error)=>{
				 var return_response = {"error":true,success: false,errorMessage:error.message};
				 return res.status(200).send(return_response);
			});
		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webPaymentAccessoriesParticular:async function(req,res,next)
	{
		try{
			//console.log(req.body);return false;
			var {normalUserAd,normalUserAdTopUrgent,ad_id,normalUserAdTopSearch,which_week,additional_photo_check} = req.body;
			var updated_at  = new Date();
			var final_price = 0;
			var top_urgent_day_number = 0; var top_urgent_price = 0;
			var top_urgent_plan_purchase_date_time = null;var top_urgent_plan_end_date_time = null;
			var access_top_urgent_record = "";var basic_plan_price = 0;
			var access_top_search_record = "";var accessories_max_photo = 0;
			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var top_search_plan_end_date_time = null; 
			var top_search_every_day = 0;
			var show_on_web = 1;

			var plan_purchase_date_time = null;
			var plan_end_date_time = null;
			var showing_number_day = 90;
			plan_purchase_date_time = new Date();
			plan_end_date_time = new Date(); // Now
			plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date

			/*
				basic_plan_price: { type: Number, default: null },
				showing_number_day: { type: Number, default: 0 },
				plan_purchase_date_time: { type: Date, default: null },
				plan_end_date_time: { type: Date, default: null },

				top_urgent: { type: Number, default: 0 },
				top_urgent_price: { type: Number, default: null },
				top_urgent_payment_status: { type: Number, default: 0 },
				top_urgent_plan_purchase_date_time: { type: Date, default: null },
				top_urgent_plan_end_date_time: { type: Date, default: null },
			*/
			var addResult = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image:1,accessoires_image_new:1});
			//console.log(addResult);
			if(!addResult)
			{
				var return_response = {"error":true,success: false,errorMessage:"Identifiant d'annonce invalide"};
				return res.status(200).send(return_response);
			}
			if(normalUserAd == '' || normalUserAd == null || normalUserAd == undefined)
      {
				if(additional_photo_check == false)
				{
					let new_image = addResult.accessoires_image_new.length;
					if(new_image > 0)
					{
						final_price = 9.11;
					}else{
						var upload_iamge_length = addResult.accessoires_image.length;
						if(upload_iamge_length > 3)
						{
							var return_response = {"error":true,success: false,errorMessage:"Veuillez choisir un plan de photos"};
							return res.status(200).send(return_response);
						}
					}
				}else{
					final_price = 9.11;
				}
			}
			var upload_iamge_length = addResult.accessoires_image.length;
			if(upload_iamge_length > 3)
			{
				show_on_web = 0;
			}

			if(normalUserAd)
			{
				var access_plan_record = await UseraccessoriesplanModel.findOne({_id:normalUserAd});
				if(access_plan_record)
				{
					if(upload_iamge_length > access_plan_record.photo_number)
					{
						let messg = "Vous avez téléchargé "+upload_iamge_length+" images Veuillez choisir le plan en fonction de votre téléchargement.";
						var return_response = {"error":true,success: false,errorMessage:messg};
						return res.status(200).send(return_response);
					}else{
						//console.log("continue");
						basic_plan_price = access_plan_record.price;
						accessories_max_photo = access_plan_record.photo_number;
						final_price = final_price + basic_plan_price;
					}
				}else{
					let messg = "Vous avez téléchargé "+upload_iamge_length+" images Veuillez choisir le plan en fonction de votre téléchargement.";
						var return_response = {"error":true,success: false,errorMessage:messg};
					return res.status(200).send(return_response);
				}
			}
			
			if(normalUserAdTopUrgent)
			{
				access_top_urgent_record = await TopUrgentPlan.findOne({_id:normalUserAdTopUrgent});
				if(access_top_urgent_record)
				{
					top_urgent_day_number = access_top_urgent_record.day_number;
					top_urgent_price = access_top_urgent_record.price;
					top_urgent_plan_purchase_date_time = new Date();
					top_urgent_plan_end_date_time = new Date();
					top_urgent_plan_end_date_time.setDate(top_urgent_plan_end_date_time.getDate() + top_urgent_day_number);
					final_price = final_price +top_urgent_price;
				}
			}

			if(normalUserAdTopSearch)
			{
				var access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:normalUserAdTopSearch});
				if(access_top_search_record)
				{
					console.log("top_search_record ", access_top_search_record);
					
					let plan_day_type = access_top_search_record.plan_day_type;
					console.log("plan_day_type ", plan_day_type);

					top_search_plan_id = access_top_search_record._id;
					top_search_days = access_top_search_record.day_number;
					top_search_price = access_top_search_record.price;
					top_search_plan_purchase_date_time = new Date();
					top_search_plan_end_date_time = new Date(); // Now
					top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
					//console.log(top_search_plan_end_date_time);
					
					final_price = final_price + top_search_price;
					if(plan_day_type == 1)
					{
						top_search_every_day = 1;
					}else{
						//top_search_once_week = 1;
						//top_search_which_day = today_day;
					}
					//return false;
					
				}
			}
			console.log("final_price " , final_price);
			final_price = final_price.toFixed(2);
			console.log("final_price " , final_price);
			// console.log("webPaymentAccessoriesParticular");
			// console.log("top_urgent_plan_purchase_date_time ", top_urgent_plan_purchase_date_time);
			// console.log("top_urgent_plan_end_date_time ", top_urgent_plan_end_date_time);
			 //return false;
			await AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				complition_status:1,
				show_on_web:show_on_web,
				basic_plan_price:basic_plan_price,
				showing_number_day:showing_number_day,
				plan_purchase_date_time:plan_purchase_date_time,
				plan_end_date_time:plan_end_date_time,
				accessories_max_photo:accessories_max_photo,
				accessories_purchase_date_time:updated_at,
				top_urgent_day_number:top_urgent_day_number,
				top_urgent_price:top_urgent_price,
				top_urgent_plan_purchase_date_time:top_urgent_plan_purchase_date_time,
				top_urgent_plan_end_date_time:top_urgent_plan_end_date_time,
				top_search_days:top_search_days,
				top_search_price:top_search_price,
				top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
				top_search_plan_end_date_time:top_search_plan_end_date_time,
				top_search_every_day:top_search_every_day,
				additional_photo_check:additional_photo_check,
				updated_at:updated_at
			}).then((result)=>{
				var return_response = {"error":false,success: true,errorMessage:"Veuillez effectuer votre paiement pour valider votre annonce",lastInsertId:ad_id,price:final_price};
				return res.status(200).send(return_response);
			}).catch((error)=>{
				 var return_response = {"error":true,success: false,errorMessage:error.message};
				 return res.status(200).send(return_response);
			});
		}catch(error){
			//console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	webRemoveCarOldImage:async function(req,res,next)
	{
		try{
			var {imageName,id} = req.body;
			var new_images = [];
			//console.log(imageName);
			//console.log(id);

			AddAdvertisementModel.findOne({_id:id},{exterior_image:1}).exec((addErr,addResult)=>{
				if(addErr)
				{
					var return_response = {"error":true,success: false,errorMessage:addErr};
						res.status(200)
						.send(return_response);
				}else{
					if(addResult)
					{
						//console.log(addResult.exterior_image);
						var uploadDir = './public/uploads/addadvertise/';
						let fileNameWithPath = uploadDir + imageName;
						//console.log(fileNameWithPath);
						if (fs.existsSync(fileNameWithPath))
						{
							fs.unlink(uploadDir + imageName, (err) => 
							{
								console.log("unlink file error "+err);
							});
						}
						var objArr = addResult.exterior_image;
						remainingArr = objArr.filter(data => data.filename != imageName);
						//console.log(remainingArr);
						for (let j = 0; j < remainingArr.length; j++)
						{
							//console.log("hereeeeeeee");
							//console.log(result[0].images[j]);
							new_images.push(remainingArr[j]);
						}
						//console.log(new_images);
						AddAdvertisementModel.updateOne({ "_id":id }, 
						{
							exterior_image:new_images,
						}, function (uerr, docs) {
						if (uerr){
							console.log(uerr);
							res.status(200)
								.send({
										error: true,
										success: false,
										errorMessage: uerr
								});
						}
						else{
							res.status(200)
								.send({
										error: false,
										success: true,
										errorMessage: "Image supprimée"
								});
							}
						});
					}else{
						res.status(200)
						.send({
								error: true,
								success: false,
								errorMessage: "ID invalide"
						});
					}
				}
			});
		}catch(error){
			console.log(error);
		}
	},
	webRemoveAccessOldImage:async function(req,res,next)
	{
		try{
			var {imageName,id} = req.body;
			var new_images = [];
			//console.log(imageName);
			//console.log(id);

			AddAdvertisementModel.findOne({_id:id},{accessoires_image:1}).exec((addErr,addResult)=>{
				if(addErr)
				{
					var return_response = {"error":true,success: false,errorMessage:addErr};
						res.status(200)
						.send(return_response);
				}else{
					if(addResult)
					{
						//console.log(addResult.accessoires_image);
						var uploadDir = './public/uploads/addadvertise/';
						let fileNameWithPath = uploadDir + imageName;
						//console.log(fileNameWithPath);
						if (fs.existsSync(fileNameWithPath))
						{
							fs.unlink(uploadDir + imageName, (err) => 
							{
								console.log("unlink file error "+err);
							});
						}
						var objArr = addResult.accessoires_image;
						remainingArr = objArr.filter(data => data.filename != imageName);
						//console.log(remainingArr);
						for (let j = 0; j < remainingArr.length; j++)
						{
							//console.log("hereeeeeeee");
							//console.log(result[0].images[j]);
							new_images.push(remainingArr[j]);
						}
						//console.log(new_images);
						AddAdvertisementModel.updateOne({ "_id":id }, 
						{
							accessoires_image:new_images,
						}, function (uerr, docs) {
						if (uerr){
							console.log(uerr);
							res.status(200)
								.send({
										error: true,
										success: false,
										errorMessage: uerr
								});
						}
						else{
							res.status(200)
								.send({
										error: false,
										success: true,
										errorMessage: "Image supprimée"
								});
							}
						});
					}else{
						res.status(200)
						.send({
								error: true,
								success: false,
								errorMessage: "ID invalide"
						});
					}
				}
			});
		}catch(error){
			console.log(error);
		}
	},
	getCarAdForImagePrice:async function(req,res,next)
	{
		try{
			console.log(req.body);
		AddAdvertisementModel.find({
		   		_id:req.body.ad_id
			},{ad_name:1,accessoires_image:1,exterior_image:1,price:1,category:1}).exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			      if(result)
	          {
							console.log("here "+result);
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":true,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  		console.log(err);
	  	}
	},


	webPaymentStepCarAdProfessional:async function(req,res,next)
	{
		try{
			var base_url = customConstant.base_url;
			var front_end_base_url = customConstant.front_end_base_url;
			//console.log("base_url"+base_url);
			//console.log("front_end_base_url"+front_end_base_url);
			//console.log(req.body);return false;
			var {proUserAd,proUserAdTopSearch,ad_id,paymentType,user_id} = req.body;
			var is_free = 0;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;


			var basic_main_plan_id = null;
			var showing_number_day = 0;
			var basic_plan_price = 0;
			var maximum_upload = 0;
			var payment_status = 0;
			var plan_purchase_date_time = null;
			var updated_at  = new Date();
			var plan_end_date_time = null; // Now
			

			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var final_price = 0;
			var top_search_plan_end_date_time = null; // Now
			var top_search_every_day = 0;

			var addResult = await AddAdvertisementModel.findOne({_id:ad_id},{userId:1});
			if(!addResult)
			{
				var return_response = {"error":true,success: false,errorMessage:"L'identifiant non valide"};
				return res.status(200).send(return_response);
			}

			var user_id = addResult.userId;
			var user_record = await UsersModel.findOne({_id:addResult.userId},{created_at:1});
			if(!user_record)
			{
				var return_response = {"error":true,success: false,errorMessage:"ID d'utilisateur non valide"};
				return res.status(200).send(return_response);
			}
			//var user_crea_date = user_record.created_at;
			const date1 = user_record.created_at;
			const date2 = new Date( );
			const diffTime = Math.abs(date2 - date1);
			const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
			console.log(diffTime + " milliseconds");
			console.log(diffDays + " days");
			console.log(diffDays );
			if(diffDays <= 90)
			{
				//free
				is_free = 1;
			}
			var p_date  = new Date();
			var resultOldPlan = await	UserPurchasedPlan.findOne({user_id:user_id,ad_type:"car",	basic_plan_id_pro_user: {$ne:null },payment_status:1,pro_user_plan_end_time:{$gt:p_date} } ,{}).limit(1).sort({$natural:-1});
			if(resultOldPlan)
			{
				//old plan
				console.log("old plan 1834");
				//console.log(resultOldPlan);
				var maximum_upload = resultOldPlan.maximum_upload;
				if(maximum_upload > 0)
				{
					var new_maximum_upload = maximum_upload -1;
					show_on_web = 1;
					if(maximum_upload > 0)
					{
						var basic_plan_status = 1;
					}else{
						var basic_plan_status = 0;
					}
					if(resultOldPlan.maximum_upload_top_search > 0)
					{
						var new_maximum_upload_top_search = resultOldPlan.maximum_upload_top_search -1;
						var top_search_payment_status = 1;
					}else{
						var top_search_payment_status = 0;
						if(resultOldPlan.top_search_id_pro_user && resultOldPlan.top_search_id_pro_user != "")
						{
							var new_maximum_upload_top_search = 0;
						}else{
							var new_maximum_upload_top_search = 0;
						}
					}
					var result = await ProUserPlanModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
					if(result)
					{
						console.log("with out top search plan 1852 line no.");
						basic_main_plan_id = resultOldPlan._id;
						maximum_upload = 1;
						showing_number_day = result.day_number;
						basic_plan_price = resultOldPlan.price;
						final_price = basic_plan_price;
						plan_purchase_date_time = new Date(resultOldPlan.updated_at);
						plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
						plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
						//console.log(plan_end_date_time);
					}else{
						var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
						if(resultTopSearch)
						{
							console.log("top search plan");
							top_search_plan_id = resultOldPlan._id;
							top_search_days = resultTopSearch.day_number;
							maximum_upload_top_search = 1;
							top_search_price = resultOldPlan.price;
							top_search_plan_purchase_date_time = new Date(resultOldPlan.updated_at);
							//final_price = top_search_price;
							top_search_plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
							top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
							//console.log(top_search_plan_end_date_time);
							top_search_every_day = 1;
							top_search = 1;

							basic_main_plan_id = resultOldPlan._id;
							maximum_upload = 1;
							showing_number_day = resultTopSearch.day_number;
							basic_plan_price = resultOldPlan.price;
							//final_price = basic_plan_price;
							plan_purchase_date_time = new Date(resultOldPlan.updated_at);
							plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); 

						}
					}
					//return false;
					//console.log("ad_id "+resultOldPlan.ad_id[0]);
					AddAdvertisementModel.updateOne({ _id:ad_id },{
						complition_status:1,
						show_on_web:1,
						basic_plan_price:basic_plan_price,
						showing_number_day:showing_number_day,
						plan_purchase_date_time:plan_purchase_date_time,
						plan_end_date_time:plan_end_date_time,
						top_search_days:top_search_days,
						top_search_price:top_search_price,
						top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
						top_search_plan_end_date_time:top_search_plan_end_date_time,
						top_search_every_day:top_search_every_day,
						updated_at:updated_at,
						basic_plan_status:basic_plan_status,
						complition_status:complition_status,
						top_search:top_search,
						top_search_payment_status:top_search_payment_status,
					},function(errrr,resulttt){
						if(errrr)
						{
							var return_response = {"error":true,success: false,errorMessage:errrr.message};
							return res.status(200).send(return_response);
						}else{
							UserPurchasedPlan.updateOne({ _id:resultOldPlan._id },{
								maximum_upload:new_maximum_upload,
								maximum_upload_top_search:new_maximum_upload_top_search,
								updated_at:updated_at
							},function(err,rr){
								if(err)
								{
									var return_response = {"error":true,success: false,errorMessage:err.message};
									return	res.status(200).send(return_response);
								}else{
									var return_response = {"error":false,success: true,errorMessage:"Votre annonce a été téléchargée avec succès.","lastInsertId":0,"price":0};
									return	res.status(200).send(return_response);
								}
							});
						}
					});
				}else{
					console.log("else no plan line no 1932");
					if(proUserAd == '' || proUserAd == null || proUserAd == undefined)
					{
						var return_response = {"error":true,errorMessage:"Veuillez choisir votre abonnement" };
						return  res.status(200).send(return_response);
						//return 
					}else{
						//now user with new plan 
						console.log("now user with new plan ");
						var otp = Math.floor(10000000 + Math.random() * 90000000);
						var result = await ProUserPlanModel.findOne({_id:proUserAd});
						if(result)
						{
							console.log("with out top search plan");
							basic_main_plan_id = result._id;
							maximum_upload = result.maximum_upload;
							showing_number_day = result.day_number;
							basic_plan_price = result.price;
							final_price = basic_plan_price;
							plan_purchase_date_time = new Date();
							plan_end_date_time = new Date(); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
							//console.log(plan_end_date_time);
							console.log("1960 final_price "+final_price);
						}else{
							var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:proUserAd});
							if(resultTopSearch)
							{
								console.log("top search plan");
								top_search_plan_id = resultTopSearch._id;
								top_search_days = resultTopSearch.day_number;
								maximum_upload_top_search = resultTopSearch.maximum_upload;
								top_search_price = resultTopSearch.top_price;
								top_search_plan_purchase_date_time = new Date();
								final_price = top_search_price;
								top_search_plan_end_date_time = new Date(); // Now
								top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
								//console.log(top_search_plan_end_date_time);
								top_search_every_day = 1;

								basic_main_plan_id = resultTopSearch._id;
								maximum_upload = resultTopSearch.maximum_upload;
								showing_number_day = resultTopSearch.day_number;
								basic_plan_price = resultTopSearch.top_price;
								//final_price = basic_plan_price;
								plan_purchase_date_time = new Date();
								plan_end_date_time = new Date(); // Now
								plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
								//console.log(plan_end_date_time);
								console.log("1986 final_price "+final_price);
							}else{
								var return_response = {"error":true,errorMessage:"ID de plan non valide" };
								return  res.status(200).send(return_response);
							}
						}
						console.log("1992 final_price "+final_price);
						var total_maximum_upload = maximum_upload;
						// if(is_free == 1)
						// {
						// 	payment_status = 1;
						// 	basic_plan_status = 1;
						// 	show_on_web = 1;
						// 	complition_status = 1; 
						// 	if(top_search_plan_id != null)
						// 	{
						// 		top_search = 1;
						// 		top_search_payment_status = 1;
						// 	}
						// 	final_price = 0;
						// }
						console.log("2006 final_price "+final_price);
						AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
							complition_status:1,
							basic_plan_price:basic_plan_price,
							showing_number_day:showing_number_day,
							plan_purchase_date_time:plan_purchase_date_time,
							plan_end_date_time:plan_end_date_time,
							top_search_days:top_search_days,
							top_search_price:top_search_price,
							top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
							top_search_plan_end_date_time:top_search_plan_end_date_time,
							top_search_every_day:top_search_every_day,
							updated_at:updated_at,
							basic_plan_status:basic_plan_status,
							show_on_web:show_on_web,
							complition_status:complition_status,
							top_search:top_search,
							top_search_payment_status:top_search_payment_status,
						},function(err,result){
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200)
									.send(return_response);
							}else{
								//console.log(result);
								//console.log(result._id);
								// var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id,price:final_price};
								// 	res.status(200)
								// 	.send(return_response);
								if(paymentType == "")
								{
									paymentType = "Stripe";
								}
								UserPurchasedPlan.create({
									order_id:otp,
									total_maximum_upload:total_maximum_upload,
									pro_user_plan_end_time:plan_end_date_time,
									ad_type:"car",
									ad_id:ad_id,
									user_id:user_id,
									maximum_upload:maximum_upload,
									maximum_upload_top_search:maximum_upload_top_search,
									basic_plan_id_pro_user:basic_main_plan_id,
									top_search_id_pro_user:top_search_plan_id,
									paymentType:paymentType,
									price:final_price,
									payment_status:payment_status
								},function(err,resultCreate){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(resultCreate);
										///console.log(resultCreate._id);
										console.log("2060 final_price "+final_price);
										var lastInsertId = resultCreate._id;
										if(final_price == 0)
										{
											var success_msg = "Votre annonce a été téléchargée avec succès";
										}else{
											var success_msg = "Il suffit d'effectuer le paiement et l'annonce sera complète.";
										}
											console.log("2068 final_price");
											console.log(final_price);
										// return false;
										var return_response = {"error":false,success: true,errorMessage:success_msg,"lastInsertId":lastInsertId,"price":final_price};
										return	res.status(200).send(return_response);

									}
								});
							}
						});
					}
				}
			}else{
				//new plan
				console.log("else no plan 2076");
				if(proUserAd == '' || proUserAd == null || proUserAd == undefined)
				{
					console.log("2079");
					var return_response = {"error":true,errorMessage:"Veuillez choisir votre abonnement" };
					return  res.status(200).send(return_response);
					//return 
				}else{
					var otp = Math.floor(10000000 + Math.random() * 90000000);
					//now user with new plan 
					console.log("now user with new plan ");
					var result = await ProUserPlanModel.findOne({_id:proUserAd});
					if(result)
					{
						console.log("with out top search plan");
						basic_main_plan_id = result._id;
						maximum_upload = result.maximum_upload;
						showing_number_day = result.day_number;
						basic_plan_price = result.price;
						final_price = basic_plan_price;
						plan_purchase_date_time = new Date();
						plan_end_date_time = new Date(); // Now
						plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
						//console.log(plan_end_date_time);
						console.log("1960 final_price "+final_price);
					}else{
						var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:proUserAd});
						if(resultTopSearch)
						{
							console.log("top search plan");
							top_search_plan_id = resultTopSearch._id;
							top_search_days = resultTopSearch.day_number;
							maximum_upload_top_search = resultTopSearch.maximum_upload;
							top_search_price = resultTopSearch.top_price;
							top_search_plan_purchase_date_time = new Date();
							final_price = top_search_price;
							top_search_plan_end_date_time = new Date(); // Now
							top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
							//console.log(top_search_plan_end_date_time);
							top_search_every_day = 1;

							basic_main_plan_id = resultTopSearch._id;
							maximum_upload = resultTopSearch.maximum_upload;
							showing_number_day = resultTopSearch.day_number;
							basic_plan_price = resultTopSearch.top_price;
							//final_price = basic_plan_price;
							plan_purchase_date_time = new Date();
							plan_end_date_time = new Date(); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
							//console.log(plan_end_date_time);
							console.log("1986 final_price "+final_price);
						}else{
							var return_response = {"error":true,errorMessage:"ID de plan non valide" };
							return  res.status(200).send(return_response);
						}
					}
					console.log("1992 final_price "+final_price);
					var total_maximum_upload = maximum_upload;
					if(is_free == 1)
					{
						payment_status = 1;
						basic_plan_status = 1;
						show_on_web = 1;
						complition_status = 1; 
						maximum_upload = maximum_upload - 1;
						if(top_search_plan_id != null)
						{
							top_search = 1;
							top_search_payment_status = 1;
							maximum_upload_top_search = maximum_upload_top_search - 1;
						}
						final_price = 0;
					}
					console.log("2006 final_price "+final_price);
					AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
						complition_status:1,
						basic_plan_price:basic_plan_price,
						showing_number_day:showing_number_day,
						plan_purchase_date_time:plan_purchase_date_time,
						plan_end_date_time:plan_end_date_time,
						top_search_days:top_search_days,
						top_search_price:top_search_price,
						top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
						top_search_plan_end_date_time:top_search_plan_end_date_time,
						top_search_every_day:top_search_every_day,
						updated_at:updated_at,
						basic_plan_status:basic_plan_status,
						show_on_web:show_on_web,
						complition_status:complition_status,
						top_search:top_search,
						top_search_payment_status:top_search_payment_status,
					},function(err,result){
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
								res.status(200)
								.send(return_response);
						}else{
							//console.log(result);
							//console.log(result._id);
							// var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id,price:final_price};
							// 	res.status(200)
							// 	.send(return_response);
							if(paymentType == "")
							{
								paymentType = "Stripe";
							}
							UserPurchasedPlan.create({
								order_id:otp,
								total_maximum_upload:total_maximum_upload,
								pro_user_plan_end_time:plan_end_date_time,
								ad_type:"car",
								ad_id:ad_id,
								user_id:user_id,
								maximum_upload:maximum_upload,
								maximum_upload_top_search:maximum_upload_top_search,
								basic_plan_id_pro_user:basic_main_plan_id,
								top_search_id_pro_user:top_search_plan_id,
								paymentType:paymentType,
								price:final_price,
								payment_status:payment_status
							},function(err,resultCreate){
								if(err)
								{
									var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200)
										.send(return_response);
								}else{
									//console.log(resultCreate);
									///console.log(resultCreate._id);
									console.log("2060 final_price "+final_price);
									var lastInsertId = resultCreate._id;
									if(final_price == 0)
									{
										var success_msg = "Votre annonce a été téléchargée avec succès";
									}else{
										var success_msg = "Il suffit d'effectuer le paiement et l'annonce sera complète.";
									}
										console.log("2068 final_price");
										console.log(final_price);
									// return false;
									var return_response = {"error":false,success: true,errorMessage:success_msg,"lastInsertId":lastInsertId,"price":final_price};
									return	res.status(200).send(return_response);

								}
							});
						}
					});
				}
			}
		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},


	webPaymentAccessAdProfessional:async function(req,res,next)
	{
		try{
			//console.log(req.body);
			//return false;
			var base_url = customConstant.base_url;
			var front_end_base_url = customConstant.front_end_base_url;
			//console.log("base_url"+base_url);
			//console.log("front_end_base_url"+front_end_base_url);
			//console.log(req.body);return false;
			var {proUserAd,proUserAdTopSearch,ad_id,paymentType,user_id} = req.body;
			var is_free = 0;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;


			var basic_main_plan_id = null;
			var showing_number_day = 0;
			var basic_plan_price = 0;
			var maximum_upload = 0;
			var payment_status = 0;
			var plan_purchase_date_time = null;
			var updated_at  = new Date();
			var plan_end_date_time = null; // Now
			

			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var final_price = 0;
			var top_search_plan_end_date_time = null; // Now
			var top_search_every_day = 0;

			var addResult = await AddAdvertisementModel.findOne({_id:ad_id},{userId:1});
			if(!addResult)
			{
				var return_response = {"error":true,success: false,errorMessage:"L'identifiant non valide"};
				return res.status(200).send(return_response);
			}

			var user_id = addResult.userId;
			var user_record = await UsersModel.findOne({_id:addResult.userId},{created_at:1});
			if(!user_record)
			{
				var return_response = {"error":true,success: false,errorMessage:"ID d'utilisateur non valide"};
				return res.status(200).send(return_response);
			}
			//var user_crea_date = user_record.created_at;
			const date1 = user_record.created_at;
			const date2 = new Date( );
			const diffTime = Math.abs(date2 - date1);
			const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
			// console.log(diffTime + " milliseconds");
			// console.log(diffDays + " days");
			// console.log(diffDays );
			if(diffDays <= 90)
			{
				//free
				is_free = 1;
			}
			var p_date  = new Date();
			var resultOldPlan = await	UserPurchasedPlan.findOne({user_id:user_id,ad_type:"accessories",	basic_plan_id_pro_user: {$ne:null },payment_status:1,pro_user_plan_end_time:{$gt:p_date} } ,{}).limit(1).sort({$natural:-1});
			if(resultOldPlan)
			{
				//old plan
				console.log("old plan 1834");
				//console.log(resultOldPlan);
				var maximum_upload = resultOldPlan.maximum_upload;
				if(maximum_upload > 0)
				{
					console.log("2242 line no. ");
					var new_maximum_upload = maximum_upload -1;
					show_on_web = 1;
					if(maximum_upload > 0)
					{
						var basic_plan_status = 1;
					}else{
						var basic_plan_status = 0;
					}
					if(resultOldPlan.maximum_upload_top_search > 0)
					{
						var new_maximum_upload_top_search = resultOldPlan.maximum_upload_top_search -1;
						var top_search_payment_status = 1;
					}else{
						var top_search_payment_status = 0;
						if(resultOldPlan.top_search_id_pro_user && resultOldPlan.top_search_id_pro_user != "")
						{
							var new_maximum_upload_top_search = 0;
						}else{
							var new_maximum_upload_top_search = 0;
						}
					}
					var result = await ProUserPlanModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
					if(result)
					{
						console.log("with out top search plan 1852 line no.");
						basic_main_plan_id = resultOldPlan._id;
						maximum_upload = 1;
						showing_number_day = result.day_number;
						basic_plan_price = resultOldPlan.price;
						final_price = basic_plan_price;
						plan_purchase_date_time = new Date(resultOldPlan.updated_at);
						plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
						plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
						//console.log(plan_end_date_time);
					}else{
						var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
						if(resultTopSearch)
						{
							console.log("top search plan");
							top_search_plan_id = resultOldPlan._id;
							top_search_days = resultTopSearch.day_number;
							maximum_upload_top_search = 1;
							top_search_price = resultOldPlan.price;
							top_search_plan_purchase_date_time = new Date(resultOldPlan.updated_at);
							//final_price = top_search_price;
							top_search_plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
							top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
							//console.log(top_search_plan_end_date_time);
							top_search_every_day = 1;
							top_search = 1;

							basic_main_plan_id = resultOldPlan._id;
							maximum_upload = 1;
							showing_number_day = resultTopSearch.day_number;
							basic_plan_price = resultOldPlan.price;
							//final_price = basic_plan_price;
							plan_purchase_date_time = new Date(resultOldPlan.updated_at);
							plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); 

						}
					}
					// console.log("2485");
					// return false;
					//console.log("ad_id "+resultOldPlan.ad_id[0]);
					AddAdvertisementModel.updateOne({ _id:ad_id },{
						complition_status:1,
						show_on_web:1,
						activation_status:1,
						basic_plan_price:basic_plan_price,
						showing_number_day:showing_number_day,
						plan_purchase_date_time:plan_purchase_date_time,
						plan_end_date_time:plan_end_date_time,
						top_search_days:top_search_days,
						top_search_price:top_search_price,
						top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
						top_search_plan_end_date_time:top_search_plan_end_date_time,
						top_search_every_day:top_search_every_day,
						updated_at:updated_at,
						basic_plan_status:basic_plan_status,
						complition_status:complition_status,
						top_search:top_search,
						top_search_payment_status:top_search_payment_status,
					},function(errrr,resulttt){
						if(errrr)
						{
							var return_response = {"error":true,success: false,errorMessage:errrr.message};
							return res.status(200).send(return_response);
						}else{
							UserPurchasedPlan.updateOne({ _id:resultOldPlan._id },{
								maximum_upload:new_maximum_upload,
								maximum_upload_top_search:new_maximum_upload_top_search,
								updated_at:updated_at
							},function(err,rr){
								if(err)
								{
									var return_response = {"error":true,success: false,errorMessage:err.message};
									return	res.status(200).send(return_response);
								}else{
									var return_response = {"error":false,success: true,errorMessage:"Votre annonce a été téléchargée avec succès.","lastInsertId":0,"price":0};
									return	res.status(200).send(return_response);
								}
							});
						}
					});
				}else{
					console.log("else no plan line no 1932");
					if(proUserAd == '' || proUserAd == null || proUserAd == undefined)
					{
						var return_response = {"error":true,errorMessage:"Veuillez choisir votre abonnement" };
						return  res.status(200).send(return_response);
						//return 
					}else{
						//now user with new plan 
						console.log("now user with new plan ");
						var otp = Math.floor(10000000 + Math.random() * 90000000);
						var result = await ProUserPlanModel.findOne({_id:proUserAd});
						if(result)
						{
							console.log("with out top search plan");
							basic_main_plan_id = result._id;
							maximum_upload = result.maximum_upload;
							showing_number_day = result.day_number;
							basic_plan_price = result.price;
							final_price = basic_plan_price;
							plan_purchase_date_time = new Date();
							plan_end_date_time = new Date(); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
							//console.log(plan_end_date_time);
							console.log("1960 final_price "+final_price);
						}else{
							var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:proUserAd});
							if(resultTopSearch)
							{
								console.log("top search plan");
								top_search_plan_id = resultTopSearch._id;
								top_search_days = resultTopSearch.day_number;
								maximum_upload_top_search = resultTopSearch.maximum_upload;
								top_search_price = resultTopSearch.top_price;
								top_search_plan_purchase_date_time = new Date();
								final_price = top_search_price;
								top_search_plan_end_date_time = new Date(); // Now
								top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
								//console.log(top_search_plan_end_date_time);
								top_search_every_day = 1;

								basic_main_plan_id = resultTopSearch._id;
								maximum_upload = resultTopSearch.maximum_upload;
								showing_number_day = resultTopSearch.day_number;
								basic_plan_price = resultTopSearch.top_price;
								//final_price = basic_plan_price;
								plan_purchase_date_time = new Date();
								plan_end_date_time = new Date(); // Now
								plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
								//console.log(plan_end_date_time);
								console.log("1986 final_price "+final_price);
							}else{
								var return_response = {"error":true,errorMessage:"ID de plan non valide" };
								return  res.status(200).send(return_response);
							}
						}
						console.log("1992 final_price "+final_price);
						var total_maximum_upload = maximum_upload;
						// if(is_free == 1)
						// {
						// 	payment_status = 1;
						// 	basic_plan_status = 1;
						// 	show_on_web = 1;
						// 	complition_status = 1; 
						// 	if(top_search_plan_id != null)
						// 	{
						// 		top_search = 1;
						// 		top_search_payment_status = 1;
						// 	}
						// 	final_price = 0;
						// }
						console.log("2006 final_price "+final_price);
						AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
							complition_status:1,
							basic_plan_price:basic_plan_price,
							showing_number_day:showing_number_day,
							plan_purchase_date_time:plan_purchase_date_time,
							plan_end_date_time:plan_end_date_time,
							top_search_days:top_search_days,
							top_search_price:top_search_price,
							top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
							top_search_plan_end_date_time:top_search_plan_end_date_time,
							top_search_every_day:top_search_every_day,
							updated_at:updated_at,
							basic_plan_status:basic_plan_status,
							show_on_web:show_on_web,
							complition_status:complition_status,
							top_search:top_search,
							top_search_payment_status:top_search_payment_status,
						},function(err,result){
							if(err)
							{
								var return_response = {"error":true,success: false,errorMessage:err};
									res.status(200)
									.send(return_response);
							}else{
								//console.log(result);
								//console.log(result._id);
								// var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id,price:final_price};
								// 	res.status(200)
								// 	.send(return_response);
								if(paymentType == "")
								{
									paymentType = "Stripe";
								}
								UserPurchasedPlan.create({
									order_id:otp,
									total_maximum_upload:total_maximum_upload,
									pro_user_plan_end_time:plan_end_date_time,
									ad_type:"accessories",
									ad_id:ad_id,
									user_id:user_id,
									maximum_upload:maximum_upload,
									maximum_upload_top_search:maximum_upload_top_search,
									basic_plan_id_pro_user:basic_main_plan_id,
									top_search_id_pro_user:top_search_plan_id,
									paymentType:paymentType,
									price:final_price,
									payment_status:payment_status
								},function(err,resultCreate){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(resultCreate);
										///console.log(resultCreate._id);
										console.log("2060 final_price "+final_price);
										var lastInsertId = resultCreate._id;
										if(final_price == 0)
										{
											var success_msg = "Votre annonce a été téléchargée avec succès";
										}else{
											var success_msg = "Il suffit d'effectuer le paiement et l'annonce sera complète.";
										}
											console.log("2068 final_price");
											console.log(final_price);
										// return false;
										var return_response = {"error":false,success: true,errorMessage:success_msg,"lastInsertId":lastInsertId,"price":final_price};
										return	res.status(200).send(return_response);

									}
								});
							}
						});
					}
				}
			}else{
				//new plan
				console.log("else no plan 2076");
				if(proUserAd == '' || proUserAd == null || proUserAd == undefined)
				{
					console.log("2079");
					var return_response = {"error":true,errorMessage:"Veuillez choisir votre abonnement" };
					return  res.status(200).send(return_response);
					//return 
				}else{
					var otp = Math.floor(10000000 + Math.random() * 90000000);
					//now user with new plan 
					console.log("now user with new plan ");
					var result = await ProUserPlanModel.findOne({_id:proUserAd});
					if(result)
					{
						console.log("with out top search plan");
						basic_main_plan_id = result._id;
						maximum_upload = result.maximum_upload;
						showing_number_day = result.day_number;
						basic_plan_price = result.price;
						final_price = basic_plan_price;
						plan_purchase_date_time = new Date();
						plan_end_date_time = new Date(); // Now
						plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
						//console.log(plan_end_date_time);
						console.log("1960 final_price "+final_price);
					}else{
						var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:proUserAd});
						if(resultTopSearch)
						{
							console.log("top search plan");
							top_search_plan_id = resultTopSearch._id;
							top_search_days = resultTopSearch.day_number;
							maximum_upload_top_search = resultTopSearch.maximum_upload;
							top_search_price = resultTopSearch.top_price;
							top_search_plan_purchase_date_time = new Date();
							final_price = top_search_price;
							top_search_plan_end_date_time = new Date(); // Now
							top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
							//console.log(top_search_plan_end_date_time);
							top_search_every_day = 1;

							basic_main_plan_id = resultTopSearch._id;
							maximum_upload = resultTopSearch.maximum_upload;
							showing_number_day = resultTopSearch.day_number;
							basic_plan_price = resultTopSearch.top_price;
							//final_price = basic_plan_price;
							plan_purchase_date_time = new Date();
							plan_end_date_time = new Date(); // Now
							plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
							//console.log(plan_end_date_time);
							console.log("1986 final_price "+final_price);
						}else{
							var return_response = {"error":true,errorMessage:"ID de plan non valide" };
							return  res.status(200).send(return_response);
						}
					}
					console.log("1992 final_price "+final_price);
					var total_maximum_upload = maximum_upload;
					if(is_free == 1)
					{
						payment_status = 1;
						basic_plan_status = 1;
						show_on_web = 1;
						complition_status = 1; 
						maximum_upload = maximum_upload - 1;
						if(top_search_plan_id != null)
						{
							top_search = 1;
							top_search_payment_status = 1;
							maximum_upload_top_search = maximum_upload_top_search - 1;
						}
						final_price = 0;
					}
					console.log("2006 final_price "+final_price);
					AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
						complition_status:1,
						basic_plan_price:basic_plan_price,
						showing_number_day:showing_number_day,
						plan_purchase_date_time:plan_purchase_date_time,
						plan_end_date_time:plan_end_date_time,
						top_search_days:top_search_days,
						top_search_price:top_search_price,
						top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
						top_search_plan_end_date_time:top_search_plan_end_date_time,
						top_search_every_day:top_search_every_day,
						updated_at:updated_at,
						basic_plan_status:basic_plan_status,
						show_on_web:show_on_web,
						complition_status:complition_status,
						top_search:top_search,
						top_search_payment_status:top_search_payment_status,
					},function(err,result){
						if(err)
						{
							var return_response = {"error":true,success: false,errorMessage:err};
								res.status(200)
								.send(return_response);
						}else{
							//console.log(result);
							//console.log(result._id);
							// var return_response = {"error":false,success: true,errorMessage:"Il suffit d'effectuer le paiement et l'annonce sera complète.",lastInsertId:ad_id,price:final_price};
							// 	res.status(200)
							// 	.send(return_response);
							if(paymentType == "")
							{
								paymentType = "Stripe";
							}
							UserPurchasedPlan.create({
								order_id:otp,
								total_maximum_upload:total_maximum_upload,
								pro_user_plan_end_time:plan_end_date_time,
								ad_type:"accessories",
								ad_id:ad_id,
								user_id:user_id,
								maximum_upload:maximum_upload,
								maximum_upload_top_search:maximum_upload_top_search,
								basic_plan_id_pro_user:basic_main_plan_id,
								top_search_id_pro_user:top_search_plan_id,
								paymentType:paymentType,
								price:final_price,
								payment_status:payment_status
							},function(err,resultCreate){
								if(err)
								{
									var return_response = {"error":true,success: false,errorMessage:err};
										res.status(200)
										.send(return_response);
								}else{
									//console.log(resultCreate);
									///console.log(resultCreate._id);
									console.log("2060 final_price "+final_price);
									var lastInsertId = resultCreate._id;
									if(final_price == 0)
									{
										var success_msg = "Votre annonce a été téléchargée avec succès";
									}else{
										var success_msg = "Il suffit d'effectuer le paiement et l'annonce sera complète.";
									}
										console.log("2068 final_price");
										console.log(final_price);
									// return false;
									var return_response = {"error":false,success: true,errorMessage:success_msg,"lastInsertId":lastInsertId,"price":final_price};
									return	res.status(200).send(return_response);

								}
							});
						}
					});
				}
			}
		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},

};
