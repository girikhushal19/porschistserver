const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const OrderModel = require("../../models/OrderModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

const paypal = require('paypal-rest-sdk');

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AScp8NkpNbol-ejqT1M5BIKu0ZPAEpIemMdLF9w-5vYKSnKb3Fhm8Mqn6zeEWbWB_RfoVC_nQ3mF62rU',
  'client_secret': 'ECDs7eQcVc_i9H8jlCw4xxROF6eY7V7cHVrm2C0W1vgyo60_-zQUFid1IaBRwmZbUjtN-cmGR_Z83t4h'
});

/*/*/
module.exports = {
  accessoiresPurchase:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
    //console.log(req.query);
    var order_id = req.query.paymentId;
    //console.log(order_id);
    OrderModel.findOne({order_id:order_id},{"final_price":1,"ad_name":1,"payment_type":1}).exec((err, result)=>
      {
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200).send(return_response);
        }else{
          /*console.log(result);
          console.log(result.final_price);
          console.log(result.ad_name);
          console.log(result.payment_type);*/
          if(result)
          {
            var price = result.final_price;
            var ad_name = result.ad_name;
            var payment_type = result.payment_type;
            if(payment_type == "Paypal")
            {
              var redirect_url_web = base_url+"api/getPaypalAccessoriesPayment?id="+order_id+"&price="+price+"&title="+ad_name;
            }else{
              var redirect_url_web = base_url+"api/getStripeAccessoriesPayment?id="+order_id+"&price="+price+"&title="+ad_name;
            }
            res.redirect(303, redirect_url_web);
          }
        }
      });
  },

  getPaypalAccessoriesPayment:async function(req,res,next)
  {
    //console.log(req.query);
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    var title = req.query.title;
    var return_url = base_url+"api/getPaypalAccessoriesSuccess";
    var cancel_url = base_url+"api/getPaypalAccessoriesCancel";
    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": return_url,
          "cancel_url": cancel_url
      },
      "transactions": [{
          "item_list": {
              "items": [{
                  "name": title,
                  "sku": lastInsertId,
                  "price": price,
                  "currency": "EUR",
                  "quantity": 1
              }]
          },
          "amount": {
              "currency": "EUR",
              "total": price
          },
          "description": lastInsertId
      }]
    };

    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
          throw error;
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
              res.redirect(payment.links[i].href);
            }
          }
      }
    });
  },

  getStripeAccessoriesPayment:async function(req,res,next)
  {
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    var title = req.query.title;
    /*console.log(lastInsertId);
    console.log(price);
    console.log(title);*/
    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price_data: {
            currency: 'eur',
            product_data: {
              name: title,
            },
            unit_amount: price * 100,
          },
          quantity: 1,
          //description: lastInsertId,
        },
      ],
      mode: 'payment',
      //description: 'One-time setup fee',
      success_url: base_url+'api/getAccessoriesStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: base_url+'api/getAccessoriesStripeCancel',
    });
    //console.log(session);
    //console.log(session.id);
    //console.log(session.payment_intent);
    //res.redirect(303, session.url);
    OrderModel.updateMany({ order_id:lastInsertId },{
      transaction_id:session.id,
      payment_intent:session.payment_intent,
      updated_at:updated_at
    },function(err,result){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        res.redirect(303, session.url);
      }
    });
    
  },
  getPaypalAccessoriesSuccess:async function(req,res,next)
  {
    //console.log("success paypal");
    var updated_at = new Date();
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
    const token = req.query.token;
    //console.log("payerId");
    //console.log(payerId);
    //console.log(token);
    

    const execute_payment_json = {
      "payer_id": payerId
    };

    // Obtains the transaction details from paypal
      paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
          //When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
        if (error)
        {
            console.log(error.response);
            throw error;
        }else{
            //console.log(JSON.stringify(payment));
            //console.log(JSON.stringify(payment.id));
            var payment_table_id = JSON.stringify(payment.transactions[0].description);
            payment_table_id = JSON.parse(payment_table_id);
            //console.log("payment_table_id");
            //console.log(payment_table_id);
            OrderModel.updateMany({ order_id:payment_table_id },{
              payment_status:1,
              transaction_id:payerId,
              payment_intent:token,
              payment_success_date:updated_at,
              updated_at:updated_at
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{

                OrderModel.findOne({order_id:payment_table_id},{"user_id":1}).exec((errUser, resultUser)=>
                {
                  if(err)
                  {
                    var return_response = {"error":true,success: false,errorMessage:errUser};
                        res.status(200).send(return_response);
                  }else{
                    /*console.log(result);
                    console.log(result.final_price);
                    console.log(result.ad_name);
                    console.log(result.payment_type);*/
                    if(resultUser)
                    {
                      var user_id = resultUser.user_id;
                      //console.log(user_id);
                      CartsModel.deleteMany({
                        user_id:user_id
                      },function(err,result){
                        if(err)
                        {
                          var return_response = {"error":true,success: false,errorMessage:err};
                            res.status(200)
                            .send(return_response);
                        }else{
                          var return_response = {"error":false,success: true,errorMessage:"Success"};
                            res.status(200)
                            .send(return_response);
                        }
                      });
                    }
                  }
                });
              }
            });
            //res.send('Success');
        }
    });
  },
  getPaypalAccessoriesCancel:async function(req,res,next)
  {
    //console.log("pay pal cancel");
    var return_response = {"error":false,success: true,errorMessage:"pay pal cancel"};
  },
  getAccessoriesStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
    var updated_at = new Date();
    const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

    const session = await stripe.checkout.sessions.retrieve(
      req.query.session_id
    );
    //console.log(session);
    //console.log(session.payment_status);
    if(session)
    {
      if(session.payment_status == "paid")
      {
        //console.log("paid");
        OrderModel.updateMany({ transaction_id:req.query.session_id },{
          payment_status:1,
          payment_success_date:updated_at,
          updated_at:updated_at
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            OrderModel.findOne({transaction_id:req.query.session_id},{"user_id":1}).exec((errUser, resultUser)=>
            {
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:errUser};
                    res.status(200).send(return_response);
              }else{
                /*console.log(result);
                console.log(result.final_price);
                console.log(result.ad_name);
                console.log(result.payment_type);*/
                if(resultUser)
                {
                  var user_id = resultUser.user_id;
                  //console.log(user_id);
                  CartsModel.deleteMany({
                    user_id:user_id
                  },function(err,result){
                    if(err)
                    {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                    }else{
                      var return_response = {"error":false,success: true,errorMessage:"Success"};
                        res.status(200)
                        .send(return_response);
                    }
                  });
                }
              }
            });
          }
        });
      }else{
        res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Not paid"
                  });
      }
    }else{
      res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Some error"
              });
    }
  },
  getAccessoriesStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":false,success: true,errorMessage:"stripe cancel"};
  },
};