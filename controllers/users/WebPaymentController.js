const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");

const UsersModel = require("../../models/UsersModel");
const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
//const customConstant = require('../../helpers/customConstant');
 
const paypal = require('paypal-rest-sdk');
// const paypal = require('paypal-rest-sdk');

 
/*/*/
module.exports = {
  
	webCarAdPayment:async function(req,res,next)
	{
		try{
			
			var base_url = customConstant.base_url;
			/* http://localhost:3001/api/webCarAdPayment?ad_id=63b6c695352efd92f323a3c8&user_id=638ef941c57d149018506d7f&basic_main_plan_id=62e3dfc8f2656b1c9cbde40f&top_urgent_plan_id=62e3e22fba3fb765e8c6a615&top_search_plan_id=63b969e566ff7085fc953415&paymentType=Stripe&price=199 */
			var {ad_id,user_id,basic_main_plan_id,top_urgent_plan_id,paymentType,price,top_search_plan_id,additional_photo_check} = req.query;
			if(top_urgent_plan_id == "" || top_urgent_plan_id == null || top_urgent_plan_id == undefined)
			{
				top_urgent_plan_id = null;
			}
			if(top_search_plan_id == "" || top_search_plan_id == null || top_search_plan_id == undefined)
			{
				top_search_plan_id = null;
			}
			if(basic_main_plan_id == "" || basic_main_plan_id == null || basic_main_plan_id == undefined)
			{
				basic_main_plan_id = null;
			}
			//console.log(req.query);return false;
			UserPurchasedPlan.create({
				ad_type:"car",
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				basic_plan_id:basic_main_plan_id,
				top_urgent_id:top_urgent_plan_id,
				top_search_plan_id:top_search_plan_id,
				paymentType:paymentType,
				price:price,
				additional_photo_check:additional_photo_check
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					//console.log(result);
					///console.log(result._id);
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getNorUsrCarAdPaymentPaypal?id="+lastInsertId+"&price="+price;
					}else{
						var redirect_url_web = base_url+"api/getNorUsrCarAdPaymentStripe?id="+lastInsertId+"&price="+price;
					}
					//console.log(redirect_url_web);return false;
					res.redirect(303, redirect_url_web);
				}
			});
		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},
	getNorUsrCarAdPaymentPaypal:async function(req,res,next)
  {
    //console.log(req.query);
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    var title = req.query.title;

		var paypal_rec = await PaypalModel.findOne({}, {});
		if(!paypal_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		paypal.configure({
			'mode': paypal_rec.mode, //sandbox or live
			'client_id': paypal_rec.client_id,
			'client_secret': paypal_rec.client_secret
		});

			//console.log(stripe_rec);return false; 


    var return_url = base_url+"api/getPaypalNorUsrCarAdSuccess";
    var cancel_url = base_url+"api/getPaypalNorUsrCarAdCancel";
    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": return_url,
          "cancel_url": cancel_url
      },
      "transactions": [{
          "item_list": {
              "items": [{
                  "name": "Porschists",
                  "sku": lastInsertId,
                  "price": price,
                  "currency": "EUR",
                  "quantity": 1
              }]
          },
          "amount": {
              "currency": "EUR",
              "total": price
          },
          "description": lastInsertId
      }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
          throw error;
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
              res.redirect(payment.links[i].href);
            }
          }
      }
    });
  },
  getPaypalNorUsrCarAdSuccess:async function(req,res,next)
  {
		try{
			let img_arr = [];
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var top_search_payment_status = 0;
			var updated_at = new Date();
			var show_on_web = 0;
			var complition_status = 0;
			var top_search = 0;
			var top_urgent = 0;
			var top_search_which_day = 0;
			var top_search_once_week = 0;
			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, async function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						//throw error;
						var return_response = {"error":true,success: false,errorMessage:error};
						return res.status(200).send(return_response);
				}else{
					//console.log(JSON.stringify(payment));
					//console.log(JSON.stringify(payment.id));
					var payment_table_id = JSON.stringify(payment.transactions[0].description);
					payment_table_id = JSON.parse(payment_table_id);
					console.log("payment_table_id");
					console.log(payment_table_id);
					
					let result = await UserPurchasedPlan.findOne({_id:payment_table_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1});
							/*
							console.log(result.final_price);
							console.log(result.ad_name);
							console.log(result.payment_type);*/
					if(result)
					{
						console.log("result ", result);
						let ad_id = result.ad_id.toString();
					//console.log("ad_id ", ad_id);
					
					let ad_record = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image_new:1,exterior_image:1});
					//console.log("ad_record ", ad_record);
					if(ad_record)
					{
						img_arr = ad_record.exterior_image.concat(ad_record.accessoires_image_new);
					}


						

						let user_idd = result.user_id.toString();
						//console.log("user_idd");
						//console.log(user_idd);
						
						let user_record = await UsersModel.findOne({_id:user_idd},{created_at:1});
						//console.log("user_record ", user_record);
						
						const date1 = user_record.created_at;
						const date2 = new Date( );
						const diffTime = Math.abs(date2 - date1);
						const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
						//console.log(diffTime + " milliseconds");
						//console.log(diffDays + " days");
						//console.log(diffDays );
						// if(diffDays <= 90)
						// {
						// 	basic_plan_status = 1;
						// 	show_on_web = 1;
						// 	complition_status = 1;
						// }else{
						// 	if(result.basic_plan_id)
						// 	{
						// 		basic_plan_status = 1;
						// 		show_on_web = 1;
						// 		complition_status = 1;
						// 	}
						// }

						basic_plan_status = 1;
						show_on_web = 1;
						complition_status = 1;


						if(result.top_urgent_id)
						{
							top_urgent_payment_status = 1;
							top_urgent = 1;
						}
						if(result.top_search_plan_id)
						{
							top_search_payment_status = 1;
							//top_search = 1;
							
							let access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:result.top_search_plan_id});
								if(access_top_search_record)
								{
									if(access_top_search_record.plan_day_type == 1)
									{
										top_search = 1;
									}else{
										top_search_once_week = 1;
										let datee = new Date();
										let today_day = datee.getDay();
										console.log("today_day ---->>>>>   ,,  ", today_day);
										today_day = today_day + 1;
										top_search_which_day = today_day;
									}
								}
								

						}

						UserPurchasedPlan.updateOne({ _id:result._id },{
							payment_status:1,
							updated_at:updated_at
						},function(err2,result2){
							if(err2)
							{
								//console.log("err2"+err2)
								var return_response = { "error":true,success: false,errorMessage:err2};
									res.status(200)
									.send(return_response);
							}else{
								//res.redirect(303, session.url);
								AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
									exterior_image:img_arr,
									accessoires_image_new:[],
									show_on_web:show_on_web,
									complition_status:complition_status,
									top_search:top_search,
									top_urgent:top_urgent,
									basic_plan_status:basic_plan_status,
									top_search_once_week:top_search_once_week,
									top_search_which_day:top_search_which_day,
									top_urgent_payment_status:top_urgent_payment_status,
									top_search_payment_status:top_search_payment_status,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										console.log("err"+err)
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(result);
										//console.log(result._id);
										// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
										// 	res.status(200)
										// 	.send(return_response);
										var redirect_url_web = base_url_web+"adSuccess";
										res.redirect(303, redirect_url_web);
									}
								});
							}
						});
								
							
					}
					//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
	getPaypalNorUsrCarAdCancel:async function(req,res,next)
	{
    //console.log("pay pal cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
	getNorUsrCarAdPaymentStripe:async function(req,res,next)
  {
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
		//console.log(req.query);return false;
    //price = 1;
    //var title = req.query.title;
    /*console.log(lastInsertId);
    console.log(price);
    console.log(title);*/
		
		var stripe_rec = await StripeModel.findOne({}, {});
		if(!stripe_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		//console.log(stripe_rec);return false;
		const stripe = require('stripe')(stripe_rec.stripe_secret);

    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price_data: {
            currency: 'eur',
            product_data: {
              name: "Porschists",
            },
            unit_amount: price * 100,
          },
          quantity: 1,
          //description: lastInsertId,
        },
      ],
      mode: 'payment',
      //description: 'One-time setup fee',
      success_url: base_url+'api/getNorUsrCarAdStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: base_url+'api/getNorUsrCarAdStripeCancel',
    });
    //console.log(session);
    //console.log(session.id);
    //console.log(session.payment_intent);
    //res.redirect(303, session.url);
    UserPurchasedPlan.updateOne({ _id:lastInsertId },{
      transaction_id:session.id,
      payment_intent:session.payment_intent,
      updated_at:updated_at
    },function(err,result){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        res.redirect(303, session.url);
      }
    });
  },
	getNorUsrCarAdStripeSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			let img_arr = [];
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var show_on_web = 0;
			var complition_status = 0;
			var top_search = 0;
			var top_urgent = 0;
			var top_urgent_payment_status = 0;
			var top_search_payment_status = 0;
			var top_search_once_week = 0;
			var top_search_which_day = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			// console.log(session);
			// console.log(session.payment_status);
			// return false;
			if(session)
			{
				if(session.payment_status == "paid")
				{
					var result = await UserPurchasedPlan.findOne({transaction_id:req.query.session_id},{"ad_id":1,"user_id":1,"basic_plan_id":1,'top_urgent_id':1,'top_search_plan_id':1});
					//console.log("result ", result);
					let user_idd = result.user_id.toString();
					let ad_id = result.ad_id.toString();
						//console.log("ad_id ", ad_id);
						
					let ad_record = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image_new:1,exterior_image:1});
					//console.log("ad_record ", ad_record);
					if(ad_record)
					{
						img_arr = ad_record.exterior_image.concat(ad_record.accessoires_image_new);
					}


					//console.log("user_idd");
					//console.log(user_idd);
					//return false;
					var user_record = await UsersModel.findOne({_id:user_idd},{created_at:1});
					//console.log(user_record);
					//return false;
					// if(!user_record)
					// {
					// 	var return_response = {"error":true,success: false,errorMessage:"ID d'utilisateur non valide"};
					// 	return res.status(200).send(return_response);
					// }
					const date1 = user_record.created_at;
					const date2 = new Date( );
					const diffTime = Math.abs(date2 - date1);
					const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
					// console.log(diffTime + " milliseconds");
					// console.log(diffDays + " days");
					// console.log(diffDays );
					// if(diffDays <= 90)
					// {
					// 	basic_plan_status = 1;
					// 	show_on_web = 1;
					// 	complition_status = 1;
					// }else{
					// 	if(result.basic_plan_id)
					// 	{
					// 		basic_plan_status = 1;
					// 		show_on_web = 1;
					// 		complition_status = 1;
					// 	}
					// }

					basic_plan_status = 1;
					show_on_web = 1;
					complition_status = 1;


					if(result.top_urgent_id)
					{
						top_urgent_payment_status = 1;
						top_urgent = 1;
					}
					if(result.top_search_plan_id)
					{
						var access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:result.top_search_plan_id});
						top_search_payment_status = 1;
						if(access_top_search_record)
						{
							if(access_top_search_record.plan_day_type == 1)
							{
								top_search = 1;
							}else{
								top_search_once_week = 1;
								let datee = new Date();
								let today_day = datee.getDay();
								console.log("today_day ---->>>>>   ,,  ", today_day);
								today_day = today_day + 1;
								top_search_which_day = today_day;
							}
						}
						
					}
					UserPurchasedPlan.updateOne({ _id:result._id },{
						payment_status:1,
						updated_at:updated_at
					},function(err2,result2){
						if(err2)
						{
							//console.log("err2"+err2)
							var return_response = { "error":true,success: false,errorMessage:err2};
								res.status(200)
								.send(return_response);
						}else{
							//res.redirect(303, session.url);
							AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{

								exterior_image:img_arr,
								accessoires_image_new:[],
								show_on_web:show_on_web,
								complition_status:complition_status,
								top_search:top_search,
								top_search_once_week:top_search_once_week,
								top_search_which_day:top_search_which_day,
								top_urgent:top_urgent,
								basic_plan_status:basic_plan_status,
								top_urgent_payment_status:top_urgent_payment_status,
								top_search_payment_status:top_search_payment_status,
								updated_at:updated_at
							},function(err,result){
								if(err)
								{
									//console.log("err"+err)
									var return_response = {"error":true,success: false,errorMessage:err.message};
										res.status(200)
										.send(return_response);
								}else{
									//console.log(result);
									//console.log(result._id);
									// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
									// 	res.status(200)
									// 	.send(return_response);
									//console.log("hereee");return false;
									var redirect_url_web = base_url_web+"adSuccess";
									res.redirect(303, redirect_url_web);
								}
							});
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error.message
			});
		}
  },
  getNorUsrCarAdStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  webAccessoriesAdPayment:async function(req,res,next)
	{
		try{
			//console.log(req.query);return false;
			var base_url = customConstant.base_url;
			/* http://localhost:3001/api/webCarAdPayment?ad_id=6398854fb434cbf8981ae746&user_id=62e4f2a998c8e35854b2dba0&basic_main_plan_id=6394447a2535bcdc27eb3898&top_urgent_plan_id=62e3e22fba3fb765e8c6a615&paymentType=Paypal&price=undefined */
			var {ad_id,user_id,basic_main_plan_id,top_urgent_plan_id,paymentType,price,normalUserAdTopSearch,additional_photo_check} = req.query;
			if(top_urgent_plan_id == "" || top_urgent_plan_id == null || top_urgent_plan_id == undefined)
			{
				top_urgent_plan_id = null;
			}
			if(basic_main_plan_id == "" || basic_main_plan_id == null || basic_main_plan_id == undefined)
			{
				basic_main_plan_id = null;
			}
			if(normalUserAdTopSearch == "" || normalUserAdTopSearch == null || normalUserAdTopSearch == undefined)
			{
				normalUserAdTopSearch = null;
			}
			UserPurchasedPlan.create({
				ad_type:"accessories",
				ad_id:ad_id,
				user_id:user_id,
				maximum_upload:0,
				basic_plan_id:basic_main_plan_id,
				top_urgent_id:top_urgent_plan_id,
				top_search_plan_id:normalUserAdTopSearch,
				paymentType:paymentType,
				price:price,
				additional_photo_check:additional_photo_check
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					// console.log(result);
					// console.log(result._id);
					// return false;
					var lastInsertId = result._id;
					if(paymentType == "Paypal")
					{
						var redirect_url_web = base_url+"api/getNorUsrAccessPaymentPaypal?id="+lastInsertId+"&price="+price;
					}else{
						var redirect_url_web = base_url+"api/getNorUsrAccessPaymentStripe?id="+lastInsertId+"&price="+price;
					}
					//console.log(redirect_url_web);return false;
					res.redirect(303, redirect_url_web);
				}
			});
		}catch(error){
			//console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},

	getNorUsrAccessPaymentStripe:async function(req,res,next)
	{
		try{
			//console.log(req.query);

			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			//var title = req.query.title;
			/*console.log(lastInsertId);
			console.log(price);
			console.log(title);*/
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.create({
				line_items: [
					{
						price_data: {
							currency: 'eur',
							product_data: {
								name: "Porschists",
							},
							unit_amount: price * 100,
						},
						quantity: 1,
						//description: lastInsertId,
					},
				],
				mode: 'payment',
				//description: 'One-time setup fee',
				success_url: base_url+'api/getNorUsrAccessStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
				cancel_url: base_url+'api/getNorUsrAccessStripeCancel',
			});
			//console.log(session);
			//console.log(session.id);
			//console.log(session.payment_intent);
			//res.redirect(303, session.url);
			//console.log("lastInsertId ", lastInsertId);
			//return false;
			UserPurchasedPlan.updateOne({ _id:lastInsertId },{
				transaction_id:session.id,
				payment_intent:session.payment_intent,
				updated_at:updated_at
			},function(err,result){
				if(err)
				{
					var return_response = {"error":true,success: false,errorMessage:err};
						res.status(200)
						.send(return_response);
				}else{
					res.redirect(303, session.url);
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},

	getNorUsrAccessStripeSuccess:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_urgent = 0;
			var top_search_payment_status = 0;
			var top_search = 0;
			var top_search_which_day = 0;
			let img_arr = [];

			var updated_at = new Date();
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);
					var result = await UserPurchasedPlan.findOne({transaction_id:req.query.session_id});
					//console.log("result ", result);
					
					if(result)
					{
						let ad_id = result.ad_id.toString();
						//console.log("ad_id ", ad_id);
						
						let ad_record = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image_new:1,accessoires_image:1});
						//console.log("ad_record ", ad_record);
						if(ad_record)
						{
							img_arr = ad_record.accessoires_image.concat(ad_record.accessoires_image_new);
						}
						//console.log("img_arr ", img_arr);
						//return false;
						// if(result.basic_plan_id)
						// {
						// 	basic_plan_status = 1;
						// 	show_on_web = 1;
						// 	complition_status = 1;
						// }
						basic_plan_status = 1;
						show_on_web = 1;
						complition_status = 1;

						if(result.top_urgent_id)
						{
							top_urgent_payment_status = 1;
							top_urgent = 1;
						}

						if(result.top_search_plan_id)
						{
							//top_search_payment_status = 1;
							//top_search = 1;

							var access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:result.top_search_plan_id});
							top_search_payment_status = 1;
							if(access_top_search_record)
							{
								if(access_top_search_record.plan_day_type == 1)
								{
									top_search = 1;
								}else{
									top_search_once_week = 1;
									let datee = new Date();
									let today_day = datee.getDay();
									console.log("today_day ---->>>>>   ,,  ", today_day);
									today_day = today_day + 1;
									top_search_which_day = today_day;
								}
							}


						}

						UserPurchasedPlan.updateOne({ _id:result._id },{
							payment_status:1,
							updated_at:updated_at
						},function(err2,result2){
							if(err2)
							{
								var return_response = { "error":true,success: false,errorMessage:err2};
									res.status(200)
									.send(return_response);
							}else{
								//res.redirect(303, session.url);
								let ad_id = result.ad_id.toString();
								AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
									accessoires_image:img_arr,
									accessoires_image_new:[],
									accessories_max_photo_status:basic_plan_status,
									show_on_web:show_on_web,
									complition_status:complition_status,
									top_search:top_search,
									top_search_which_day:top_search_which_day,
									top_urgent:top_urgent,
									top_urgent_payment_status:top_urgent_payment_status,
									top_search_payment_status:top_search_payment_status,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										//console.log(result);
										//console.log(result._id);
										// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
										// 	res.status(200)
										// 	.send(return_response);
										var redirect_url_web = base_url_web+"adSuccess";
										res.redirect(303, redirect_url_web);
									}
								});
							}
						});
						
					}else{
						res.status(200)
							.send({
								error: true,
								success: false,
								errorMessage: "Non payé"
						});
					}
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},

	getNorUsrAccessStripeCancel:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
			res.status(200).send(return_response);
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getNorUsrAccessPaymentPaypal:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			//console.log(req.query);
			var updated_at = new Date();
			var base_url = customConstant.base_url; 
			var lastInsertId = req.query.id;
			var price = req.query.price;
			//price = 1;
			var title = req.query.title;
			var return_url = base_url+"api/getPaypalNorUsrAccessSuccess";
			var cancel_url = base_url+"api/getPaypalNorUsrAccessCancel";
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			const create_payment_json = {
				"intent": "sale",
				"payer": {
						"payment_method": "paypal"
				},
				"redirect_urls": {
						"return_url": return_url,
						"cancel_url": cancel_url
				},
				"transactions": [{
						"item_list": {
								"items": [{
										"name": "Porschists",
										"sku": lastInsertId,
										"price": price,
										"currency": "EUR",
										"quantity": 1
								}]
						},
						"amount": {
								"currency": "EUR",
								"total": price
						},
						"description": lastInsertId
				}]
			};
			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
						throw error;
				} else {
						for(let i = 0;i < payment.links.length;i++){
							if(payment.links[i].rel === 'approval_url'){
								res.redirect(payment.links[i].href);
							}
						}
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getPaypalNorUsrAccessSuccess:async function(req,res,next)
	{
		try{
			//console.log("success paypal");
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_urgent_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_urgent = 0;
			var updated_at = new Date();
			var top_search_payment_status = 0;
			var top_search = 0;
			var top_search_which_day = 0;
			var top_search_once_week = 0;
			let img_arr = [];

			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			const execute_payment_json = {
				"payer_id": payerId
			};
			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, async function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						console.log("payment_table_id");
						console.log(payment_table_id);
						let result = await UserPurchasedPlan.findOne({_id:payment_table_id},{});
						//console.log("result ", result);
								/*console.log(result);
								console.log(result.final_price);
								console.log(result.ad_name);
								console.log(result.payment_type);*/
								if(result)
								{
									let ad_id = result.ad_id.toString();
									console.log("ad_id 1048 line ", ad_id);
										
									let ad_record = await AddAdvertisementModel.findOne({_id:ad_id},{accessoires_image_new:1,accessoires_image:1});
									//console.log("ad_record ", ad_record);
									if(ad_record)
									{
										img_arr = ad_record.accessoires_image.concat(ad_record.accessoires_image_new);
									}
									// if(result.basic_plan_id)
									// {
									// 	basic_plan_status = 1;
									// 	show_on_web = 1;
									// 	complition_status = 1;
									// }
									basic_plan_status = 1;
									show_on_web = 1;
									complition_status = 1;
									if(result.top_urgent_id)
									{
										top_urgent_payment_status = 1;
										top_urgent = 1;
									}

									if(result.top_search_plan_id)
									{
										top_search_payment_status = 1;
										//top_search = 1;

										let access_top_search_record = await NormalUserTopSearchPlanModel.findOne({_id:result.top_search_plan_id});
											if(access_top_search_record)
											{
												if(access_top_search_record.plan_day_type == 1)
												{
													top_search = 1;
												}else{
													top_search_once_week = 1;
													let datee = new Date();
													let today_day = datee.getDay();
													console.log("today_day ---->>>>>   ,,  ", today_day);
													today_day = today_day + 1;
													top_search_which_day = today_day;
												}
											}
									}

									UserPurchasedPlan.updateOne({ _id:result._id },{
										payment_status:1,
										updated_at:updated_at
									},function(err2,result2){
										if(err2)
										{
											//console.log("err2"+err2)
											var return_response = { "error":true,success: false,errorMessage:err2};
												res.status(200)
												.send(return_response);
										}else{
											//res.redirect(303, session.url);
											AddAdvertisementModel.findOneAndUpdate({ _id:result.ad_id },{
												accessoires_image:img_arr,
												accessoires_image_new:[],
												show_on_web:show_on_web,
												complition_status:complition_status,
												top_search:top_search,
												top_urgent:top_urgent,
												basic_plan_status:basic_plan_status,
												top_search_once_week:top_search_once_week,
												top_search_which_day:top_search_which_day,
												top_urgent_payment_status:top_urgent_payment_status,
												top_search_payment_status:top_search_payment_status,
												updated_at:updated_at
											},function(err,result){
												if(err)
												{
													console.log("err"+err)
													var return_response = {"error":true,success: false,errorMessage:err};
														res.status(200)
														.send(return_response);
												}else{
													//console.log(result);
													//console.log(result._id);
													// var return_response = {"error":false,success: true,errorMessage:"Succès du paiement entièrement réalisé"};
													// 	res.status(200)
													// 	.send(return_response);
													//return false;
													var redirect_url_web = base_url_web+"adSuccess";
													res.redirect(303, redirect_url_web);
												}
											});
										}
									});
									
								}else{
									var return_response = {"error":true,success: false,errorMessage:"Identifiant invalide"};
									res.status(200).send(return_response);
								}
							
						
						//res.send('Success');
				}
			});
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
	getPaypalNorUsrAccessCancel:async function(req,res,next)
	{
		try{
			//console.log(req.query);
			var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
			res.status(200).send(return_response);
		}catch(error){
			//console.log(error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
	},
};