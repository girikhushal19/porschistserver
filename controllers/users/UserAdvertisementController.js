const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	userAllAdvertisement:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
    	const queryJoin = [
					{
		        path:'userId',
		        select:['firstName','lastName']
			    },
			    {
			    	path:'modelId',
			        select:['model_name']
			    },
			    {
			    	path:'subModelId',
			        select:['model_name']
			    },
			    {
			    	path:'fuel',
			        select:['attribute_name']
			    },
			    {
			    	path:'vehicle_condition',
			        select:['attribute_name']
			    },
			    {
			    	path:'type_of_gearbox',
			        select:['attribute_name']
			    },
			    {
			    	path:'attribute_air_rim',
			        select:['attribute_name']
			    },
			    {
			    	path:'colorId',
			        select:['color_name']
			    }

		    ];

		    //console.log(req.body);
		    //console.log(req.query.category);
		    var { userId,category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
		    var String_qr = {};
		    String_qr['userId'] = userId;
		    if(category && category != "")
		    {
		    	String_qr['category'] = category;
		    }
		    if(model_id && model_id != "")
		    {
		    	String_qr['modelId'] = model_id;
		    }
		    if(fuel && fuel != "")
		    {
		    	String_qr['fuel'] = fuel;
		    }
		    if(vehicle_condition && vehicle_condition != "")
		    {
		    	String_qr['vehicle_condition'] = vehicle_condition;
		    }
		    if(type_of_gearbox && type_of_gearbox != "")
		    {
		    	String_qr['type_of_gearbox'] = type_of_gearbox;
		    }
		    if(colorId && colorId != "")
		    {
		    	String_qr['colorId'] = colorId;
		    }

		    if(attribute_air_rim && attribute_air_rim != "")
		    {
		    	String_qr['attribute_air_rim'] = attribute_air_rim;
		    }
		    if(warranty && warranty != "")
		    {
		    	String_qr['warranty'] = warranty;
		    }
		    if(maintenance_booklet && maintenance_booklet != "")
		    {
		    	String_qr['maintenance_booklet'] = maintenance_booklet;
		    }
		    if(maintenance_invoice && maintenance_invoice != "")
		    {
		    	String_qr['maintenance_invoice'] = maintenance_invoice;
		    }
		    if(accidented && accidented != "")
		    {
		    	String_qr['accidented'] = accidented;
		    }
		    if(original_paint && original_paint != "")
		    {
		    	String_qr['original_paint'] = original_paint;
		    }
		    if(matching_number && matching_number != "")
		    {
		    	String_qr['matching_number'] = matching_number;
		    }
		    if(matching_color_paint && matching_color_paint != "")
		    {
		    	String_qr['matching_color_paint'] = matching_color_paint;
		    }
		    if(matching_color_interior && matching_color_interior != "")
		    {
		    	String_qr['matching_color_interior'] = matching_color_interior;
		    }
		    if(priceMin && priceMin != ""  && priceMax != ""  && priceMax != "" )
		    {
		    	String_qr['price'] = {  $gte:req.query.priceMin, $lte: req.query.priceMax } 
		    }

		  console.log(String_qr);
      AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'images_name':1, 'images_path':1, 'price':1, 'pro_price':1, 'userId':1, 'created_at':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  
	},
	userSingleAdvertisement:async function(req, res,next)
	{
		try{
			var { id } = req.body;
			const query = [
			{
		        path:'userId',
		        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'subModelId',
		        select:['model_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		    {
		    	path:'attribute_air_rim',
		        select:['attribute_name']
		    },
		    {
		    	path:'colorId',
		        select:['color_name']
		    }

		    ];
			AddAdvertisementModel.findOne({
		   		_id:id
			}).populate(query).lean().exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			       if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  		console.log(err);
	  	}
	},

	
};