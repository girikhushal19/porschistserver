const express = require('express');
const UsersModel = require("../../models/UsersModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

const customConstant = require('../../helpers/customConstant');

const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.porschists.com',
    port: 465,
    auth: {
       user: 'contact@porschists.com',
       pass: 'OD-oeu&mMbU%'
    }
});



/*/*/
module.exports = {
  webUserLoginSubmit: async function(req,res,next)
  {
      //console.log("login function");
    try{
      // Get user input
      const { email, password } = req.body;
      // Validate user input
      // Validate if user exist in our database
      var encryptedPassword = await bcrypt.hash(password, 10);
      var date_time = new Date();
      UsersModel.findOne({ email,"deleted_at":null },function(err,user){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        //console.log("user record"+user);
        if(user)
        {
          bcrypt.compare(req.body.password, user.password, function(errPW, resPw) {
            if (errPW){
              var return_response = {"error":true,success: false,errorMessage:errPW};
                res.status(200)
                .send(return_response);
            }
            if(resPw)
            {
              //console.log("password match");
              /*console.log(encryptedPassword);
              console.log(user.password);*/
              if(user.email_verify == 1)
              {
                // Create token
                const token = jwt.sign(
                  { user_id: user._id, email },
                  "Porschists",
                  /*process.env.TOKEN_KEY,
                  {
                    expiresIn: "2h",
                  }*/
                );
                // save user token
                user.token = token;
                UsersModel.updateOne({ "_id":user._id }, 
                {
                  token:token,
                  loggedin_time:date_time
                }, function (uerr, docs) {
                  if (uerr){
                      //console.log(uerr);
                      res.status(200)
                        .send({
                          error: true,
                          success: false,
                          errorMessage: uerr,
                          //userRecord:user
                      });
                  }else{
                    let response_record = { "_id":user._id,"user_type":user.user_type,"firstName":user.firstName,"lastName":user.lastName,"email":user.email,"token":user.token,"loggedin_time":user.loggedin_time };
                    //console.log("User controller login method : ", docs);
                    res.status(200)
                      .send({
                        error: false,
                        success: true,
                        errorMessage: "Connexion réussie",
                        userRecord:response_record
                    });
                  }
                });
                // user
                //res.status(200).json(user);
              }else{
                //res.status(400).send("Invalid Credentials");
                res.status(200)
                  .send({
                    error: true,
                    success: false,
                    errorMessage: "Votre email n'est pas vérifié"
                  });
              }
            }else {
              //console.log("password not match");
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Connexion invalide"
              });
            }
          });
        }else{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Connexion invalide"
          });
        }
      }
      });
    }catch(err) {
        console.log(err);
    }
  },

  webGetUserProfile:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //console.log(req.socket.remoteAddress);
      //console.log("my IP is  "+req.ip);
      var user_id = req.body.user_id;
      //console.log(user_id);
      UsersModel.findOne({ "_id":user_id },{firstName:1,lastName:1,email:1,mobileNumber:1,userImage:1,loggedin_time:1,userName:1}).exec((err,userRecord)=>{
        if(err)
        {
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: err
          });
        }else{
          //console.log("userRecord"+userRecord);
          if(userRecord)
          {
            if(userRecord.length === 0)
            {
              res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Détail de l'utilisateur non valide"
                  });
            }else{
              res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "",
                      userRecord:userRecord,
                  });
            }
          }else{
            console.log("here");
            res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Détail de l'utilisateur non valide"
                  });
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  }, 
  webGetFullUserProfile:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //console.log(req.socket.remoteAddress);
      //console.log("my IP is  "+req.ip);
      var user_id = req.body.user_id;
      //console.log(user_id);
      UsersModel.findOne({ "_id":user_id },{}).exec((err,userRecord)=>{
        if(err)
        {
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: err
          });
        }else{
          //console.log("userRecord"+userRecord);
          if(userRecord)
          {
            if(userRecord.length === 0)
            {
              res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Invalid user detail"
                  });
            }else{
              res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "",
                      userRecord:userRecord,
                  });
            }
          }else{
            console.log("here");
            res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Invalid user detail"
                  });
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  }, 
  webUserRegistrationSubmit: async function(req,res,next)
  {
    var encryptedPassword = "";
       try {
        
        //console.log("req.body", req.body);
        //return false;
        var { userName, email, password,user_type,firstName,lastName,mobileNumber,telephone,zip_code,city,country,whatsapp } = req.body;

         



        var otp = Math.floor(100000 + Math.random() * 900000);
        //console.log(otp);
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
        //console.log("encryptedPassword khushal "+encryptedPassword);
        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );
        let emailCount = await UsersModel.count( { email } );
        if(emailCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Email déjà existant"
          });
        }
        let userNameCount = await UsersModel.count( { userName } );
        if(userNameCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Nom d'utilisateur déjà existant"
          });
        }
        let mobileNumberCount = await UsersModel.count( { mobileNumber } );
        if(mobileNumberCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Numéro de téléphone mobile déjà existant"
          });
        }
        if(userName)
        {
          userName = userName;
        }else{
          userName = email.toLowerCase();
        }
        UsersModel.create({
          user_type:"normal_user",
          mobileNumber,
          firstName,
          lastName,
          userName: userName,
          email: email.toLowerCase(), // sanitize: convert email to lowercase
          password: encryptedPassword,
          token: token,
          address: req.body.address,
          latitude: req.body.latitude,
          longitude: req.body.longitude,
          otp: otp,
          otp_used_or_not: 0,
          telephone,
          zip_code,
          city,
          country,
          whatsapp
        },function(err,result){
            if(err)
            {
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
              var lastInsertId = result._id
              var email_verify_url = customConstant.front_end_base_url+"emailVerification/"+lastInsertId;
              let fullname = firstName+" "+lastName;
              var base_url_server = customConstant.base_url;
             
              let text_msg = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+" . Et votre nom d'utilisateur est "+userName+"  vous pouvez vous connecter avec ce nom d'utilisateur et ce mot de passe." + "<a href='"+email_verify_url+"'  >Vérifier l'email</a>";
              var imagUrl = base_url_server+"public/uploads/logo.png";
              var html = "";
              html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
              html += "<div class='content'></div>";
              html += "<p>	Bienvenue ,</p>";
              html += "<p>"+fullname+"</p>";
              html += "<p>"+text_msg+"</p>";
              html += "</div>";
              html += "<div class='mail-footer'>";
              html += "<img src='"+imagUrl+"' height='200' width='250'>";
              html += "</div>";
              html += "<i>Porschists</i>";
                //var messageEmail = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+".";
                const message = {
                  from: 'contact@porschists.com Porschists', // Sender address
                  to: email,         // recipients
                  subject: "Inscription réussie", // Subject line
                  html: html // Plain text body
                };
              transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err);
                  //error_have = 1;
                  var return_response = {"error":false,success: true,errorMessage:"Votre inscription a pleinement réussi","email_verify_url":email_verify_url};
                  res.status(200)
                  .send(return_response);
                } else {
                  console.log('mail has sent.');
                  //console.log(info);
                  var return_response = {"error":false,success: true,errorMessage:"Votre inscription a pleinement réussi","email_verify_url":email_verify_url};
                  res.status(200)
                  .send(return_response);
                }
              });
            }
          });
    }catch(error){
      console.log(error);
    }
  },
  webEmailOTPVerification: async function(req,res,next)
  {
    try{
      var date_time = new Date();
      //console.log(req.body);
      var otp = parseInt(req.body.otp);
      var user_id = req.body.user_id;
      UsersModel.findOne({_id:user_id,otp:otp},{_id:1,otp_used_or_not:1}).exec((err,result)=>{
        if(err)
        {
          //console.log(err);
          var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
        }else{
          //console.log(result);
          if(result)
          {
            if(result.otp_used_or_not == 0)
            {
              UsersModel.updateOne({ "_id":user_id }, 
              {
                email_verify:1,
                otp_used_or_not:1,
                updated_at:date_time
              }, function (uerr, docs) {
                if (uerr){
                  res.status(200)
                    .send({
                    error: true,
                    success: false,
                    errorMessage: uerr,
                    //userRecord:user
                  });
                }else{
                  res.status(200)
                    .send({
                    error: false,
                    success: true,
                    errorMessage: "Vérification d'otp avec succès ,vous pouvez maintenant vous connecter avec nous"
                  });
                }
              });
            }else{
              res.status(200)
                .send({
                error: true,
                success: false,
                errorMessage: "Vous avez tous déjà utilisé cet otp"
              });
            }
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Ce code ne correspond pas"};
              res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
        res.status(200)
        .send(return_response);
    }
  },
  webResendOTP: async function(req,res,next)
  {
    try{
      var otp = Math.floor(100000 + Math.random() * 900000);
      var date_time = new Date();
      //console.log(req.body);
      var user_id = req.body.user_id;
      UsersModel.findOne({_id:user_id},{_id:1,email:1}).exec((err,result)=>{
        if(err)
        {
          //console.log(err);
          var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
        }else{
          //console.log(result);
          if(result)
          {
            UsersModel.updateOne({ "_id":user_id }, 
            {
              email_verify:0,
              otp_used_or_not:0,
              otp:otp,
              updated_at:date_time
            }, function (uerr, docs) {
              if (uerr){
                res.status(200)
                  .send({
                  error: true,
                  success: false,
                  errorMessage: uerr,
                  //userRecord:user
                });
              }else{
                var lastInsertId = result._id
                var email_verify_url = customConstant.front_end_base_url+"emailVerification/"+lastInsertId;
                html = "";
                html+= "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+" ";
                html+= '<a href="'+email_verify_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';

                var messageEmail = html;
                const message = {
                  from: 'contact@porschists.com', // Sender address
                  to: result.email,         // recipients
                  subject: "Vérification d'Otp", // Subject line
                  html: messageEmail // Plain text body
                };
                transport.sendMail(message, function(err, info) {
                  if (err) {
                    //console.log(err);
                    //error_have = 1;
                    var return_response = {"error":false,success: true,errorMessage:"Veuillez vérifier votre adresse e-mail pour un nouvel OTP"};
                    res.status(200)
                    .send(return_response);
                  } else {
                    //console.log('mail has sent.');
                    //console.log(info);
                    var return_response = {"error":false,success: true,errorMessage:"Veuillez vérifier votre adresse e-mail pour un nouvel OTP"};
                    res.status(200)
                    .send(return_response);
                  }
                });
              }
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Utilisateur non valide "};
              res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
        res.status(200)
        .send(return_response);
    }
  },

  webResendOTPOnEmail: async function(req,res,next)
  {
    try{
      var otp = Math.floor(100000 + Math.random() * 900000);
      var date_time = new Date();
      //console.log(req.body);
      var email = req.body.email;
      UsersModel.findOne({email:email,deleted_at:null},{_id:1,email:1}).exec((err,result)=>{
        if(err)
        {
          //console.log(err);
          var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
        }else{
          //console.log(result);
          if(result)
          {
            UsersModel.updateOne({ "_id":result._id }, 
            {
              email_verify:0,
              otp_used_or_not:0,
              otp:otp,
              updated_at:date_time
            }, function (uerr, docs) {
              if (uerr){
                res.status(200)
                  .send({
                  error: true,
                  success: false,
                  errorMessage: uerr,
                  //userRecord:user
                });
              }else{
                var lastInsertId = result._id
                var email_verify_url = customConstant.front_end_base_url+"emailVerification/"+lastInsertId;
                html = "";
                html+= "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+" ";
                html+= '<a href="'+email_verify_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';

                var messageEmail = html;
                const message = {
                  from: 'contact@porschists.com', // Sender address
                  to: result.email,         // recipients
                  subject: "Vérification d'Otp", // Subject line
                  html: messageEmail // Plain text body
                };
                transport.sendMail(message, function(err, info) {
                  if (err) {
                    //console.log(err);
                    //error_have = 1;
                    var return_response = {"error":false,success: true,errorMessage:"Veuillez vérifier votre adresse e-mail pour un nouvel OTP"};
                    res.status(200)
                    .send(return_response);
                  } else {
                    //console.log('mail has sent.');
                    //console.log(info);
                    var return_response = {"error":false,success: true,errorMessage:"Veuillez vérifier votre adresse e-mail pour un nouvel OTP"};
                    res.status(200)
                    .send(return_response);
                  }
                });
              }
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Utilisateur non valide "};
              res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
        res.status(200)
        .send(return_response);
    }
  },
  webUserEditProfileSubmit:async function(req,res,next)
  {
      // console.log("update profile");
       //console.log(req.body);
      //  console.log("heree")
      // console.log(req.files);
      // //console.log(req.file.filename);
      // return false;
      try{
        var user_id = req.body.edit_id;
        var email = req.body.email;
        var mobileNumber = req.body.mobileNumber;
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;
        var date_of_birth = req.body.date_of_birth;
        var longitude = req.body.longitude;
        var latitude = req.body.latitude;
        var address = req.body.address;
        var gender = req.body.gender;
        var userName = req.body.userName;
        var zip_code = req.body.zip_code;
        var city = req.body.city;
        var country = req.body.country;
        var telephone = req.body.telephone;
        var whastapp = req.body.whastapp;
        var whatsapp = req.body.whatsapp;
        var additional_information = req.body.additional_information;
        var enterprise = req.body.enterprise;
        var activity_description = req.body.activity_description;
        var website_url = req.body.website_url;
        if(telephone == null || telephone == undefined || telephone == 'null')
        {
         
          telephone = 0;
        }
        if(zip_code == null || zip_code == undefined || zip_code == 'null')
        {
         
          zip_code = 0;
        }
        

        //console.log(user_id);
        var userRecord = await UsersModel.findOne({ "_id":user_id }); 
        //console.log("userRecord"+userRecord);return false;
        if(req.files)
        {
          console.log("iffffffffff 625 dsadsa ");
          if(req.files.filename)
          {
            if(req.files.filename.length > 0)
            { 
              var userImage = req.files.filename[0].filename;
               
            }else{
              var userImage = userRecord.userImage;
            }
          }else{
            var userImage = userRecord.userImage;
          }
          
          var oldFileName = userRecord.userImage;
          var uploadDir = './public/uploads/userProfile/';
          let fileNameWithPath = uploadDir + oldFileName;
          // console.log("fileNameWithPath ",fileNameWithPath);
          // console.log("iffffffffff 632");
          // console.log("fs.existsSync(fileNameWithPath) ",fs.existsSync(fileNameWithPath));
          // console.log("iffffffffff 634");
          // return false;
          
        }else{
          console.log("here 674")
          if(userRecord.userImage)
          {
            var userImage = userRecord.userImage;
          }else{
            var userImage = null;
          }
        }
        if(req.files)
        {
          //console.log("iffffffffff 625");
          if(req.files.logo)
          {
            //console.log("herre")
            if(req.files.logo.length > 0)
            {
              //console.log(req.files.logo[0].filename)
              var userLogo = req.files.logo[0].filename;
            }else{
              var userLogo = userRecord.companyImage;
            }
            
          }else{
            var userLogo = userRecord.companyImage;
          }
          
          var oldFileName = userRecord.companyImage;
          var uploadDir = './public/uploads/userProfile/';
          let fileNameWithPath = uploadDir + oldFileName;
          // console.log("fileNameWithPath ",fileNameWithPath);
          // console.log("iffffffffff 632");
          // console.log("fs.existsSync(fileNameWithPath) ",fs.existsSync(fileNameWithPath));
          // console.log("iffffffffff 634");
          // return false;
          // if (fs.existsSync(fileNameWithPath) == true)
          // {
          //   fs.unlink(uploadDir + oldFileName, (err) => 
          //   {
          //     console.log("unlink file error "+err);
          //   });
          // }
        }else{
          if(userRecord.companyImage)
          {
            var userLogo = userRecord.companyImage;
          }else{
            var userLogo = null;
          }
        }
        // console.log("userImage ",userImage);
        // console.log("userLogo ",userLogo);
        // console.log("hereeeeeeeee");return false;
        if(userRecord)
        {

          let userNameCount = await UsersModel.count( { "_id":{$ne:user_id},userName } );
          if(userNameCount > 0)
          {
            return res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Nom d'utilisateur déjà existant"
            });
          }


          if(gender == "" || gender == null)
          {
            gender = userRecord.gender;
          }
          var userRecordEmailCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"email":email }); 
          //console.log("userRecordEmailCheck"+userRecordEmailCheck);
          if(userRecordEmailCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Cet email existe déjà chez un autre utilisateur"
            });
          }else{
            
            var userRecordMobileCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"mobileNumber":mobileNumber }); 
            //console.log("userRecordMobileCheck"+userRecordMobileCheck);
            if(userRecordMobileCheck)
            {
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Ce numéro de portable existe déjà chez un autre utilisateur"
              });
            }else{

              
              // console.log(" userImage:userImage ",userImage)
              // console.log(" userImage:userLogo ",userLogo);
              // return false;
              UsersModel.findByIdAndUpdate({ _id: user_id },{
                 email: email.toLowerCase(),
                 mobileNumber:mobileNumber,
                 firstName:firstName,
                 lastName:lastName,
                 userImage:userImage,
                 companyImage:userLogo,
                 date_of_birth:date_of_birth, 
                 longitude:longitude,
                 latitude:latitude,
                 address:address,
                 gender:gender,
                 telephone,
                 whatsapp,
                  zip_code,
                  city,
                  country,
                  additional_information,
                  enterprise,
                  activity_description,
                  website_url
                }, function (err, user) {
                if (err) {
                  console.log("err " , err);
                  res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: err,
                      errorOrignalMessage: err
                  });
                } else {
                  //console.log("Record updated success-fully");
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Votre profil a été mis à jour avec succès"
                  });
                }
               })
            }
          }
        }else{
          //console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Détail de l'utilisateur non valide"
                });
        }
        
      }catch(err){
        //console.log(err);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: err
          });
      }
  },
  
  webUserChangePasswordSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {user_id,old_password,new_password} = req.body;
      
      var encryptedPassword = await bcrypt.hash(new_password, 10);

      if(old_password == new_password)
      {
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Mot de passe identique à l'ancien",
        });
      }else{
        UsersModel.findOne({ _id:user_id },function(err,user){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
            //console.log("user record"+user);
            if(user)
            {
              bcrypt.compare(old_password, user.password, function(errPW, resPw) {
                if (errPW){
                  var return_response = {"error":true,success: false,errorMessage:errPW};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resPw)
                  {
                    //console.log("password match");
                    UsersModel.findByIdAndUpdate({ _id: user_id },{
                      password: encryptedPassword,
                    }, function (err, user) {
                    if (err) {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: err,
                          errorOrignalMessage: err
                      });
                    } else {
                      //console.log("Record updated success-fully");
                      res.status(200)
                      .send({
                          error: false,
                          success: true,
                          errorMessage: "Votre mot de passe a été modifié avec succès. Vous pouvez maintenant vous connecter avec votre nouveau mot de passe."
                      });
                    }
                    });
                  }else{
                    //console.log("password not match");
                    res.status(200)
                      .send({
                        error: true,
                        success: false,
                        errorMessage: "Votre ancien mot de passe ne correspond pas"
                    });
                  }
                }
              });
            }else{
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Connexion invalide"
              });
            }
        }
        });
      }
    }catch(error){
      console.log(error);
    }
  },


  webForgotPasswordSubmit: async function(req,res,next)
  {
    try{
      //var otp = Math.floor(100000 + Math.random() * 900000);
      var date_time = new Date();
      //console.log(req.body);
      var email = req.body.email;
      UsersModel.findOne({email:email,deleted_at:null},{_id:1,email:1}).exec((err,result)=>{
        if(err)
        {
          //console.log(err);
          var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
        }else{
          //console.log(result);
          if(result)
          {
            
            var lastInsertId = result._id
            var email_verify_url = customConstant.front_end_base_url+"resetPassword/"+lastInsertId;
            html = "";
            html+= "Vous pouvez réinitialiser votre mot de passe, nous avons fourni un lien pour changer votre mot de passe d'ici cliquez sur le lien ci-dessous ";
            html+= '<a href="'+email_verify_url+'">'+' Cliquez ici '+'</strong>';

            var messageEmail = html;
            const message = {
              from: 'contact@porschists.com Porschists', // Sender address
              to: result.email,         // recipients
              subject: "Réinitialiser le mot de passe", // Subject line
              html: messageEmail // Plain text body
            };
            transport.sendMail(message, function(err, info) {
              if (err) {
                //console.log(err);
                //error_have = 1;
                var return_response = {"error":false,success: true,errorMessage:"E-mail envoyé ! "};
                res.status(200)
                .send(return_response);
              } else {
                //console.log('mail has sent.');
                //console.log(info);
                var return_response = {"error":false,success: true,errorMessage:"E-mail envoyé !"};
                res.status(200)
                .send(return_response);
              }
            });
              
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Utilisateur non valide "};
              res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(error)
    {
      //console.log(error);
      var return_response = {"error":true,success: false,errorMessage:error};
        res.status(200)
        .send(return_response);
    }
  },
  webResetPasswordSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {user_id,password} = req.body;
      
      var encryptedPassword = await bcrypt.hash(password, 10);

      
        UsersModel.findOne({ _id:user_id },function(err,user){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
            //console.log("user record"+user);
            if(user)
            {
              UsersModel.findByIdAndUpdate({ _id: user_id },{
                  password: encryptedPassword,
                }, function (err, user) {
                if(err){
                  res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: err,
                      errorOrignalMessage: err
                  });
                }else{
                  //console.log("Record updated success-fully");
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Votre mot de passe a été modifié avec succès. Vous pouvez maintenant vous connecter avec votre nouveau mot de passe."
                  });
                }
              });
            }else{
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Connexion invalide"
              });
            }
        }
        });
      
    }catch(error){
      console.log(error);
    }
  },
  addUpdatePaypalEmail:async(req,res)=>{
    try{
      var {user_id,paypal_email} = req.body;
      var users = await UsersModel.findOne({_id:user_id,deleted_at:null},{_id:1});
      if(users)
      {
        await UsersModel.updateOne({_id:user_id},{paypal_email:paypal_email}).then((result)=>{
          res.status(200)
            .send({
              error: false,
              success: true,
              errorMessage: "Votre email paypal a été mis à jour avec succès"
          });
        }).catch((error)=>{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: error.message
          });
        });
      }
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  addUpdatePaymentOption:async(req,res)=>{
    try{
      var {user_id,payment_option} = req.body;
      var users = await UsersModel.findOne({_id:user_id,deleted_at:null},{_id:1});
      if(users)
      {
        await UsersModel.updateOne({_id:user_id},{payment_option:payment_option}).then((result)=>{
          res.status(200)
            .send({
              error: false,
              success: true,
              errorMessage: "L'option de paiement a été mise à jour avec succès"
          });
        }).catch((error)=>{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: error.message
          });
        });
      }
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  addBankInformation:async(req,res)=>{
    try{
      console.log(req.body);
      //return false;
      // {
      //   address: "Rte de l'Aéroport, 60000 Tillé, France",
      //   latitude: 49.4544677,
      //   longitude: 2.1115111,
      //   zip_code: '60000',
      // }
      var {user_id,bank_name,account_holder_name,iban,bic,country,city,address,latitude,longitude,zip_code} = req.body;
      var new_bank = {
        bank_name:bank_name,
        account_holder_name:account_holder_name,
        iban:iban,
        bic:bic,
        country:country,
        city:city,
        address:address,
        latitude:latitude,
        longitude:longitude,
        zip_code:zip_code
      };
      var users = await UsersModel.findOne({_id:user_id},{bank:1});
      if(users)
      {
        if(users.bank.length > 0)
        {
          return res.status(200)
            .send({
              error: false,
              success: true,
              errorMessage: "Vous ne pouvez ajouter qu'un seul enregistrement bancaire"
          });
        }
        var bank_record = users.bank;
        bank_record.push(new_bank);
        await UsersModel.updateOne({_id:user_id},{bank:bank_record}).then((result)=>{
          res.status(200)
            .send({
              error: false,
              success: true,
              errorMessage: "Enregistrement bancaire ajouté sous cet utilisateur"
          });
        }).catch((error)=>{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "L'identifiant de l'utilisateur n'est pas valide"
          });
        })
      }else{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur n'est pas valide"
        });
      }
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },

  deleteBankInformation:async(req,res)=>{
    try{
      //console.log(req.body);
      var {user_id} = req.body;
      var record = await UsersModel.findOne({_id:user_id},{bank:1});
      if(!record)
      {
        return res.status(200).send({
          error: true,
          success: false,
          errorMessage: "ID de l'annonce invalide"
        });
      }
      await UsersModel.updateOne({_id:user_id},{bank:[] }).then((result)=>{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "La banque a supprimé le record de réussite dans son intégralité"
        });
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: error.message
        });
      })
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  getAllBankInformation:async(req,res)=>{
    try{
      
      var user_id = req.params.id;
     // console.log("user_id"+user_id);
      await UsersModel.findOne({_id:user_id},{bank:1}).then((result)=>{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: result
        });
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur n'est pas valide"
        });
      });
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  getSingleBankInformation:async(req,res)=>{
    try{
      
      var {user_id,bank_id} = req.body;
      //console.log("user_id"+user_id);
      await UsersModel.findOne({_id:user_id},{bank:1}).then((result)=>{
        if(result)
        {
          var newBank = result.bank.filter(val=>{
            return val._id ==  bank_id;
            //console.log("val"+val);
          });
          //console.log("newBank" +newBank);
          res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "Success",
            record:newBank
          });
        }else{
          res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'banque n'est pas valide"
          });
        }
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur n'est pas valide"
        });
      });
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  updateBankInformation:async(req,res)=>{
    try{
      //console.log(req.body);
      var {user_id,bank_name,account_holder_name,iban,bic,country,city,bank_id} = req.body;
      
      var users = await UsersModel.findOne({_id:user_id},{bank:1});
      if(users)
      {
        var old_bank = users.bank.filter(e=>{
          return e._id == bank_id;
        });
        if(old_bank)
        {
          var not_match_bank = users.bank.filter(e=>{
            return e._id != bank_id;
          });
          //console.log(old_bank);
          var new_bank = {
            bank_name:bank_name ? bank_name : old_bank[0].bank_name,
            account_holder_name:account_holder_name ? account_holder_name : old_bank[0].account_holder_name,
            iban:iban ? iban : old_bank[0].iban,
            bic:bic ? bic : old_bank[0].bic,
            country:country ? country : old_bank[0].country,
            city:city ? city : old_bank[0].city,
          };
          not_match_bank.push(new_bank);
          await UsersModel.updateOne({_id:user_id},{bank:not_match_bank}).then((result)=>{
            res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Succès de la mise à jour du relevé bancaire"
            });
          }).catch((error)=>{
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: error.message
            });
          });
        }else{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "L'identifiant de l'banque n'est pas valide"
          });
        }
      }else{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur n'est pas valide"
        });
      }
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
};