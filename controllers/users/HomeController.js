const express = require('express');
const UsersModel = require("../../models/UsersModel");
const BannerModel = require("../../models/BannerModel");
const CategoryModel = require("../../models/CategoryModel");
const ColorModel = require("../../models/ColorModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const YesnoModel = require("../../models/YesnoModel");
const ColorinternamesModel = require("../../models/ColorinternamesModel");
const ColorextnamesModel = require("../../models/ColorextnamesModel");
const ColorinteriorsModel = require("../../models/ColorinteriorsModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const { async } = require('rxjs');
/*/*/
module.exports = {
  getBannerHomePage:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await BannerModel.find({}); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier de catégorie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getColor:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
         

        ColorModel.find({ }, {}).exec((err, categoryRecord)=>
        {
          if(err)
          {
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: err
              });
          }else{
            //console.log("categoryRecord"+categoryRecord);
            //console.log("categoryRecord"+categoryRecord.length);
            if(categoryRecord)
            {
              if(categoryRecord.length === 0)
              {
                res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Pas d'enregistrement"
                    });
              }else{
                res.status(200)
                  .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier de catégorie",
                    record:categoryRecord,
                  });
              }
            }else{
              console.log("here");
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Pas d'enregistrement"
                    });
            }
          }

        });
      }catch(err){
        console.log(err);
      }
  },
  getColorInterior:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
         

        ColorinteriorsModel.find({ }, {}).exec((err, categoryRecord)=>
        {
          if(err)
          {
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: err
              });
          }else{
            //console.log("categoryRecord"+categoryRecord);
            //console.log("categoryRecord"+categoryRecord.length);
            if(categoryRecord)
            {
              if(categoryRecord.length === 0)
              {
                res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Pas d'enregistrement"
                    });
              }else{
                res.status(200)
                  .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier de catégorie",
                    record:categoryRecord,
                  });
              }
            }else{
              console.log("here");
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Pas d'enregistrement"
                    });
            }
          }

        });
      }catch(err){
        console.log(err);
      }
  },
  getCategoryOnAdPage:async function(req,res,next)
  {
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await CategoryModel.find({ status:1 },{ }).exec((err,categoryRecord)=>{
          if(err){
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: err
              });
          }else{
            if(categoryRecord)
            {
              if(categoryRecord.length === 0)
              {
                res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Pas d'enregistrement"
                    });
              }else{
                res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Succès",
                        record:categoryRecord,
                    });
              }
            }else{
              //console.log("here");
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Pas d'enregistrement"
                    });
            }
          }
        }) 
     
      }catch(err){
        console.log(err);
      }
  },
  getCategory:async function(req,res,next)
  {
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await CategoryModel.find({ status:1 },{category:1,images:1,first_heading:1}).exec((err,categoryRecord)=>{
          if(err){
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: err
              });
          }else{
            if(categoryRecord)
            {
              if(categoryRecord.length === 0)
              {
                res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Pas d'enregistrement"
                    });
              }else{
                res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Succès",
                        record:categoryRecord,
                    });
              }
            }else{
              //console.log("here");
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Pas d'enregistrement"
                    });
            }
          }
        }) 
     
      }catch(err){
        console.log(err);
      }
  },
  registration_year:async function(req,res,next)
  {
      //console.log("get profile");
    try{
    	arr = [];
    	var current_year = new Date().getFullYear();
    	var after_thirty_year = current_year - 75;
    	console.log(current_year);
    	console.log("after_thirty_year "+after_thirty_year);
    	var x=0;
    	for(let i=current_year; i>=after_thirty_year; i--)
    	{
    		//arr["year"] = i;
        arr.push({'year': i})
    		x++;
    	}
    	//console.log(arr);

    	res.status(200)
	        .send({
	            error: false,
	            success: true,
	            errorMessage: "Year Record",
	            record:arr,
	        });
    }catch(err){
        console.log(err);
    }
  },

  allYesNoApi:async function(req,res,next)
  {
      //console.log("get profile");
    try{
    	YesnoModel.find({}, {}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Attribute Record",
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Attribute Record",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getModel: async function(req,res,next)
  {
  	try{
  		const { attribute_type } = req.body;
  		//console.log("attribute_type_val"+attribute_type);
  		var imagePath = customConstant.modelsImagePath;
  		/*console.log("base_url"+JSON.stringify(customConstant));
  		console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);*/
	  	ModelsModel.find({attribute_type:attribute_type,status:1,parent_id:null}, {model_name:1}).exec((err, result)=>
	    {
	    	//console.log("err "+err);
	    	//console.log("result "+result);
	    	if(err)
	    	{
	    		console.log(err);
	    		res.status(200)
		        .send({
		            error: true,
		            success: false,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:err,
		        });

	    	}else{
	    		res.status(200)
		        .send({
		            error: false,
		            success: true,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:result,
		        });
	    	}
	    });
  	}catch(err){
        console.log(err);
    }
  },
  getSubModel: async function(req,res,next)
  {
    try{
      const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      var imagePath = customConstant.modelsImagePath;
      /*console.log("base_url"+JSON.stringify(customConstant));
      console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);*/
      ModelsModel.find({parent_id:parent_id}, {}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Models Record",
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Models Record",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getAttribute: async function(req,res,next)
  {
    try{
      const { attribute_type } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      /*console.log("base_url"+JSON.stringify(customConstant));
      console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);*/
      AttributeModel.find({attribute_type:attribute_type,parent_id:null}, {}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Attribute Record",
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Attribute Record",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getSubAttribute: async function(req,res,next)
  {
    try{
      const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      var imagePath = customConstant.modelsImagePath;
      /*console.log("base_url"+JSON.stringify(customConstant));
      console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);*/
      AttributeModel.find({parent_id:parent_id,status:1}, {attribute_name:1}).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Models Record",
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Models Record",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getHomepageAdvertisement: async function(req,res,next)
  {
    try{
      //const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);

      const queryJoin = [
          {
            path:'userId',
            select:['firstName','lastName']
          },
          {
            path:'modelId',
              select:['model_name']
          },
          {
            path:'subModelId',
              select:['model_name']
          },
          {
            path:'fuel',
              select:['attribute_name']
          },
          {
            path:'vehicle_condition',
              select:['attribute_name']
          },
          {
            path:'type_of_gearbox',
              select:['attribute_name']
          },
          {
            path:'colorId',
              select:['color_name']
          }

        ];

      var imagePath = customConstant.base_url;
      AddAdvertisementModel.find({"category":"Porsche Voitures","complition_status":1,"activation_status":1}, {"_id":1,"ad_name":1,"user_type":1,"model_variant":1,"registration_year":1,"price":1,"price_for_pors":1,"pro_price":1,"exterior_image":1,"mileage_kilometer":1,"engine_operation_hour":1,"cylinder_capacity":1}).populate(queryJoin).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getHomepageAccessoires: async function(req,res,next)
  {
    try{
      //const { parent_id } = req.body;
      //console.log("attribute_type_val"+attribute_type);

      const queryJoin = [
          {
            path:'userId',
            select:['firstName','lastName']
          },
          {
            path:'modelId',
              select:['model_name']
          },
          {
            path:'subModelId',
              select:['model_name']
          },
          {
            path:'type_de_piece',
              select:['attribute_name']
          },
          {
            path:'colorId',
              select:['color_name']
          }

        ];

      var imagePath = customConstant.base_url;
      AddAdvertisementModel.find({"category":"Pièces et accessoires","complition_status":1,"activation_status":1}, {"_id":1,"ad_name":1,"user_type":1,"model_variant":1,"registration_year":1,"price":1,"price_for_pors":1,"pro_price":1,"state":1,"OEM":1,"delivery_price":1,"delivery_price":1,"accessoires_image":1}).populate(queryJoin).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  getExtColorNameById:async(req,res)=>{
    try{
      let {id} = req.params;
      console.log(id);
      let clr_res = await ColorModel.findOne({_id:id},{color_name:1});
      if(!clr_res)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Aucun enregistrement n'a été trouvé"
        });
      }else{
        let all_color_rec = await ColorextnamesModel.find({color_ext:clr_res.color_name},{color_name:1});
        if(all_color_rec.length > 0)
        {
          return res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès",
              record:all_color_rec
          });
        }else{
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Aucun enregistrement n'a été trouvé"
          });
        }        
      }
    }catch(e){
      return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
    }
  },
  getIntColorNameById:async(req,res)=>{
    try{
      let {id} = req.params;
      //console.log(id);
      let clr_res = await ColorinteriorsModel.findOne({_id:id},{color_name:1});
      if(!clr_res)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Aucun enregistrement n'a été trouvé"
        });
      }else{
        let all_color_rec = await ColorinternamesModel.find({color_ext:clr_res.color_name},{color_name:1});
        if(all_color_rec.length > 0)
        {
          return res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès",
              record:all_color_rec
          });
        }else{
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Aucun enregistrement n'a été trouvé"
          });
        }        
      }
    }catch(e){
      return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
    }
  }
};