const express = require('express');
const UsersModel = require("../../models/UsersModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');

const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.porschists.com',
    port: 465,
    auth: {
       user: 'contact@porschists.com',
       pass: 'OD-oeu&mMbU%'
    }
});



/*/*/
module.exports = {
  userRegistration: async function(req, res,next)
  {
    //console.log("controller registration method");
    //console.log(req.body);
    //res.send("herfsdfe");
    var encryptedPassword = "";
       try {
        const { userName, email, password,user_type,firstName,lastName,mobileNumber,gender } = req.body;
        var otp = Math.floor(100000 + Math.random() * 900000);
        //console.log(otp);
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
        //console.log("encryptedPassword khushal "+encryptedPassword);
        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );

        let emailCount = await UsersModel.count( { email } );
        if(emailCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Email déjà existant"
          });
        }
        let userNameCount = await UsersModel.count( { userName } );
        if(userNameCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Nom d'utilisateur déjà existant"
          });
        }
        let mobileNumberCount = await UsersModel.count( { mobileNumber } );
        if(mobileNumberCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Numéro de téléphone mobile déjà existant"
          });
        }

        const user =  UsersModel.create({
          user_type:"normal_user",
          mobileNumber,
          firstName,
          lastName,
          userName: userName,
          email: email.toLowerCase(), // sanitize: convert email to lowercase
          password: encryptedPassword,
          token: token,
          gender: req.body.gender,
          address: req.body.address,
          latitude: req.body.latitude,
          longitude: req.body.longitude,
          otp: otp,
          otp_used_or_not: 0,
        },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
            let fullname = firstName+" "+lastName;
            var base_url_server = customConstant.base_url;
            let text_msg = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+" . Et votre nom d'utilisateur est "+userName+"  vous pouvez vous connecter avec ce nom d'utilisateur et ce mot de passe.";
            var imagUrl = base_url_server+"public/uploads/logo.png";
            var html = "";
            html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
            html += "<div class='content'></div>";
            html += "<p>	Bienvenue ,</p>";
            html += "<p>"+fullname+"</p>";
            html += "<p>"+text_msg+"</p>";
            html += "</div>";
            html += "<div class='mail-footer'>";
            html += "<img src='"+imagUrl+"' height='200' width='250'>";
            html += "</div>";
            html += "<i>Porschists</i>";
              //var messageEmail = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+".";
              const message = {
                from: 'contact@porschists.com Porschists', // Sender address
                to: email,         // recipients
                subject: "Inscription réussie", // Subject line
                html: html // Plain text body
              };
            transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err);
                  error_have = 1;
                } else {
                  //console.log('mail has sent.');
                  //console.log(info);
                }
            });

          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch (err) {
        console.log(err);
    }
  },
  proUserRegistration: async function(req, res,next)
  {
    //console.log("controller registration method");
    // console.log(req.body);
    // console.log(req.files);
    // return false;
    //res.send("herfsdfe");
    var encryptedPassword = "";
       try {
        const { userName, email, password,user_type,firstName,lastName,mobileNumber,gender,country,enterprise,postalcode,siret,registration_number,zip_code,city,additional_information,website_url,activity_description,main_activity,whatsapp } = req.body;
        //console.log(req.file);
        
        //registration_certificate
        var userDocument = null;
        var registration_certificate = null;
        if(req.files)
        {
          if(req.files.userDocument)
          {
            var userDocument = req.files.userDocument[0].filename;
          }else{
            var userDocument = null;
          }
        }
        if(req.files)
        {
          if(req.files.registration_certificate)
          {
            var registration_certificate = req.files.registration_certificate[0].filename;
          }else{
            var registration_certificate = null;
          }
        }
        
        // console.log(req.files);
        // console.log("userDocument "+userDocument);
        // console.log("registration_certificate "+registration_certificate);
        // return false;
        var otp = Math.floor(100000 + Math.random() * 900000);
        //console.log(otp);
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
        //console.log("encryptedPassword khushal "+encryptedPassword);
        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );
        
        
      

        let emailCount = await UsersModel.count( { email } );
        if(emailCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Email déjà existant"
          });
        }
        let userNameCount = await UsersModel.count( { userName } );
        if(userNameCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Nom d'utilisateur déjà existant"
          });
        }
        let mobileNumberCount = await UsersModel.count( { mobileNumber } );
        if(mobileNumberCount > 0)
        {
          return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Numéro de téléphone mobile déjà existant"
          });
        }
        const user =  UsersModel.create({
          user_type:"pro_user",
          mobileNumber,
          firstName,
          lastName,
          userName: userName,
          email: email.toLowerCase(), // sanitize: convert email to lowercase
          password: encryptedPassword,
          token: token,
          gender: req.body.gender,
          address: req.body.address,
          latitude: req.body.latitude,
          longitude: req.body.longitude,
          country: country,
          enterprise: enterprise,
          postalcode: postalcode,
          siret: siret,
          userDocument: userDocument,
          otp: otp,
          otp_used_or_not: 0,
          registration_number:registration_number,
          registration_certificate:registration_certificate,
          zip_code,
          city,
          additional_information,
          website_url,
          activity_description,
          main_activity,
          whatsapp:whatsapp
        },function(err,result){
            if(err)
            {
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
                var lastInsertId = result._id
                var email_verify_url = customConstant.front_end_base_url+"emailVerification/"+lastInsertId;
                  
                
                let fullname = firstName+" "+lastName;
                  var base_url_server = customConstant.base_url;
                  let text_msg = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+" . Et votre nom d'utilisateur est "+userName+"  vous pouvez vous connecter avec ce nom d'utilisateur et ce mot de passe." + "<a href='"+email_verify_url+"'  >Vérifier l'email</a>";
                  var imagUrl = base_url_server+"public/uploads/logo.png";
                  var html = "";
                  html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                  html += "<div class='content'></div>";
                  html += "<p>	Bienvenue ,</p>";
                  html += "<p>"+fullname+"</p>";
                  html += "<p>"+text_msg+"</p>";
                  html += "</div>";
                  html += "<div class='mail-footer'>";
                  html += "<img src='"+imagUrl+"' height='200' width='250'>";
                  html += "</div>";
                  html += "<i>Porschists</i>";


                   


                    //var messageEmail = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+".";
                    const message = {
                      from: 'contact@porschists.com Porschists', // Sender address
                      to: email,         // recipients
                      subject: "Inscription réussie", // Subject line
                      html: html // Plain text body
                    };


                  transport.sendMail(message, function(err, info) {
                      if (err) {
                        console.log('mail has not sent.');
                        console.log(err);
                        error_have = 1;
                        var return_response = {"error":false,success: true,errorMessage:"Votre inscription est terminée, veuillez vérifier votre compte","email_verify_url":email_verify_url};
                        res.status(200)
                        .send(return_response);
                      } else {
                        console.log('mail has sent.');
                        //console.log(info);
                        var return_response = {"error":false,success: true,errorMessage:"Votre inscription est terminée, veuillez vérifier votre compte","email_verify_url":email_verify_url};
                        res.status(200)
                        .send(return_response);
                      }
                  });


            }
          });


    }catch (err) {
        console.log(err);
    }
  },
  userLogin: async function(req,res,next)
  {
      //console.log("login function");
    try{
      // Get user input
      const { email, password } = req.body;
      // Validate user input
      // Validate if user exist in our database
      var encryptedPassword = await bcrypt.hash(password, 10);
      UsersModel.findOne({ email,"deleted_at":null },function(err,user){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        //console.log("user record"+user);
        if(user)
        {
          bcrypt.compare(req.body.password, user.password, function(errPW, resPw) {
            if (errPW){
              var return_response = {"error":true,success: false,errorMessage:errPW};
                res.status(200)
                .send(return_response);
            }
            if(resPw)
            {
              //console.log("password match");
              /*console.log(encryptedPassword);
              console.log(user.password);*/
              if(user.email_verify == 1)
              {
                // Create token
                const token = jwt.sign(
                  { user_id: user._id, email },
                  "Porschists",
                  /*process.env.TOKEN_KEY,
                  {
                    expiresIn: "2h",
                  }*/
                );
                // save user token
                user.token = token;
                UsersModel.updateOne({ "_id":user._id }, 
                {token:token}, function (uerr, docs) {
                  if (uerr){
                      //console.log(uerr);
                      res.status(200)
                        .send({
                          error: true,
                          success: false,
                          errorMessage: uerr,
                          //userRecord:user
                      });
                  }else{
                    let response_record = { "_id":user._id,"user_type":user.user_type,"firstName":user.firstName,"lastName":user.lastName,"email":user.email,"token":user.token };
                    //console.log("User controller login method : ", docs);
                    res.status(200)
                      .send({
                        error: false,
                        success: true,
                        errorMessage: "Connexion réussie",
                        userRecord:response_record
                    });
                  }
                });
                // user
                //res.status(200).json(user);
              }else{
                //res.status(400).send("Invalid Credentials");
                res.status(200)
                  .send({
                    error: true,
                    success: false,
                    errorMessage: "Votre email n'est pas vérifié"
                  });
              }
            }else {
              //console.log("password not match");
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Connexion invalide"
              });
            }
          });
        }else{
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Connexion invalide"
          });
        }
      }
      });
    }catch(err) {
        console.log(err);
    }
  },
  getUserProfile:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //console.log(req.socket.remoteAddress);
      //console.log("my IP is  "+req.ip);
      var user_id = req.body.user_id;
      //console.log(user_id);
      var userRecord = await UsersModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      if(userRecord)
      {
        if(userRecord.length === 0)
        {
          res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Invalid user detail"
              });
        }else{
          res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "User detail",
                  userRecord:userRecord,
              });
        }
      }else{
        console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Invalid user detail"
              });
      }
    }catch(err){
      console.log(err);
    }
  },  
  updateUserProfile:async function(req,res,next)
  {
      //console.log("update profile");
      //console.log(req.body);
      //console.log(req.file.filename);
      try{
        var user_id = req.body.user_id;
        var email = req.body.email;
        var mobileNumber = req.body.mobileNumber;
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;


        //console.log(user_id);
        var userRecord = await UsersModel.findOne({ "_id":user_id }); 
        console.log("userRecord"+userRecord);
        if(req.file)
        {
          var userImage = req.file.filename;
          var oldFileName = userRecord.userImage;
          var uploadDir = './public/uploads/userProfile/';
          let fileNameWithPath = uploadDir + oldFileName;
          console.log(fileNameWithPath);

          if (fs.existsSync(fileNameWithPath))
          {
            fs.unlink(uploadDir + oldFileName, (err) => 
            {
              console.log("unlink file error "+err);
            });
          }
        }else{
          if(userRecord.userImage)
          {
            var userImage = userRecord.userImage;
          }else{
            var userImage = null;
          }
        }
        if(userRecord)
        {
          
          var userRecordEmailCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"email":email }); 
          console.log("userRecordEmailCheck"+userRecordEmailCheck);
          if(userRecordEmailCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "This email already exists with another user"
            });
          }else{
            
            var userRecordMobileCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"mobileNumber":mobileNumber }); 
            console.log("userRecordMobileCheck"+userRecordMobileCheck);
            if(userRecordMobileCheck)
            {
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "This mobile number already exists with another user"
              });
            }else{
              UsersModel.findByIdAndUpdate({ _id: user_id },{ email: email.toLowerCase(),mobileNumber:mobileNumber,firstName:firstName,lastName:lastName,userImage:userImage }, function (err, user) {
                if (err) {
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Something went wrong",
                        errorOrignalMessage: err
                    });
                  
                } else {
                  console.log("Record updated success-fully");
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Record updated success-fully"
                    });
                }
               })
            }
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Invalid user detail"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  }, 
  emailVerification:async function(req,res,next)
  {
      //console.log("update profile");
      //console.log(req.body);
      //console.log(req.file.filename);
      try{
        var otp = req.body.otp;
        var email = req.body.email;

        UsersModel.findOne({ "email":email,"deleted_at":null },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          if(result)
          {
            //console.log(result);
            if(result.otp_used_or_not == 0)
            {
              if(result.otp == otp)
              {
                //console.log("otp matched");
                //console.log(otp);
                UsersModel.findOneAndUpdate({ "email":email,"deleted_at":null },{ otp_used_or_not:1,email_verify:1 }, function (err, user) {
                  if (err) {
                    res.status(200)
                      .send({
                      error: true,
                      success: false,
                      errorMessage: "Quelque chose a mal tourné",
                      errorOrignalMessage: err
                    });
                  }else{
                    //console.log("Enregistrement mis à jour avec succès");
                      res.status(200)
                      .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement mis à jour avec succès"
                    });
                  }
                });
              }else{
                res.status(200)
                  .send({
                    error: true,
                    success: false,
                    errorMessage: "Otp ne correspond pas"
                });
              }
            }else{
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Otp est utilisé"
              });
            }
          }else{
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Cet email n'est pas disponible"
            });
          }
        }
      });
      }catch(err){
        console.log(err);
      }
  }, 
  resendCodeForEmailVerification:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //console.log(req.socket.remoteAddress);
      //console.log("my IP is  "+req.ip);
      var email = req.body.email;
      var otp = Math.floor(100000 + Math.random() * 900000);
      //console.log(user_id);
      UsersModel.findOne({ "email":email,"deleted_at":null },function(err,userRecord){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          //console.log("userRecord"+userRecord);
          if(userRecord)
          {
            if(userRecord.length === 0)
            {
              res.status(200)
                .send({
                error: true,
                success: false,
                errorMessage: "Détails de l'utilisateur non valides"
              });
            }else{
              var messageEmail = "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+otp+".";
              const message = {
                from: 'contact@porschists.com', // Sender address
                to: email,         // recipients
                subject: "Vérification d'Otp", // Subject line
                text: messageEmail // Plain text body
              };
              transport.sendMail(message, function(err, info) {
                  if (err) {
                    //console.log(err);
                    error_have = 1;
                    var return_response = {"error":true,success: false,errorMessage:err};
                      res.status(200)
                      .send(return_response);
                  } else {
                    //console.log('mail has sent.');
                    //console.log(info);
                    UsersModel.findOneAndUpdate({ "email":email,"deleted_at":null },{otp:otp, otp_used_or_not:0,email_verify:0 }, function (err, user) {
                      if (err) {
                        res.status(200)
                          .send({
                          error: true,
                          success: false,
                          errorMessage: "Quelque chose a mal tourné",
                          errorOrignalMessage: err
                        });
                      }else{
                        //console.log("Enregistrement mis à jour avec succès");
                          res.status(200)
                          .send({
                            error: false,
                            success: true,
                            errorMessage: "Enregistrement mis à jour avec succès"
                        });
                      }
                    });
                  }
              });
            }
          }else{
            //console.log("here");
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Détails de l'utilisateur non valides"
            });
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  }, 
};