const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const UserfavadsModel = require("../../models/UserfavadsModel");
const FollowSellerModel = require("../../models/FollowSellerModel");
const AdReportModel = require("../../models/AdReportModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
//const customConstant = require('../../helpers/customConstant');
const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

 

/*/*/
module.exports = {

  adUserFavAdd:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var {user_id,ad_id,status} = req.body;
      AddAdvertisementModel.findOne({ _id:req.body.ad_id },{userId:1}).exec((errAd,resultAd)=>{
        if(errAd)
        {
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "C'est votre propre annonce",
          });
        }else{
          if(resultAd)
          {
            if(resultAd.userId == user_id)
            {
              res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Il s'agit de votre propre article",
              });
            }else{
              UserfavadsModel.findOne({user_id:user_id,ad_id:ad_id}).exec((err,result)=>{
                if(err)
                {
                  res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: err,
                  });
                }else{
                  if(result)
                  {
                    //console.log("result "+result);return false;
                    UserfavadsModel.deleteOne({user_id:user_id,ad_id:ad_id},function(errDelete,resultDelete){
                      if(errDelete)
                      {
                        res.status(200)
                        .send({
                          error: true,
                          success: false,
                          errorMessage: errDelete,
                        });
                      }else{
                        res.status(200)
                        .send({
                          error: false,
                          success: true,
                          errorMessage: "Suppression d'un élément dans votre liste de favoris",
                        });
                      }
                    });
                  }else{
                    UserfavadsModel.create({user_id:user_id,ad_id:ad_id,ad_type:status},function(err2,result2){
                      if(err2)
                      {
                        res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: err2,
                        });
                      }else{
                        if(result2)
                        {
                          res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Article à ajouter à votre liste de favoris",
                          });
                        }else{
                          res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Quelque chose a mal tourné",
                          });
                        }
                      }
                    });
                  }
                }
              });
            }
          }
        }
      });
    }catch(error)
    {
      //console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error,
      });
    }
  },
  getWebAllUserFavAd:async(req,res)=>{
    try{
      var {user_id} = req.body;
      if(user_id == "" || user_id == null || user_id == undefined  || user_id == 'null' || user_id == 'undefined')
      {
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requise"
        });
      }
      UserfavadsModel.aggregate([
        {
          $match:{
            user_id: mongoose.Types.ObjectId(user_id)
          }
        },
        {
          $addFields:{
            "advertId":"$ad_id"
          }
        },
        // {
        //   $addFields:{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $lookup:{
            from:"advertisements",
            let:{"add_id":{"$toObjectId":"$advertId"} },
            pipeline:[
              {
                $match:{ $expr:{ $eq:["$_id","$$add_id"] } }
              },
              {
                $match:{
                  $and:[
                    {show_on_web:1},
                    {complition_status:1},
                    {activation_status:1},
                    {delete_at:0},
                  ]
                }
              }
            ],
            as:"ad_data"
          }
        },
        {
          $project:{
            'ad_data.show_on_web':1,
            'ad_data.top_urgent':1,
            'ad_data.category':1,
            'ad_data.ad_name':1,
            'ad_data._id':1,
            'ad_data.exterior_image':1,
            'ad_data.accessoires_image':1,
            'ad_data.address':1,
            'ad_data.price':1,
            'ad_data.pro_price':1,
            'ad_data.userId':1,
            'ad_data.created_at':1,
            'ad_data.top_search':1,
            'ad_data.isFav':1,
            //'tmpOrder':1,
            //'ad_id':1,
           // 'user_id':1,

          }
        },
        // {
        //   $sort:{
        //     'tmpOrder':1
        //   }
        // }
      ]).then((result)=>{
        res.status(200)
        .send({
          error: false,
          success: true,
          errorMessage: "Success",
          data:result
        });
      }).catch((error)=>{
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
        });
      });
      const queryJoin = [
        {
          path:'userId',
          select:['firstName','lastName']
        },
        {
          path:'modelId',
            select:['model_name']
        },
        {
          path:'subModelId',
            select:['model_name']
        },
        {
          path:'fuel',
            select:['attribute_name']
        },
        {
          path:'vehicle_condition',
            select:['attribute_name']
        },
        {
          path:'type_of_gearbox',
            select:['attribute_name']
        },
        {
          path:'colorId',
            select:['color_name']
        },
        {
          path:'type_de_piece',
            select:['attribute_name']
        },
      ];
      
      //console.log(req.body);
      //console.log(req.query.category);
      
      // var String_qr = {};
      // //Porsche Voitures
      // var allFav = [];
      // UserfavadsModel.find({user_id:user_id},{ad_id:1}).exec((err,result)=>{
      //   if(err)
      //   {
      //     res.status(200)
      //     .send({
      //         error: true,
      //         success: false,
      //         errorMessage: err,
      //     });
      //   }else{
      //     if(result.length > 0)
      //     {
      //       var x=0;
      //       var mm = result.length - 1;
      //       result.forEach(objectAd=>{
      //         String_qr['_id'] = objectAd.ad_id;
      //         AddAdvertisementModel.findOne( String_qr , {'ad_name':1, '_id':1, 'exterior_image':1, 'images_path':1, 'price':1, 'pro_price':1, 'userId':1, 'created_at':1,"registration_year":1,"price_for_pors":1,"state":1,"OEM":1,"delivery_price":1,"accessoires_image":1,"category":1}).populate(queryJoin).sort({ }).lean(true).exec((errAd, resultAd)=>{
      //           if(errAd)
      //           {
      //             console.log(errAd);
      //           }else{
      //             allFav.push(resultAd);
      //             if(mm == x)
      //             {
      //               //console.log(allFav);
      //               var return_response = {"error":false,errorMessage:"success","record":allFav,"userFav":result};
      //               res.status(200).send(return_response);
      //             }
      //             x++;
      //           }
      //         });
      //       });
      //     }else{
      //       var return_response = {"error":true,errorMessage:"No Record","record":result};
      //       res.status(200).send(return_response);
      //     }
      //   }
      // });
    }catch(error){
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  followUnfollowSeller:async(req,res)=>{
    try{
      var {user_id,seller_id} = req.body;
      if(!user_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Veuillez vous connecter pour suivre les vendeurs",
        });
      }
      if(!seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant du vendeur n'est pas valide",
        });
      }
      if(user_id == seller_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Vous ne pouvez pas suivre votre propre personne",
        });
      }
      var allReadyLike = await FollowSellerModel.count({user_id:user_id,seller_id:seller_id});
      if(allReadyLike)
      {
        await FollowSellerModel.deleteOne({
          user_id:user_id,
          seller_id:seller_id
        }).then((result)=>{
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Vous ne suivez plus du tout ce vendeur",
          });
        }).catch((error)=>{
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: error.message,
          });
        });
      }else{
        await FollowSellerModel.create({
          user_id:user_id,
          seller_id:seller_id
        }).then((result)=>{
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Vous avez suivi le succès de ce vendeur pleinement",
          });
        }).catch((error)=>{
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: error.message,
          });
        });
      }
    }catch(error)
    {
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error.message,
      });
    }
  },
  getAdFollowedSeller:async(req,res)=>
  {
    try{
      //mongoose.set("debug",true);
      var {user_id} = req.body;
      console.log("user_id "+user_id);
      if(!user_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Veuillez vous connecter pour suivre les vendeurs",
        });
      }
      await FollowSellerModel.aggregate([
        {
          $match:{
            user_id:mongoose.Types.ObjectId(user_id)
          }
        },
        {
          $addFields:{
            "sllr_id":"$seller_id"
          }
        },
        // {
        //   $addFields:{
        //     "tmpOrder":{'$rand':{} }
        //   }
        // },
        {
          $lookup:{
            from:"users",
            let:{"usr_id":{"$toObjectId":"$sllr_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$usr_id"] 
                  }
                }
              }
            ],
            as:"sellers"
          }
        },
        {
          $lookup:{
            from:"advertisements",
            let:{"slr_id":{"$toObjectId":"$sllr_id"}},
           
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$userId","$$slr_id"] 
                  }
                }
              },
              {
                $match:
                {
                  $and:
                  [
                    {show_on_web:1},
                    {complition_status:1},
                    {activation_status:1},
                    {delete_at:0},
                  ]
                }
                
              }
            ],
            as:"ad_data"
          }
        },
        {
          $project:{
            'ad_data.show_on_web':1,
            'ad_data.top_urgent':1,
            'ad_data.category':1,
            'ad_data.ad_name':1,
            'ad_data._id':1,
            'ad_data.exterior_image':1,
            'ad_data.accessoires_image':1,
            'ad_data.address':1,
            'ad_data.price':1,
            'ad_data.pro_price':1,
            'ad_data.userId':1,
            'ad_data.created_at':1,
            'ad_data.top_search':1,
            'ad_data.isFav':1,
            //'tmpOrder':1,
            'user_id':1,
            'seller_id':1,
            'sellers.firstName':1,
            'sellers.lastName':1,
            'sellers.userImage':1,
            
          }
        },

        // "firstName": "steve",
        // "lastName": "smith",

        // {
        //   $sort:{
        //     'tmpOrder':1
        //   }
        // }
      ]).then((result)=>{
        res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: result,
        });
      }).catch((error)=>{
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: error.message,
        });
      });
      
    }catch(error)
    {
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error.message,
      });
    }
  },
  adUserReport:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var {user_id,ad_id,reason,message} = req.body;
      if(!user_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requis",
        });
      }
      if(!ad_id)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'annonceur est requis",
        });
      }
      if(!reason)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Veuillez choisir une catégorie",
        });
      }
      let own_ad = await AddAdvertisementModel.findOne({  _id:ad_id,userId:user_id });
      if(own_ad)
      {
        return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "C'est votre propre publicité",
        });
      }
      await AdReportModel.create({
        user_id,
        ad_id,
        reason,
        message
      }).then((result)=>{
        res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Votre signalement a bien été envoyé <br>Nous vous remercions pour votre action. Nous allons vérifier l'annonce signalée dans les meilleurs délais et prendrons les mesures adéquates pour préserver le contenu de notre site. Si l'annonce est contraire à nos règles de diffusion, elle fera l'objet d'une suppression et vous en serez informé par email. Dans le cas contraire, elle restera en ligne.",
        });
      }).catch((error)=>{
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: error,
        });
      })
    }catch(error)
    {
      //console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error,
      });
    }
  },
  
};