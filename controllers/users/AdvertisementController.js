const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	addAdvertisement:async function(req,res,next)
	{
		try{
			//var { user_id,model_id,maintenance_booklet,maintenance_invoice,accidented  } = req.body;
				console.log(req.body);
				console.log("all data  "+JSON.stringify(req.body));
	      //var fileImage = req.file.modelFile;
	      console.log(req.files);
	      console.log(typeof req.body.paint_thickness_parts);
	      console.log(typeof req.body.report_piwi_dates);
	      //return false;
	      //console.log(req.files.length);
	      //console.log('req.files.file.data:' , req.files[0].filename);
	      var images_name = [];
	      var images_path = [];
	      for(let i=0; i<req.files.length; i++)
	      {
	      	images_name.push(req.files[i].filename); 
	      	images_path.push(req.files[i].path); 
	      }
	     	console.log(images_name);
	     	console.log(images_path);

	     	if(req.body.model_variant)
	     	{
	     		var model_variant = req.body.model_variant;
	     	}else{
	     		var model_variant = null;
	     	}
	     	if(req.body.sub_model_id)
	     	{
	     		var sub_model_id = req.body.sub_model_id;
	     	}else{
	     		var sub_model_id = null;
	     	}
	     	if(req.body.pro_price)
	     	{
	     		var pro_price = req.body.pro_price;
	     	}else{
	     		var pro_price = null;
	     	}
	     	if(req.body.user_type == "normal_user")
	     	{
	     		var show_on_web = 1;
	     	}else{
	     		var show_on_web = 0;
	     	}
	     	console.log("sub_model_id "+sub_model_id);
	     	//return false;
	     	AddAdvertisementModel.create({
	     		  ad_name:req.body.ad_name,
	     		  category:req.body.category,
            user_type:req.body.user_type,
            userId:req.body.user_id,
            modelId:req.body.model_id,
            subModelId:sub_model_id,
            model_variant:model_variant,
            fuel:req.body.fuel,
            vehicle_condition:req.body.vehicle_condition,
            type_of_gearbox:req.body.type_of_gearbox,
            colorId:req.body.color,
            attribute_air_rim:req.body.attribute_air_rim,
            registration_year:req.body.registration_year,
            warranty:req.body.warranty,
            warranty_month:req.body.warranty_month,
            maintenance_booklet:req.body.maintenance_booklet,
            maintenance_invoice:req.body.maintenance_invoice,
            accidented:req.body.accidented,
            original_paint:req.body.original_paint,
            matching_number:req.body.matching_number,
            matching_color_paint:req.body.matching_color_paint,
            matching_color_interior:req.body.matching_color_interior,
            price:req.body.price,
            pro_price:pro_price,
            vat_tax:req.body.vat_tax,
            deductible_VAT:req.body.deductible_VAT,
            number_of_owners:req.body.number_of_owners,
            cylinder_capacity:req.body.cylinder_capacity,
            mileage:req.body.mileage,
            paint_thickness:req.body.paint_thickness,
            description:req.body.description,
            images_name:images_name,
            images_path:images_path,
            payment_type:req.body.payment_type,
            complition_status:1,
            show_on_web:show_on_web,
            address:req.body.address,
            location: {
					   type: "Point",
					   coordinates: [req.body.longitude,req.body.latitude]
					  },
            city:req.body.city,
            zipcode:req.body.zipcode,
					});
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Models added success-fully"
          });
		}catch(err){
	  		console.log(err);
	  }
	},
	allAdvertisementCount:async function(req,res,next)
	{
    try{
    	//mongoose.set('debug', true);
		    //console.log(req.body);
		    //console.log(req.query.category);
		    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
		    var String_qr = {};
		    if(category && category != "")
		    {
		    	String_qr['category'] = category;
		    }
		    if(model_id && model_id != "")
		    {
		    	String_qr['modelId'] = model_id;
		    }
		    if(fuel && fuel != "")
		    {
		    	String_qr['fuel'] = fuel;
		    }
		    if(vehicle_condition && vehicle_condition != "")
		    {
		    	String_qr['vehicle_condition'] = vehicle_condition;
		    }
		    if(type_of_gearbox && type_of_gearbox != "")
		    {
		    	String_qr['type_of_gearbox'] = type_of_gearbox;
		    }
		    if(colorId && colorId != "")
		    {
		    	String_qr['colorId'] = colorId;
		    }
		    if(attribute_air_rim && attribute_air_rim != "")
		    {
		    	String_qr['attribute_air_rim'] = attribute_air_rim;
		    }
		    if(warranty && warranty != "")
		    {
		    	String_qr['warranty'] = warranty;
		    }
		    if(maintenance_booklet && maintenance_booklet != "")
		    {
		    	String_qr['maintenance_booklet'] = maintenance_booklet;
		    }
		    if(maintenance_invoice && maintenance_invoice != "")
		    {
		    	String_qr['maintenance_invoice'] = maintenance_invoice;
		    }
		    if(accidented && accidented != "")
		    {
		    	String_qr['accidented'] = accidented;
		    }
		    if(original_paint && original_paint != "")
		    {
		    	String_qr['original_paint'] = original_paint;
		    }
		    if(matching_number && matching_number != "")
		    {
		    	String_qr['matching_number'] = matching_number;
		    }
		    if(matching_color_paint && matching_color_paint != "")
		    {
		    	String_qr['matching_color_paint'] = matching_color_paint;
		    }
		    if(matching_color_interior && matching_color_interior != "")
		    {
		    	String_qr['matching_color_interior'] = matching_color_interior;
		    }
		    if(priceMin && priceMin != ""  && priceMax != ""  && priceMax != "" )
		    {
		    	String_qr['price'] = {  $gte:req.query.priceMin, $lte: req.query.priceMax } 
		    }
		  console.log(String_qr);
      AddAdvertisementModel.count(String_qr,(err, resultCount)=>
      {
         console.log("result"+resultCount);
        	var return_response = {"error":false,errorMessage:"success","record":resultCount};
        	res.status(200).send(return_response);
      });

    }catch(err){
      console.log(err);
    }
  
	},
	allAdvertisement:async function(req,res,next)
	{
		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
    	const queryJoin = [
					{
		        path:'userId',
		        select:['firstName','lastName']
			    },
			    {
			    	path:'modelId',
			        select:['model_name']
			    },
			    {
			    	path:'subModelId',
			        select:['model_name']
			    },
			    {
			    	path:'fuel',
			        select:['attribute_name']
			    },
			    {
			    	path:'vehicle_condition',
			        select:['attribute_name']
			    },
			    {
			    	path:'type_of_gearbox',
			        select:['attribute_name']
			    },
			    {
			    	path:'attribute_air_rim',
			        select:['attribute_name']
			    },
			    {
			    	path:'colorId',
			        select:['color_name']
			    }

		    ];

		    //console.log(req.body);
		    //console.log(req.query.category);
				//{  $gte:req.query.priceMin, $lte: req.query.priceMax } 
		    var { category,model_id,fuel,vehicle_condition,type_of_gearbox,colorId,attribute_air_rim,warranty,maintenance_booklet,maintenance_invoice,accidented,original_paint,matching_number,matching_color_paint,matching_color_interior,price ,priceMin ,priceMax } = req.query;
		    var String_qr = {};
		    if(category && category != "")
		    {
		    	String_qr['category'] = category;
		    }
		    if(model_id && model_id != "")
		    {
		    	String_qr['modelId'] = model_id;
		    }
		    if(fuel && fuel != "")
		    {
		    	String_qr['fuel'] = fuel;
		    }
		    if(vehicle_condition && vehicle_condition != "")
		    {
		    	String_qr['vehicle_condition'] = vehicle_condition;
		    }
		    if(type_of_gearbox && type_of_gearbox != "")
		    {
		    	String_qr['type_of_gearbox'] = type_of_gearbox;
		    }
		    if(colorId && colorId != "")
		    {
		    	String_qr['colorId'] = colorId;
		    }

		    if(attribute_air_rim && attribute_air_rim != "")
		    {
		    	String_qr['attribute_air_rim'] = attribute_air_rim;
		    }
		    if(warranty && warranty != "")
		    {
		    	String_qr['warranty'] = warranty;
		    }
		    if(maintenance_booklet && maintenance_booklet != "")
		    {
		    	String_qr['maintenance_booklet'] = maintenance_booklet;
		    }
		    if(maintenance_invoice && maintenance_invoice != "")
		    {
		    	String_qr['maintenance_invoice'] = maintenance_invoice;
		    }
		    if(accidented && accidented != "")
		    {
		    	String_qr['accidented'] = accidented;
		    }
		    if(original_paint && original_paint != "")
		    {
		    	String_qr['original_paint'] = original_paint;
		    }
		    if(matching_number && matching_number != "")
		    {
		    	String_qr['matching_number'] = matching_number;
		    }
		    if(matching_color_paint && matching_color_paint != "")
		    {
		    	String_qr['matching_color_paint'] = matching_color_paint;
		    }
		    if(matching_color_interior && matching_color_interior != "")
		    {
		    	String_qr['matching_color_interior'] = matching_color_interior;
		    }
		    if(priceMin && priceMin != ""  && priceMax != ""  && priceMax != "" )
		    {
		    	String_qr['price'] = {  $gte:req.query.priceMin, $lte: req.query.priceMax } 
		    }

		  console.log(String_qr);
      AddAdvertisementModel.find( String_qr , {'ad_name':1, '_id':1, 'images_name':1, 'images_path':1, 'price':1, 'pro_price':1, 'userId':1, 'created_at':1}).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  
	},
	singleAdvertisement:async function(req, res,next)
	{
		try{
			var { id } = req.body;
			const query = [
			{
		        path:'userId',
		        select:['firstName','lastName']
		    },
		    {
		    	path:'modelId',
		        select:['model_name']
		    },
		    {
		    	path:'subModelId',
		        select:['model_name']
		    },
		    {
		    	path:'fuel',
		        select:['attribute_name']
		    },
		    {
		    	path:'vehicle_condition',
		        select:['attribute_name']
		    },
		    {
		    	path:'type_of_gearbox',
		        select:['attribute_name']
		    },
		   
		    {
		    	path:'colorId',
		        select:['color_name']
		    }

		    ];
			AddAdvertisementModel.findOne({
		   		_id:id
			}).populate(query).lean().exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			       if(result)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  		console.log(err);
	  	}
	},
	step1CarAdName:async function(req,res,next)
	{
		//console.log("here");
		try{
			//var { user_id,model_id,maintenance_booklet,maintenance_invoice,accidented  } = req.body;
			var ad_id = req.body.ad_id;
				//console.log(req.body);
				//console.log("all data  "+JSON.stringify(req.body));return false;
	      //var fileImage = req.file.modelFile;
	      
	     	if(req.body.user_type == "normal_user")
	     	{
	     		var show_on_web = 1;
	     	}else{
	     		var show_on_web = 0;
	     	}
	     	//return false;
	     	if(ad_id)
	     	{
	     		AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
			      ad_name:req.body.ad_name,
			      updated_at:updated_at
			    },function(err,result){
				    if(err)
				    {
				      var return_response = {"error":true,success: false,errorMessage:err};
				        res.status(200)
				        .send(return_response);
				    }else{
				      //console.log(result);
				      //console.log(result._id);

				      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
				        res.status(200)
				        .send(return_response);
				    }
				  });
	     	}else{
	     		AddAdvertisementModel.create({
	     		  ad_name:req.body.ad_name,
	     		  category:req.body.category,
            user_type:req.body.user_type,
            userId:req.body.user_id,
            show_on_web:show_on_web,
					},function(err,result){
            if(err)
            {
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
              //console.log(result);
              //console.log(result._id);
              var lastInsertId = result._id;
              var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:lastInsertId};
                res.status(200)
                .send(return_response);
            }
          });
	     	}
	     	
           
		}catch(err){
	  		console.log(err);
	  }
	},
	step2CarAdName:async function(req,res,next)
	{
		/*console.log("hereeee");
		console.log(req.body);
		//console.log(typeof req.body.paint_thickness_parts);
		console.log("fileseee");
		console.log(req.files);
		if(req.files.piwi_report_image)
		{
			console.log("piwi image");
			console.log(req.files.piwi_report_image);
		}
		if(req.files.mileage_maintanance_report_image)
		{
			console.log("mileage image");
			console.log(req.files.mileage_maintanance_report_image);
		}*/
		 
		try{
			var ad_id = req.body.ad_id;
				var updated_at = new Date();
		    var piwi_report_image =  [];
		    var mileage_maintanance_report_image = [];

				if(req.files.piwi_report_image)
				{
					var p_r_i = req.files.piwi_report_image;
					/*console.log("piwi image");
					console.log(req.files.piwi_report_image);*/
					for(let m=0; m<p_r_i.length; m++)
					{
						/*console.log(req.files.piwi_report_image[0]);
						console.log(req.files.piwi_report_image[1]);
						console.log(req.files.piwi_report_image[m].filename);
						console.log(req.files.piwi_report_image[m].path);*/
						let images_name_obj = { "filename":req.files.piwi_report_image[m].filename,"path":req.files.piwi_report_image[m].path  };
						 piwi_report_image.push(images_name_obj);
						}
						//console.log(piwi_report_image);
					}
					if(req.files.mileage_maintanance_report_image)
					{
						/*console.log("mileage image");
						console.log(req.files.mileage_maintanance_report_image);*/
						for(let m=0; m<req.files.mileage_maintanance_report_image.length; m++)
						{
							/*console.log(req.files.piwi_report_image[0]);
							console.log(req.files.piwi_report_image[1]);
							console.log(req.files.piwi_report_image[m].filename);
							console.log(req.files.piwi_report_image[m].path);*/
							let images_name_obj = { "filename":req.files.mileage_maintanance_report_image[m].filename,"path":req.files.mileage_maintanance_report_image[m].path  };
							 mileage_maintanance_report_image.push(images_name_obj);
							}
							console.log(mileage_maintanance_report_image);
					}

				if(req.body.model_variant)
		   	{
		   		var model_variant = req.body.model_variant;
		   	}else{
		   		var model_variant = null;
		   	}
		   	if(req.body.sub_model_id)
		   	{
		   		var sub_model_id = req.body.sub_model_id;
		   	}else{
		   		var sub_model_id = null;
		   	}
		   	//console.log("sub_model_id "+sub_model_id);
		   	//return false;
		   	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      modelId:req.body.model_id,
		      subModelId:sub_model_id,
		      model_variant:model_variant,
		      registration_year:req.body.registration_year,
		      type_of_chassis:req.body.type_of_chassis,
		      fuel:req.body.fuel,
		      type_of_gearbox:req.body.type_of_gearbox,
		      vehicle_condition:req.body.vehicle_condition,
		      colorId:req.body.color,
		      attribute_air_rim:req.body.attribute_air_rim,
		      warranty:req.body.warranty,
		      warranty_month:req.body.warranty_month,
		      maintenance_booklet:req.body.maintenance_booklet,
		      maintenance_invoice:req.body.maintenance_invoice,
		      accidented:req.body.accidented,
		      original_paint:req.body.original_paint,
		      matching_number:req.body.matching_number,
		      matching_color_paint:req.body.matching_color_paint,
		      matching_color_interior:req.body.matching_color_interior,
		      price_for_pors:req.body.price_for_pors,
		      deductible_VAT:req.body.deductible_VAT,
		      number_of_owners:req.body.number_of_owners,
		      cylinder_capacity:req.body.cylinder_capacity,
		      mileage_kilometer:req.body.mileage_kilometer,
		      paint_thickness_parts:req.body.paint_thickness_parts,
		      body_repainted:req.body.body_repainted,
		      engine_operation_hour:req.body.engine_operation_hour,
		      piwi_checkbox:req.body.piwi_checkbox,
		      piwi_report_date:req.body.piwi_report_date,
		      piwi_report_image:piwi_report_image,
		      mileage_maintanance_report_date:req.body.mileage_maintanance_report_date,
		      mileage_maintanance_report_kilometer:req.body.mileage_maintanance_report_kilometer,
		      mileage_maintanance_report_image:mileage_maintanance_report_image,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
	  }
		
	},
	step3CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
				AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      ad_name:req.body.ad_name,
		      description:req.body.description,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step4CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var pro_price = req.body.pro_price;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.find({ _id:ad_id },(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          /*console.log("record");
          console.log(record);
          console.log(record[0].price_for_pors);*/
          var price_for_pors = record[0].price_for_pors;
          if(price_for_pors == "yes")
          {
          	if(pro_price)
          	{
	          	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
					      price:req.body.price,
					      updated_at:updated_at
					    },function(err,result){
						    if(err)
						    {
						      var return_response = {"error":true,success: false,errorMessage:err};
						        res.status(200)
						        .send(return_response);
						    }else{
						      //console.log(result);
						      //console.log(result._id);

						      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
						        res.status(200)
						        .send(return_response);
						    }
						  });
          	}else{
          		var return_response = {"error":true,success: false,errorMessage:"Pro price needed"};
					        res.status(200)
					        .send(return_response);
          	}
          	
          }else{
          	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
				      price:req.body.price,
				      updated_at:updated_at
				    },function(err,result){
					    if(err)
					    {
					      var return_response = {"error":true,success: false,errorMessage:err};
					        res.status(200)
					        .send(return_response);
					    }else{
					      //console.log(result);
					      //console.log(result._id);

					      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
					        res.status(200)
					        .send(return_response);
					    }
					  });
          }
        }
      });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step5CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;

		var updated_at = new Date();
		try{
			var exterior_image = [];
			var interior_image = [];
			var trunk_engine_image = [];
			if(req.files.exterior_image || req.files.interior_image || req.files.trunk_engine_image)
			{
				if(req.files.exterior_image)
				{
					var p_r_i = req.files.exterior_image;
					/*console.log("piwi image");
					console.log(req.files.exterior_image);
					console.log(req.files.interior_image);
					console.log(req.files.trunk_engine_image);*/
					for(let m=0; m<p_r_i.length; m++)
					{
						/*console.log(req.files.exterior_image[0]);
						console.log(req.files.exterior_image[1]);
						console.log(req.files.exterior_image[m].filename);
						console.log(req.files.exterior_image[m].path);*/
						let images_name_obj = { "filename":req.files.exterior_image[m].filename,"path":req.files.exterior_image[m].path  };
						 exterior_image.push(images_name_obj);
						}
						//console.log(exterior_image);
				}
				if(req.files.interior_image)
				{
					/*console.log("mileage image");
					console.log(req.files.interior_image);*/
					for(let m=0; m<req.files.interior_image.length; m++)
					{
						/*console.log(req.files.interior_image[0]);
						console.log(req.files.interior_image[1]);
						console.log(req.files.interior_image[m].filename);
						console.log(req.files.interior_image[m].path);*/
						let images_name_obj = { "filename":req.files.interior_image[m].filename,"path":req.files.interior_image[m].path  };
						 interior_image.push(images_name_obj);
						}
						//console.log(interior_image);
				}
				if(req.files.trunk_engine_image)
				{
					/*console.log("mileage image");
					console.log(req.files.trunk_engine_image);*/
					for(let m=0; m<req.files.trunk_engine_image.length; m++)
					{
						/*console.log(req.files.trunk_engine_image[0]);
						console.log(req.files.trunk_engine_image[1]);
						console.log(req.files.trunk_engine_image[m].filename);
						console.log(req.files.trunk_engine_image[m].path);*/
						let images_name_obj = { "filename":req.files.trunk_engine_image[m].filename,"path":req.files.trunk_engine_image[m].path  };
						 trunk_engine_image.push(images_name_obj);
						}
						//console.log(trunk_engine_image);
				}
				/*console.log(exterior_image);
				console.log(interior_image);
				console.log(trunk_engine_image);
				return false;*/

				AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      exterior_image:exterior_image,
		      interior_image:interior_image,
		      trunk_engine_image:trunk_engine_image,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
			}else{
				var return_response = {"error":false,success: true,errorMessage:"No image",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			}
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step6CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        address:req.body.address,
        location: {
			   type: "Point",
			   coordinates: [req.body.longitude,req.body.latitude]
			  },
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step7CarAdName:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        contact_number:req.body.contact_number,
        complition_status:1,
        activation_status:1,
        show_on_web:1,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step2Accessoires:async function(req,res,next)
	{
		//console.log(req.body);
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        modelId:req.body.model_id,
        registration_year:req.body.registration_year,
        type_de_piece:req.body.type_de_piece,
        state:req.body.state,
        OEM:req.body.OEM,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step3Accessoires:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
				AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      ad_name:req.body.ad_name,
		      description:req.body.description,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step4Accessoires:async function(req,res,next)
	{
		//console.log(req.body);
		var ad_id = req.body.ad_id;
		var pro_price = req.body.pro_price;
		var updated_at = new Date();
		try{
      /*console.log("record");
      console.log(record);
      console.log(record[0].price_for_pors);*/
    	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
	      price:req.body.price,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);

		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step5Accessoires:async function(req,res,next)
	{
		//console.log("hereeeee");
		//console.log(req.file);
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		var accessoires_image = [];
		if(req.file)
		{
			let images_name_obj = { "filename":req.file.filename,"path":req.file.path  };
			accessoires_image.push(images_name_obj);
			try{
	      /*console.log("record");
	      console.log(record);
	      console.log(record[0].price_for_pors);*/
	    	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      accessoires_image:accessoires_image,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
			}catch(err){
		  		console.log(err);
		  		var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		  }
		}else{
      var return_response = {"error":true,success: false,errorMessage:"No image",lastInsertId:ad_id};
        res.status(200)
        .send(return_response);
    }
	},
	step5AccessoiresProfessional:async function(req,res,next)
	{
		//console.log("hereeeee");
		//console.log(req.files);
		 
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		var accessoires_image = [];

		if(req.files)
		{
			/*console.log("mileage image");
			console.log(req.files.accessoires_image);*/
			for(let m=0; m<req.files.length; m++)
			{
				/*console.log(req.files.accessoires_image[0]);
				console.log(req.files.accessoires_image[1]);
				console.log(req.files.accessoires_image[m].filename);
				console.log(req.files.accessoires_image[m].path);*/
				let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
				 accessoires_image.push(images_name_obj);
			}
		//console.log(accessoires_image);return false;
			try{
	      /*console.log("record");
	      console.log(record);
	      console.log(record[0].price_for_pors);*/
	    	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      accessoires_image:accessoires_image,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
			}catch(err){
		  		console.log(err);
		  		var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		  }
		}else{
      var return_response = {"error":true,success: false,errorMessage:"No image",lastInsertId:ad_id};
        res.status(200)
        .send(return_response);
    }
	},
	step6Accessoires:async function(req,res,next)
	{
		try{
			var updated_at = new Date();
			var ad_id = req.body.ad_id;
	      /*console.log("record");
	      console.log(record);
	      console.log(record[0].price_for_pors);*/
	    	AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
		      delivery_price:req.body.delivery_price,
		      updated_at:updated_at
		    },function(err,result){
			    if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
			      //console.log(result);
			      //console.log(result._id);

			      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
			        res.status(200)
			        .send(return_response);
			    }
			  });
			}catch(err){
		  		console.log(err);
		  		var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		  }
	},
	step7Accessoires:async function(req,res,next)
	{
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        address:req.body.address,
        location: {
			   type: "Point",
			   coordinates: [req.body.longitude,req.body.latitude]
			  },
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	step8Accessoires:async function(req,res,next)
	{
		var ad_id = req.body.ad_id;
		var updated_at = new Date();
		try{
			AddAdvertisementModel.findOneAndUpdate({ _id:req.body.ad_id },{
        contact_number:req.body.contact_number,
        show_on_web:1,
        complition_status:1,
        activation_status:1,
	      updated_at:updated_at
	    },function(err,result){
		    if(err)
		    {
		      var return_response = {"error":true,success: false,errorMessage:err};
		        res.status(200)
		        .send(return_response);
		    }else{
		      //console.log(result);
		      //console.log(result._id);
		      var return_response = {"error":false,success: true,errorMessage:"Succès",lastInsertId:ad_id};
		        res.status(200)
		        .send(return_response);
		    }
		  });
		}catch(err){
	  		console.log(err);
	  		var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	  }
	},
	addAdImageTesting:async function(req,res,next)
	{
		//var ad_id = req.body.ad_id;
		//var updated_at = new Date();
		try{
			console.log(req.body);
			console.log(req.files);
		}catch(err){
			//console.log(err);
			var return_response = {"error":true,success: false,errorMessage:err};
			res.status(200)
			.send(return_response);
	  }
	},
	
};