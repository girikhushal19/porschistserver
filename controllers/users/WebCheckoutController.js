const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const OrderModel = require("../../models/OrderModel");
const RatingModel = require("../../models/RatingModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const SettingModel = require("../../models/SettingModel");
/*/*/
module.exports = {
  checkoutShippingCheck:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      let {user_id,keyValueShipping} = req.body;
      let cart_data = await CartsModel.find({user_id:user_id});

      // console.log("cart_data ",cart_data);
      // console.log("cart_data length ",cart_data.length);

      let arr_key = []; let arr_price = [];
      for (const key in keyValueShipping)
      {
        // console.log("key ",key);
        // console.log("keyValueShipping ", keyValueShipping[key]);
        arr_key.push(key);
        arr_price.push(keyValueShipping[key]);
      }
      // console.log("arr_key ",arr_key);
      // console.log("arr_price ", arr_price);
      if(cart_data.length == arr_key.length)
      {
        for(let x=0; x< cart_data.length; x++)
        {
          //console.log("arr_key ", arr_key[x]);
          await CartsModel.updateOne({user_id:user_id,ad_id:arr_key[x]},{shipping_charge:arr_price[x] });
        }
        var return_response = {"error":false,success: true,errorMessage:"Expédition appliquée avec succès dans son intégralité"};
        return res.status(200).send(return_response);
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Veuillez sélectionner l'option d'expédition pour tous les produits"};
        return res.status(200).send(return_response);
      }
    }catch(err){
      //console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err.messaage};
      return res.status(200).send(return_response);
    } 
  },
  webCheckout:async function(req,res,next)
  {
    try{

      //console.log("req.body ", req.body);return false;
      var tax_percent = 0;
			var tax_val = await SettingModel.findOne({ attribute_key:"access_pur_tax_percent" }, {});
			if(tax_val)
			{
				tax_percent = tax_val.attribute_value
			}


      //console.log("heree  cxvcxv"+req.body.user_id);return false;
      var random_str = Math.floor(10000000 + Math.random() * 90000000);
      var user_id = req.body.user_id;
      //var self_pickup = req.body.self_pickup;
      var payment_type = req.body.payment_type;
      if(payment_type == "" || payment_type == undefined || payment_type == null)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant du type de paiement est requis"});
      }
      if(user_id == "" || user_id == undefined || user_id == null)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis"});
      }

     
      var multi_price = 0;
      var final_price = 0;
      var shipping_charge = 0;
      var firstName = null;
      var lastName = null;
      var email = null;
      var mobileNumber = null;
      var user_record =  await UsersModel.findOne({_id:user_id});
      if(user_record)
      {
        firstName = user_record.firstName;
        lastName = user_record.lastName;
        email = user_record.email;
        mobileNumber = user_record.mobileNumber;
      }
      CartsModel.find({user_id:user_id},(err, recordOld)=>{
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:errsC};
        res.status(400)
          .send(return_response);
      }else{
        // console.log(recordOld);
        // return false;
        if(recordOld.length > 0)
        {
          //console.log(recordOld);
          var shipping_charge = 0;
          var break_loop = recordOld.length - 1;
          for (let i=0; i <recordOld.length; i++)
          {
            // console.log("ad_id " , recordOld[i].ad_id.user_type);
            // return false;
            //console.log(recordOld[i].user_id);
            //console.log("recordOld[i].quantity "+recordOld[i].quantity);
            var ad_owner_user_id = recordOld[i].ad_owner_user_id;
            var ad_id = recordOld[i].ad_id;
            //var payment_type = recordOld[i].payment_type;
            var quantity = recordOld[i].quantity;
            //var price = recordOld[i].price;
            //var tax = recordOld[i].tax;
            var tax = tax_percent;
            shipping_charge = shipping_charge + parseFloat(recordOld[i].shipping_charge);
            var address_id = recordOld[i].address_id;
            var self_pickup = recordOld[i].self_pickup;
            
            AddAdvertisementModel.findOne({_id:ad_id},{"ad_name":1,"price":1, 'pro_price':1,'category':1,'user_type':1,'registration_year':1,'price_for_pors':1,'description':1,'address':1,'location':1,'state':1,'OEM':1,'type_de_piece':1,'shipping_price':1,'contact_number':1 }).exec((err, Advertresult)=>
            {
              //console.log("Advertresult"+Advertresult);return false;
              var price = Advertresult.price;
              //console.log("price"+price);
              //console.log("quantity"+quantity);
              multi_price = (price * recordOld[i].quantity)+multi_price;
              //console.log("multi_price"+multi_price);
              var ad_name = Advertresult.ad_name;
              var category = Advertresult.category;
              var registration_year = Advertresult.registration_year;
              var description = Advertresult.description;
              var pick_up_address = Advertresult.address;
              var contact_number = Advertresult.contact_number;
              var pick_up_location = Advertresult.location;
              var state = Advertresult.state;
              var OEM = Advertresult.OEM;

              var user_type = Advertresult.user_type;

              // console.log("pick_up_location "+pick_up_location);
              // console.log("pick_up_location "+pick_up_location.type);
              // console.log("pick_up_location "+pick_up_location.coordinates);
              // console.log("pick_up_location "+pick_up_location.coordinates[0]);
              // console.log("pick_up_location "+pick_up_location.coordinates[1]);

              //pick_up_location.coordinates[0],pick_up_location.coordinates[1]
              //return false;
              /*
              */

              //console.log("delivery_price dfgfd "+Advertresult.shipping_price);
              // if(self_pickup == 1)
              // {
              //   shipping_charge = 0
              // }else{
              //   shipping_charge = shipping_charge + parseFloat(Advertresult.shipping_price);
              // }
              
              //console.log("shipping_charge "+shipping_charge);
              AttributeModel.findOne({_id:Advertresult.type_de_piece},{"attribute_name":1}).exec((errs, AttributeResult)=>
              {
                //console.log(AttributeResult);return false;
                //console.log(AttributeResult.attribute_name);
                if(AttributeResult)
                {
                  var type_de_piece = AttributeResult.attribute_name;
                }else{
                  var type_de_piece = null;
                }
                
                //user_type
                console.log("user_type "+user_type);
                var tax_price = 0;
                if(user_type == "pro_user")
                {
                  tax_price = multi_price * tax / 100;
                }
                
                //console.log("tax_price "+tax_price);
                final_price = parseFloat(multi_price) + parseFloat(tax_price) + parseFloat(shipping_charge);
                let seller_profit = parseFloat(multi_price) + parseFloat(shipping_charge);
                // console.log("multi_price "+multi_price);
                // console.log("shipping_charge "+shipping_charge);
                // console.log("tax_price "+tax_price);
                // console.log("final_price "+final_price);
                // return false;

                DeliveryAddressUser.findOne({ _id:address_id },(AddressErr, AddRecord)=>
                {
                  if(AddressErr)
                  {
                    console.log("AddressErr");
                    console.log(AddressErr);
                  }else{
                    // console.log("address_id "+address_id);
                    // console.log("else no error AddRecord"+AddRecord);
                    // console.log("else no error AddRecord"+AddRecord._id);
                    // console.log("else no error AddRecord"+AddRecord.address);
                    // console.log(AddRecord.location);
                    // return false;
                    if(AddRecord)
                    {
                      var delivery_address = AddRecord.address;
                      var delivery_location = AddRecord.location;
                      var l1 = delivery_location.coordinates[0];
                      var l2 = delivery_location.coordinates[1];
                    }else{
                      var delivery_address = "";
                      var l1 = 0;
                      var l2 = 0;
                    }
                    
                    ///console.log();
                    //console.log("delivery_location"+AddRecord._id);
                    //return false;
                    /*console.log(AddRecord.location.type);
                    console.log(delivery_location);return false;*/
                    // var delivery_location.type;
                    // var delivery_location.coordinates[0];
                    // var delivery_location.coordinates[1];
                   

                    OrderModel.create({
                      order_id:random_str,
                      user_id:user_id,
                      ad_owner_user_id:ad_owner_user_id,
                      ad_id:ad_id,
                      quantity:recordOld[i].quantity,
                      tax:tax,
                      shipping_charge:shipping_charge,
                      price:price,
                      final_price:final_price,
                      seller_profit:seller_profit,
                      pick_up_address:pick_up_address,
                      pick_up_location: {
                       type: pick_up_location.type,
                       coordinates: pick_up_location.coordinates
                      },
                      ad_name:ad_name,
                      category:category,
                      registration_year:registration_year,
                      description:description,
                      state:state,
                      OEM:OEM,
                      type_de_piece:type_de_piece,
                      contact_number:contact_number,
                      payment_type:payment_type,
                      address:delivery_address,
                      location: {
                        type: 'Point',
                        coordinates: [l1,l2]
                       },
                       firstName:firstName,
                       lastName:lastName,
                       email:email,
                       mobileNumber:mobileNumber
                    },function(err,Finalresult){
                      if(err)
                      {
                        //console.log("iffff");
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200).send(return_response);
                      }else{
                        if(i == break_loop)
                        {
                          //console.log("here");
                          //console.log(Finalresult);
                          //console.log(Finalresult._id);

                          var return_response = {"error":false,success: true,errorMessage:"Please do payment",order_id:random_str};
                          res.status(200).send(return_response);
                        }
                        
                      }
                    });
                  }
                });
              });
            });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"No item in your cart"};
          res.status(200)
            .send(return_response);
        }
      }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err};
        res.status(200)
        .send(return_response);
    } 
  },

  webAddRatingReviews:async function(req,res,next)
  {
    try{
      //RatingModel
      let {order_id,user_id,seller_id,rating,review,ad_id} = req.body;
      if(!ad_id)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant de l'annonceur est requis"});
      }
      if(!order_id)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant de la commande est requis"});
      }
      if(!user_id)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis"});
      }
      if(!seller_id)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"L'identifiant du vendeur est requis"});
      }
      if(!rating)
      {
        return res.status(200).send({"error":true,success: false,errorMessage:"Le classement est requis"});
      }
      let rating_record = await RatingModel.findOne({order_id:order_id});
      let order_record = await OrderModel.findOne({_id:order_id});
      if(order_record)
      {
        if(rating_record)
        {
          return res.status(200).send({"error":true,success: false,errorMessage:"Note déjà ajoutée pour cette commande"});
        }else{
          if(order_record.shipping_status == 3)
          {
            await RatingModel.create({ad_id:ad_id,order_id:order_id,user_id:user_id,seller_id:seller_id,rating:rating,review:review});
            return res.status(200).send({"error":false,success: true,errorMessage:"Evaluation de la réussite ajoutée pleinement "});
          }else{
            return res.status(200).send({"error":true,success: false,errorMessage:"Cette commande n'est pas encore livrée"});
          }
        }
      }else{
        return res.status(200).send({"error":true,success: false,errorMessage:"ID de commande invalide"});
      }
      
      
    }catch(e)
    {
      var return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
    }
  }
};