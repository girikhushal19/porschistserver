const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const UserPurchasedPlan = require("../../models/UserPurchasedPlan");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {

  proUserPaymentHistory:async(req,res)=>{
    try{
      //mongoose.set("debug",true);
      var {user_id,pageNo} = req.body;
      var limitt = 10;
      pageNo = parseInt(pageNo);
      limitt = parseInt(limitt);
      pageNo = pageNo ? pageNo * limitt : 0;
      // _id:mongoose.Types.ObjectId('user_id')
      if(user_id == "" || user_id == null || user_id == undefined  || user_id == 'null' || user_id == 'undefined')
      {
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requise"
        });
      }
      await UserPurchasedPlan.aggregate([
        {
          $match:{user_id:mongoose.Types.ObjectId(user_id)}
        },
        {
          $match:{payment_status: 1}
        },
        // {
        //   $addFields:{
        //     "b_p_id_p_u":{$arrayElemAt:["$basic_plan_id_pro_user",0]}
        //   }
        // },
        // {
        //   $lookup:{
        //     from:"prouserplans",
        //     let:{"bpidpu":{"$toObjectId":"$b_p_id_p_u"}},
        //     pipeline:[
        //       {
        //         $match:{$expr:{$eq:["$_id","$$bpidpu"]}}
        //       }
        //     ],
        //     as:"proplans"
        //   }
        // }
        {
          $skip:pageNo
        },
        {
          $limit:limitt
        }
      
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        res.status(200)
        .send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        res.status(200)
        .send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200)
      .send(return_response);
    }
  }

}