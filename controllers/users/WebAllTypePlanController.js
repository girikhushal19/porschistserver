const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const ProUserPlanTopSearchModel = require("../../models/ProUserPlanTopSearchModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");

const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {

	webGetUserParticularPlan:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      UserPlanModel.find({},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  webGetUserParticularPlanTopUrgent:async function(req,res,next)
  {
    try{
      let plan_type = req.body.plan_type;

      TopUrgentPlan.find({plan_type:plan_type},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },

  webGetUserParticularPlanTopSearchList:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      NormalUserTopSearchPlanModel.find({plan_type:"Car"},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  webGetUserParticularPlanTopSearchListAccess:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      NormalUserTopSearchPlanModel.find({plan_type:"Accessoires"},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },

	webGetUserAccessoriesParticularPlan:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      //var { day_number , price } = req.body;
      UseraccessoriesplanModel.find({user_type:req.body.user_type},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  webGetUserAccessoriesParticularPlanTopUrgent:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      TopUrgentAccessoriesPlanModel.find({user_type:req.body.user_type},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },

  webGetUserProPlan:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      var {plan_type} = req.body;
      ProUserPlanModel.find({plan_type:plan_type},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
  webGetUserProPlanTopSearch:async function(req,res,next)
  {
    //console.log(req.body);
    try{
      //var { day_number , price } = req.body;
      var {plan_type} = req.body;
      ProUserPlanTopSearchModel.find({plan_type:plan_type},(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"record",record:record};
          res.status(200)
                .send(return_response);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error};
      res.status(200).send(return_response);
    }
  },
};