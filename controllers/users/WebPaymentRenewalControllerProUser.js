const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const ProUserPlanTopSearchModel = require("../../models/ProUserPlanTopSearchModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();

const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
//const customConstant = require('../../helpers/customConstant');
 

 const paypal = require('paypal-rest-sdk');
module.exports = {

  webPaymentRenewalAdProfessional:async function(req,res,next)
	{
		try{
			var base_url = customConstant.base_url;
			var front_end_base_url = customConstant.front_end_base_url;
			//console.log("base_url"+base_url);
			//console.log("front_end_base_url"+front_end_base_url);
			//console.log(req.body);return false;
			var {proUserAd,proUserAdTopSearch,ad_id,paymentType,user_id,ad_type} = req.body;
      if(!proUserAd)
      {
        var return_response = {"error":true,success: false,errorMessage:"Vous devez choisir un abonnement"};
		    return res.status(200).send(return_response);
      }
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis"};
		    return res.status(200).send(return_response);
      }
      if(!paymentType)
      {
        var return_response = {"error":true,success: false,errorMessage:"Le type de paiement est requis"};
		    return res.status(200).send(return_response);
      }
			var is_free = 0;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;


			var basic_main_plan_id = null;
			var showing_number_day = 0;
			var basic_plan_price = 0;
			var maximum_upload = 0;
			var payment_status = 0;
			var plan_purchase_date_time = null;
			var updated_at  = new Date();
			var plan_end_date_time = null; // Now
			

			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var final_price = 0;
			var top_search_plan_end_date_time = null; // Now
			var top_search_every_day = 0;

			if(proUserAd == '' || proUserAd == null || proUserAd == undefined)
      {
        var return_response = {"error":true,errorMessage:"Vous n'avez pas de plan veuillez sélectionner un plan" };
        return  res.status(200).send(return_response);
        //return 
      }else{
        //now user with new plan 
        console.log("now user with new plan ");
        var result = await ProUserPlanModel.findOne({_id:proUserAd});
        if(result)
        {
          console.log("with out top search plan");
          basic_main_plan_id = result._id;
          maximum_upload = result.maximum_upload;
          showing_number_day = result.day_number;
          basic_plan_price = result.price;
          final_price = basic_plan_price;
          plan_purchase_date_time = new Date();
          plan_end_date_time = new Date(); // Now
          plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
          //console.log(plan_end_date_time);
          console.log("1960 final_price "+final_price);
        }else{
          var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:proUserAd});
          if(resultTopSearch)
          {
            console.log("top search plan");
            top_search_plan_id = resultTopSearch._id;
            top_search_days = resultTopSearch.day_number;
            maximum_upload_top_search = resultTopSearch.maximum_upload;
            top_search_price = resultTopSearch.top_price;
            top_search_plan_purchase_date_time = new Date();
            final_price = top_search_price;
            top_search_plan_end_date_time = new Date(); // Now
            top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
            //console.log(top_search_plan_end_date_time);
            top_search_every_day = 1;

            basic_main_plan_id = resultTopSearch._id;
            maximum_upload = resultTopSearch.maximum_upload;
            showing_number_day = resultTopSearch.day_number;
            basic_plan_price = resultTopSearch.top_price;
            //final_price = basic_plan_price;
            plan_purchase_date_time = new Date();
            plan_end_date_time = new Date(); // Now
            plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
            //console.log(plan_end_date_time);
            console.log("1986 final_price "+final_price);
          }else{
            var return_response = {"error":true,errorMessage:"ID de plan non valide" };
            return  res.status(200).send(return_response);
          }
        }
        console.log("1992 final_price "+final_price);
        
        console.log("2006 final_price "+final_price);
        if(paymentType == "")
        {
          paymentType = "Stripe";
        }
        UserPurchasedPlan.create({
          ad_type:ad_type,
          ad_id:ad_id,
          user_id:user_id,
          maximum_upload:maximum_upload,
          maximum_upload_top_search:maximum_upload_top_search,
          basic_plan_id_pro_user:basic_main_plan_id,
          top_search_id_pro_user:top_search_plan_id,
          paymentType:paymentType,
          price:final_price,
          payment_status:payment_status
        },function(err,resultCreate){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            //console.log(resultCreate);
            ///console.log(resultCreate._id);
            console.log("2060 final_price "+final_price);
            var lastInsertId = resultCreate._id;
            if(final_price == 0)
            {
              var success_msg = "Votre annonce a été téléchargée avec succès";
            }else{
              var success_msg = "Effectuez votre paiement, pour valider votre abonnement";
            }
              console.log("2068 final_price");
              console.log(final_price);
            // return false;
            var return_response = {"error":false,success: true,errorMessage:success_msg,"lastInsertId":lastInsertId,"price":final_price};
            return	res.status(200).send(return_response);

          }
        });
      }
      
		}catch(error){
			console.log("error "+error);
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200)
			.send(return_response);
		}
	},

  getProUsrCarAdPaymentRenewalStripe:async function(req,res,next)
  {
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    //var title = req.query.title;
    /*console.log(lastInsertId);
    console.log(price);
    console.log(title);*/
		var stripe_rec = await StripeModel.findOne({}, {});
		if(!stripe_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		//console.log(stripe_rec);return false;
		const stripe = require('stripe')(stripe_rec.stripe_secret);

    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price_data: {
            currency: 'eur',
            product_data: {
              name: "Porschists",
            },
            unit_amount: price * 100,
          },
          quantity: 1,
          //description: lastInsertId,
        },
      ],
      mode: 'payment',
      //description: 'One-time setup fee',
      success_url: base_url+'api/getProUsrCarAdStripeRenewalSuccess?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: base_url+'api/getProUsrCarAdStripeRenewalCancel',
    });
    //console.log(session);
    //console.log(session.id);
    //console.log(session.payment_intent);
    //res.redirect(303, session.url);
    UserPurchasedPlan.updateOne({ _id:lastInsertId },{
      transaction_id:session.id,
      payment_intent:session.payment_intent,
      updated_at:updated_at
    },function(err,result){
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
      }else{
        res.redirect(303, session.url);
      }
    });
  },
	getProUsrCarAdStripeRenewalSuccess:async function(req,res,next)
  {
    //console.log("success stripe");
    //console.log(req.query.session_id);
    //var session_ID = req
		try{
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;
			var updated_at = new Date();
			// const stripe = require('stripe')('key-here');
			var stripe_rec = await StripeModel.findOne({}, {});
			if(!stripe_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			//console.log(stripe_rec);return false;
			const stripe = require('stripe')(stripe_rec.stripe_secret);

			const session = await stripe.checkout.sessions.retrieve(
				req.query.session_id
			);
			//console.log(session);
			//console.log(session.payment_status);
			if(session)
			{
				if(session.payment_status == "paid")
				{
					//console.log("paid");
					//var return_response = {"error":false,success: true,errorMessage:"Success"};
					//res.status(200)
					//.send(return_response);

          
					UserPurchasedPlan.updateOne({ transaction_id:req.query.session_id },{
            payment_status:1,
            updated_at:updated_at
          },function(err2,result2){
            if(err2)
            {
              var return_response = { "error":true,success: false,errorMessage:err2};
                res.status(200)
                .send(return_response);
            }else{
              var redirect_url_web = base_url_web+"proPlanRenewalSuccess";
              res.redirect(303, redirect_url_web);
						}
					});
				}else{
					res.status(200)
						.send({
							error: true,
							success: false,
							errorMessage: "Non payé"
					});
				}
			}else{
				res.status(200)
				.send({
					error: true,
					success: false,
					errorMessage: "Quelque chose a mal tourné"
				});
			}
		}catch(error){
			res.status(200)
			.send({
				error: true,
				success: false,
				errorMessage: error
			});
		}
  },
  getProUsrCarAdStripeRenewalCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  getProUsrCarAdPaymentRenewalPaypal:async function(req,res,next)
  {
    //console.log(req.query);
    var updated_at = new Date();
    var base_url = customConstant.base_url; 
    var lastInsertId = req.query.id;
    var price = req.query.price;
    //price = 1;
    if(req.query.title)
    {
      var title = req.query.title;
    }else{
      var title = "Porschists";
    }
    var paypal_rec = await PaypalModel.findOne({}, {});
		if(!paypal_rec)
		{
			var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
			return res.status(200).send(return_response);
		}
		paypal.configure({
			'mode': paypal_rec.mode, //sandbox or live
			'client_id': paypal_rec.client_id,
			'client_secret': paypal_rec.client_secret
		});

    var return_url = base_url+"api/getPaypalProUsrCarAdRenewalSuccess";
    var cancel_url = base_url+"api/getPaypalProUsrCarAdRenewalCancel";
    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": return_url,
          "cancel_url": cancel_url
      },
      "transactions": [{
          "item_list": {
              "items": [{
                  "name": "Porschists",
                  "sku": lastInsertId,
                  "price": price,
                  "currency": "EUR",
                  "quantity": 1
              }]
          },
          "amount": {
              "currency": "EUR",
              "total": price
          },
          "description": lastInsertId
      }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
          throw error;
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
              res.redirect(payment.links[i].href);
            }
          }
      }
    });
  },
  getPaypalProUsrCarAdRenewalSuccess:async function(req,res,next)
  {
		try{
			var base_url_web = customConstant.front_end_base_url;
			var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var updated_at = new Date();
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;
			//console.log("success paypal");
			var updated_at = new Date();
			const payerId = req.query.PayerID;
			const paymentId = req.query.paymentId;
			const token = req.query.token;
			//console.log("payerId");
			//console.log(payerId);
			//console.log(token);
			var paypal_rec = await PaypalModel.findOne({}, {});
			if(!paypal_rec)
			{
				var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
				return res.status(200).send(return_response);
			}
			paypal.configure({
				'mode': paypal_rec.mode, //sandbox or live
				'client_id': paypal_rec.client_id,
				'client_secret': paypal_rec.client_secret
			});

			const execute_payment_json = {
				"payer_id": payerId
			};
			// Obtains the transaction details from paypal
			paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
					//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
				if (error)
				{
						console.log(error.response);
						throw error;
				}else{
						//console.log(JSON.stringify(payment));
						//console.log(JSON.stringify(payment.id));
						var payment_table_id = JSON.stringify(payment.transactions[0].description);
						payment_table_id = JSON.parse(payment_table_id);
						//console.log("payment_table_id");
						//console.log(payment_table_id);
            //return false;
						UserPurchasedPlan.updateOne({ _id:payment_table_id },{
              payment_status:1,
              updated_at:updated_at
            },function(err2,result2){
              if(err2)
              {
                var return_response = { "error":true,success: false,errorMessage:err2};
                  res.status(200)
                  .send(return_response);
              }else{
                var redirect_url_web = base_url_web+"proPlanRenewalSuccess";
                res.redirect(303, redirect_url_web);
							}
						});
						
						//res.send('Success');
				}
			});
		}catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			res.status(200).send(return_response);
		}
  },
	getPaypalProUsrCarAdRenewalCancel:async function(req,res,next)
	{
    //console.log("pay pal cancel");
    var return_response = {"error":true,success: false,errorMessage:"Annulation du paiement"};
		res.status(200).send(return_response);
  },
  proUserReactivate:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      //mongoose.set("debug",true);
      var {ad_id,ad_type,user_id} = req.body;
			var today_date  = new Date();
      var resultOldPlan = await UserPurchasedPlan.findOne({user_id:user_id,ad_type:ad_type,	basic_plan_id_pro_user: {$ne:null },payment_status:1,maximum_upload:{$gt : 0} }).sort({$natural:1});

      // console.log("resultOldPlan");
      // console.log(resultOldPlan);
      if(!resultOldPlan)
      {
        var return_response = {"error":true,success: false,errorMessage:"Vous n'avez pas d'abonnement ou d'expiration"};
		    return res.status(200).send(return_response);
      }
      if(resultOldPlan.maximum_upload <= 0)
      {
        var return_response = {"error":true,success: false,errorMessage:"Vous n'avez pas d'abonnement ou d'expiration"};
		    return res.status(200).send(return_response);
      }

      var basic_plan_status = 0;
			var top_search_payment_status = 0;
			var show_on_web = 0;
			var complition_status = 0; 
			var top_search = 0;


			var basic_main_plan_id = null;
			var showing_number_day = 0;
			var basic_plan_price = 0;
			var maximum_upload = 0;
			var payment_status = 0;
			var plan_purchase_date_time = null;
			var updated_at  = new Date();
			var plan_end_date_time = null; // Now
			

			var top_search_plan_id = null;
			var top_search_days = 0;
			var maximum_upload_top_search = 0;
			var top_search_price = 0;
			var top_search_plan_purchase_date_time = null;
			var final_price = 0;
			var top_search_plan_end_date_time = null; // Now
			var top_search_every_day = 0;

      var maximum_upload = resultOldPlan.maximum_upload;
      if(maximum_upload > 0)
      {
        var new_maximum_upload = maximum_upload -1;
        show_on_web = 1;
        if(maximum_upload > 0)
        {
          var basic_plan_status = 1;
        }else{
          var basic_plan_status = 0;
        }
        if(resultOldPlan.maximum_upload_top_search > 0)
        {
          var new_maximum_upload_top_search = resultOldPlan.maximum_upload_top_search -1;
          var top_search_payment_status = 1;
        }else{
          var top_search_payment_status = 0;
          if(resultOldPlan.top_search_id_pro_user && resultOldPlan.top_search_id_pro_user != "")
          {
            var new_maximum_upload_top_search = 1;
          }else{
            var new_maximum_upload_top_search = 0;
          }
        }
        var result = await ProUserPlanModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
        if(result)
        {
          console.log("with out top search plan 1852 line no.");
          basic_main_plan_id = resultOldPlan._id;
          maximum_upload = 1;
          showing_number_day = result.day_number;
          basic_plan_price = resultOldPlan.price;
          final_price = basic_plan_price;
          plan_purchase_date_time = new Date(resultOldPlan.updated_at);
          plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
          plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); // Set now + 30 days as the new date
          //console.log(plan_end_date_time);
        }else{
          var resultTopSearch = await ProUserPlanTopSearchModel.findOne({_id:resultOldPlan.basic_plan_id_pro_user});
          if(resultTopSearch)
          {
            console.log("top search plan");
            top_search_plan_id = resultOldPlan._id;
            top_search_days = resultTopSearch.day_number;
            maximum_upload_top_search = 1;
            top_search_price = resultOldPlan.price;
            top_search_plan_purchase_date_time = new Date(resultOldPlan.updated_at);
            //final_price = top_search_price;
            top_search_plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
            top_search_plan_end_date_time.setDate(top_search_plan_end_date_time.getDate() + top_search_days); // Set now + 30 days as the new date
            //console.log(top_search_plan_end_date_time);
            top_search_every_day = 1;
            top_search = 1;

            basic_main_plan_id = resultOldPlan._id;
            maximum_upload = 1;
            showing_number_day = resultTopSearch.day_number;
            basic_plan_price = resultOldPlan.price;
            //final_price = basic_plan_price;
            plan_purchase_date_time = new Date(resultOldPlan.updated_at);
            plan_end_date_time = new Date(resultOldPlan.updated_at); // Now
            plan_end_date_time.setDate(plan_end_date_time.getDate() + showing_number_day); 

          }
        }
        //return false;
        //console.log("ad_id "+resultOldPlan.ad_id[0]);
        AddAdvertisementModel.updateOne({ _id:ad_id },{
          complition_status:1,
          show_on_web:1,
          basic_plan_price:basic_plan_price,
          showing_number_day:showing_number_day,
          plan_purchase_date_time:plan_purchase_date_time,
          plan_end_date_time:plan_end_date_time,
          top_search_days:top_search_days,
          top_search_price:top_search_price,
          top_search_plan_purchase_date_time:top_search_plan_purchase_date_time,
          top_search_plan_end_date_time:top_search_plan_end_date_time,
          top_search_every_day:top_search_every_day,
          updated_at:updated_at,
          basic_plan_status:basic_plan_status,
          complition_status:complition_status,
          top_search:top_search,
          top_search_payment_status:top_search_payment_status,
        },function(errrr,resulttt){
          if(errrr)
          {
            var return_response = {"error":true,success: false,errorMessage:errrr.message};
            return res.status(200).send(return_response);
          }else{
            UserPurchasedPlan.updateOne({ _id:resultOldPlan._id },{
              maximum_upload:new_maximum_upload,
              maximum_upload_top_search:new_maximum_upload_top_search,
              updated_at:updated_at
            },function(err,rr){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err.message};
                return	res.status(200).send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Cette publicité sera affichée sur le site web avec succès.","lastInsertId":0,"price":0};
                return	res.status(200).send(return_response);
              }
            });
          }
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Vous n'avez pas d'abonnement ou d'expiration"};
		    return res.status(200).send(return_response);
      }
      

    }catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			return res.status(200).send(return_response);
		}
  },
  getActiveCarAccessPlan:async(req,res)=>{
    try{
      // console.log("req.body");
      // console.log(req.body);
     // mongoose.set("debug",true);
      var {user_id} = req.body;
      var resultCarOldPlan = await UserPurchasedPlan.findOne({user_id:user_id,ad_type:"car",	basic_plan_id_pro_user: {$ne:null },payment_status:1,maximum_upload:{$gt : 0} }).sort({$natural:1});
      var resultAccessOldPlan = await UserPurchasedPlan.findOne({user_id:user_id,ad_type:"accessories",	basic_plan_id_pro_user: {$ne:null },payment_status:1,maximum_upload:{$gt : 0} }).sort({$natural:1});

      var resultAllOldPlanCount = await UserPurchasedPlan.count({user_id:user_id ,	basic_plan_id_pro_user: {$ne:null },payment_status:1  });
      
      var return_response = {
          "error":false,
          success: true,
          errorMessage:"Success",
          resultCarOldPlan:resultCarOldPlan,
          resultAccessOldPlan:resultAccessOldPlan,
          resultAllOldPlanCount:resultAllOldPlanCount
        };
		    return res.status(200).send(return_response);

    }catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			return res.status(200).send(return_response);
		}
  },
  getAllOldPlanSubscription:async(req,res)=>{
    try{
      // console.log("req.body");
      // console.log(req.body);
     // mongoose.set("debug",true);
      var {user_id,pageNo} = req.body;
       
      var limitt = 10;
      pageNo = parseInt(pageNo);
      limitt = parseInt(limitt);
      pageNo = pageNo ? pageNo * limitt : 0;

      var resultAllOldPlanCount = await UserPurchasedPlan.find({user_id:user_id ,	basic_plan_id_pro_user: {$ne:null },payment_status:1  }).skip(pageNo).limit(limitt);
      
      var return_response = {
          "error":false,
          success: true,
          errorMessage:"Success", 
          resultAllOldPlan:resultAllOldPlanCount
        };
		    return res.status(200).send(return_response);

    }catch(error){
			var return_response = {"error":true,success: false,errorMessage:error};
			return res.status(200).send(return_response);
		}
  },
};