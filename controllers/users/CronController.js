const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const UserfavadsModel = require("../../models/UserfavadsModel");
var cron = require('node-cron');
const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
//const customConstant = require('../../helpers/customConstant');
const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.porschists.com',
    port: 465,
    auth: {
       user: 'contact@porschists.com',
       pass: 'OD-oeu&mMbU%'
    }
});
/*/*/
cron.schedule('* * * * *', async() => {
  console.log('running a task every minute');
  //console.log( new Date()  ); 

  try{
    //console.log("hereee");
    let today_date = new Date();
    // var addRecord = await AddAdvertisementModel.find({
    //   category: 'Porsche Voitures',
    //   complition_status: 1,
    //   show_on_web: 1,
    //   plan_end_date_time:{ $lte:today_date }
    // });
    // //console.log(addRecord);
    // return res.send({
    //   error:false,
    //   message:"Success",
    //   record:addRecord
    // });
    // {
    //   category: 'Pièces et accessoires',
    // },
    // {
    //   user_type: 'pro_user'
    // },
    await AddAdvertisementModel.updateMany({
      category: 'Pièces et accessoires',
      user_type: 'pro_user',
      complition_status: 1,
      show_on_web: 1,
      plan_end_date_time:{ $lte:today_date }
    },{show_on_web:0});

    await AddAdvertisementModel.updateMany({
      category: 'Porsche Voitures',
      complition_status: 1,
      show_on_web: 1,
      plan_end_date_time:{ $lte:today_date }
    },{show_on_web:0});
     
    // var addRecord = await AddAdvertisementModel.find({
    //   complition_status: 1,
    //   show_on_web: 1,
    //   top_search_plan_end_date_time:{ $lte:today_date }
    // });

    await AddAdvertisementModel.updateMany({
      complition_status: 1,
      show_on_web: 1,
      top_search:1,
      top_search_payment_status:1,
      top_search_plan_end_date_time:{ $lte:today_date }
    },{top_search:0});

    // var addRecord = await AddAdvertisementModel.find({
    //   complition_status: 1,
    //   show_on_web: 1,
    //   top_search_plan_purchase_date_time:{ $gte:today_date }
    // });
    // //console.log(addRecord);
    // return res.send({
    //   error:false,
    //   message:"Success",
    //   record:addRecord
    // });
    await AddAdvertisementModel.updateMany({
      complition_status: 1,
      show_on_web: 1,
      top_search:1,
      top_search_payment_status:1,
      top_search_plan_purchase_date_time:{ $gte:today_date }
    },{top_search:0});

    // var addRecord = await AddAdvertisementModel.find({
    //   complition_status: 1,
    //   show_on_web: 1,
    //   top_search:0,
    //   top_search_payment_status:1,
    //   top_search_plan_purchase_date_time:{ $lte:today_date },
    //   top_search_plan_end_date_time:{ $gte:today_date }
    // });
    // //console.log(addRecord);
    // return res.send({
    //   error:false,
    //   message:"Success",
    //   record:addRecord
    // });
    await AddAdvertisementModel.updateMany({
      complition_status: 1,
      show_on_web: 1,
      top_search:0,
      top_search_payment_status:1,
      top_search_plan_purchase_date_time:{ $lte:today_date },
      top_search_plan_end_date_time:{ $gte:today_date }
    },{top_search:1});

    // var addRecord = await AddAdvertisementModel.find({
    //   complition_status: 1,
    //   show_on_web: 1,
    //   top_urgent:1,
    //   top_urgent_plan_end_date_time:{ $lte:today_date }
    // });
    // return res.send({
    //   error:false,
    //   message:"Success",
    //   record:addRecord
    // });
    
    await AddAdvertisementModel.updateMany({
      complition_status: 1,
      show_on_web: 1,
      top_urgent:1,
      top_urgent_plan_end_date_time:{ $lte:today_date }
    },{top_urgent:0});
    //console.log("success");


    // Bonjour {user}

      // Votre abonnement expire le {date}, vous pouvez le renouveler en payant les frais correspondants pour continuer à bénéficier des services de Porschists. 

      // Bien Cordialement,
      // L'équipe de Porschists
      //mongoose.set("debug",true);
      //let today_date = new Date();
      // var record = await AddAdvertisementModel.find({
      //   category: 'Porsche Voitures',
      //   complition_status: 1,
      //   show_on_web: 1,
      //   email_reminder:0,
      //   delete_at:0,
      //   plan_end_date_time:{ $gte:today_date }
      // },{ad_name:1,plan_end_date_time:1});

      
      var record = await AddAdvertisementModel.aggregate([
        {
          $match:{
            $and:[
              {
                category: 'Porsche Voitures',
              },
              {
                email_reminder:0,
              },
              {
                show_on_web: 1,
              },
              {
                complition_status: 1,
              },
              {
                delete_at:0,
              },
              {
                plan_end_date_time:{ $gte:today_date }
              }
            ]
          }
        },
        {
          $addFields:{
            "user_id":"$userId"
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"user_id":"$user_id"},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$user_id"]
                  }
                }
              }
            ],
            as:"usr"
          }
        },
        {
          $skip:0
        },
        {
          $limit:3
        },
        {
          $project:{
            "ad_name":1,
            "user_id":1,
            "plan_end_date_time":1,
            "usr.email":1,
            "usr.firstName":1,
            "usr.lastName":1
          }
        }
        
      ]);
      //console.log(record);
      record.map((obj)=>{
        // console.log("obj");
        // console.log(obj);
        // console.log(obj._id);
        //console.log(obj);
        const today = new Date(obj.plan_end_date_time);
        const yyyy = today.getFullYear();
        let mm = today.getMonth() + 1; // Months start at 0!
        let dd = today.getDate();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;

        const formattedToday = dd + '/' + mm + '/' + yyyy;


        const date1 = obj.plan_end_date_time;
        const date2 = new Date( );
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));  
        // console.log(diffDays + " days");
        // console.log(diffDays );
        if(diffDays == 3)
        {
          //console.log(obj.usr);
          // console.log(obj._id);
          // console.log(obj.usr.length);
          if(obj.usr.length > 0)
          {
            //console.log(obj.usr[0].email);
            if(obj.usr[0].email)
            {
              let email = obj.usr[0].email;
              let firstName = obj.usr[0].firstName;
              let lastName = obj.usr[0].lastName;
              var html = "";
              html += "<div><p>Bonjour "+firstName+" "+lastName+" </p><p>Votre abonnement expire le "+formattedToday+", vous pouvez le renouveler en payant les frais correspondants pour continuer à bénéficier des services de Porschists.</p></div>";
              html += "<div><p>Bien Cordialement,</p><p>L'équipe de Porschists</p></div>";
              const message = {
                from: 'contact@porschists.com', // Sender address
                to: email,         // recipients
                subject: "Rappel de renouvellement d'annonces", // Subject line
                html: html // Plain text body
              };
              transport.sendMail(message, function(err, info) {
                  if (err) {
                    console.log(err);
                    error_have = 1;
                  } else {
                    AddAdvertisementModel.updateOne({_id:obj._id},{email_reminder:1}).then((result)=>{

                    }).catch((error)=>{

                    });
                    //console.log('mail has sent.');
                    //console.log(info);
                  }
              });
            }
          }
        }
      });


      var record_2 = await AddAdvertisementModel.aggregate([
        {
          $match:{
            $and:[
              {
                category: 'Pièces et accessoires',
              },
              {
                user_type: 'pro_user'
              },
              {
                email_reminder:0,
              },
              {
                show_on_web: 1,
              },
              {
                complition_status: 1,
              },
              {
                delete_at:0,
              },
              {
                plan_end_date_time:{ $gte:today_date }
              }
            ]
          }
        },
        {
          $addFields:{
            "user_id":"$userId"
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"user_id":"$user_id"},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$user_id"]
                  }
                }
              }
            ],
            as:"usr"
          }
        },
        {
          $skip:0
        },
        {
          $limit:3
        },
        {
          $project:{
            "ad_name":1,
            "user_id":1,
            "plan_end_date_time":1,
            "usr.email":1,
            "usr.firstName":1,
            "usr.lastName":1
          }
        }
        
      ]);
      //console.log(record_2);
      record_2.map((obj)=>{
        // console.log("obj");
        // console.log(obj);
        // console.log(obj._id);
        //console.log(obj);
        const today_2 = new Date(obj.plan_end_date_time);
        const yyyy_2 = today_2.getFullYear();
        let mm_2 = today_2.getMonth() + 1; // Months start at 0!
        let dd_2 = today_2.getDate();

        if (dd_2 < 10) dd_2 = '0' + dd_2;
        if (mm_2 < 10) mm_2 = '0' + mm_2;

        const formattedToday_2 = dd_2 + '/' + mm_2 + '/' + yyyy_2;


        const date1 = obj.plan_end_date_time;
        const date2 = new Date( );
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));  
        // console.log(diffDays + " days");
        // console.log(diffDays );
        if(diffDays == 3)
        {
          //console.log(obj.usr);
          // console.log(obj._id);
          // console.log(obj.usr.length);
          if(obj.usr.length > 0)
          {
            //console.log(obj.usr[0].email);
            if(obj.usr[0].email)
            {
              let email = obj.usr[0].email;
              let firstName = obj.usr[0].firstName;
              let lastName = obj.usr[0].lastName;
              var html = "";
              html += "<div><p>Bonjour "+firstName+" "+lastName+" </p><p>Votre abonnement expire le "+formattedToday_2+", vous pouvez le renouveler en payant les frais correspondants pour continuer à bénéficier des services de Porschists.</p></div>";
              html += "<div><p>Bien Cordialement,</p><p>L'équipe de Porschists</p></div>";
              const message = {
                from: 'contact@porschists.com', // Sender address
                to: email,         // recipients
                subject: "Rappel de renouvellement d'annonces", // Subject line
                html: html // Plain text body
              };
              transport.sendMail(message, function(err, info) {
                  if (err) {
                    console.log(err);
                    error_have = 1;
                  } else {
                    AddAdvertisementModel.updateOne({_id:obj._id},{email_reminder:1}).then((result)=>{

                    }).catch((error)=>{

                    });
                    //console.log('mail has sent.');
                    //console.log(info);
                  }
              });
            }
          }
        }
      });

      
  }catch(error)
  {
    console.log(error);
    return res.send({
      error:true,
      message:error.message
    });
  }

});


module.exports = {



  // checkAdExpiry:async (req,res)=>{
  //   try{
  //     //console.log("hereee");
  //     let today_date = new Date();
  //     // var addRecord = await AddAdvertisementModel.find({
  //     //   category: 'Porsche Voitures',
  //     //   complition_status: 1,
  //     //   show_on_web: 1,
  //     //   plan_end_date_time:{ $lte:today_date }
  //     // });
  //     // //console.log(addRecord);
  //     // return res.send({
  //     //   error:false,
  //     //   message:"Success",
  //     //   record:addRecord
  //     // });
  //     await AddAdvertisementModel.updateMany({
  //       category: 'Porsche Voitures',
  //       complition_status: 1,
  //       show_on_web: 1,
  //       plan_end_date_time:{ $lte:today_date }
  //     },{show_on_web:0});
       
  //     // var addRecord = await AddAdvertisementModel.find({
  //     //   complition_status: 1,
  //     //   show_on_web: 1,
  //     //   top_search_plan_end_date_time:{ $lte:today_date }
  //     // });

  //     await AddAdvertisementModel.updateMany({
  //       complition_status: 1,
  //       show_on_web: 1,
  //       top_search:1,
  //       top_search_payment_status:1,
  //       top_search_plan_end_date_time:{ $lte:today_date }
  //     },{top_search:0});

  //     // var addRecord = await AddAdvertisementModel.find({
  //     //   complition_status: 1,
  //     //   show_on_web: 1,
  //     //   top_search_plan_purchase_date_time:{ $gte:today_date }
  //     // });
  //     // //console.log(addRecord);
  //     // return res.send({
  //     //   error:false,
  //     //   message:"Success",
  //     //   record:addRecord
  //     // });
  //     await AddAdvertisementModel.updateMany({
  //       complition_status: 1,
  //       show_on_web: 1,
  //       top_search:1,
  //       top_search_payment_status:1,
  //       top_search_plan_purchase_date_time:{ $gte:today_date }
  //     },{top_search:0});

  //     // var addRecord = await AddAdvertisementModel.find({
  //     //   complition_status: 1,
  //     //   show_on_web: 1,
  //     //   top_search:0,
  //     //   top_search_payment_status:1,
  //     //   top_search_plan_purchase_date_time:{ $lte:today_date },
  //     //   top_search_plan_end_date_time:{ $gte:today_date }
  //     // });
  //     // //console.log(addRecord);
  //     // return res.send({
  //     //   error:false,
  //     //   message:"Success",
  //     //   record:addRecord
  //     // });
  //     await AddAdvertisementModel.updateMany({
  //       complition_status: 1,
  //       show_on_web: 1,
  //       top_search:0,
  //       top_search_payment_status:1,
  //       top_search_plan_purchase_date_time:{ $lte:today_date },
  //       top_search_plan_end_date_time:{ $gte:today_date }
  //     },{top_search:1});

  //     // var addRecord = await AddAdvertisementModel.find({
  //     //   complition_status: 1,
  //     //   show_on_web: 1,
  //     //   top_urgent:1,
  //     //   top_urgent_plan_end_date_time:{ $lte:today_date }
  //     // });
  //     // return res.send({
  //     //   error:false,
  //     //   message:"Success",
  //     //   record:addRecord
  //     // });
      
  //     await AddAdvertisementModel.updateMany({
  //       complition_status: 1,
  //       show_on_web: 1,
  //       top_urgent:1,
  //       top_urgent_plan_end_date_time:{ $lte:today_date }
  //     },{top_urgent:0});
  //     console.log("success");
  //   }catch(error)
  //   {
  //     console.log(error);
  //     return res.send({
  //       error:true,
  //       message:error.message
  //     });
  //   }
  // },









};