const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const UsersModel = require("../../models/UsersModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
//const watermark = require('image-watermark');
const SettingModel = require("../../models/SettingModel");
const NormalUserTopSearchPlanModel = require("../../models/NormalUserTopSearchPlanModel");
const OrderModel = require("../../models/OrderModel");
const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const app = express();
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
const PaymentRequest = require('../../models/PaymentRequest');
//const customConstant = require('../../helpers/customConstant');

const paypal = require('paypal-rest-sdk');
// const paypal = require('paypal-rest-sdk');

module.exports = {

  makePaymentRequest:async(req,res)=>{
    try{
      let {user_id,bank_id,amount} = req.body;
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis" };
        return res.status(200).send(return_response);
      }
      if(!amount)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de la banque est requis" };
        return res.status(200).send(return_response);
      }
      if(!bank_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"Montant est requis" };
        return res.status(200).send(return_response);
      }

      let user_rec = await UsersModel.findOne({_id:user_id},{seller_pending_amount:1,bank:1});
      if(!user_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Identité d'utilisateur invalide" };
        return res.status(200).send(return_response);
      }
      let seller_pending_amount = user_rec.seller_pending_amount;
      if(amount > seller_pending_amount)
      {
        var return_response = {"error":true,success: false,errorMessage:"Vous ne pouvez pas demander un paiement supérieur à vos revenus" };
        return res.status(200).send(return_response);
      }
      let payment_rec = await PaymentRequest.findOne({user_id:user_id,status:1});
      if(payment_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Veuillez attendre la finalisation de votre demande de paiement précédente" };
        return res.status(200).send(return_response);
      }
      let bank = user_rec.bank;
      amount = parseFloat(amount);
      await PaymentRequest.create(
        {
          user_id:user_id,
          bank:bank,
          amount:amount
        }
      ).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Votre demande a été soumise pour paiement, veuillez attendre l'approbation de l'administrateur"};
        return  res.status(200).send(return_response);
      }).catch((e)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        return  res.status(200).send(return_response);
      })
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  getAllPaymentRequestCount:async(req,res)=>{
    try{
      let {user_id} = req.params;
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis" };
        return res.status(200).send(return_response);
      }
      await PaymentRequest.aggregate([
        {
          $match:{
            user_id:user_id
          }
        },
        {
          $group:{
            _id:null,
            count:{$sum:1}
          }
        }
      ]).then((result)=>{
        if(result.length > 0)
        {
          var return_response = {"error":false,success: true,errorMessage:"Success",record:result[0].count};
        return  res.status(200).send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Success",record:0};
          return  res.status(200).send(return_response);
        }
        
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        return  res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  getAllPaymentRequest:async(req,res)=>{
    try{
      let {user_id,page_no} = req.params;
      page_no = page_no ? page_no : 0;
      let limit = 10;
      let skip = page_no*limit;
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis" };
        return res.status(200).send(return_response);
      }
      await PaymentRequest.aggregate([
        {
          $match:{
            user_id:user_id
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"userObjId":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$userObjId"]
                  }
                }
              },
              {
                $project:{
                  "firstName": 1,
                  "lastName": 1,
                  "email": 1,
                  "mobileNumber": 1,
                  "user_type": 1,
                  "enterprise": 1,
                  "telephone": 1,
                  "companyImage": 1,
                  "whatsapp": 1
                }
              }
            ],
            as:"user_record"
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        },
        {
          $sort:{
            "created_at":-1
          }
        }
      ]).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:result};
        return  res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message};
        return  res.status(200).send(return_response);
      });
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  getAllBanks:async(req,res)=>{
    try{
      let {user_id} = req.params;
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis" };
        return res.status(200).send(return_response);
      }
      let bank_rec = await UsersModel.findOne({_id:user_id},{bank:1});
      if(!bank_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Identité d'utilisateur invalide" };
        return res.status(200).send(return_response);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"Success",record:bank_rec.bank };
        return res.status(200).send(return_response);
      }
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  rejectPaymentRequest:async(req,res)=>{
    try{
      let id = req.params.id;
      let payment_rec = await PaymentRequest.findOne({_id:id},{status:1,user_id:1,amount:1});
      if(!payment_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Identifiant de paiement invalide" };
        return res.status(200).send(return_response);
      }else{
        if(payment_rec.status == 1)
        {
          await PaymentRequest.updateOne({
            _id:id
          },{status:2}).then((result)=>{
            var return_response = {"error":false,success: true,errorMessage:"Demande de paiement rejetée succès complet"};
            return  res.status(200).send(return_response);
          }).catch((e)=>{
            var return_response = {"error":true,success: false,errorMessage:e.message};
            return  res.status(200).send(return_response);
          })
        }else if(payment_rec.status == 2)
        {
          var return_response = {"error":true,success: false,errorMessage:"Demande de paiement annulée" };
        return res.status(200).send(return_response);
        }else if(payment_rec.status == 3)
        {
          var return_response = {"error":true,success: false,errorMessage:"Transfert de paiement effectué pour cette demande" };
          return res.status(200).send(return_response);
        }
      }
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  acceptPaymentRequest:async(req,res)=>{
    try{
      let id = req.params.id;
      let payment_rec = await PaymentRequest.findOne({_id:id},{status:1,user_id:1,amount:1});
      if(!payment_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Identifiant de paiement invalide" };
        return res.status(200).send(return_response);
      }else{
        if(payment_rec.status == 1)
        {
          let user_rec = await UsersModel.findOne({_id:payment_rec.user_id},{seller_paid_amount:1,seller_pending_amount:1});
          if(user_rec.seller_pending_amount >= payment_rec.amount)
          {
            let pending_amount = parseFloat(user_rec.seller_pending_amount - payment_rec.amount);
            let paid_amount = parseFloat(user_rec.seller_paid_amount + payment_rec.amount);
            await PaymentRequest.updateOne({
              _id:id
            },{status:3}).then(async (result)=>{
              await UsersModel.updateOne({_id:payment_rec.user_id},{seller_paid_amount:paid_amount,seller_pending_amount:pending_amount}).then((result2)=>{
                var return_response = {"error":false,success: true,errorMessage:"Le montant transféré a été entièrement transféré"};
                return  res.status(200).send(return_response);
              }).catch((e)=>{
                var return_response = {"error":true,success: false,errorMessage:e.message};
                return  res.status(200).send(return_response);
              })
            }).catch((e)=>{
              var return_response = {"error":true,success: false,errorMessage:e.message};
              return  res.status(200).send(return_response);
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Montant du paiement invalide" };
            return res.status(200).send(return_response);
          }
        }else if(payment_rec.status == 2)
        {
          var return_response = {"error":true,success: false,errorMessage:"Demande de paiement annulée" };
        return res.status(200).send(return_response);
        }else if(payment_rec.status == 3)
        {
          var return_response = {"error":true,success: false,errorMessage:"Transfert de paiement effectué pour cette demande" };
          return res.status(200).send(return_response);
        }
      }
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message};
      return  res.status(200).send(return_response);
    }
  },
  getAllSellerEarning:async(req,res)=>{
    try{
      let {user_id} = req.params;
      let record = [];
      let all_earned_amount = 0;
      let all_paid_amount = 0;
      let all_pending_amount = 0;
      if(!user_id)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'identifiant de l'utilisateur est requis" };
        return res.status(200).send(return_response);
      }
      let user_record = await UsersModel.findOne({_id:user_id},{seller_paid_amount:1,seller_pending_amount:1});

      if(!user_record)
      {
        var return_response = {"error":true,success: false,errorMessage:"Identité d'utilisateur invalide" };
        return res.status(200).send(return_response);
      }

      let result = await OrderModel.aggregate([
        {
          $match:{
            ad_owner_user_id:mongoose.Types.ObjectId(user_id)
          }
        },
        {
          $match:{
            $and:[
              {payment_status: 1},
              {shipping_status: 3}
            ]
          }
        },
        {
          $group:{
            _id:null,
            count:{$sum:"$seller_profit"}
          }
        }
      ]);

      if(result.length > 0)
      {
        all_earned_amount = result[0].count;
      }
      let obj = {
        all_earned_amount:all_earned_amount,
        all_paid_amount:user_record.seller_paid_amount,
        all_pending_amount:user_record.seller_pending_amount
      };
      //record.push(obj);
      var return_response = {"error":false,success: true,errorMessage:"Success",record:obj};
      return  res.status(200).send(return_response);
    }catch(e){
      var return_response = {"error":true,success: false,errorMessage:e.message,record:{}};
      return  res.status(200).send(return_response);
    }
  },

};