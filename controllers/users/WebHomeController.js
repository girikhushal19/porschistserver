const express = require('express');
const UsersModel = require("../../models/UsersModel");
const BannerModel = require("../../models/BannerModel");
const CategoryModel = require("../../models/CategoryModel");
const ColorModel = require("../../models/ColorModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const FollowSellerModel = require("../../models/FollowSellerModel");
const mongoose = require("mongoose");

const RatingModel = require("../../models/RatingModel");
const ContactModel = require("../../models/ContactModel");


//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express(); 
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {

  webGetModel: async function(req,res,next)
  {
  	try{
  		const { attribute_type } = req.body;
  		//console.log("attribute_type_val"+attribute_type);
  		var imagePath = customConstant.modelsImagePath;
  		/*console.log("base_url"+JSON.stringify(customConstant));
  		console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);*/
	  	ModelsModel.find({attribute_type:attribute_type,status:1}, { }).exec((err, result)=>
	    {
	    	//console.log("err "+err);
	    	//console.log("result "+result);
	    	if(err)
	    	{
	    		console.log(err);
	    		res.status(200)
		        .send({
		            error: true,
		            success: false,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:err,
		        });

	    	}else{
	    		res.status(200)
		        .send({
		            error: false,
		            success: true,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:result,
		        });
	    	}
	    });
  	}catch(err){
        console.log(err);
    }
  },
  webGetSubModel: async function(req,res,next)
  {
  	
    try{
      //console.log(req.body);return false;
  		const { attribute_type } = req.body;
  		//mongoose.set("debug",true);
  		//console.log("attribute_type_val"+attribute_type);
  		var imagePath = customConstant.modelsImagePath;
  		/*console.log("base_url"+JSON.stringify(customConstant));
  		console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);
      Porsche Voitures
      */
      
	  	ModelsModel.find(
        { attribute_type:"Pièces et accessoires" , status:1}, { }).lean().exec((err, result)=>
	    {
	    	//console.log("err "+err);
	    	//console.log("result "+result);
	    	if(err)
	    	{
	    		//console.log(err);
	    		res.status(200)
		        .send({
		            error: true,
		            success: false,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:err,
		        });

	    	}else{
          var x = 0;
          if(result.length > 0)
          {
              // console.log("ifffff");
              // console.log(result);
              // console.log(result.length);
            var xx = result.length - 1;
            result.forEach(object=>{
              //console.log("result "+object);
              ModelsModel.find({ parent_id:object._id}, {model_name:1 }).lean().exec((errSubModel, resultSubModel)=>
              {
                if(errSubModel)
                {
                  console.log(errSubModel);
                }else{
                  //console.log(resultSubModel);
                  object.subModel = resultSubModel;
                  if(x == xx)
                  {
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Models Record",
                        imagePath: imagePath,
                        record:result,
                    });
                  }
                  x++;
                }
              });
            });
          }else{
              console.log("elseeee");
            res.status(200)
		        .send({
		            error: false,
		            success: true,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:result,
		        });
          }
	    	}
	    });
  	}catch(err){
        console.log(err);
    }
  },
  webGetSubModel2: async function(req,res,next)
  {
  	
    try{
      //console.log(req.body);return false;
  		const { attribute_type } = req.body;
  		//mongoose.set("debug",true);
  		//console.log("attribute_type_val"+attribute_type);
  		var imagePath = customConstant.modelsImagePath;
  		/*console.log("base_url"+JSON.stringify(customConstant));
  		console.log("base_url"+customConstant.base_url);
      console.log("base_url"+customConstant.modelsImagePath);
      Porsche Voitures
      */
      
	  	ModelsModel.find(
        {attribute_type:attribute_type,parent_id: null,status:1}, { }).lean().exec((err, result)=>
	    {
	    	//console.log("err "+err);
	    	//console.log("result "+result);
	    	if(err)
	    	{
	    		//console.log(err);
	    		res.status(200)
		        .send({
		            error: true,
		            success: false,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:err,
		        });

	    	}else{
          var x = 0;
          if(result.length > 0)
          {
              // console.log("ifffff");
              // console.log(result);
              // console.log(result.length);
            var xx = result.length - 1;
            result.forEach(object=>{
              //console.log("result "+object);
              ModelsModel.find({ parent_id:object._id}, {model_name:1 }).lean().exec((errSubModel, resultSubModel)=>
              {
                if(errSubModel)
                {
                  console.log(errSubModel);
                }else{
                  //console.log(resultSubModel);
                  object.subModel = resultSubModel;
                  if(x == xx)
                  {
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Models Record",
                        imagePath: imagePath,
                        record:result,
                    });
                  }
                  x++;
                }
              });
            });
          }else{
              console.log("elseeee");
            res.status(200)
		        .send({
		            error: false,
		            success: true,
		            errorMessage: "Models Record",
                imagePath: imagePath,
		            record:result,
		        });
          }
	    	}
	    });
  	}catch(err){
        console.log(err);
    }
  },

  webGetUserOwnAd: async function(req,res,next)
  {
    try{
      const { user_id,search_var,category,sort_var,pageNo } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      var skipp = 0;
      limitt = 10;
      if(pageNo > 0)
      {
        skipp = pageNo * limitt;
      }
      var so = {"created_at":-1} 
      if(sort_var == "date_asc")
      {
        so = {"created_at":1} 
      }else if(sort_var == "price_asc")
      {
        so = {"price":1} 
      }else if(sort_var == "price_desc")
      {
        so = {"price":-1} 
      }else{
        so = {"created_at":-1} 
      }
      var String_qr = {};
      String_qr["userId"] = user_id;
      String_qr["complition_status"] = 1;
      String_qr["delete_at"] = 0;
      String_qr["show_on_web"] = 1;
      if(category)
      {
        String_qr["category"] = category;
      }
      if(search_var)
      {
        String_qr["ad_name"] = {'$regex' : '^'+search_var, '$options' : 'i'}
      }
     
      var imagePath = customConstant.base_url;
      //"complition_status":1,"activation_status":1,
      //mongoose.set("debug",true);
      AddAdvertisementModel.find(String_qr, {"_id":1,"ad_name":1,"user_type":1,"model_variant":1,"registration_year":1,"price":1,"price_for_pors":1,"pro_price":1,"exterior_image":1,"category":1,"created_at":1,"accessoires_image":1,"activation_status":1,"top_urgent":1,"top_search":1,"show_on_web":1,"plan_purchase_date_time":1,"plan_end_date_time":1,"chat_count":1,"mobile_count":1,"view_count":1}).sort(so).skip(skipp).limit(limitt).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err,
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },

  webGetUserOwnAdExpired: async function(req,res,next)
  {
    try{
      const { user_id,search_var,category,sort_var,pageNo } = req.body;
      //console.log("attribute_type_val"+attribute_type);
      var skipp = 0;
      limitt = 10;
      if(pageNo > 0)
      {
        skipp = pageNo * limitt;
      }
      var so = {"created_at":-1} 
      if(sort_var == "date_asc")
      {
        so = {"created_at":1} 
      }else if(sort_var == "price_asc")
      {
        so = {"price":1} 
      }else if(sort_var == "price_desc")
      {
        so = {"price":-1} 
      }else{
        so = {"created_at":-1} 
      }
      var String_qr = {};
      String_qr["userId"] = user_id;
      String_qr["complition_status"] = 1;
      String_qr["delete_at"] = 0;
      String_qr["show_on_web"] = 0;
      if(category)
      {
        String_qr["category"] = category;
      }
      if(search_var)
      {
        String_qr["ad_name"] = {'$regex' : '^'+search_var, '$options' : 'i'}
      }
       

      var imagePath = customConstant.base_url;
      //"complition_status":1,"activation_status":1,
      //mongoose.set("debug",true);
      AddAdvertisementModel.find(String_qr, {"_id":1,"ad_name":1,"user_type":1,"model_variant":1,"registration_year":1,"price":1,"price_for_pors":1,"pro_price":1,"exterior_image":1,"category":1,"created_at":1,"accessoires_image":1,"activation_status":1,"top_urgent":1,"top_search":1,"show_on_web":1,"plan_purchase_date_time":1,"plan_end_date_time":1,"chat_count":1,"mobile_count":1,"view_count":1}).sort(so).skip(skipp).limit(limitt).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err,
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                imagePath: imagePath,
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  webGetUserOwnAdActiveAdCount: async function(req,res,next)
  {
    try{
      const { user_id} = req.body;
      //console.log("attribute_type_val"+attribute_type);
     
      var String_qr = {};
      String_qr["userId"] = user_id;
      String_qr["complition_status"] = 1;
      String_qr["show_on_web"] = 1;
      String_qr["delete_at"] = 0;
      
      //"complition_status":1,"activation_status":1,delete_at:1
      //mongoose.set("debug",true);
      AddAdvertisementModel.count(String_qr).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err,
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },
  webGetUserOwnAdInActiveAdCount: async function(req,res,next)
  {
    try{
      const { user_id} = req.body;
      //console.log("attribute_type_val"+attribute_type);
     
      var String_qr = {};
      String_qr["userId"] = user_id;
      String_qr["complition_status"] = 1;
      String_qr["show_on_web"] = 0;
      String_qr["delete_at"] = 0;
      
      //"complition_status":1,"activation_status":1,
      //mongoose.set("debug",true);
      AddAdvertisementModel.count(String_qr).exec((err, result)=>
      {
        //console.log("err "+err);
        //console.log("result "+result);
        if(err)
        {
          //console.log(err);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err,
                imagePath: imagePath,
                record:err,
            });

        }else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Enregistrement des annonces",
                record:result,
            });
        }
      });
    }catch(err){
        console.log(err);
    }
  },


  webUserDetailAllAdCount:async function(req,res,next)
  {
    try{
      var {user_id,pageNo} = req.body;
      let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;

      // _id:mongoose.Types.ObjectId('user_id')
      if(user_id == "" || user_id == null || user_id == undefined  || user_id == 'null' || user_id == 'undefined')
      {
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requise"
        });
      }

     

      // console.log("pageNo "+pageNo);
      // console.log("limitt "+limitt);
      var result  = await AddAdvertisementModel.count({userId:user_id,show_on_web:1,complition_status:1,delete_at:0,activation_status:1} );
      total = result;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
      res.status(200)
        .send({
        error: false,
        success: true,
        errorMessage: "Success",
        record:totalPageNumber
      });
       
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },

  webUserDetailAllAd:async function(req,res,next)
  {
    try{
      var {user_id,pageNo,loggedIn_user_id} = req.body;
      
      console.log("loggedIn_user_id  "+ loggedIn_user_id);
      
      var limitt = 10;
      pageNo = parseInt(pageNo);
      limitt = parseInt(limitt);
      pageNo = pageNo ? pageNo * limitt : 0;
      // _id:mongoose.Types.ObjectId('user_id')

      var allReadyLike = await FollowSellerModel.count({user_id:loggedIn_user_id,seller_id:user_id});
      
      let rating_rec = await RatingModel.aggregate(
        [
          {
            $match:{
              seller_id:user_id
            }
          },
          {
            $group:
              {
                "_id": "$id",
                
                avgRating: { $avg: "$rating" },
                totalRating: { $sum: 1 }
              }
          }
        ]
     );

      if(user_id == "" || user_id == null || user_id == undefined  || user_id == 'null' || user_id == 'undefined')
      {
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de l'utilisateur est requise"
        });
      }
      // console.log("pageNo "+pageNo);
      // console.log("limitt "+limitt);
      // var result  = await AddAdvertisementModel.find({},{userId:1});
      // result.forEach((val)=>{
      //   //console.log(val);
      //   console.log(val.userId);
      //   //AddAdvertisementModel.updateOne({_id:val._id},{userId:val.userId[0]});
      // })
      // String_qr['$match']['show_on_web'] = 1;
      // String_qr['$match']['complition_status'] = 1;
      // String_qr['$match']["delete_at"] = 0;
      // String_qr['$match']["activation_status"] = 1;
      //mongoose.set("debug",true);
      await UsersModel.aggregate([
        {
          $match:{
            _id:mongoose.Types.ObjectId(user_id)
          }
        },
        {
          $addFields:{
            "userrrrr_id":"$_id"
          }
        },
        {
          $lookup:{
            from:"advertisements",
            let:{"usr_ID":{"$toObjectId":"$userrrrr_id"} },
            pipeline : [
              {
                $match:{

                  $expr:{
                    $and:[
                      {$eq:["$userId","$$usr_ID"]},
                      {$eq:["$show_on_web",1]},
                      {$eq:["$complition_status",1]},
                      {$eq:["$delete_at",0]},
                      {$eq:["$activation_status",1]}
                    ]
                  }
                }
              },
              {
                  "$skip" : pageNo
              },
              {
                "$limit" : limitt
              },
           ],
            as:"Ad"
          }
        }
      ]).then((result)=>{
        res.status(200)
          .send({
          error: false,
          success: true,
          errorMessage: "Success",
          rating_rec:rating_rec,
          allReadyLike:allReadyLike,
          record:result
        });
      }).catch((error)=>{
        res.status(200)
          .send({
          error: true,
          success: false,
          errorMessage: error.message
        });
      });
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
	webGetSingleAd:async function(req, res,next)
	{
		try{
			var { ad_id } = req.body;
			
			AddAdvertisementModel.findOne({
		   		_id:ad_id
			}).lean().exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			       if(result)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  		console.log(err);
	  	}
	},
  getWebChatProduct:async function(req, res,next)
	{
		try{
			var { ad_id } = req.body;
      const queryJoin = [
        {
          path:'userId',
          select:['firstName','lastName','mobileNumber','userImage','created_at','userName']
        },
        {
          path:'modelId',
            select:['model_name']
        },
        {
          path:'subModelId',
            select:['model_name']
        },
        {
          path:'fuel',
            select:['attribute_name']
        },
        {
          path:'vehicle_condition',
            select:['attribute_name']
        },
        {
          path:'type_of_gearbox',
            select:['attribute_name']
        },
        {
          path:'colorId',
            select:['color_name']
        },
        {
          path:'type_de_piece',
            select:['attribute_name']
        },
      ];

			AddAdvertisementModel.find({
		   		_id:ad_id
			}).populate(queryJoin).lean().exec((err, result) =>{
			    if(err){
			       console.log(err)
			    }else{
			       if(result)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }
			    }
			});
		}catch(err){
	  	console.log(err);
	  }
	},
  webHeaderAutoSuggestion:async(req,res)=>
  {
    try{
      var {advertisemnet_name} = req.body;
      if(advertisemnet_name == "" || advertisemnet_name == null || advertisemnet_name == undefined  || advertisemnet_name == 'null' || advertisemnet_name == 'undefined')
      {
        return res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "Le nom de l'annonceur est requise"
        });
      }
      //var query = { "$match":{"title": {'$regex' : '^'+product_name, '$options' : 'i'}  } }
      //mongoose.set("debug",true);
       

      await AddAdvertisementModel.aggregate([
        {
          "$match":{
              "$and":[
              {
                "ad_name":{ "$regex" : '^'+advertisemnet_name,'$options':'i' }
              },
              {
                "show_on_web":1
              },
              {
                "complition_status":1
              },
              {
                "delete_at":0
              },
              {
                "activation_status":1
              }
            ]
          }
        },
        {
          "$group":{
            "_id":"$ad_name",
            "category":{"$first":"$category"},
            "ad_id":{"$first":"$_id"},
            "top_search":{"$first":"$top_search"}
          }
        },
        {
          "$project":{
            "_id":1,
            "category":1,
            "ad_id":1,
            "top_search":1
          }
        },
        {
          $sort:{"top_search": -1}
        }
      ]).then((result)=>{
        res.status(200)
          .send({
          error: false,
          success: true,
          errorMessage: "Success",
          record:result
        });
      }).catch((error)=>{
        res.status(200)
          .send({
          error: true,
          success: false,
          errorMessage: error.message
        });
      });
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
  },
  updatePausePlayAd:async(req,res)=>{
    try{
      await AddAdvertisementModel.updateOne({
        _id:req.body.ad_id
      },{activation_status:req.body.activation_status}).then((result)=>{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Success"
        });
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: error.message
        });
      })
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
     
  },
  deleteAdSubmit:async(req,res)=>{
    try{
      await AddAdvertisementModel.updateOne({
        _id:req.body.ad_id
      },{delete_at:1}).then((result)=>{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Success"
        });
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: error.message
        });
      })
    }catch(error)
    {
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: error.message
      });
    }
     
  },
  webContactSubmit:async(req,res)=>{
    try{
      console.log(req.body);
      await ContactModel.create({
        nom:req.body.nom,
        prenom:req.body.prenom,
        email:req.body.email,
        enterprise_name:req.body.enterprise_name,
        message:req.body.message,
        subject:req.body.subject,
        mobile:req.body.mobile
      }).then((result)=>{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Votre message a été envoyé avec succès"
        });
      }).catch((error)=>{
        res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: error.message
        });
      });
    }catch(error)
    {
      res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: error.message
        });
    }
  }
};