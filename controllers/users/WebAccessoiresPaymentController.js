const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const OrderModel = require("../../models/OrderModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');

const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
 

 const paypal = require('paypal-rest-sdk');

 

/*/*/
module.exports = {
  webaccessoiresPurchase:async function(req,res,next)
  {
    try{
      var base_url = customConstant.base_url;
      //console.log(req.query);
      var order_id = req.query.paymentId;
      console.log(order_id);
      OrderModel.findOne({order_id:order_id},{}).exec((err, result)=>
      {
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200).send(return_response);
        }else{
          // console.log(result);
          // console.log(result.final_price);
          // console.log(result.ad_name);
          // console.log(result.payment_type);
          // return false;
          if(result)
          {
            var price = result.final_price;
            var ad_name = result.ad_name;
            var payment_type = result.payment_type;
            if(payment_type == "Paypal")
            {
              var redirect_url_web = base_url+"api/webgetPaypalAccessoriesPayment?id="+order_id+"&price="+price+"&title="+ad_name;
            }else{
              var redirect_url_web = base_url+"api/webgetStripeAccessoriesPayment?id="+order_id+"&price="+price+"&title="+ad_name;
            }
            res.redirect(303, redirect_url_web);
          }
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
              res.status(200).send(return_response);
    }
    
  },

  webgetPaypalAccessoriesPayment:async function(req,res,next)
  {
    try{
      //console.log(req.query);
      var updated_at = new Date();
      var base_url = customConstant.base_url; 
      var lastInsertId = req.query.id;
      var price = req.query.price;
      //price = 1;
      var title = req.query.title;
      var return_url = base_url+"api/webgetPaypalAccessoriesSuccess";
      var cancel_url = base_url+"api/webgetPaypalAccessoriesCancel";

      var paypal_rec = await PaypalModel.findOne({}, {});
        if(!paypal_rec)
        {
          var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
          return res.status(200).send(return_response);
        }
        //console.log(paypal_rec);return false;
        paypal.configure({
          'mode': paypal_rec.mode, //sandbox or live
          'client_id': paypal_rec.client_id,
          'client_secret': paypal_rec.client_secret
        });


      const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": return_url,
            "cancel_url": cancel_url
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": title,
                    "sku": lastInsertId,
                    "price": price,
                    "currency": "EUR",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "EUR",
                "total": price
            },
            "description": lastInsertId
        }]
      };

      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error;
        } else {
            for(let i = 0;i < payment.links.length;i++){
              if(payment.links[i].rel === 'approval_url'){
                res.redirect(payment.links[i].href);
              }
            }
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
              res.status(200).send(return_response);
    }
  },

  webgetStripeAccessoriesPayment:async function(req,res,next)
  {
    try{
      var updated_at = new Date();
      var base_url = customConstant.base_url; 
      var lastInsertId = req.query.id;
      var price = parseFloat(req.query.price);
      //price = 1;
      var title = req.query.title;
      /*console.log(lastInsertId);
      console.log(price);
      console.log(title);*/
      var stripe_rec = await StripeModel.findOne({}, {});
      if(!stripe_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
        return res.status(200).send(return_response);
      }
      //console.log(stripe_rec);return false;
      const stripe = require('stripe')(stripe_rec.stripe_secret);

      const session = await stripe.checkout.sessions.create({
        line_items: [
          {
            price_data: {
              currency: 'eur',
              product_data: {
                name: title,
              },
              unit_amount: price * 100,
            },
            quantity: 1,
            //description: lastInsertId,
          },
        ],
        mode: 'payment',
        //description: 'One-time setup fee',
        success_url: base_url+'api/webgetAccessoriesStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
        cancel_url: base_url+'api/webgetAccessoriesStripeCancel',
      });
      //console.log(session);
      //console.log(session.id);
      //console.log(session.payment_intent);
      //res.redirect(303, session.url);
      OrderModel.updateMany({ order_id:lastInsertId },{
        transaction_id:session.id,
        payment_intent:session.payment_intent,
        updated_at:updated_at
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          res.redirect(303, session.url);
        }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
              res.status(200).send(return_response);
    }
  },
  webgetPaypalAccessoriesSuccess:async function(req,res,next)
  {
    try{
      //console.log("success paypal");
      var updated_at = new Date();
      const payerId = req.query.PayerID;
      const paymentId = req.query.paymentId;
      const token = req.query.token;
      //console.log("payerId");
      //console.log(payerId);
      //console.log(token);
      var front_end_base_url = customConstant.front_end_base_url;
      var paypal_rec = await PaypalModel.findOne({}, {});
        if(!paypal_rec)
        {
          var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
          return res.status(200).send(return_response);
        }
        paypal.configure({
          'mode': paypal_rec.mode, //sandbox or live
          'client_id': paypal_rec.client_id,
          'client_secret': paypal_rec.client_secret
        });
      const execute_payment_json = {
        "payer_id": payerId
      };

      // Obtains the transaction details from paypal
        paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
            //When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
          if (error)
          {
              console.log(error.response);
              throw error;
          }else{
              //console.log(JSON.stringify(payment));
              //console.log(JSON.stringify(payment.id));
              var payment_table_id = JSON.stringify(payment.transactions[0].description);
              payment_table_id = JSON.parse(payment_table_id);
              //console.log("payment_table_id");
              //console.log(payment_table_id);
              OrderModel.updateMany({ order_id:payment_table_id },{
                payment_status:1,
                transaction_id:payerId,
                payment_intent:token,
                payment_success_date:updated_at,
                updated_at:updated_at
              },function(err,result){
                if(err)
                {
                  var return_response = {"error":true,success: false,errorMessage:err};
                    res.status(200)
                    .send(return_response);
                }else{

                  OrderModel.findOne({order_id:payment_table_id},{"user_id":1}).exec((errUser, resultUser)=>
                  {
                    if(err)
                    {
                      var return_response = {"error":true,success: false,errorMessage:errUser};
                          res.status(200).send(return_response);
                    }else{
                      /*console.log(result);
                      console.log(result.final_price);
                      console.log(result.ad_name);
                      console.log(result.payment_type);*/
                      if(resultUser)
                      {
                        var user_id = resultUser.user_id;
                        //console.log(user_id);
                        CartsModel.deleteMany({
                          user_id:user_id
                        },function(err,result){
                          if(err)
                          {
                            var return_response = {"error":true,success: false,errorMessage:err};
                              res.status(200)
                              .send(return_response);
                          }else{
                            var redirect_url_web = front_end_base_url+"userPurchaseSuccess";
                            res.redirect(303, redirect_url_web);
                            /*var return_response = {"error":false,success: true,errorMessage:"Success"};
                              res.status(200)
                              .send(return_response);*/
                          }
                        });
                      }
                    }
                  });
                }
              });
              //res.send('Success');
          }
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
              res.status(200).send(return_response);
    }
  },
  webgetPaypalAccessoriesCancel:async function(req,res,next)
  {
    //console.log("pay pal cancel");
    var return_response = {"error":false,success: true,errorMessage:"pay pal cancel"};
  },
  webgetAccessoriesStripeSuccess:async function(req,res,next)
  {
    try{
      var front_end_base_url = customConstant.front_end_base_url;
      //console.log("success stripe");
      //console.log(req.query.session_id);
      //var session_ID = req
      var updated_at = new Date();
      //const stripe = require('stripe')('key-here');
      var stripe_rec = await StripeModel.findOne({}, {});
      if(!stripe_rec)
      {
        var return_response = {"error":true,success: false,errorMessage:"Il y a un problème, contactez l'administrateur"};
        return res.status(200).send(return_response);
      }
      //console.log(stripe_rec);return false;
      const stripe = require('stripe')(stripe_rec.stripe_secret);

      const session = await stripe.checkout.sessions.retrieve(
        req.query.session_id
      );
      //console.log(session);
      //console.log(session.payment_status);
      if(session)
      {
        if(session.payment_status == "paid")
        {
          //console.log("paid");
          OrderModel.updateMany({ transaction_id:req.query.session_id },{
            payment_status:1,
            payment_success_date:updated_at,
            updated_at:updated_at
          },function(err,result){
            if(err)
            {
              var return_response = {"error":true,success: false,errorMessage:err};
                res.status(200)
                .send(return_response);
            }else{
              OrderModel.findOne({transaction_id:req.query.session_id},{"user_id":1}).exec((errUser, resultUser)=>
              {
                if(err)
                {
                  var return_response = {"error":true,success: false,errorMessage:errUser};
                      res.status(200).send(return_response);
                }else{
                  /*console.log(result);
                  console.log(result.final_price);
                  console.log(result.ad_name);
                  console.log(result.payment_type);*/
                  if(resultUser)
                  {
                    var user_id = resultUser.user_id;
                    //console.log(user_id);
                    CartsModel.deleteMany({
                      user_id:user_id
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        //
                        var redirect_url_web = front_end_base_url+"userPurchaseSuccess";
                        res.redirect(303, redirect_url_web);

                        //var return_response = {"error":false,success: true,errorMessage:"Success"};
                        // res.status(200)
                          //.send(return_response);
                      }
                    });
                  }
                }
              });
            }
          });
        }else{
          res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Not paid"
                    });
        }
      }else{
        res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Some error"
                });
      }
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      res.status(200).send(return_response);
    }
  },
  webgetAccessoriesStripeCancel:async function(req,res,next)
  {
    //console.log("stripe cancel");
    var return_response = {"error":false,success: true,errorMessage:"stripe cancel"};
  },
};