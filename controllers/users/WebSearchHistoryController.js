const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const ModelsModel = require("../../models/ModelsModel");
const AdminModel = require('../../models/AdminModel');
const UserPlanModel = require('../../models/UserPlanModel');
const TopUrgentPlan = require('../../models/TopUrgentPlan');
const UserPurchasedPlan = require('../../models/UserPurchasedPlan');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const UseraccessoriesplanModel = require("../../models/UseraccessoriesplanModel");
const TopUrgentAccessoriesPlanModel = require("../../models/TopUrgentAccessoriesPlanModel");
const UserfavadsModel = require("../../models/UserfavadsModel");
const SearchHistoryModel = require("../../models/SearchHistoryModel");

const watermark = require('jimp-watermark');
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const { async } = require('rxjs');
const { parse } = require('path');
const app = express();
//const customConstant = require('../../helpers/customConstant');

const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.porschists.com',
    port: 465,
    auth: {
       user: 'contact@porschists.com',
       pass: 'OD-oeu&mMbU%'
    }
});

module.exports = {
  webGetHistoryList:async(req,res)=>{
    try{
      var {user_id} = req.body;
      if(user_id == "" || user_id == null || user_id == undefined)
      {
        var return_response = {"error":true,errorMessage:"Pas d'enregistrement",record:[]};
        return res.status(200).send(return_response);
      }
      await SearchHistoryModel.aggregate([
        {
          $match:{
            "user_id":user_id
          }
        },
        {
          $sort:{
            "searched_time":-1
          }
        }
      ]).then((result)=>{
        var return_response = {"error":false,errorMessage:"Succès",record:result};
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,errorMessage:error.message};
        return res.status(200).send(return_response);
      })
    }catch(error)
    {
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  webHistoryActiveDeactiveEmail:async(req,res)=>{
    try{
      var {id,status} = req.body;
      var record = await SearchHistoryModel.findOne({_id:id},{is_email:1});
      if(!record)
      {
        var return_response = {"error":true,errorMessage:"Identifiant invalide"};
        return res.status(200).send(return_response);
      }
      await SearchHistoryModel.updateOne({_id:id},
        {
          is_email:status
        }).then((result)=>{
          var return_response = {"error":false,errorMessage:"Enregistrement complet des succès actualisés"};
          return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,errorMessage:error.message};
          return res.status(200).send(return_response);
        });
    }catch(error){
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  webHistoryDelete:async(req,res)=>{
    try{
      var id = req.params.id;
      //console.log(id)
      var record = await SearchHistoryModel.findOne({_id:id},{is_email:1});
      if(!record)
      {
        var return_response = {"error":true,errorMessage:"Identifiant invalide"};
        return res.status(200).send(return_response);
      }
      await SearchHistoryModel.deleteOne({_id:id}).then((result)=>{
        var return_response = {"error":false,errorMessage:"Enregistrer les succès supprimés dans leur intégralité"};
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,errorMessage:error.message};
        return res.status(200).send(return_response);
      }); 
    }catch(error)
    {
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  sendEmailBeforeExpiry:async(req,res)=>{
    try{
      // Bonjour {user}

      // Votre abonnement expire le {date}, vous pouvez le renouveler en payant les frais correspondants pour continuer à bénéficier des services de Porschists. 

      // Bien Cordialement,
      // L'équipe de Porschists
      //mongoose.set("debug",true);
      let today_date = new Date();
      // var record = await AddAdvertisementModel.find({
      //   category: 'Porsche Voitures',
      //   complition_status: 1,
      //   show_on_web: 1,
      //   email_reminder:0,
      //   delete_at:0,
      //   plan_end_date_time:{ $gte:today_date }
      // },{ad_name:1,plan_end_date_time:1});
      var record = await AddAdvertisementModel.aggregate([
        {
          $match:{
            $and:[
              {
                category: 'Porsche Voitures',
              },
              {
                email_reminder:0,
              },
              {
                show_on_web: 1,
              },
              {
                complition_status: 1,
              },
              {
                delete_at:0,
              },
              {
                plan_end_date_time:{ $gte:today_date }
              }
            ]
          }
        },
        {
          $addFields:{
            "user_id":"$userId"
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"user_id":{"$toObjectId":"$user_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$user_id"]
                  }
                }
              }
            ],
            as:"usr"
          }
        },
        {
          $skip:0
        },
        {
          $limit:3
        },
        {
          $project:{
            ad_name:1,
            user_id:1,
            plan_end_date_time:1,
            "usr.email":1,
            "usr.firstName":1,
            "usr.lastName":1
          }
        }
        
      ]);
      //console.log(record);
      record.map((obj)=>{
        // console.log("obj");
        // console.log(obj);
        // console.log(obj._id);
        //console.log(obj);
        const today = new Date(obj.plan_end_date_time);
        const yyyy = today.getFullYear();
        let mm = today.getMonth() + 1; // Months start at 0!
        let dd = today.getDate();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;

        const formattedToday = dd + '/' + mm + '/' + yyyy;


        const date1 = obj.plan_end_date_time;
        const date2 = new Date( );
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));  
        // console.log(diffDays + " days");
        // console.log(diffDays );
        if(diffDays == 3)
        {
          //console.log(obj.usr);
          // console.log(obj._id);
          // console.log(obj.usr.length);
          if(obj.usr.length > 0)
          {
            //console.log(obj.usr[0].email);
            if(obj.usr[0].email)
            {
              let email = obj.usr[0].email;
              let firstName = obj.usr[0].firstName;
              let lastName = obj.usr[0].lastName;
              var html = "";
              html += "<div><p>Bonjour "+firstName+" "+lastName+" </p><p>Votre abonnement expire le "+formattedToday+", vous pouvez le renouveler en payant les frais correspondants pour continuer à bénéficier des services de Porschists.</p></div>";
              html += "<div><p>Bien Cordialement,</p><p>L'équipe de Porschists</p></div>";
              const message = {
                from: 'contact@porschists.com', // Sender address
                to: email,         // recipients
                subject: "Rappel de renouvellement d'annonces", // Subject line
                html: html // Plain text body
              };
              transport.sendMail(message, function(err, info) {
                  if (err) {
                    console.log(err);
                    error_have = 1;
                  } else {
                    AddAdvertisementModel.updateOne({_id:obj._id},{email_reminder:1}).then((result)=>{

                    }).catch((error)=>{

                    });
                    //console.log('mail has sent.');
                    //console.log(info);
                  }
              });
            }
          }
        }
      });
      //var return_response = {"error":false,errorMessage:"Success",record:record};
      //return res.status(200).send(return_response);
    }catch(error)
    {
      var return_response = {"error":true,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  }
};