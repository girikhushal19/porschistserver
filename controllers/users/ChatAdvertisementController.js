const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const AdChatModel = require('../../models/AdChatModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
module.exports = {
	requestStartAdChat:async function(req,res,next)
	{
		try{
			var {ad_id,sender_id,message} = req.body;
			//var randomNumber = Math.random().toString(36).slice(2); 
			var randomNumber = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
			//console.log("randomNumber "+Math.random().toString(36).slice(4));
			AddAdvertisementModel.findOne({ad_id} , { userId:1 }).lean(true).exec((err,result)=>{
				if(err)
				{
					console.log("ifff "+err);
				}else{
					if(sender_id == result.userId)
					{
						//console.log("iffff "+result.userId);
						var return_response = {"error":true,success: false,errorMessage:"This is your own add"};
						res.status(200)
							            .send(return_response);
					}else{
						/*console.log("else "+result.userId);
						console.log("else "+sender_id);*/
						AdChatModel.count({ad_id,sender_id},(err,resultCount)=>{
							if(err)
							{
								console.log(err);

							}else{
								//console.log("resultCount "+resultCount);
								if(resultCount === 0)
								{
									let receiver_id = result.userId;
									let message_by = 1;
									//console.log("iff "+resultCount);
									AdChatModel.create({
										ad_id,
										randomNumber,
										sender_id,
										receiver_id,
										message,
										message_by
									});
									var return_response = {"error":false,success: true,errorMessage:"Message send success fully"};
									res.status(200)
							            .send(return_response);
								}else{
									var return_response = {"error":true,success: false,errorMessage:"Allready chat started for this Ad with you"};
									res.status(200)
							            .send(return_response);
								}
							}
						});
					}


				}
			});
		}catch(err){
	  		console.log(err);
		}
	},
	messageAdChat:async function(req,res,next)
	{
		try{
			var {chat_id,sender_id,message} = req.body;
			AdChatModel.findOne({_id:chat_id}).lean(true).exec((err,result)=>{
				if(err)
				{
					console.log("ifff "+err);
				}else{
					if(!result)
					{
						var return_response = {"error":true,success: false,errorMessage:"This is invalid chat id"};
						res.status(200)
							            .send(return_response);
			        }else{
			        	if(result.length == 0)
						{
							//console.log("iffff "+result.userId);
							var return_response = {"error":true,success: false,errorMessage:"This is invalid chat id"};
							res.status(200)
								            .send(return_response);
						}else{
							//console.log("result "+JSON.stringify(result));
							//console.log("result "+result.ad_id);
							var ad_id = result.ad_id;
							var randomNumber = result.randomNumber;
							if(result.sender_id == sender_id)
							{
								var message_by = 1;
								var receiver_id = result.receiver_id;
							}else{
								var message_by = 2;
								var receiver_id = result.sender_id;
							}
							/*console.log("else "+result.userId);
							console.log("else "+sender_id);*/
							
							//console.log("iff "+resultCount);
							AdChatModel.create({
								ad_id,
								randomNumber,
								sender_id,
								receiver_id,
								message,
								message_by
							});
							var return_response = {"error":false,success: true,errorMessage:"Message send success fully"};
							res.status(200)
					            .send(return_response);
						}
			        }
					
				}
			});
		}catch(err){
	  		console.log(err);
		}
	},
	getAdChat:async function(req,res,next)
	{
		try{
			/*var ran_number = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');

			console.log(ran_number);*/
			var {ad_id,randomNumber} = req.body;
		AdChatModel.find( { ad_id,randomNumber } ).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          /*console.log("result ");
	          console.log(typeof result);*/
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	            var return_response = {"error":false,errorMessage:"success","record":result};
	            res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"No Record","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });
		}catch (error) {
		  	console.log(error)
		    res.status(404).json({
		      status: 'fail',
		      message: error,
		    });
		}
	},
	getAllAdChatGroup:async function(req,res,next)
	{
		/*
			Very important line
			$match: { receiver_id : new mongoose.Types.ObjectId(currentUserId)  },
		*/
		try {
			//mongoose.set('debug', true);
		    const currentUserId = req.body.user_id;
		    const stats = await AdChatModel.aggregate([
		      {
		        $match: {
			        $or:[{ sender_id : new mongoose.Types.ObjectId(currentUserId)  },
			             { receiver_id : new mongoose.Types.ObjectId(currentUserId)  }] 
			    },
		      },
		      {
		        "$group": {
		         "_id": "$randomNumber",
						 "message_by": { "$first": "$message_by" },
						 "message": { "$first": "$message" },
		         "chat_id": { "$first": "$_id" },
		         "ad_id": { "$first": "$ad_id" },
		         "sender_id": { "$first": "$sender_id" },
		         "receiver_id": { "$first": "$receiver_id" }
		          }, 
		      },
		      { 
				   $lookup:{ 
				      "from":"users",
				      "localField":"sender_id",
				      "foreignField":"_id",
				      "as":"userDetail"
				   }
				},
		      { 
				   $lookup:{ 
				      "from":"users",
				      "localField":"receiver_id",
				      "foreignField":"_id",
				      "as":"userDetail2"
				   }
				},
				{ 
				   $unwind:"$userDetail"
				},
				{ 
				   $unwind:"$userDetail2"
				},
				{
					"$project": {
						"ad_id":1,
						"randomNumber":1,
						"userDetail._id": 1,
						"userDetail.firstName": 1,
						"userDetail.lastName": 1,
						"userDetail2._id": 1,
						"userDetail2.firstName": 1,
						"userDetail2.lastName": 1,
						
					}
				},

				//{ "$sort": { "payment_time": -1 } },

		    ]);
		    //console.log(stats);
				if(stats)
				{
					var return_response = {"error":false,errorMessage:"Succès","record":stats};
					res.status(200).send(return_response);
				}else{
					var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":stats};
					res.status(200).send(return_response);
				}
		  } catch (error) {
		  	//console.log(error)
		    res.status(404).json({
		      status: 'fail',
		      message: error,
		    });
		  }
	},
};