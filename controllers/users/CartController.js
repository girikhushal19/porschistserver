const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const DeliveryAddressUser = require("../../models/DeliveryAddressUser");
const CartsModel = require("../../models/CartsModel");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  addAddress:async function(req,res,next)
  {
    try{
      var user_id = req.body.user_id;
      DeliveryAddressUser.find({ user_id:user_id },(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          if(record.length > 0)
          {
            /*console.log("update id");
            console.log(record);console.log(record[0]._id);return false;*/
            
            DeliveryAddressUser.findOneAndUpdate({_id:record[0]._id},{
              address:req.body.address,
              location: {
               type: "Point",
               coordinates: [req.body.longitude,req.body.latitude]
              }
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Plan purchased",lastInsertId:result._id};
                  res.status(200)
                  .send(return_response);
              }
            });
          }else{
            DeliveryAddressUser.create({
              user_id:req.body.user_id,
              address:req.body.address,
              location: {
               type: "Point",
               coordinates: [req.body.longitude,req.body.latitude]
              }
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                //console.log(result);
                //console.log(result._id);
                var return_response = {"error":false,success: true,errorMessage:"Plan purchased",lastInsertId:result._id};
                  res.status(200)
                  .send(return_response);
              }
            });
          }
        }
      });
      
    }catch(err){
        console.log(err);
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },
  getAddress:async function(req,res,next)
  {
    try{
      var user_id = req.body.user_id;
      DeliveryAddressUser.find({ user_id:user_id },(errs, record)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
          res.status(400)
                .send(return_response);
        }else{
          if(record.length > 0)
          {
            var return_response = {"error":false,success: true,errorMessage:"success",record:record};
            res.status(400)
                .send(return_response);
          }else{
            var return_response = {"error":true,success: false,errorMessage:"No Record"};
            res.status(200)
                .send(return_response);
          }
        }
      });
      
    }catch(err){
        console.log(err);
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },
  addToCart:async function(req,res,next)
  {
    //console.log(req.body);
    var tax = 10;
    //var quantity = 1;
    var {user_id,ad_id,address_id,shipping_charge,quantity} = req.body;
    //  delivery_type 
    //CartsModel.find()
    if(quantity == "" || quantity == null || quantity == undefined  || quantity == "null" || quantity == "undefined" )
    {
      quantity = 1;
    }
    AddAdvertisementModel.find({_id:ad_id},{'_id':1,'userId':1,'price':1,'pro_price':1},(errs, record)=>{
      if(errs)
      {
        var return_response = {"error":true,success: false,errorMessage:errs};
        res.status(400)
          .send(return_response);
      }else{
        if(record.length > 0)
        {
          //console.log(record);
          var ad_owner_user_id = record[0].userId;
          if(ad_owner_user_id == user_id)
          {
            var return_response = {"error":true,success: false,errorMessage:"Il s'agit de votre propre ajout. Vous ne pouvez pas ajouter votre propre article dans un panier."};
                    res.status(200)
                    .send(return_response);
          }else{
            var price = record[0].price;
            price = price * quantity;
            CartsModel.find({user_id:user_id,ad_id:ad_id},(errsC, recordOld)=>{
              if(errs)
              {
                var return_response = {"error":true,success: false,errorMessage:errsC};
                res.status(400)
                  .send(return_response);
              }else{
                /*console.log(recordOld);
                return false;*/
                if(recordOld.length > 0)
                {
                  CartsModel.findOneAndUpdate({_id:recordOld[0]._id},{
                    quantity:quantity,
                    address_id:address_id,
                    tax:tax,
                    shipping_charge:shipping_charge,
                    price:price
                  },function(err,result){
                    if(err)
                    {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                    }else{
                      //console.log(result);
                      //console.log(result._id);
                      var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                        res.status(200)
                        .send(return_response);
                    }
                  });
                }else{
                  CartsModel.create({
                    user_id:user_id,
                    ad_owner_user_id:ad_owner_user_id,
                    ad_id:ad_id,
                    quantity:quantity,
                    address_id:address_id,
                    tax:tax,
                    shipping_charge:shipping_charge,
                    price:price
                  },function(err,result){
                    if(err)
                    {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                    }else{
                      //console.log(result);
                      //console.log(result._id);
                      var return_response = {"error":false,success: true,errorMessage:"Article ajouté au panier."};
                        res.status(200)
                        .send(return_response);
                    }
                  });
                }
              }
            }); 
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"ID d'annonce invalide"};
          res.status(200)
            .send(return_response);
        }
      }
    });
  },
  getCart:async function(req,res,next)
  {
    const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName']
          },
          {
            path:'ad_owner_user_id',
              select:['firstName','lastName']
          },
          {
            path:'ad_id',
              select:['ad_name','price','pro_price','contact_number','description','address','location','accessoires_image']
          },
          {
            path:'address_id',
              select:['address','location']
          }

        ];

    //console.log(String_qr);
    var user_id = req.body.user_id;
      CartsModel.find( {user_id:user_id} , {}).populate(queryJoin).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result};
            res.status(200).send(return_response);
          }

        }
      });
  }
};