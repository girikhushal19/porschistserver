const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const AttributeModel = require("../../models/AttributeModel");
//const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const AddAdvertisementModel = require('../../models/AddAdvertisementModel');
const AdFavorite = require('../../models/AdFavorite');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	addFavAd:async function(req,res,next)
	{
		console.log(req.body);
		try{
			var { user_id , ad_id } = req.body;
			AddAdvertisementModel.count({userId:user_id,_id:ad_id},(err, resultCount)=>
		    {
		    	if(resultCount == 0)
		    	{
		    		AdFavorite.count({user_id:user_id,ad_id:ad_id},(errs, allReadyLike)=>
				    {
				    	//console.log("allReadyLike"+allReadyLike);
				    	if(allReadyLike == 0)
				    	{
				    		AdFavorite.create({
								ad_id,
								user_id
							});
							var return_response = {"error":false,success: true,errorMessage:"Ad success fully added in your favorite list"};
							res.status(200)
					            .send(return_response);
				    	}else{
				    		var return_response = {"error":true,success: false,errorMessage:"You all-ready liked this add"};
		        			res.status(200).send(return_response);
				    	}
				    })
		    	}else{
		    		//console.log("result"+resultCount);
		        	var return_response = {"error":true,success: false,errorMessage:"This is your own add"};
		        	res.status(200).send(return_response);
		    	}
		         
		    });
		}catch(error)
		{
			console.log(error)
		    res.status(404).json({
		      status: 'fail',
		      message: error,
		    });
		}
		
	},
	getUserFavAllAd:async function(req,res,next)
	{

		//mongoose.set('debug', true);
    var xx = 0;
    var total = 0;
    var perPageRecord = 15;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
    	/*,
			    {
			    	path:'modelId',
			        select:['model_name']
			    },*/
    	const queryJoin = [
				{
		        	path:'ad_id'
			    }
			];
		var user_id = req.body.user_id;	
      AdFavorite.find( {user_id} ).skip(fromindex).limit(perPageRecord).populate(queryJoin).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"No Record","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  
	
	},
};