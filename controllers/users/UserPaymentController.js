const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const UserPurchasedPlan = require("../../models/UserPurchasedPlan");
const AddAdvertisementModel = require("../../models/AddAdvertisementModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  paymentCarAddSubmit:async function(req,res,next)
  {
    //console.log("here");return false;
    var plan_purchase_date_time = new Date();
    //console.log(plan_purchase_date_time);
     
    var { ad_id,user_id,basic_main_plan_id,top_search_plan_id,top_urgent_plan_id } = req.body;
    /*console.log(ad_id);
    console.log(user_id);*/
    AddAdvertisementModel.findOne({_id:ad_id},{user_type:1},(err,userRecord)=>{
      console.log(userRecord);
      if(err)
      {
        var return_response = {"error":true,success: false,errorMessage:err};
          res.status(400)
          .send(return_response);
      }else{
        if(userRecord.user_type == "normal_user")
        {
          console.log("basic_main_plan_id "+basic_main_plan_id);
          console.log("top_search_plan_id "+top_search_plan_id);
          console.log("top_urgent_plan_id "+top_urgent_plan_id);
          if(basic_main_plan_id)
          {
            UserPlanModel.findOne({_id:basic_main_plan_id},(errs,planResult)=>{
              if(errs)
              {
                var return_response = {"error":true,success: false,errorMessage:errs};
                res.status(400)
                .send(return_response);
              }else{
                //console.log(planResult);
                var day_number = planResult.day_number;
                var price = planResult.price;
                AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
                  showing_number_day:day_number,
                  basic_plan_price:price,
                  plan_purchase_date_time:plan_purchase_date_time,
                });
              }
            });
          }else{
            var day_number = 90;
            var price = 0;
            AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
              showing_number_day:day_number,
              basic_plan_price:price,
              plan_purchase_date_time:plan_purchase_date_time,
            });
          }
          /*
          var top_urgent_date = new Date();
          if(top_urgent_plan_id)
          {
            TopUrgentPlan.findOne({_id:top_urgent_plan_id},(errss,topPlanResult)=>{
              if(errss)
              {
                var return_response = {"error":true,success: false,errorMessage:errss};
                    res.status(400)
                    .send(return_response);
              }else{
                 var top_urgent_price = topPlanResult.price;
                 AddAdvertisementModel.findOneAndUpdate({ _id:ad_id },{
                  top_urgent:1,
                  top_urgent_price:top_urgent_price,
                  top_urgent_date:top_urgent_date,
                });
              }
            });
          }*/

        }else if(userRecord.user_type == "pro_user")
        {
          if(basic_main_plan_id)
          {
            ProUserPlanModel.findOne({_id:basic_main_plan_id},(err,proUserPlan)=>{
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(400)
                  .send(return_response);
              }else{
                console.log(proUserPlan);
                var plan_purchase_date_time = new Date();
                var price = proUserPlan.price;
                var maximum_upload = proUserPlan.maximum_upload;
                var showing_number_day = proUserPlan.day_number;
                var top_search_price = proUserPlan.top_price;

                UserPurchasedPlan.create({
                  ad_id:ad_id,
                  user_id:user_id,
                  maximum_upload:maximum_upload,
                  basic_plan_price:price,
                  showing_number_day:showing_number_day,
                  plan_purchase_date_time:plan_purchase_date_time,
                  top_search:1,
                  top_search_days:showing_number_day,
                  top_search_price:top_search_price,
                },function(err,result){
                  if(err)
                  {
                    var return_response = {"error":true,success: false,errorMessage:err};
                      res.status(200)
                      .send(return_response);
                  }else{
                    console.log(result);
                    console.log(result._id);
                    var lastInsertId = result._id;

                    var return_response = {"error":false,success: true,errorMessage:"Plan purchased",record:lastInsertId};
                      res.status(200)
                      .send(return_response);

                  
                  }
                });
                
              }
            });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Something went wrong"};
          res.status(200)
          .send(return_response);
        } 
      }
    });
  },
};