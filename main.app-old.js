const express = require('express');
const bodyParser = require('body-parser');
//const http = require('http');
const path = require('path');
const adminRouter = require("./router/adminRouter");
const mobileApiRouter = require("./router/mobileApiRouter");
//var multer  = require('multer');
const mongoose = require('mongoose');
var cors = require('cors')
var db = require("./models/connection");
const ngrok = require('ngrok');

const nodemailer = require('nodemailer');

const app = express();

app.use(cors());

//const app = require('express')();





//const ngrok = require('ngrok');
/*(async function() {
  console.log("here");
  const url = await ngrok.connect(4040,(err, urlrr)=>{
    console.log("err"+err);
        console.log(`Node.js local server is publicly-accessible at ${urlrr}`);
  });
})();*/
 
/*const port = 4040;
const server = http.createServer((req, res) => {
    //res.end('Hello, World!');
});

server.listen(port, (err) => {
  console.log(ngrok);
    if (err) return console.log(`Something bad happened: ${err}`);
    console.log(`Node.js server listening on ${port}`);
    console.log("http://localhost:4040");
    //console.log(ngrok);
    ngrok.connect(port, function (err, url) {
      console.log("err"+err);
        console.log(`Node.js local server is publicly-accessible at ${url}`);
    });

});*/




app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist/porschists')));

app.use('/public/uploads', express.static('public/uploads'));


/*app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/porschists/index.html'));
});
*/
app.use('/webadmin',mobileApiRouter);
app.use('/api',mobileApiRouter);




const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer, {
  cors: {origin : '*'}
});

app.use((req, res, next) => {
  req.io = io;
  return next();
});


const port = 36976;

io.on('connection', (socket) => {
  console.log('a user connected');

   

  // socket.on('sendMessage', (message) => {
  //   console.log(message);

  //   io.emit('getMessage', `${socket.id.substr(0, 2)}: ${message}`);
  // });

  socket.on('disconnect', () => {
    console.log('a user disconnected!');
  });
});
require("./controllers/users/TestSocketController")(io);
//require("../controllers/users/TestSocketController")(io)


httpServer.listen(port, () => console.log(`listening on port  ${port}`));

// http.createServer(app).listen(3001,()=>{
//   console.log('Server Running at http://localhost:3001');
// });



//app.listen(4040);

/*const port = 3000;
const host = '0.0.0.0';

app.listen(port, host, () => {
  console.log('Listening on port ' + port);
});*/



///DNS ->  192.168.96.110 IPV4->   192.168.96.242
