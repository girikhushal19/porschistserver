const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const path = require("path");
const auth = require("../middleware/auth");
const userController = require("../controllers/users/userController");
const HomeController = require("../controllers/users/HomeController");
const FavoriteAdController = require("../controllers/users/FavoriteAdController");
const AdvertisementController = require("../controllers/users/AdvertisementController");
const ChatAdvertisementController = require("../controllers/users/ChatAdvertisementController");
const UserAdvertisementController = require("../controllers/users/UserAdvertisementController");
const CartController = require("../controllers/users/CartController");
const WebCartController = require("../controllers/users/WebCartController");
const CheckoutController = require("../controllers/users/CheckoutController");
const AllTypePlanController = require("../controllers/users/AllTypePlanController");
const UserPaymentController = require("../controllers/users/UserPaymentController");
const StripePaymentController = require("../controllers/users/StripePaymentController");
const AccessoiresPaymentController = require("../controllers/users/AccessoiresPaymentController");
const WebUserController = require("../controllers/users/WebUserController");
const WebAdvertisementController = require("../controllers/users/WebAdvertisementController");
const WebHomeController = require("../controllers/users/WebHomeController");
const WebAllTypePlanController = require("../controllers/users/WebAllTypePlanController");
const WebPaymentController = require("../controllers/users/WebPaymentController");
const WebAdFrontController = require("../controllers/users/WebAdFrontController");
const WebAdFavController = require("../controllers/users/WebAdFavController");
const WebAdChatController = require("../controllers/users/WebAdChatController");
const WebCheckoutController = require("../controllers/users/WebCheckoutController");
const WebAccessoiresPaymentController = require("../controllers/users/WebAccessoiresPaymentController");
const WebUserPaymentController = require("../controllers/users/WebUserPaymentController");
const PaymentController = require("../controllers/admin/PaymentController");

const WebPaymentControllerProUser = require("../controllers/users/WebPaymentControllerProUser");
const WebPaymentHistoryController = require("../controllers/users/WebPaymentHistoryController");
const WebAccessoriesAdController = require("../controllers/users/WebAccessoriesAdController");
const CronController = require("../controllers/users/CronController");
const WebPaymentRenewalController = require("../controllers/users/WebPaymentRenewalController");
const WebSearchHistoryController = require("../controllers/users/WebSearchHistoryController");
const WebPaymentRenewalControllerProUser = require("../controllers/users/WebPaymentRenewalControllerProUser");

const UserController = require("../controllers/admin/UserController");
const adminController = require("../controllers/admin/adminController");
const CategoryController = require("../controllers/admin/CategoryController");
const AttributeController = require("../controllers/admin/AttributeController");
const PlanController = require("../controllers/admin/PlanController");
const PlanSecondController = require("../controllers/admin/PlanSecondController");

const AccessoriesAdController = require("../controllers/admin/AccessoriesAdController");
const CarAdController = require("../controllers/admin/CarAdController");
const CommonController = require("../controllers/admin/CommonController");
const PageController = require("../controllers/admin/PageController");
const AccessoriesOrdersController = require("../controllers/admin/AccessoriesOrdersController");
const UserPermissionController = require("../controllers/admin/UserPermissionController");
const FaqAdminController = require("../controllers/admin/FaqAdminController");

const validationMiddleware = require('../middleware/validation-middleware');
const userLoginValidation = require('../middleware/userLoginValidation');
const userGetProfile = require('../middleware/userGetProfile');
const userEditProfile = require('../middleware/userEditProfile');
const getModelValid = require('../middleware/getModelValid');
const getSubModelValid = require('../middleware/getSubModelValid');
const AttributeValid = require('../middleware/AttributeValid');
const AttributeSubValid = require('../middleware/AttributeSubValid');
const AddAdvertisementValid = require('../middleware/AddAdvertisementValid');
const AllAdvertisementValid = require('../middleware/AllAdvertisementValid');
const UserAddValid = require('../middleware/UserAddValid');
const UserSingleAddValid = require('../middleware/UserSingleAddValid');
const ChatStartValid = require('../middleware/ChatStartValid');
const ChatMessageValid = require('../middleware/ChatMessageValid');
const GetChatValid = require('../middleware/GetChatValid');
const GetGroupChatValid = require('../middleware/GetGroupChatValid');
const AddFavoriteAd = require('../middleware/AddFavoriteAd');
const AllFavoriteAd = require('../middleware/AllFavoriteAd');
const UserCarAdPayValid = require('../middleware/UserCarAdPayValid');
const AdvertisementStep1Valid = require('../middleware/AdvertisementStep1Valid');
const AdvertisementStep2Valid = require('../middleware/AdvertisementStep2Valid');
const AdvertisementStep3Valid = require('../middleware/AdvertisementStep3Valid');
const AdvertisementStep4Valid = require('../middleware/AdvertisementStep4Valid');
const AdvertisementStep5Valid = require('../middleware/AdvertisementStep5Valid');
const AdvertisementStep6Valid = require('../middleware/AdvertisementStep6Valid');
const AdvertisementStep7Valid = require('../middleware/AdvertisementStep7Valid');
const AccessoiresStep2Valid = require('../middleware/AccessoiresStep2Valid');
const AccessoiresStep3Valid = require('../middleware/AccessoiresStep3Valid');
const AccessoiresStep4Valid = require('../middleware/AccessoiresStep4Valid');
const AccessoiresStep5Valid = require('../middleware/AccessoiresStep5Valid');
const AccessoiresStep6Valid = require('../middleware/AccessoiresStep6Valid');
const AccessoiresStep7Valid = require('../middleware/AccessoiresStep7Valid');
const AccessoiresStep8Valid = require('../middleware/AccessoiresStep8Valid');
const AddAddressValid = require('../middleware/AddAddressValid');
const GetAddressValid = require('../middleware/GetAddressValid');
const AddCartvalidation = require('../middleware/AddCartvalidation');
const GetCartvalidation = require('../middleware/GetCartvalidation');
const Checkoutvalidation = require('../middleware/Checkoutvalidation');
const EmailVerificationValid = require('../middleware/EmailVerificationValid');
const ProUserValid = require('../middleware/ProUserValid');

 
var multer  = require('multer');
 
var storage = multer.diskStorage({

   destination: function (req, file, cb) {
      cb(null, './public/uploads/userProfile/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
   }

});
var upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }

});
var storageAccessoires = multer.diskStorage({

   destination: function (req, file, cb) {
      cb(null, './public/uploads/addadvertise/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
   }

});
var uploadAccessoires = multer({ 
    storage: storageAccessoires,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }

});
const storage2 = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArray = multer({ 
  storage: storage2,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});
const storage3 = multer.diskStorage({

    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
      //console.log(req.body);
      //console.log(req.file);
      //console.log(file.originalname);
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArrayPiwi = multer({ 
  storage: storage3,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});
const storage4 = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArrayMainRepImg = multer({ 
  storage: storage4,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "pdf") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});
var Adminuploadstorage = multer.diskStorage({

   destination: function (req, file, cb) {
    console.log("here");
      cb(null, './public/uploads/models/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'models_'+ dynamicFileName + '-' + file.originalname);
   }

});
var adminupload = multer({ storage: Adminuploadstorage });
const storageBanner = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/banner');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadBanner = multer({ 
  storage: storageBanner
});
const storageAdminImage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/adminProfile');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadAdminImage = multer({ 
  storage: storageAdminImage
});
const storage2Admin = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/category/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArrayAdmin = multer({ 
  storage: storage2Admin
});
const storageAdImageTesting = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, './public/uploads/testAd/');
  },

  filename: function(req, file, cb) {
      var dynamicFileName = Math.floor(Math.random() * Date.now());
    cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
  }
});
var AdImageTesting = multer({ 
storage: storageAdImageTesting
});
//addadvertise
const storageAdImageTesting2 = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, './public/uploads/addadvertise/');
  },

  filename: function(req, file, cb) {
      var dynamicFileName = Math.floor(Math.random() * Date.now());
    cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
  }
});
var AdImageTesting2 = multer({ 
storage: storageAdImageTesting2
});
const storageUsersImage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/userProfile');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadUsersImage = multer({ 
  storage: storageUsersImage
});

router.post('/userRegistration', validationMiddleware.signup, userController.userRegistration);
//router.post('/proUserRegistration',upload.single('userDocument'),ProUserValid.proSignup, userController.proUserRegistration);

router.post('/proUserRegistration',upload.fields([{
  name: 'userDocument', maxCount: 1
}, {
  name: 'registration_certificate', maxCount: 1
} ]),ProUserValid.proSignup, userController.proUserRegistration);
router.post('/userLogin', userLoginValidation.login, userController.userLogin);
router.post("/getUserProfile",userGetProfile.getProfile,userController.getUserProfile);
router.post('/emailVerification',EmailVerificationValid.verificationValid,userController.emailVerification);
router.post('/resendCodeForEmailVerification',userController.resendCodeForEmailVerification);
//router.post("/updateUserProfile",userEditProfile.updateProfile,upload.single('user_image'),userController.updateUserProfile);
router.post("/updateUserProfile",upload.single('userImage'),userEditProfile.updateProfile,userController.updateUserProfile);

router.get('/getBannerHomePage',HomeController.getBannerHomePage);
router.post('/getCategory',HomeController.getCategory);
router.get('/getCategoryOnAdPage',HomeController.getCategoryOnAdPage);

router.get('/registration_year',HomeController.registration_year);
router.get('/allYesNoApi',HomeController.allYesNoApi);

router.post('/getModel',getModelValid.getModelvalidation,HomeController.getModel);
router.post('/getSubModel',getSubModelValid.getSubModelvalidation,HomeController.getSubModel);
router.post('/getAttribute',AttributeValid.getAttributevalidation,HomeController.getAttribute);
router.post('/getSubAttribute',AttributeSubValid.getAttributeSubvalidation,HomeController.getSubAttribute);
router.get('/getHomepageAdvertisement',HomeController.getHomepageAdvertisement);
router.get('/getHomepageAccessoires',HomeController.getHomepageAccessoires);
router.get("/getColor",HomeController.getColor);
router.get("/getColorInterior",HomeController.getColorInterior);
router.get("/getExtColorNameById/:id",HomeController.getExtColorNameById);
router.get("/getIntColorNameById/:id",HomeController.getIntColorNameById);






router.post('/addAdvertisement', uploadArray.array('images',8),AddAdvertisementValid.getAdvertisementvalidation,AdvertisementController.addAdvertisement);

router.post('/step1CarAdName', AdvertisementStep1Valid.getAdvertisementvalidation,AdvertisementController.step1CarAdName);
//router.post('/step2CarAdName', uploadArrayPiwi.array('piwi_report_image',6),uploadArrayMainRepImg.array('mileage_maintanance_report_image',6),AdvertisementStep2Valid.getAdvertisementvalidation,AdvertisementController.step2CarAdName);
router.post('/step2CarAdName', uploadArrayPiwi.fields([{
           name: 'piwi_report_image', maxCount: 6
         }, {
           name: 'mileage_maintanance_report_image', maxCount: 6
         }]),AdvertisementStep2Valid.getAdvertisementvalidation,AdvertisementController.step2CarAdName);

router.post('/addAdImageTesting',AdImageTesting.fields([{
          name: 'file', maxCount: 6
        }]),AdvertisementController.addAdImageTesting);

router.post('/step3CarAdName', AdvertisementStep3Valid.getAdvertisementvalidation,AdvertisementController.step3CarAdName);
router.post('/step4CarAdName', AdvertisementStep4Valid.getAdvertisementvalidation,AdvertisementController.step4CarAdName);

router.post('/step5CarAdName', uploadArrayPiwi.fields([{
           name: 'exterior_image', maxCount: 8
         }, {
           name: 'interior_image', maxCount: 6
         },{
           name: 'trunk_engine_image', maxCount: 6
         }]),AdvertisementStep5Valid.getAdvertisementvalidation,AdvertisementController.step5CarAdName);

router.post('/step6CarAdName', AdvertisementStep6Valid.getAdvertisementvalidation,AdvertisementController.step6CarAdName);
router.post('/step7CarAdName', AdvertisementStep7Valid.getAdvertisementvalidation,AdvertisementController.step7CarAdName);



router.post('/step2Accessoires', AccessoiresStep2Valid.getAdvertisementvalidation,AdvertisementController.step2Accessoires);
router.post('/step3Accessoires', AccessoiresStep3Valid.getAdvertisementvalidation,AdvertisementController.step3Accessoires);
router.post('/step4Accessoires', AccessoiresStep4Valid.getAdvertisementvalidation,AdvertisementController.step4Accessoires);
router.post('/step5Accessoires', uploadAccessoires.single('accessoires_image'),AccessoiresStep5Valid.getAdvertisementvalidation,AdvertisementController.step5Accessoires);

router.post('/step5AccessoiresProfessional', uploadArray.array('accessoires_image',8),AccessoiresStep5Valid.getAdvertisementvalidation,AdvertisementController.step5AccessoiresProfessional);


router.post('/step6Accessoires', AccessoiresStep6Valid.getAdvertisementvalidation,AdvertisementController.step6Accessoires);
router.post('/step7Accessoires', AccessoiresStep7Valid.getAdvertisementvalidation,AdvertisementController.step7Accessoires);
router.post('/step8Accessoires', AccessoiresStep8Valid.getAdvertisementvalidation,AdvertisementController.step8Accessoires);

router.get('/accessoiresNormalUserPayment',StripePaymentController.accessoiresNormalUserPayment);

router.get('/allAdvertisementCount', AdvertisementController.allAdvertisementCount);
router.get('/allAdvertisement', AdvertisementController.allAdvertisement);
router.post('/singleAdvertisement',AllAdvertisementValid.detailAdvalidation,AdvertisementController.singleAdvertisement);
router.get('/userAllAdvertisement', UserAddValid.getAddOfUser,UserAdvertisementController.userAllAdvertisement);
router.post('/userSingleAdvertisement',UserSingleAddValid.getSingleAddOfUser,UserAdvertisementController.userSingleAdvertisement);
router.post('/requestStartAdChat',ChatStartValid.getStartChat,ChatAdvertisementController.requestStartAdChat);
router.post('/messageAdChat',ChatMessageValid.getMessageChat,ChatAdvertisementController.messageAdChat);
router.post('/getAdChat',GetChatValid.getChatvalidation,ChatAdvertisementController.getAdChat);
router.post('/getAllAdChatGroup',GetGroupChatValid.getGroupChatvalidation,ChatAdvertisementController.getAllAdChatGroup);
router.post('/addFavAd',AddFavoriteAd.getFavoriteAdvalidation,FavoriteAdController.addFavAd);
router.post('/getUserFavAllAd',AllFavoriteAd.getAllFavoriteAdvalidation,FavoriteAdController.getUserFavAllAd);
router.get('/normalUserBasicMainPlan',AllTypePlanController.normalUserBasicMainPlan);
router.get('/normalUserTopUrgent',AllTypePlanController.normalUserTopUrgent);
router.get('/professionalUserBasicMainPlan',AllTypePlanController.professionalUserBasicMainPlan);
router.post('/paymentCarAddSubmit',UserCarAdPayValid.getPayCarAdvalidation,UserPaymentController.paymentCarAddSubmit);
router.post('/addAddress',AddAddressValid.getAddressvalidation,CartController.addAddress);
router.post('/getAddress',GetAddressValid.getAddressvalidation,CartController.getAddress);
router.post('/addToCart',AddCartvalidation.getAddressvalidation,CartController.addToCart);
router.post('/getCart',GetCartvalidation.getAddressvalidation,CartController.getCart);
router.post('/checkout',Checkoutvalidation.getAddressvalidation,CheckoutController.checkout);
router.get('/accessoiresPurchase',AccessoiresPaymentController.accessoiresPurchase);
router.get('/getPaypalAccessoriesPayment',AccessoiresPaymentController.getPaypalAccessoriesPayment);
router.get('/getStripeAccessoriesPayment',AccessoiresPaymentController.getStripeAccessoriesPayment);
router.get('/getPaypalAccessoriesSuccess',AccessoiresPaymentController.getPaypalAccessoriesSuccess);
router.get('/getPaypalAccessoriesCancel',AccessoiresPaymentController.getPaypalAccessoriesCancel);

router.get('/getAccessoriesStripeSuccess',AccessoiresPaymentController.getAccessoriesStripeSuccess);
router.get('/getAccessoriesStripeCancel',AccessoiresPaymentController.getAccessoriesStripeCancel);

/* Web user route start */
router.post('/webUserLoginSubmit',WebUserController.webUserLoginSubmit);
router.post('/webUserRegistrationSubmit',WebUserController.webUserRegistrationSubmit);
router.post("/webGetUserProfile",WebUserController.webGetUserProfile);
router.post("/webGetFullUserProfile",WebUserController.webGetFullUserProfile);


//router.post("/webUserEditProfileSubmit",upload.single('filename'),upload.single('logo'),WebUserController.webUserEditProfileSubmit);

router.post('/webUserEditProfileSubmit',upload.fields([{
  name: 'filename', maxCount: 1
}, {
  name: 'logo', maxCount: 1
} ]), WebUserController.webUserEditProfileSubmit);




router.post("/webEmailOTPVerification",WebUserController.webEmailOTPVerification);
router.post("/webResendOTP",WebUserController.webResendOTP);
router.post("/webResendOTPOnEmail",WebUserController.webResendOTPOnEmail);
router.post("/webUserChangePasswordSubmit",WebUserController.webUserChangePasswordSubmit);
router.post("/webForgotPasswordSubmit",WebUserController.webForgotPasswordSubmit);
router.post("/webResetPasswordSubmit",WebUserController.webResetPasswordSubmit);

router.post("/addUpdatePaypalEmail",WebUserController.addUpdatePaypalEmail);
router.post("/addUpdatePaymentOption",WebUserController.addUpdatePaymentOption);

router.post("/addBankInformation",WebUserController.addBankInformation);
router.get("/getAllBankInformation/:id",WebUserController.getAllBankInformation);
router.post("/getSingleBankInformation",WebUserController.getSingleBankInformation);
router.post("/updateBankInformation",WebUserController.updateBankInformation);
router.post("/deleteBankInformation",WebUserController.deleteBankInformation);


router.post('/getCarAdForImagePrice',WebAdvertisementController.getCarAdForImagePrice);

router.post('/webStep1CarAdName',WebAdvertisementController.webStep1CarAdName);
router.post('/webStep2CarAdName',AdImageTesting.fields([{
  name: 'piwi_report_image', maxCount: 6
},{
  name: 'mileage_maintanance_report_image', maxCount: 15
},{
   name: 'engine_operation_hour_image', maxCount: 1
}
]),WebAdvertisementController.webStep2CarAdName);

router.post('/webStep3CarAdName',WebAdvertisementController.webStep3CarAdName);
router.post('/allWebSubModel',WebAdvertisementController.allWebSubModel);
router.post('/nameOfModel',WebAdvertisementController.nameOfModel);

router.post('/allWebSubModelByName',WebAdvertisementController.allWebSubModelByName);
router.post('/allWebVersion',WebAdvertisementController.allWebVersion);

router.post('/webStep4CarAdName',AdImageTesting2.fields([{
  name: 'exterior_image', maxCount: 10
}]),WebAdvertisementController.webStep4CarAdName);

router.post('/webStepBefore4CarAdName',WebAdvertisementController.webStepBefore4CarAdName);
router.post('/webStep5CarAdName',WebAdvertisementController.webStep5CarAdName);
router.post('/webStep6CarAdName',WebAdvertisementController.webStep6CarAdName);

router.post('/webPaymentStepCarAdName',WebAdvertisementController.webPaymentStepCarAdName);

router.post('/webPaymentStepCarAdProfessional',WebAdvertisementController.webPaymentStepCarAdProfessional);

router.post('/webPaymentAccessoriesParticular',WebAdvertisementController.webPaymentAccessoriesParticular);
router.post('/webPaymentAccessAdProfessional',WebAdvertisementController.webPaymentAccessAdProfessional);




router.post('/webPaymentRenewalAdProfessional',WebPaymentRenewalControllerProUser.webPaymentRenewalAdProfessional);

router.get('/getProUsrCarAdPaymentRenewalStripe',WebPaymentRenewalControllerProUser.getProUsrCarAdPaymentRenewalStripe);
router.get('/getProUsrCarAdStripeRenewalSuccess',WebPaymentRenewalControllerProUser.getProUsrCarAdStripeRenewalSuccess);
router.get('/getProUsrCarAdStripeRenewalCancel',WebPaymentRenewalControllerProUser.getProUsrCarAdStripeRenewalCancel);


router.get('/getProUsrCarAdPaymentRenewalPaypal',WebPaymentRenewalControllerProUser.getProUsrCarAdPaymentRenewalPaypal);
router.get('/getPaypalProUsrCarAdRenewalSuccess',WebPaymentRenewalControllerProUser.getPaypalProUsrCarAdRenewalSuccess);
router.get('/getPaypalProUsrCarAdRenewalCancel',WebPaymentRenewalControllerProUser.getPaypalProUsrCarAdRenewalCancel);

router.post('/proUserReactivate',WebPaymentRenewalControllerProUser.proUserReactivate);

router.post('/getActiveCarAccessPlan',WebPaymentRenewalControllerProUser.getActiveCarAccessPlan);
router.post('/getAllOldPlanSubscription',WebPaymentRenewalControllerProUser.getAllOldPlanSubscription);



router.get('/webCarAdPayment',WebPaymentController.webCarAdPayment);
router.get('/getNorUsrCarAdPaymentPaypal',WebPaymentController.getNorUsrCarAdPaymentPaypal);
router.get('/getPaypalNorUsrCarAdSuccess',WebPaymentController.getPaypalNorUsrCarAdSuccess);
router.get('/getPaypalNorUsrCarAdCancel',WebPaymentController.getPaypalNorUsrCarAdCancel);

router.get('/getNorUsrCarAdPaymentStripe',WebPaymentController.getNorUsrCarAdPaymentStripe);
router.get('/getNorUsrCarAdStripeSuccess',WebPaymentController.getNorUsrCarAdStripeSuccess);
router.get('/getNorUsrCarAdStripeCancel',WebPaymentController.getNorUsrCarAdStripeCancel);

router.get('/webAccessoriesAdPayment',WebPaymentController.webAccessoriesAdPayment);
router.get('/getNorUsrAccessPaymentPaypal',WebPaymentController.getNorUsrAccessPaymentPaypal);
router.get('/getPaypalNorUsrAccessSuccess',WebPaymentController.getPaypalNorUsrAccessSuccess);
router.get('/getPaypalNorUsrAccessCancel',WebPaymentController.getPaypalNorUsrAccessCancel);

router.get('/getNorUsrAccessPaymentStripe',WebPaymentController.getNorUsrAccessPaymentStripe);
router.get('/getNorUsrAccessStripeSuccess',WebPaymentController.getNorUsrAccessStripeSuccess);
router.get('/getNorUsrAccessStripeCancel',WebPaymentController.getNorUsrAccessStripeCancel);

router.get('/webCarAdPaymentProUser',WebPaymentControllerProUser.webCarAdPaymentProUser);
router.get('/getProUsrCarAdPaymentPaypal',WebPaymentControllerProUser.getProUsrCarAdPaymentPaypal);
router.get('/getPaypalProUsrCarAdSuccess',WebPaymentControllerProUser.getPaypalProUsrCarAdSuccess);
router.get('/getPaypalProUsrCarAdCancel',WebPaymentControllerProUser.getPaypalProUsrCarAdCancel);

router.get('/getProUsrCarAdPaymentStripe',WebPaymentControllerProUser.getProUsrCarAdPaymentStripe);
router.get('/getProUsrCarAdStripeSuccess',WebPaymentControllerProUser.getProUsrCarAdStripeSuccess);
router.get('/getProUsrCarAdStripeCancel',WebPaymentControllerProUser.getProUsrCarAdStripeCancel);

router.post('/webStep1CarEditAdName',WebAdvertisementController.webStep1CarEditAdName);
router.post('/webRemoveCarOldImage',WebAdvertisementController.webRemoveCarOldImage);
router.post('/webRemoveAccessOldImage',WebAdvertisementController.webRemoveAccessOldImage);

router.post('/webGetAdTitle',WebAdvertisementController.webGetAdTitle);

router.post('/webStep2AccessoriesAdName',WebAdvertisementController.webStep2AccessoriesAdName);
router.post('/webStep3AccessoriesAdName',WebAdvertisementController.webStep3AccessoriesAdName);
router.post('/webStep4AccessoriesAdName',WebAdvertisementController.webStep4AccessoriesAdName);
//

router.post('/webStep5AccessoriesAdNameNormal',AdImageTesting2.fields([{
  name: 'exterior_image', maxCount: 10
}]),WebAdvertisementController.webStep5AccessoriesAdNameNormal);


router.post('/webStep5AccessoriesAdName',AdImageTesting2.fields([{
  name: 'exterior_image', maxCount: 10
}]),WebAdvertisementController.webStep5AccessoriesAdName);
router.post('/webStep6BeforeAccessoriesAdName',WebAdvertisementController.webStep6BeforeAccessoriesAdName);
router.post('/webStep6AccessoriesAdName',WebAdvertisementController.webStep6AccessoriesAdName);
router.post('/webStep7AccessoriesAdName',WebAdvertisementController.webStep7AccessoriesAdName);


router.post('/webContactSubmit',WebHomeController.webContactSubmit);

router.post('/webUserDetailAllAd',WebHomeController.webUserDetailAllAd);
router.post('/webUserDetailAllAdCount',WebHomeController.webUserDetailAllAdCount);
router.post('/webHeaderAutoSuggestion',WebHomeController.webHeaderAutoSuggestion);
router.post('/proUserPaymentHistory',WebPaymentHistoryController.proUserPaymentHistory);


router.post('/webGetModel',WebHomeController.webGetModel);
router.post('/webGetSubModel',WebHomeController.webGetSubModel);
router.post('/webGetSubModel2',WebHomeController.webGetSubModel2);

router.post('/webGetUserOwnAd',WebHomeController.webGetUserOwnAd);
router.post('/webGetUserOwnAdActiveAdCount',WebHomeController.webGetUserOwnAdActiveAdCount);
router.post('/webGetUserOwnAdInActiveAdCount',WebHomeController.webGetUserOwnAdInActiveAdCount); 
router.post('/webGetUserOwnAdExpired',WebHomeController.webGetUserOwnAdExpired);
router.post('/deleteAdSubmit',WebHomeController.deleteAdSubmit);


router.post('/webGetSingleAd',WebHomeController.webGetSingleAd);
router.post('/updatePausePlayAd',WebHomeController.updatePausePlayAd);
router.post('/getWebChatProduct', WebHomeController.getWebChatProduct);

router.post('/webInitiateChatSubmit', WebAdChatController.webInitiateChatSubmit);
router.post('/webAllAdChatGroup', WebAdChatController.webAllAdChatGroup);
router.post('/webGetAllAdSingleChat', WebAdChatController.webGetAllAdSingleChat);

router.post('/sendChatMessageSubmit', WebAdChatController.sendChatMessageSubmit);
//
router.get('/webGetUserParticularPlan',WebAllTypePlanController.webGetUserParticularPlan);
router.post('/webGetUserParticularPlanTopUrgent',WebAllTypePlanController.webGetUserParticularPlanTopUrgent);
router.get('/webGetUserParticularPlanTopSearchList',WebAllTypePlanController.webGetUserParticularPlanTopSearchList);
router.get('/webGetUserParticularPlanTopSearchListAccess',WebAllTypePlanController.webGetUserParticularPlanTopSearchListAccess);


router.post('/webGetUserAccessoriesParticularPlan',WebAllTypePlanController.webGetUserAccessoriesParticularPlan);
router.post('/webGetUserAccessoriesParticularPlanTopUrgent',WebAllTypePlanController.webGetUserAccessoriesParticularPlanTopUrgent);

router.post('/webGetUserProPlan',WebAllTypePlanController.webGetUserProPlan);
router.post('/webGetUserProPlanTopSearch',WebAllTypePlanController.webGetUserProPlanTopSearch);

router.post('/getWebHomeCarAd',WebAdFrontController.getWebHomeCarAd);
router.post('/getWebHomeCarAd_TEST',WebAdFrontController.getWebHomeCarAd_TEST);
router.post('/getWebHomeAccessAd_TEST',WebAdFrontController.getWebHomeAccessAd_TEST);


router.post('/getWebHomeAccessAd',WebAdFrontController.getWebHomeAccessAd);
router.post('/getWebProductList',WebAdFrontController.getWebProductList);
router.post('/getWebProductListCount',WebAdFrontController.getWebProductListCount);
router.post('/webSingleProduct',WebAdFrontController.webSingleProduct);
router.post('/webSingleProductCar',WebAdFrontController.webSingleProductCar);
router.post('/updateAccessNumberCount',WebAdFrontController.updateAccessNumberCount);
router.post("/updateAdViewCount",WebAdFrontController.updateAdViewCount);


router.post('/getWebHomeUserFavAd',WebAdFrontController.getWebHomeUserFavAd);
router.post('/webGetHistoryList',WebSearchHistoryController.webGetHistoryList);
router.post("/webHistoryActiveDeactiveEmail",WebSearchHistoryController.webHistoryActiveDeactiveEmail);
router.get("/webHistoryDelete/:id",WebSearchHistoryController.webHistoryDelete);
router.get("/sendEmailBeforeExpiry",WebSearchHistoryController.sendEmailBeforeExpiry);
 

router.post('/getWebHomeProductCount',WebAdFrontController.getWebHomeProductCount);

router.post('/getWebAllUserFavAd',WebAdFavController.getWebAllUserFavAd);
router.post('/adUserFavAdd',WebAdFavController.adUserFavAdd);

router.post('/followUnfollowSeller',WebAdFavController.followUnfollowSeller);
router.post('/getAdFollowedSeller',WebAdFavController.getAdFollowedSeller);
router.post('/adUserReport',WebAdFavController.adUserReport);


router.post('/webAddToCart',WebCartController.webAddToCart);
router.post('/webCheckItemInCartApi',WebCartController.webCheckItemInCartApi);
router.post('/webGetUserCart',WebCartController.webGetUserCart);
router.post('/webAddDeliveryAddress',WebCartController.webAddDeliveryAddress);
router.post('/webGetDeliveryAddress',WebCartController.webGetDeliveryAddress);
router.post('/webSubmitCart',WebCartController.webSubmitCart);
router.post('/webRemoveCartItem',WebCartController.webRemoveCartItem);
router.post('/updateCartQuantity',WebCartController.updateCartQuantity);

router.get('/testFunction',WebCartController.testFunction);


router.post('/checkoutShippingCheck',WebCheckoutController.checkoutShippingCheck);
router.post('/webCheckout',WebCheckoutController.webCheckout);

router.post('/webAddRatingReviews',WebCheckoutController.webAddRatingReviews);


router.get('/webaccessoiresPurchase',WebAccessoiresPaymentController.webaccessoiresPurchase);
router.get('/webgetPaypalAccessoriesPayment',WebAccessoiresPaymentController.webgetPaypalAccessoriesPayment);
router.get('/webgetStripeAccessoriesPayment',WebAccessoiresPaymentController.webgetStripeAccessoriesPayment);
router.get('/webgetPaypalAccessoriesSuccess',WebAccessoiresPaymentController.webgetPaypalAccessoriesSuccess);
router.get('/webgetPaypalAccessoriesCancel',WebAccessoiresPaymentController.webgetPaypalAccessoriesCancel);
router.get('/webgetAccessoriesStripeSuccess',WebAccessoiresPaymentController.webgetAccessoriesStripeSuccess);
router.get('/webgetAccessoriesStripeCancel',WebAccessoiresPaymentController.webgetAccessoriesStripeCancel);

router.post('/webGetAccessoriesPurchase',WebAccessoriesAdController.webGetAccessoriesPurchase);
router.post('/webGetAccessoriesPurchaseSingle',WebAccessoriesAdController.webGetAccessoriesPurchaseSingle);

router.get('/webGetOwnSalesCount/:id',WebAccessoriesAdController.webGetOwnSalesCount);
router.post('/webGetOwnSales',WebAccessoriesAdController.webGetOwnSales);

router.get('/webGetOwnSalesProcessCount/:id',WebAccessoriesAdController.webGetOwnSalesProcessCount);
router.post('/webGetOwnSalesProcess',WebAccessoriesAdController.webGetOwnSalesProcess);
router.get('/webGetOwnSalesCancelCount/:id',WebAccessoriesAdController.webGetOwnSalesCancelCount);
router.post('/webGetOwnSalesCancel',WebAccessoriesAdController.webGetOwnSalesCancel);
router.get('/webGetOwnSalesCompleteCount/:id',WebAccessoriesAdController.webGetOwnSalesCompleteCount);
router.post('/webGetOwnSalesComplete',WebAccessoriesAdController.webGetOwnSalesComplete);

router.get('/webShippingProcess/:id',WebAccessoriesAdController.webShippingProcess);
router.get('/webShippingCancel/:id',WebAccessoriesAdController.webShippingCancel);
router.get('/webShippingComplete/:id',WebAccessoriesAdController.webShippingComplete);
router.get('/webGetOwnSingleSales/:id',WebAccessoriesAdController.webGetOwnSingleSales);
//
//getWebHomeCarAd
/* Web user route end */


/* Admin route start */

router.post("/getAdminProfile",(adminController.getAdminProfile ));
router.post("/editAdminProfileSubmitApi",uploadAdminImage.array('file',1),(adminController.editAdminProfileSubmitApi ));
router.post("/adminChangePasswordSubmitApi",(adminController.adminChangePasswordSubmitApi ));

router.post("/allUsersCountDashboard",(UserController.allUsersCountDashboard));

router.post("/allProUsers",(UserController.allProUsers));
router.post("/allProUsersCount",(UserController.allProUsersCount));

router.get("/exportCsvNormalUserApi",(UserController.exportCsvNormalUserApi));
router.get("/exportCsvProUserApi",(UserController.exportCsvProUserApi));

router.post("/allUsersNameOnly",(UserController.allUsersNameOnly));

router.post("/allNormalUsers",(UserController.allNormalUsers));
router.post("/allNormalUsersCount",(UserController.allNormalUsersCount));
router.post("/getSingleUserApi",(UserController.getSingleUserApi));
router.post("/editUserSubmit",uploadUsersImage.array('file',1),(UserController.editUserSubmit));
router.post("/updateUserStatusApi",(UserController.updateUserStatusApi));
router.post("/allUsersBankDetail",(UserController.allUsersBankDetail));


router.post("/adminLogin",adminController.adminLogin);
router.post("/adminRegistration",adminController.adminRegistration);
//router.post("/getModelNewww",CategoryController.getModelNewww);
router.post("/getModelAdmin",CategoryController.getModelAdmin);


router.post("/getSingleModel",CategoryController.getSingleModel);
router.post("/updateCatStatusApi",CategoryController.updateCatStatusApi);

//router.post("/allModel",CategoryController.allModel);
router.post("/modelTotalCount",CategoryController.modelTotalCount);
router.post("/allModelList",CategoryController.allModelList);
router.get("/exportCsvallModelApi",CategoryController.exportCsvallModelApi);
router.post("/updateModelStatusApi",CategoryController.updateModelStatusApi);


router.post("/allVersionSubmodel",CategoryController.allVersionSubmodel);
router.post("/allSubModel",CategoryController.allSubModel);
router.post("/getSingleSubModel",CategoryController.getSingleSubModel);
router.post("/subModelTotalCount",CategoryController.subModelTotalCount);
router.get("/exportCsvallSubModelApi",CategoryController.exportCsvallSubModelApi);



router.post("/updateSubModelStatus",CategoryController.updateSubModelStatus);
router.post("/addModelsSubmit",adminupload.fields([{
  name: 'file', maxCount: 1
},{
  name: 'file2', maxCount: 1
}]),CategoryController.addModelsSubmit);
router.post("/editModelsSubmit",adminupload.fields([{
  name: 'file', maxCount: 1
},{
  name: 'file2', maxCount: 1
}]),CategoryController.editModelsSubmit);


router.post("/getCarAttribute",AttributeController.getCarAttribute);
router.post("/getSingleCarAttribute",AttributeController.getSingleCarAttribute);
router.post("/updateAttributeStatusApi",AttributeController.updateAttributeStatusApi);

router.get("/exportallCarAttributeApi",AttributeController.exportallCarAttributeApi);

router.post("/getWhereCarAttribute",AttributeController.getWhereCarAttribute);
router.post("/addCarAttributeSubmit",AttributeController.addCarAttributeSubmit);
router.post("/editCarAttributeSubmit",AttributeController.editCarAttributeSubmit);
router.post("/allSubAttributeCount",AttributeController.allSubAttributeCount);
router.post("/allSubAttribute",AttributeController.allSubAttribute);

router.post("/getCarCategory",CategoryController.getCarCategory);
router.post("/addColorsSubmit",AttributeController.addColorsSubmit);
router.post("/allColorCount",AttributeController.allColorCount);
router.post("/allColor",AttributeController.allColor);
router.post("/getSingleColor",AttributeController.getSingleColor);
router.post("/editColorsSubmit",AttributeController.editColorsSubmit);
router.post("/updateColorStatusApi",AttributeController.updateColorStatusApi);


router.post("/addColorsExtSubmit",AttributeController.addColorsExtSubmit);
router.post("/allColorExtCount",AttributeController.allColorExtCount);
router.post("/allColorExt",AttributeController.allColorExt);
router.post("/updateColorExtStatusApi",AttributeController.updateColorExtStatusApi);
router.post("/getSingleColorExt",AttributeController.getSingleColorExt);
router.post("/editColorsExtSubmit",AttributeController.editColorsExtSubmit);


router.post("/addColorsInteriorSubmit",AttributeController.addColorsInteriorSubmit);
router.post("/allColorInteriorCount",AttributeController.allColorInteriorCount);
router.post("/allColorInterior",AttributeController.allColorInterior);
router.post("/updateColorInteriorStatusApi",AttributeController.updateColorInteriorStatusApi);
router.post("/getSingleColorInterior",AttributeController.getSingleColorInterior);
router.post("/editColorsInteriorSubmit",AttributeController.editColorsInteriorSubmit);

router.post("/addColorsInteriorNameSubmit",AttributeController.addColorsInteriorNameSubmit);
router.post("/allColorInteriorNameCount",AttributeController.allColorInteriorNameCount);
router.post("/allColorInteriorName",AttributeController.allColorInteriorName);
router.post("/updateColorInteriorNameStatusApi",AttributeController.updateColorInteriorNameStatusApi);
router.post("/getSingleColorInteriorName",AttributeController.getSingleColorInteriorName);
router.post("/editColorsInteriorNameSubmit",AttributeController.editColorsInteriorNameSubmit);


router.post('/addPlanNormalUser',PlanController.addPlanNormalUser);
router.post('/editPlanNormalUser',PlanController.editPlanNormalUser);
router.post('/getSinglePlanNormalUser',PlanController.getSinglePlanNormalUser);
router.post('/allPlanNormalUserCount',PlanController.allPlanNormalUserCount);
router.post('/allPlanNormalUser',PlanController.allPlanNormalUser);

router.post('/addPlanProUser',PlanController.addPlanProUser);
router.post('/editPlanProUser',PlanController.editPlanProUser);
router.post('/allPlanProUserCount',PlanController.allPlanProUserCount);
router.post('/allPlanProUser',PlanController.allPlanProUser);

router.post('/addPlanProUserTopSearch',PlanController.addPlanProUserTopSearch);
router.post('/webProUserSingleTopSearch',PlanController.webProUserSingleTopSearch);
router.post('/editPlanProUserTopSearch',PlanController.editPlanProUserTopSearch);

router.post('/allPlanProUserTopSearchCount',PlanController.allPlanProUserTopSearchCount);
router.post('/allPlanProUserTopSearch',PlanController.allPlanProUserTopSearch);
router.post('/allPlanProUserAccess',PlanController.allPlanProUserAccess);


router.post('/getSinglePlanProUser',PlanController.getSinglePlanProUser);
router.post('/addTopUrgentPlan',PlanController.addTopUrgentPlan);
router.post('/allTopUrgentPlan',PlanController.allTopUrgentPlan);
router.post('/allTopUrgentPlanAccessories',PlanController.allTopUrgentPlanAccessories);
router.post('/addTopUrgentPlanAccessories',PlanController.addTopUrgentPlanAccessories);



router.post('/addAccessoriesPlanNormalUser',PlanController.addAccessoriesPlanNormalUser);
router.post('/allAccessoriesPlanNormalUser',PlanController.allAccessoriesPlanNormalUser);
router.post('/getSingleAccessoriesPlanNormalUser',PlanController.getSingleAccessoriesPlanNormalUser);
router.post('/editAccessoriesPlanNormalUser',PlanController.editAccessoriesPlanNormalUser);

router.post('/addTopListPlanNormalUser',PlanSecondController.addTopListPlanNormalUser);
router.post('/allPlanNormalUserTopListCount',PlanSecondController.allPlanNormalUserTopListCount);
router.post('/allPlanNormalUserTopList',PlanSecondController.allPlanNormalUserTopList);
router.post('/editPlanNormalUserTopList',PlanSecondController.editPlanNormalUserTopList);
router.post('/getSinglePlanNormalUserTopList',PlanSecondController.getSinglePlanNormalUserTopList);



router.post('/addTopUrgentPlanNormalUser',PlanSecondController.addTopUrgentPlanNormalUser);
router.post('/allPlanNormalUserTopUrgentCount',PlanSecondController.allPlanNormalUserTopUrgentCount);
router.post('/allPlanNormalUserTopUrgent',PlanSecondController.allPlanNormalUserTopUrgent);


router.post('/addBannerImageSubmit', uploadBanner.array('file',1),adminController.addBannerImageSubmit);

router.post('/editBannerImageSubmit', uploadBanner.array('file',1),adminController.editBannerImageSubmit);
router.post('/getSingleBanner', adminController.getSingleBanner);
router.post('/allBannerCount', adminController.allBannerCount);
router.post('/allBanner', adminController.allBanner);
router.post('/deleteBanner', adminController.deleteBanner);


router.post("/editCategorySubmit",uploadArrayAdmin.array('file',1),CategoryController.editCategorySubmit);

router.post("/allCategory",(CategoryController.allCategory));
router.post("/allCategoryCount",(CategoryController.allCategoryCount));
router.post("/getSingleCategory",(CategoryController.getSingleCategory));

 
router.post("/updateAdStatusApi",AccessoriesAdController.updateAdStatusApi);

router.post("/getAttrTypeDePiece",(AccessoriesAdController.getAttrTypeDePiece));
router.post("/getAttrTypeDeChassis",(AccessoriesAdController.getAttrTypeDeChassis));
router.post('/registration_yearAdmin',AccessoriesAdController.registration_yearAdmin);

router.post("/allAccessoriesNormalUserCount",(AccessoriesAdController.allAccessoriesNormalUserCount));
router.post("/allAccessoriesNormalUser",(AccessoriesAdController.allAccessoriesNormalUser));
router.post("/allAccessoriesActiveNormalUserCount",(AccessoriesAdController.allAccessoriesActiveNormalUserCount));
router.post("/allAccessoriesActiveNormalUser",(AccessoriesAdController.allAccessoriesActiveNormalUser));

router.post("/allAccessoriesProUserCount",(AccessoriesAdController.allAccessoriesProUserCount));
router.post("/allAccessoriesProUser",(AccessoriesAdController.allAccessoriesProUser));

router.post("/allAccessoriesActiveProUserCount",(AccessoriesAdController.allAccessoriesActiveProUserCount));
router.post("/allAccessoriesActiveProUser",(AccessoriesAdController.allAccessoriesActiveProUser));
router.post("/allAccessoriesInActiveProUserCount",(AccessoriesAdController.allAccessoriesInActiveProUserCount));
router.post("/allAccessoriesInActiveProUser",(AccessoriesAdController.allAccessoriesInActiveProUser));



router.post("/getCarModelAdmin",(CarAdController.getCarModelAdmin));

router.post("/allCarNormalUserCount",(CarAdController.allCarNormalUserCount));
router.post("/allCarNormalUser",(CarAdController.allCarNormalUser));

router.post("/allCarProUserCount",(CarAdController.allCarProUserCount));
router.post("/allCarProUser",(CarAdController.allCarProUser));

router.post("/allCarActiveNormalUserCount",(CarAdController.allCarActiveNormalUserCount));
router.post("/allCarActiveNormalUser",(CarAdController.allCarActiveNormalUser));

router.post("/allCarInActiveNormalUserCount",(CarAdController.allCarInActiveNormalUserCount));
router.post("/allCarInActiveNormalUser",(CarAdController.allCarInActiveNormalUser));

router.post("/allCarActiveProUserCount",(CarAdController.allCarActiveProUserCount));
router.post("/allCarActiveProUser",(CarAdController.allCarActiveProUser));

router.post("/allCarInActiveProUserCount",(CarAdController.allCarInActiveProUserCount));
router.post("/allCarInActiveProUser",(CarAdController.allCarInActiveProUser));


router.post("/sendPushNotificationSubmit",CommonController.sendPushNotificationSubmit);
router.post('/allPushNotificationCount', CommonController.allPushNotificationCount);
router.post('/allPushNotification', CommonController.allPushNotification);
router.post('/deletePushNotification', CommonController.deletePushNotification);

router.post("/sendEmailNotificationSubmit",CommonController.sendEmailNotificationSubmit);
router.post('/allEmailNotificationCount', CommonController.allEmailNotificationCount);
router.post('/allEmailNotification', CommonController.allEmailNotification);
router.post('/deleteEmailNotification', CommonController.deleteEmailNotification);

router.post("/getSingleStripe",CommonController.getSingleStripe);
router.post("/editStripeSubmit",CommonController.editStripeSubmit);

router.post("/getSinglePaypal",CommonController.getSinglePaypal);
router.post("/editPaypalSubmit",CommonController.editPaypalSubmit);

router.post('/testEmailSend', CommonController.testEmailSend);
router.post('/addOptions', CommonController.addOptions);
router.get('/allOptions', CommonController.allOptions);
router.get('/getOptions/:id', CommonController.getOptions);
router.post('/editOptions', CommonController.editOptions);

router.post('/addVersions', CommonController.addVersions);
router.get('/allVersions', CommonController.allVersions);
router.get('/getVersions/:id', CommonController.getVersions);
router.post('/editVersions', CommonController.editVersions);


router.post('/addCountry', CommonController.addCountry);
router.get('/allCountry', CommonController.allCountry);
router.get('/getCountry/:id', CommonController.getCountry);
router.post('/editCountry', CommonController.editCountry);


router.get('/getHomeTitle', CommonController.getHomeTitle);
router.post('/edittHomeTitle', CommonController.edittHomeTitle);


router.post('/addPageSubmit', PageController.addPageSubmit);
router.post('/editPageSubmit', PageController.editPageSubmit);
router.post('/allPageCount', PageController.allPageCount);
router.post('/allPage', PageController.allPage);
router.post('/getSinglePage', PageController.getSinglePage);

router.post('/deletePage', PageController.deletePage);
router.post('/updatePageStatusApi', PageController.updatePageStatusApi);



router.post('/allAccessoriesOrdersProcessCount', AccessoriesOrdersController.allAccessoriesOrdersProcessCount);
router.post('/allAccessoriesOrdersProcessApi', AccessoriesOrdersController.allAccessoriesOrdersProcessApi);
router.post('/allAccessoriesOrdersCancelCount', AccessoriesOrdersController.allAccessoriesOrdersCancelCount);
router.post('/allAccessoriesOrdersCancelApi', AccessoriesOrdersController.allAccessoriesOrdersCancelApi);
router.post('/allAccessoriesOrdersCompleteCount', AccessoriesOrdersController.allAccessoriesOrdersCompleteCount);
router.post('/allAccessoriesOrdersCompleteApi', AccessoriesOrdersController.allAccessoriesOrdersCompleteApi);


router.post('/allAccessoriesOrdersCount', AccessoriesOrdersController.allAccessoriesOrdersCount);
router.post('/allAccessoriesOrdersApi', AccessoriesOrdersController.allAccessoriesOrdersApi);


router.post('/allOrderIncompleteInvoice', AccessoriesOrdersController.allOrderIncompleteInvoice);
router.post('/generateInvoice', AccessoriesOrdersController.generateInvoice);
router.post('/getSellerEarning', AccessoriesOrdersController.getSellerEarning);
router.post('/getSellerEarningMultipleOrder', AccessoriesOrdersController.getSellerEarningMultipleOrder);
router.post('/allPaidInvoice', AccessoriesOrdersController.allPaidInvoice);


router.post('/addSubAdminSubmit',uploadAdminImage.array('file',1),adminController.addSubAdminSubmit);
router.post('/editSubAdminSubmit',uploadAdminImage.array('file',1),adminController.editSubAdminSubmit);

router.get("/exportCsvSubAdminApi",(adminController.exportCsvSubAdminApi));
router.post("/allSubAdminCount",(adminController.allSubAdminCount));
router.post("/allSubAdmin",(adminController.allSubAdmin));
router.post("/updateSubAdminApi",(adminController.updateSubAdminApi));
router.post("/getSingleSubAdminApi",(adminController.getSingleSubAdminApi));

router.post("/permissionSubAdminSubmit",(UserPermissionController.permissionSubAdminSubmit));
router.post("/userPermissionRecord",(UserPermissionController.userPermissionRecord));
router.post("/getSubAdminSingle",(UserPermissionController.getSubAdminSingle));
router.post("/getSubAdminAllPermission",(UserPermissionController.getSubAdminAllPermission));
router.post("/settingSubmit",CommonController.settingSubmit);
router.post("/getSetting",CommonController.getSetting);
router.get("/getWebTax",CommonController.getWebTax);

router.get("/getAboutUs",CommonController.getAboutUs);
router.get("/getTermsCondition",CommonController.getTermsCondition);
router.get("/getPrivacyPolicy",CommonController.getPrivacyPolicy);
router.get("/getcontactUs",CommonController.getcontactUs);

router.get("/getUserFaq",CommonController.getUserFaq);

router.post("/addFaqUserSubmit",FaqAdminController.addFaqUserSubmit);
router.post("/allUserFaq",FaqAdminController.allUserFaq);
router.post("/allUserFaqCount",FaqAdminController.allUserFaqCount);
router.post("/updateUserFaqStatusApi",FaqAdminController.updateUserFaqStatusApi);
router.post("/getFaqUserSingle",FaqAdminController.getFaqUserSingle);
router.post("/editFaqUserSubmit",FaqAdminController.editFaqUserSubmit);
//router.get("/checkAdExpiry",CronController.checkAdExpiry);
router.post("/normalUserCarTopList",WebPaymentRenewalController.normalUserCarTopList);
router.get("/getTopSrchNorUsrCarAdPaymentPaypal",WebPaymentRenewalController.getTopSrchNorUsrCarAdPaymentPaypal);
router.get("/getPaypalTopSrchNorUsrCarAdSuccess",WebPaymentRenewalController.getPaypalTopSrchNorUsrCarAdSuccess);
router.get("/getPaypalTopSrchNorUsrCarAdCancel",WebPaymentRenewalController.getPaypalTopSrchNorUsrCarAdCancel);

router.get("/getTopSrchNorUsrCarAdPaymentStripe",WebPaymentRenewalController.getTopSrchNorUsrCarAdPaymentStripe);
router.get("/getTopSrchNorUsrCarAdStripeSuccess",WebPaymentRenewalController.getTopSrchNorUsrCarAdStripeSuccess);
router.get("/getTopSrchNorUsrCarAdStripeCancel",WebPaymentRenewalController.getTopSrchNorUsrCarAdStripeCancel);

router.post("/normalUserCarTopUrgent",WebPaymentRenewalController.normalUserCarTopUrgent);
router.get("/getTopUrgntNorUsrCarAdPaymentPaypal",WebPaymentRenewalController.getTopUrgntNorUsrCarAdPaymentPaypal);
router.get("/getPaypalTopUrgntNorUsrCarAdSuccess",WebPaymentRenewalController.getPaypalTopUrgntNorUsrCarAdSuccess);
router.get("/getPaypalTopUrgntNorUsrCarAdCancel",WebPaymentRenewalController.getPaypalTopUrgntNorUsrCarAdCancel);

router.get("/getTopUrgntNorUsrCarAdPaymentStripe",WebPaymentRenewalController.getTopUrgntNorUsrCarAdPaymentStripe);
router.get("/getTopUrgntNorUsrCarAdStripeSuccess",WebPaymentRenewalController.getTopUrgntNorUsrCarAdStripeSuccess);
router.get("/getTopUrgntNorUsrCarAdStripeCancel",WebPaymentRenewalController.getTopUrgntNorUsrCarAdStripeCancel);


router.post("/normalUserCarAdRenewal",WebPaymentRenewalController.normalUserCarAdRenewal);

router.get("/getNorUsrCarAdMainPlanPaymentStripe",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentStripe);
router.get("/getNorUsrCarAdMainPlanPaymentStripeSuccess",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentStripeSuccess);
router.get("/getNorUsrCarAdMainPlanPaymentStripeCancel",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentStripeCancel);


router.get("/getNorUsrCarAdMainPlanPaymentPaypal",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentPaypal);
router.get("/getNorUsrCarAdMainPlanPaymentPaypalSuccess",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentPaypalSuccess);
router.get("/getNorUsrCarAdMainPlanPaymentPaypalCancel",WebPaymentRenewalController.getNorUsrCarAdMainPlanPaymentPaypalCancel);


router.post("/webAcesPhotoPack", uploadAccessoires.fields([{
  name: 'accessoires_image_new', maxCount: 10
}]),WebPaymentRenewalController.webAcesPhotoPack);
router.post("/webPaymentAccessParticularRenewal",WebPaymentRenewalController.webPaymentAccessParticularRenewal);
router.get("/webAccessAdPaymentRenewal",WebPaymentRenewalController.webAccessAdPaymentRenewal);

router.get("/getNorUsrAccessPaymentStripeRenewal",WebPaymentRenewalController.getNorUsrAccessPaymentStripeRenewal);
router.get("/getNorUsrAccessStripeSuccessRenewal",WebPaymentRenewalController.getNorUsrAccessStripeSuccessRenewal);
router.get("/getNorUsrAccessStripeCancelRenewal",WebPaymentRenewalController.getNorUsrAccessStripeCancelRenewal);

router.get("/getNorUsrAccessPaymentPaypalRenewal",WebPaymentRenewalController.getNorUsrAccessPaymentPaypalRenewal);
router.get("/getPaypalNorUsrAccessSuccessRenewal",WebPaymentRenewalController.getPaypalNorUsrAccessSuccessRenewal);
router.get("/getPaypalNorUsrAccessCancelRenewal",WebPaymentRenewalController.getPaypalNorUsrAccessCancelRenewal);

router.post("/normalUserReactivate",WebPaymentRenewalController.normalUserReactivate);

router.get("/getAllPaymentRequest/:user_id/:page_no",WebUserPaymentController.getAllPaymentRequest);
router.get("/getAllPaymentRequestCount/:user_id",WebUserPaymentController.getAllPaymentRequestCount);
router.post("/makePaymentRequest",WebUserPaymentController.makePaymentRequest);
router.get("/getAllBanks/:user_id",WebUserPaymentController.getAllBanks);
router.get("/acceptPaymentRequest/:id",WebUserPaymentController.acceptPaymentRequest);
router.get("/rejectPaymentRequest/:id",WebUserPaymentController.rejectPaymentRequest);

router.get("/getAllSellerEarning/:user_id",WebUserPaymentController.getAllSellerEarning);

router.get("/adminGetAllPaymentRequestCount",PaymentController.adminGetAllPaymentRequestCount);
router.post("/adminGetAllPaymentRequest",PaymentController.adminGetAllPaymentRequest);


router.get("/adminGetAllPendingPaymentRequestCount",PaymentController.adminGetAllPendingPaymentRequestCount);
router.post("/adminGetAllPendingPaymentRequest",PaymentController.adminGetAllPendingPaymentRequest);


router.get("/adminGetAllPaidPaymentRequestCount",PaymentController.adminGetAllPaidPaymentRequestCount);
router.post("/adminGetAllPaidPaymentRequest",PaymentController.adminGetAllPaidPaymentRequest);


router.get("/adminGetAllRejectedPaymentRequestCount",PaymentController.adminGetAllRejectedPaymentRequestCount);
router.post("/adminGetAllRejectedPaymentRequest",PaymentController.adminGetAllRejectedPaymentRequest);



router.get("/myTestApi",PaymentController.myTestApi);



//getNorUsrCarAdMainPlanPaymentPaypal   getNorUsrCarAdMainPlanPaymentStripe webPaymentAccessParticularRenewal
/* Admin route end */

module.exports = router;