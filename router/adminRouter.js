const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admin/adminController");
const CategoryController = require("../controllers/admin/CategoryController");
const AttributeController = require("../controllers/admin/AttributeController");
//var multer  = require('multer');
var bodyParser = require('body-parser');
const PlanController = require("../controllers/admin/PlanController");
 const modelsImage = require('../middleware/modelsImage');
//const bodyParser = require('body-parser');
var path = require('path')
var multer  = require('multer')
 
var Adminuploadstorage = multer.diskStorage({

   destination: function (req, file, cb) {
   	console.log("here");
      cb(null, './public/uploads/models/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'models_'+ dynamicFileName + '-' + file.originalname);
   }

});
var adminupload = multer({ storage: Adminuploadstorage });



router.post("/adminLogin",adminController.adminLogin);
router.post("/adminRegistration",adminController.adminRegistration);
router.post("/getModelAdmin",CategoryController.getModelAdmin);
//router.post("/allModel",CategoryController.allModel);
router.post("/modelTotalCount",CategoryController.modelTotalCount);
router.post("/allModelList",CategoryController.allModelList);
router.post("/allSubModel",CategoryController.allSubModel);
router.post("/subModelTotalCount",CategoryController.subModelTotalCount);
router.post("/addModelsSubmit",adminupload.single('file'),CategoryController.addModelsSubmit);
router.post("/getCarAttribute",AttributeController.getCarAttribute);
router.post("/getWhereCarAttribute",AttributeController.getWhereCarAttribute);
router.post("/addCarAttributeSubmit",AttributeController.addCarAttributeSubmit);
router.post("/allSubAttributeCount",AttributeController.allSubAttributeCount);
router.post("/allSubAttribute",AttributeController.allSubAttribute);
router.post("/getCarCategory",CategoryController.getCarCategory);
router.post("/addColorsSubmit",AttributeController.addColorsSubmit);
router.post("/allColorCount",AttributeController.allColorCount);
router.post("/allColor",AttributeController.allColor);
router.post('/addPlanNormalUser',PlanController.addPlanNormalUser);
router.post('/allPlanNormalUser',PlanController.allPlanNormalUser);
router.post('/addPlanProUser',PlanController.addPlanProUser);
router.post('/allPlanProUser',PlanController.allPlanProUser);
router.post('/addTopUrgentPlan',PlanController.addTopUrgentPlan);
router.post('/allTopUrgentPlan',PlanController.allTopUrgentPlan);
//router.post("/updateUserProfile",auth,upload.single('userImage'),userEditProfile.updateProfile,userController.updateUserProfile);
//router.post("/adminLoginCheck",adminController.adminLoginCheck);
/*
router.post("/addCategorySubmit",(categoryController.addCategorySubmit));
router.post("/getCategory",(categoryController.getCategory));

router.post("/allCategory",(categoryController.allCategory));
router.post("/categoryTotalCount",(categoryController.categoryTotalCount));*/


module.exports = router;